package flirora.crackslashmine.loot;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

import flirora.crackslashmine.core.Levels;
import flirora.crackslashmine.item.baggim.LootBagItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.condition.LootCondition;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.context.LootContextParameters;
import net.minecraft.loot.function.ConditionalLootFunction;
import net.minecraft.loot.function.LootFunctionType;
import net.minecraft.util.math.Vec3d;

public class FillLootBagLootFunction extends ConditionalLootFunction {
  public FillLootBagLootFunction(LootCondition[] conditions) {
    super(conditions);
  }

  @Override
  public LootFunctionType getType() {
    return CsmLootFunctionTypes.FILL_LOOT_BAG;
  }

  @Override
  protected ItemStack process(ItemStack stack, LootContext context) {
    Item item = stack.getItem();
    if (!(item instanceof LootBagItem))
      throw new JsonSyntaxException(item + " is not a loot bag item");
    Vec3d location = context.get(LootContextParameters.ORIGIN);
    int level = location == null ? 0
        : Levels.positionToLevel(context.getWorld(), location);
    return ((LootBagItem) item).create(context.getRandom(), level);
  }

  public static class Serializer
      extends ConditionalLootFunction.Serializer<FillLootBagLootFunction> {
    @Override
    public FillLootBagLootFunction fromJson(JsonObject json,
        JsonDeserializationContext context, LootCondition[] conditions) {
      return new FillLootBagLootFunction(conditions);
    }
  }
}
