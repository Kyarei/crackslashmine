package flirora.crackslashmine.loot;

import flirora.crackslashmine.CrackSlashMineMod;
import net.minecraft.loot.function.LootFunction;
import net.minecraft.loot.function.LootFunctionType;
import net.minecraft.util.Identifier;
import net.minecraft.util.JsonSerializer;
import net.minecraft.util.registry.Registry;

public class CsmLootFunctionTypes {
  private static LootFunctionType register(String id,
      JsonSerializer<? extends LootFunction> jsonSerializer) {
    return Registry.register(Registry.LOOT_FUNCTION_TYPE,
        new Identifier(CrackSlashMineMod.MOD_ID, id),
        new LootFunctionType(jsonSerializer));
  }

  public static LootFunctionType FILL_LOOT_BAG =
      register("fill_loot_bag", new FillLootBagLootFunction.Serializer());

  public static void load() {
  }
}
