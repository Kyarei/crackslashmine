package flirora.crackslashmine.generation;

import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BuiltinBiomes;

import java.util.*;

public class AreaNamerV1 implements AreaNamer {
    private static class Pool {
        private final Biome.Category category;
        private final int nBiomeComponents;
        private final int nDescriptorComponents;

        private Pool(Biome.Category category, int nBiomeComponents,
                     int nDescriptorComponents) {
            this.category = category;
            this.nBiomeComponents = nBiomeComponents;
            this.nDescriptorComponents = nDescriptorComponents;
        }

        public Biome.Category getCategory() {
            return category;
        }

        public int getnBiomeComponents() {
            return nBiomeComponents;
        }

        public TranslatableText select(Random r) {
            return new TranslatableText(
                    "csm.area.v1.biomeComponent." + category.getName() + "[" + r.nextInt(nBiomeComponents) + "]");
        }

        public void addDescriptorsToPool(Collection<String> descriptors) {
            for (int i = 0; i < nDescriptorComponents; ++i) {
                descriptors.add("biome." + category.getName() + "[" + i + "]");
            }
        }
    }

    private static final EnumMap<Biome.Category, Pool> biomesByCategory;

    static {
        biomesByCategory = new EnumMap<>(Biome.Category.class);
        biomesByCategory.put(Biome.Category.NONE,
                new Pool(Biome.Category.NONE, 1, 0));
        biomesByCategory.put(Biome.Category.TAIGA,
                new Pool(Biome.Category.TAIGA, 5, 0));
        biomesByCategory.put(Biome.Category.EXTREME_HILLS,
                new Pool(Biome.Category.EXTREME_HILLS, 4, 0));
        biomesByCategory.put(Biome.Category.JUNGLE,
                new Pool(Biome.Category.JUNGLE, 5, 1));
        biomesByCategory.put(Biome.Category.MESA,
                new Pool(Biome.Category.MESA, 4, 4));
        biomesByCategory.put(Biome.Category.PLAINS,
                new Pool(Biome.Category.PLAINS, 5, 0));
        biomesByCategory.put(Biome.Category.SAVANNA,
                new Pool(Biome.Category.SAVANNA, 5, 0));
        biomesByCategory.put(Biome.Category.ICY, new Pool(Biome.Category.ICY,
                3, 0));
        biomesByCategory.put(Biome.Category.THEEND,
                new Pool(Biome.Category.THEEND, 3, 0));
        biomesByCategory.put(Biome.Category.BEACH,
                new Pool(Biome.Category.BEACH, 3, 0));
        biomesByCategory.put(Biome.Category.FOREST,
                new Pool(Biome.Category.FOREST, 10, 0));
        biomesByCategory.put(Biome.Category.OCEAN,
                new Pool(Biome.Category.OCEAN, 4, 0));
        biomesByCategory.put(Biome.Category.DESERT,
                new Pool(Biome.Category.DESERT, 3, 0));
        biomesByCategory.put(Biome.Category.RIVER,
                new Pool(Biome.Category.RIVER, 5, 0));
        biomesByCategory.put(Biome.Category.SWAMP,
                new Pool(Biome.Category.SWAMP, 10, 0));
        biomesByCategory.put(Biome.Category.MUSHROOM,
                new Pool(Biome.Category.MUSHROOM, 3, 3));
        biomesByCategory.put(Biome.Category.NETHER,
                new Pool(Biome.Category.NETHER, 5, 1));
    }

    private Set<String> getEligibleDescriptors(Biome biome, Area a) {
        float temperature = biome.getTemperature();
        float precipitation = biome.getDownfall();
        float depth = biome.getDepth();
        float scale = biome.getScale();
        Set<String> descriptors = new HashSet<>();
        if (temperature > 0.99f) {
            descriptors.add("scorching");
            descriptors.add("burning");
            descriptors.add("fiery");
        } else if (temperature < 0.11f) {
            descriptors.add("cold");
            descriptors.add("freezing");
            descriptors.add("icy");
        }
        if (precipitation > 0.84f) {
            descriptors.add("sweltering");
            descriptors.add("humid");
        } else if (precipitation > 0.69f) {
            descriptors.add("lush");
            descriptors.add("vibrant");
        } else if (precipitation < 0.11f) {
            descriptors.add("parched");
            descriptors.add("rainless");
        }
        if (depth > 0.75f) {
            descriptors.add("high");
        } else if (depth < -1.5f) {
            descriptors.add("deep");
        }

        if (scale > 0.3f) {
            descriptors.add("hilly");
            descriptors.add("rolling");
        }
        if (scale > 0.5f) {
            descriptors.add("roaring");
            descriptors.add("soaring");
            descriptors.add("flying");
            descriptors.add("frightening");
        }
        if (scale < 0.1f) {
            descriptors.add("calm");
            descriptors.add("flat");
        }

        if (a.size() == 1) {
            descriptors.add("tiny");
        }
        if (a.size() >= 200) {
            descriptors.add("vast");
        }
        if (a.size() >= 300) {
            descriptors.add("immense");
        }
        if (a.size() >= 400) {
            descriptors.add("enormous");
        }

        descriptors.add("laughing");
        descriptors.add("weeping");
        descriptors.add("suspicious");
        descriptors.add("lurching");
        descriptors.add("promised");
        descriptors.add("creepy");
        descriptors.add("singing");
        descriptors.add("whispering");
        descriptors.add("cursed");
        descriptors.add("mystical");
        descriptors.add("charming");
        descriptors.add("seething");
        descriptors.add("ruined");

        biomesByCategory.get(biome.getCategory()).addDescriptorsToPool(descriptors);

        return descriptors;
    }

    @Override
    public Text getName(Biome biome, long seed, Area a) {
        Random r = new Random(seed);
        Biome.Category category = biome.getCategory();
        if (category == Biome.Category.NONE && biome != BuiltinBiomes.THE_VOID) {
            category = Biome.Category.BEACH;
        }
        TranslatableText biomeComponent =
                biomesByCategory.get(category).select(r);
        Set<String> descriptors = getEligibleDescriptors(biome, a);
        List<String> descList = new ArrayList<>(descriptors);
        String selectedDescriptor = descList.get(r.nextInt(descList.size()));
        TranslatableText descriptorComponent = new TranslatableText(
                "csm.area.v1.descriptorComponent." + selectedDescriptor);
        return new TranslatableText("csm.area.v1.combine",
                descriptorComponent,
                biomeComponent);
    }
}
