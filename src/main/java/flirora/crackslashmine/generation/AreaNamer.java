package flirora.crackslashmine.generation;

import net.minecraft.text.Text;
import net.minecraft.world.biome.Biome;

/**
 * A class for naming areas.
 */
@FunctionalInterface
public interface AreaNamer {
    /**
     * Generate a name for an area.
     *
     * @param biome the biome of the area
     * @param seed  the adjusted name seed. Use this for random stuff instead
     *              of getting the area's name seed explicitly.
     * @param a     the {@link Area} that this name should be generated for
     * @return a {@link Text} holding this area's name under this namer
     */
    Text getName(Biome biome, long seed, Area a);

    AreaNamer[] AREA_NAMERS = new AreaNamer[]{
            new AreaNamerV1()
    };

    /**
     * Get an {@link AreaNamer} for a particular name version.
     *
     * @param version the name version to get the namer for
     * @return an {@link AreaNamer} for that version, or the latest version if
     * the version is out of range
     */
    static AreaNamer byVersion(int version) {
        int index = version - 1;
        if (index >= AREA_NAMERS.length)
            return AREA_NAMERS[AREA_NAMERS.length - 1];
        return AREA_NAMERS[index];
    }
}
