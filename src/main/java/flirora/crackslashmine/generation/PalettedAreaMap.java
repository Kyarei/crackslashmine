package flirora.crackslashmine.generation;

import flirora.crackslashmine.CrackSlashMineMod;
import io.netty.buffer.Unpooled;
import it.unimi.dsi.fastutil.objects.Object2ShortMap;
import it.unimi.dsi.fastutil.objects.Object2ShortOpenHashMap;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.network.ServerSidePacketRegistry;
import net.fabricmc.fabric.api.server.PlayerStream;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.PlayerManager;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.ChunkPos;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * A paletted representation of area information for a finite 2D grid of chunks.
 * UUIDs are stored in a "palette", and the UUIDs of the areas for each chunk
 * are stored as indices into the palette.
 */
public class PalettedAreaMap {
    public static final Identifier SYNC_S2C = new Identifier(CrackSlashMineMod.MOD_ID, "area_sync");

    private final ChunkPos centerChunk;
    private final int radius; // generate a square with `radius` chunks from center
    private final List<UUID> uuidPalette;
    private final short[] paletteIndices;

    /**
     * Create a {@link PalettedAreaMap} centered around {@code centerChunk}
     * and extending {@code radius} chunks in each direction, creating a
     * (2 * radius + 1) by (2 * radius + 1) grid of chunks.
     *
     * @param manager     the {@link AreaManager} to query from
     * @param centerChunk the chunk that this grid should be centered around
     * @param radius      the radius in chunks to extend the grid to
     */
    public PalettedAreaMap(AreaManager manager, ChunkPos centerChunk, int radius) {
        if (radius > 127)
            throw new IllegalArgumentException("That number of chunks doesn't fit in a short!");
        this.centerChunk = centerChunk;
        this.radius = radius;
        uuidPalette = new ArrayList<>();
        Object2ShortMap<UUID> reversePalette = new Object2ShortOpenHashMap<>();
        reversePalette.defaultReturnValue((short) -1);
        paletteIndices = new short[(2 * radius + 1) * (2 * radius + 1)];
        int index = 0;
        for (int xOffset = -radius; xOffset <= radius; ++xOffset) {
            for (int zOffset = -radius; zOffset <= radius; ++zOffset) {
                Area area = manager.getOrCreateSync(
                        new ChunkPos(centerChunk.x + xOffset, centerChunk.z + zOffset));
                short paletteIndex = reversePalette.getShort(area.getUuid());
                if (paletteIndex < 0) {
                    paletteIndex = (short) uuidPalette.size();
                    uuidPalette.add(area.getUuid());
                }
                paletteIndices[index] = paletteIndex;
                ++index;
            }
        }
    }

    private PalettedAreaMap(ChunkPos centerChunk, int radius, List<UUID> uuidPalette, short[] paletteIndices) {
        this.centerChunk = centerChunk;
        this.radius = radius;
        this.uuidPalette = uuidPalette;
        this.paletteIndices = paletteIndices;
    }

    /**
     * Load the data in this structure into a {@link ClientAreaManager}.
     *
     * @param manager the {@link ClientAreaManager}
     */
    @Environment(EnvType.CLIENT)
    public void decode(ClientAreaManager manager) {
        int index = 0;
        for (int xOffset = -radius; xOffset <= radius; ++xOffset) {
            for (int zOffset = -radius; zOffset <= radius; ++zOffset) {
                short paletteIndex = paletteIndices[index];
                UUID uuid = uuidPalette.get(paletteIndex);
                long l = ChunkPos.toLong(
                        centerChunk.x + xOffset,
                        centerChunk.z + zOffset);
                manager.addArea(l, uuid);
                ++index;
            }
        }
    }

    /**
     * Serialize this structure into a packet.
     *
     * @param buf a {@link PacketByteBuf} to which the data should be written
     */
    public void writeToPacket(PacketByteBuf buf) {
        buf.writeInt(centerChunk.x);
        buf.writeInt(centerChunk.z);
        buf.writeInt(radius);
        buf.writeInt(uuidPalette.size());
        for (UUID uuid : uuidPalette) {
            buf.writeUuid(uuid);
        }
        for (short pi : paletteIndices) {
            buf.writeShort(pi);
        }
    }

    /**
     * Deserialize this structure from a packet.
     *
     * @param buf a {@link PacketByteBuf} from which the data should be read
     * @return the deserialized object
     */
    public static PalettedAreaMap readFromPacket(PacketByteBuf buf) {
        ChunkPos centerChunk = new ChunkPos(buf.readInt(), buf.readInt());
        int radius = buf.readInt();
        int uuidCount = buf.readInt();
        List<UUID> uuids = new ArrayList<>(uuidCount);
        for (int i = 0; i < uuidCount; ++i) {
            uuids.add(buf.readUuid());
        }
        short[] paletteIndices = new short[(2 * radius + 1) * (2 * radius + 1)];
        for (int i = 0; i < paletteIndices.length; ++i) {
            paletteIndices[i] = buf.readShort();
        }
        return new PalettedAreaMap(centerChunk, radius, uuids, paletteIndices);
    }

    public static void sendAreasToPlayersInWorld(ServerWorld world, AreaManager manager, PlayerLastChunkStore store) {
        MinecraftServer server = world.getServer();
        PlayerManager playerManager = server.getPlayerManager();
        // TODO: would it be worth it to group by chunk position to calculate
        // only one paletted map for each applicable chunk position?
        PlayerStream.world(world).forEach(player -> {
            ChunkPos chunkPos = new ChunkPos(player.getBlockPos());
            if (!store.update(player, chunkPos)) return;
            // If you actually set your view distance above 126 chunkim, then :concern:
            PalettedAreaMap map = new PalettedAreaMap(
                    manager, chunkPos, Math.min(127, playerManager.getViewDistance() + 1));
            PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
            map.writeToPacket(buf);
            Area area = manager.getOrCreateSync(chunkPos);
            area.writeToPacket(buf);
            ServerSidePacketRegistry.INSTANCE.sendToPlayer(player, SYNC_S2C, buf);
        });
    }

    public static void sendAreasToAllPlayers(MinecraftServer server, PlayerLastChunkStore store) {
        for (ServerWorld w : server.getWorlds()) {
            AreaManager manager = ((AreaManagerProvider) w).getAreaManager();
            sendAreasToPlayersInWorld(w, manager, store);
        }
    }

    @Environment(EnvType.CLIENT)
    public static void receivePacket(PalettedAreaMap map, Area currentArea, ClientAreaManager manager) {
        map.decode(manager);
        manager.setCurrentArea(currentArea);
    }
}
