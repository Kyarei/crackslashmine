package flirora.crackslashmine.generation.feature.condition;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

public class LevelFeatureConditionConfig implements FeatureConditionConfig {
    public static final Codec<LevelFeatureConditionConfig> CODEC = RecordCodecBuilder.create(instance ->
            instance.group(
                    Codec.INT.fieldOf("min_level").forGetter(c -> c.minLevel),
                    Codec.INT.fieldOf("max_level").forGetter(c -> c.maxLevel)
            ).apply(instance, LevelFeatureConditionConfig::new)
    );

    private final int minLevel;
    private final int maxLevel;

    public LevelFeatureConditionConfig(int minLevel, int maxLevel) {
        this.minLevel = minLevel;
        this.maxLevel = maxLevel;
    }

    public int getMinLevel() {
        return minLevel;
    }

    public int getMaxLevel() {
        return maxLevel;
    }
}
