package flirora.crackslashmine.generation.feature.condition;

import com.mojang.serialization.Codec;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.StructureWorldAccess;
import net.minecraft.world.gen.chunk.ChunkGenerator;
import net.minecraft.world.gen.feature.Feature;

import java.util.Random;

public class ConditionalFeature extends Feature<ConditionalFeatureConfig> {
    public ConditionalFeature(Codec<ConditionalFeatureConfig> configCodec) {
        super(configCodec);
    }

    @Override
    public boolean generate(StructureWorldAccess world, ChunkGenerator chunkGenerator, Random random, BlockPos blockPos, ConditionalFeatureConfig featureConfig) {
        for (ConfiguredFeatureCondition<?, ?> condition :
                featureConfig.getConditions()) {
            if (!condition.canPlaceAt(world, chunkGenerator, random, blockPos))
                return false;
        }
        return featureConfig.getFeature().get().generate(world, chunkGenerator, random, blockPos);
    }
}
