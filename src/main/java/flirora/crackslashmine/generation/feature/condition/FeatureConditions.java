package flirora.crackslashmine.generation.feature.condition;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.CsmRegistries;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.gen.feature.Feature;

public class FeatureConditions {
    public static final Feature<ConditionalFeatureConfig> CONDITIONAL = new ConditionalFeature(ConditionalFeatureConfig.CODEC);

    public static final FeatureCondition<LevelFeatureConditionConfig> LEVEL =
            register("level", new LevelFeatureCondition(LevelFeatureConditionConfig.CODEC));

    public static void register() {
        Registry.register(Registry.FEATURE, new Identifier(CrackSlashMineMod.MOD_ID, "conditional"), CONDITIONAL);
    }

    private static <C extends FeatureConditionConfig> FeatureCondition<C>
    register(String path, FeatureCondition<C> condition) {
        Registry.register(
                CsmRegistries.FEATURE_CONDITION,
                new Identifier(CrackSlashMineMod.MOD_ID, path),
                condition);
        return condition;
    }
}
