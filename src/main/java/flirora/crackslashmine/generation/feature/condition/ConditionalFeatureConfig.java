package flirora.crackslashmine.generation.feature.condition;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Stream;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.util.registry.Registry;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.FeatureConfig;

public class ConditionalFeatureConfig implements FeatureConfig {
  public static final Codec<ConditionalFeatureConfig> CODEC =
      RecordCodecBuilder.create(instance -> instance
          .group(
              ConfiguredFeature.REGISTRY_CODEC.fieldOf("feature")
                  .forGetter(config -> config.feature),
              ConfiguredFeatureCondition.LIST_CODEC.fieldOf("condition")
                  .forGetter(config -> config.conditions))
          .apply(instance, ConditionalFeatureConfig::new));

  private final Supplier<ConfiguredFeature<?, ?>> feature;
  private final List<ConfiguredFeatureCondition<?, ?>> conditions;

  public ConditionalFeatureConfig(Supplier<ConfiguredFeature<?, ?>> feature,
      List<ConfiguredFeatureCondition<?, ?>> conditions) {
    this.feature = feature;
    this.conditions = conditions;
  }

  public Supplier<ConfiguredFeature<?, ?>> getFeature() {
    return feature;
  }

  public List<ConfiguredFeatureCondition<?, ?>> getConditions() {
    return conditions;
  }

  @Override
  public String toString() {
    return String.format("< %s [%s | %s] >", this.getClass().getSimpleName(),
        Registry.FEATURE.getId((this.feature.get()).getFeature()),
        this.conditions);
  }

  @Override
  // Who the fuck knows what this does.
  public Stream<ConfiguredFeature<?, ?>> method_30649() {
    return this.feature.get().method_30648();
  }
}
