package flirora.crackslashmine.generation.feature.condition;

import java.util.Random;

import com.mojang.serialization.Codec;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.StructureWorldAccess;
import net.minecraft.world.gen.chunk.ChunkGenerator;

/**
 * Represents a condition needed in order to generate a feature.
 * <p>
 * If you know why we didn't use decorators, it's because we need access to the
 * world's {@link flirora.crackslashmine.generation.AreaManager}.
 *
 * @param <C> a class that holds options for this condition
 */
public abstract class FeatureCondition<C extends FeatureConditionConfig> {
  private final Codec<ConfiguredFeatureCondition<FeatureCondition<C>, C>> codec;

  public FeatureCondition(Codec<C> configCodec) {
    this.codec = configCodec.fieldOf("config")
        // Eclipse why u complain about not having this?
        .<ConfiguredFeatureCondition<FeatureCondition<C>, C>>xmap(
            config -> new ConfiguredFeatureCondition<>(this, config),
            ConfiguredFeatureCondition::getConfig)
        .codec();
  }

  /**
   * Returns whether a feature can be placed with the following settings.
   *
   * @param world          the world in which the feature would be placed
   * @param chunkGenerator the world's chunk generator
   * @param random         an instance of {@link Random} to sample from, if you
   *                       insist
   * @param blockPos       the position to generate the feature at
   * @param config         the config class for this condition
   * @return true if the feature can be placed
   */
  public abstract boolean canPlaceAt(StructureWorldAccess world,
      ChunkGenerator chunkGenerator, Random random, BlockPos blockPos,
      C config);

  public Codec<ConfiguredFeatureCondition<FeatureCondition<C>, C>> getCodec() {
    return codec;
  }
}
