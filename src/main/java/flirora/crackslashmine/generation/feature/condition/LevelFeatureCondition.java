package flirora.crackslashmine.generation.feature.condition;

import com.mojang.serialization.Codec;
import flirora.crackslashmine.generation.Area;
import flirora.crackslashmine.generation.AreaManager;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.StructureWorldAccess;
import net.minecraft.world.gen.chunk.ChunkGenerator;

import java.util.Random;

public class LevelFeatureCondition extends FeatureCondition<LevelFeatureConditionConfig> {
    public LevelFeatureCondition(Codec<LevelFeatureConditionConfig> configCodec) {
        super(configCodec);
    }

    @Override
    public boolean canPlaceAt(StructureWorldAccess world,
                              ChunkGenerator chunkGenerator,
                              Random random,
                              BlockPos blockPos,
                              LevelFeatureConditionConfig config) {
        AreaManager manager = AreaManager.getFor(world.toServerWorld());
        ChunkPos c = new ChunkPos(blockPos);
        Area a = manager.getOrCreateSync(c);
        int level = a.getLevel();
        return level >= config.getMinLevel() && level <= config.getMaxLevel();
    }
}
