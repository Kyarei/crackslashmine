package flirora.crackslashmine.generation.feature.condition;

import com.mojang.serialization.Codec;
import flirora.crackslashmine.core.CsmRegistries;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.StructureWorldAccess;
import net.minecraft.world.gen.chunk.ChunkGenerator;

import java.util.List;
import java.util.Random;

public class ConfiguredFeatureCondition<
        Cd extends FeatureCondition<Cf>, Cf extends FeatureConditionConfig> {
    public static final Codec<ConfiguredFeatureCondition<?, ?>> CODEC =
            CsmRegistries.FEATURE_CONDITION.dispatch(
                    ConfiguredFeatureCondition::getCondition,
                    FeatureCondition::getCodec);
    public static final Codec<List<ConfiguredFeatureCondition<?, ?>>> LIST_CODEC =
            Codec.list(CODEC);

    private final Cd condition;
    private final Cf config;

    public ConfiguredFeatureCondition(Cd condition, Cf config) {
        this.condition = condition;
        this.config = config;
    }

    public Cd getCondition() {
        return condition;
    }

    public Cf getConfig() {
        return config;
    }

    public boolean canPlaceAt(
            StructureWorldAccess world,
            ChunkGenerator chunkGenerator,
            Random random,
            BlockPos blockPos) {
        return condition.canPlaceAt(world, chunkGenerator, random, blockPos, config);
    }
}
