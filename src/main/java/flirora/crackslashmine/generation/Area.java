package flirora.crackslashmine.generation;

import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.longs.LongList;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Pair;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.biome.Biome;
import org.jetbrains.annotations.NotNull;
import org.mapdb.DataInput2;
import org.mapdb.DataOutput2;

import java.io.IOException;
import java.util.UUID;

/**
 * A set of chunks with a certain level of mobs, as well as a name.
 * <p>
 * Note that the generation of areas must be deterministic with respect to
 * the seed, since we will add generation features that depend on level.
 */
public class Area {
    private final UUID uuid;
    private int level;
    private final LongList chunks;
    // XXX: is it safe to use a raw ID here? Perhaps we'll need to remap if
    // the biome registry mismatches.
    private int biome;
    private final int nameVersion;
    private long nameSeed;

    public Area(UUID uuid, int level, LongList chunks, int biome, int nameVersion, long nameSeed) {
        this.uuid = uuid;
        this.level = level;
        this.chunks = chunks;
        this.biome = biome;
        this.nameVersion = nameVersion;
        this.nameSeed = nameSeed;
    }

    public Area() {
        this(UUID.randomUUID(), -1, new LongArrayList(), -1, 1, 0);
    }

    public void addChunk(ChunkPos pos) {
        chunks.add(pos.toLong());
    }

    public int size() {
        return chunks.size();
    }

    public UUID getUuid() {
        return uuid;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Biome getBiome(Registry<Biome> biomes) {
        return biomes.get(biome);
    }

    public void setBiome(Registry<Biome> biomes, Biome biome) {
        this.biome = biomes.getRawId(biome);
    }

    public LongList getChunks() {
        return chunks;
    }

    public int getNameVersion() {
        return nameVersion;
    }

    public long getNameSeed() {
        return nameSeed;
    }

    public void setNameSeed(long nameSeed) {
        this.nameSeed = nameSeed;
    }

    /**
     * Gets the mean point of all chunks in this area.
     *
     * @return the mean point as a {@link BlockPos}
     */
    public BlockPos meanPoint() {
        long xSum = 0, zSum = 0;
        for (long pos : chunks) {
            xSum += ChunkPos.getPackedX(pos);
            zSum += ChunkPos.getPackedZ(pos);
        }
        int n = chunks.size();
        int x = (int) ((16 * xSum + n / 2) / n + 8);
        int z = (int) ((16 * zSum + n / 2) / n + 8);
        return new BlockPos(x, 0, z);
    }

    @Override
    public String toString() {
        return "Area{" +
                "uuid=" + uuid +
                ", level=" + level +
                ", chunks=" + chunks +
                ", biome=" + biome +
                ", nameVersion=" + nameVersion +
                ", nameSeed=" + nameSeed +
                '}';
    }

    /**
     * Gets the title and subtitle to show upon visiting this area.
     *
     * @param biomes the registry of biomes for naming
     * @return a {@link Pair} of two {@link Text} objects
     */
    public Pair<Text, Text> getTitleAndSubtitle(Registry<Biome> biomes) {
        Text title = AreaNamer.byVersion(nameVersion).getName(
                biomes.get(biome), nameSeed + nameVersion, this);
        Text subtitle = new TranslatableText("csm.hud.areaLevel", level);
        return new Pair<>(title, subtitle);
    }

    public static class Serializer implements org.mapdb.Serializer<Area> {
        @Override
        public void serialize(@NotNull DataOutput2 out, @NotNull Area value) throws IOException {
            Serializer.UUID.serialize(out, value.uuid);
            out.writeInt(value.level);
            Serializer.LONG_ARRAY.serialize(out, value.chunks.toLongArray());
            out.writeInt(value.biome);
            out.writeInt(value.nameVersion);
            out.writeLong(value.nameSeed);
        }

        @Override
        public Area deserialize(@NotNull DataInput2 input, int available) throws IOException {
            return new Area(
                    Serializer.UUID.deserialize(input, available),
                    input.readInt(),
                    new LongArrayList(Serializer.LONG_ARRAY.deserialize(input, available)),
                    input.readInt(),
                    input.readInt(),
                    input.readLong());
        }
    }

    public static final Serializer SERIALIZER = new Serializer();

    public static Area readFromPacket(PacketByteBuf buf) {
        UUID uuid = buf.readUuid();
        int level = buf.readInt();
        int nChunks = buf.readInt();
        LongList chunks = new LongArrayList(buf.readLongArray(new long[nChunks]));
        int biome = buf.readInt();
        int version = buf.readInt();
        long seed = buf.readLong();
        return new Area(uuid, level, chunks, biome, version, seed);
    }

    public void writeToPacket(PacketByteBuf buf) {
        buf.writeUuid(uuid);
        buf.writeInt(level);
        buf.writeInt(chunks.size());
        buf.writeLongArray(chunks.toArray(new long[0]));
        buf.writeInt(biome);
        buf.writeInt(nameVersion);
        buf.writeLong(nameSeed);
    }
}
