package flirora.crackslashmine.generation;

import java.util.Comparator;
import java.util.UUID;
import java.util.function.Predicate;

import org.eclipse.collections.api.stack.primitive.MutableLongStack;
import org.eclipse.collections.impl.stack.mutable.primitive.LongArrayStack;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.generation.dimension.DimensionConfig;
import flirora.crackslashmine.generation.dimension.DimensionConfigManager;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.longs.LongSet;
import it.unimi.dsi.fastutil.objects.Object2LongMap;
import it.unimi.dsi.fastutil.objects.Object2LongOpenHashMap;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BuiltinBiomes;

/**
 * A class that creates areas for a server world.
 */
public class AreaFactory {
  private final ServerWorld w;
  private final GriddedVoronoiNoise voronoi;
  private final Identifier dimensionId;
  private final Registry<Biome> biomes;

  /**
   * Construct a new {@link AreaFactory}.
   *
   * @param w a {@link ServerWorld} to generate areas for
   */
  public AreaFactory(ServerWorld w) {
    this.w = w;
    this.dimensionId = w.getRegistryKey().getValue();
    this.voronoi =
        new GriddedVoronoiNoise(CrackSlashMineMod.config.voronoiCellSize,
            w.getSeed() ^ dimensionId.hashCode(),
            CrackSlashMineMod.config.voronoiWeightRandomness);
    this.biomes = w.getRegistryManager().get(Registry.BIOME_KEY);
  }

  /**
   * Create an {@link Area} at the specified chunk.
   *
   * @param origin    the position of the chunk to generate an area for
   * @param validUuid a {@link Predicate} that returns whether a UUID should be
   *                  assigned to this area
   * @return an {@link Area} for the chunk
   */
  public Area createArea(ChunkPos origin, Predicate<UUID> validUuid) {
    DimensionConfig config = DimensionConfigManager.INSTANCE.get(dimensionId);
    Area area;
    do {
      area = new Area();
    } while (!validUuid.test(area.getUuid()));
    Biome biome = fillChunks(area, origin);
    BlockPos pos = area.meanPoint();
    area.setLevel(config
        .levelFor(new Vec3d(pos.getX() + 0.5, pos.getY(), pos.getZ() + 0.5)));
    area.setBiome(biomes, biome);
    // We want to get a seed for an area based on its chunks.
    // Of course, I don't know if this way is any good.
    long seed = w.getSeed();
    for (long chunk : area.getChunks()) {
      seed ^= 0x8AC0_E67B_BA52_05EFL * (chunk & 0xFFFF_FFFFL)
          + 0x1DA5_14B7_A774_5F75L * (chunk >> 32);
    }
    area.setNameSeed(seed);
    return area;
  }

  private Biome fillChunks(Area area, ChunkPos origin) {
    Object2LongMap<Biome> biomeFrequencies = new Object2LongOpenHashMap<>();
    biomeFrequencies.defaultReturnValue(0);
    LongSet visited = new LongOpenHashSet();
    MutableLongStack stack = new LongArrayStack();
    stack.push(origin.toLong());
    long voronoiCell = getVoronoiAtChunk(origin.toLong());
    while (!stack.isEmpty()) {
      long pos = stack.pop();
      if (visited.contains(pos) || getVoronoiAtChunk(pos) != voronoiCell)
        continue;
      visited.add(pos);
      area.addChunk(new ChunkPos(pos));
      Biome b = getAtChunk(pos);
      biomeFrequencies.mergeLong(b, 1, Long::sum);
      long pos1 = offsetX(pos, 1);
      Biome bn1 = getAtChunk(pos1);
      if (areBiomesCompatible(b, bn1))
        stack.push(pos1);
      long pos2 = offsetZ(pos, 1);
      Biome bn2 = getAtChunk(pos2);
      if (areBiomesCompatible(b, bn2))
        stack.push(pos2);
      long pos3 = offsetX(pos, -1);
      Biome bn3 = getAtChunk(pos3);
      if (areBiomesCompatible(b, bn3))
        stack.push(pos3);
      long pos4 = offsetZ(pos, -1);
      Biome bn4 = getAtChunk(pos4);
      if (areBiomesCompatible(b, bn4))
        stack.push(pos4);
    }
    return biomeFrequencies.object2LongEntrySet().stream()
        .max(Comparator.comparingLong(Object2LongMap.Entry::getLongValue))
        .map(Object2LongMap.Entry::getKey).orElse(BuiltinBiomes.THE_VOID);
  }

  private Biome getAtChunk(long pos) {
    // We query from the generator instead of the chunk itself, because
    // when I tried to use w.getBiome(insertBlockPosHere), then
    // Minecraft would deadlock when a thread other than the main server
    // thread tried to get an area but ended up creating one.
    // I've always been very frisky with concurrency, and Minecraft's
    // chunk-handling code is chock full of futures, so I just gave up
    // and decided to do it the way below.
    // -- +merlan #flirora
    return w.getGeneratorStoredBiome(ChunkPos.getPackedX(pos) * 4 + 2, 0,
        ChunkPos.getPackedZ(pos) * 4 + 2);
  }

  private long getVoronoiAtChunk(long pos) {
    return voronoi
        .getCellPosAtAsLong(new Vec3d(ChunkPos.getPackedX(pos) * 16 + 8.5, 0,
            ChunkPos.getPackedZ(pos) * 16 + 8.5));
  }

  // IMPORTANT: make sure this is commutative!
  private boolean areBiomesCompatible(Biome b1, Biome b2) {
    if (b1 == null || b2 == null) {
      throw new IllegalArgumentException("cannot compare null biomes");
    }
    return b1 == b2;
  }

  private static long offsetX(long pos, int offset) {
    return ChunkPos.toLong(ChunkPos.getPackedX(pos) + offset,
        ChunkPos.getPackedZ(pos));
  }

  private static long offsetZ(long pos, int offset) {
    return ChunkPos.toLong(ChunkPos.getPackedX(pos),
        ChunkPos.getPackedZ(pos) + offset);
  }
}
