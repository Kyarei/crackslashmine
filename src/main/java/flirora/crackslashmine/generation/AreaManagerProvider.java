package flirora.crackslashmine.generation;

public interface AreaManagerProvider {
    AreaManager getAreaManager();
}
