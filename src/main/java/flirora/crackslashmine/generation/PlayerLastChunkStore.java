package flirora.crackslashmine.generation;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.ChunkPos;

import java.util.HashMap;
import java.util.Map;

/**
 * Storage that keeps data on which chunks players are in.
 */
public class PlayerLastChunkStore {
    public interface Provider {
        PlayerLastChunkStore getPlayerLastChunkStore();
    }

    private final Map<PlayerEntity, ChunkPos> lastChunkByPlayer;

    public PlayerLastChunkStore() {
        this.lastChunkByPlayer = new HashMap<>();
    }

    /**
     * Update the last-visited chunk for a player.
     *
     * @param player the {@link PlayerEntity} to set the last-visited chunk of
     * @param pos    the {@link ChunkPos} to assign
     * @return true if the value was different from the previous value
     */
    public boolean update(PlayerEntity player, ChunkPos pos) {
        return !pos.equals(lastChunkByPlayer.put(player, pos));
    }

    /**
     * Remove the last-visited chunk assignment of a player.
     *
     * @param playerEntity the {@link PlayerEntity} to remove from the store
     */
    public void remove(PlayerEntity playerEntity) {
        lastChunkByPlayer.remove(playerEntity);
    }
}
