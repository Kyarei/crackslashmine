package flirora.crackslashmine.generation.dimension;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import flirora.crackslashmine.CrackSlashMineMod;
import net.minecraft.util.math.Vec3d;

public class DimensionConfig {
    private final double minLevel;
    private final double distancePerLevel;
    private final int maxLevel;

    public static final DimensionConfig DEFAULT = new DimensionConfig(1.0, 200.0, Integer.MAX_VALUE);

    public DimensionConfig(double minLevel, double distancePerLevel, int maxLevel) {
        this.minLevel = minLevel;
        this.distancePerLevel = distancePerLevel;
        this.maxLevel = maxLevel;
    }

    public double getMinLevel() {
        return minLevel;
    }

    public double getDistancePerLevel() {
        return distancePerLevel;
    }

    public int getMaxLevel() {
        return maxLevel;
    }

    public int levelFor(Vec3d position) {
        double dist = Math.hypot(position.getX(), position.getZ());
        return Math.max(1,
                Math.min((int) Math.round(minLevel + (dist / distancePerLevel)),
                        Math.min(maxLevel, CrackSlashMineMod.config.maxLevel)));
    }

    private static class JsonFormat {
        public double minLevel;
        public double distancePerLevel;
        public int maxLevel;

        public JsonFormat() {
            minLevel = 1.0;
            distancePerLevel = 200.0;
            maxLevel = Integer.MAX_VALUE;
        }
    }

    private static final Gson GSON = (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();

    public static DimensionConfig fromJson(JsonElement json) {
        JsonFormat deserialized = GSON.fromJson(json, JsonFormat.class);
        return new DimensionConfig(
                deserialized.minLevel,
                deserialized.distancePerLevel,
                deserialized.maxLevel
        );
    }
}
