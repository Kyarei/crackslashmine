package flirora.crackslashmine.generation.dimension;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import flirora.crackslashmine.CrackSlashMineMod;
import net.fabricmc.fabric.api.resource.IdentifiableResourceReloadListener;
import net.minecraft.entity.Entity;
import net.minecraft.resource.JsonDataLoader;
import net.minecraft.resource.ResourceManager;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.Identifier;
import net.minecraft.util.profiler.Profiler;
import org.apache.logging.log4j.Level;

import java.util.Map;

public class DimensionConfigManager extends JsonDataLoader implements IdentifiableResourceReloadListener {
    private static final Gson GSON = (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();
    private static final Identifier ID = new Identifier(CrackSlashMineMod.MOD_ID, "dimension_configs");

    public static final DimensionConfigManager INSTANCE = new DimensionConfigManager();

    private ImmutableMap<Identifier, DimensionConfig> configsById = ImmutableMap.of();

    private DimensionConfigManager() {
        super(GSON, "csm/dimensions");
    }

    @Override
    protected void apply(Map<Identifier, JsonElement> loader, ResourceManager manager, Profiler profiler) {
        int count = 0;
        ImmutableMap.Builder<Identifier, DimensionConfig> builder =
                new ImmutableMap.Builder<>();
        for (Map.Entry<Identifier, JsonElement> entry : loader.entrySet()) {
            Identifier id = entry.getKey();
            JsonElement json = entry.getValue();
            try {
                DimensionConfig config =
                        DimensionConfig.fromJson(json);
                builder.put(id, config);
            } catch (Exception e) {
                CrackSlashMineMod.logException("Failed to load dimension config " + id, e);
            }
            ++count;
        }
        configsById = builder.build();
        CrackSlashMineMod.log(Level.INFO, "Loaded " + count + " dimension configs");
    }

    @Override
    public Identifier getFabricId() {
        return ID;
    }

    public DimensionConfig get(Identifier id) {
        return configsById.getOrDefault(id, DimensionConfig.DEFAULT);
    }

    public DimensionConfig get(ServerWorld world) {
        return get(world.getRegistryKey().getValue());
    }

    public DimensionConfig get(Entity e) {
        if (e.getEntityWorld().isClient()) {
            throw new UnsupportedOperationException("tried to get dim config from client");
        }
        return get((ServerWorld) e.getEntityWorld());
    }
}
