package flirora.crackslashmine.generation;

import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.longs.LongList;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.Vec3d;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.UUID;

/**
 * A structure that keeps track of areas on the client.
 */
public class ClientAreaManager {
    public interface Provider {
        ClientAreaManager getClientAreaManager();
    }

    private final Long2ObjectMap<UUID> areas;
    private Area currentArea;

    public ClientAreaManager() {
        areas = new Long2ObjectOpenHashMap<>();
    }

    public void addArea(long chunkPos, UUID uuid) {
        areas.put(chunkPos, uuid);
    }

    /**
     * Gets the UUID of the area at a location.
     *
     * @param chunkPos a packed {@link ChunkPos} of the location
     * @return the UUID, or null if not stored
     */
    public @Nullable UUID getAreaUuid(long chunkPos) {
        return areas.get(chunkPos);
    }

    /**
     * Gets the UUID of the area at a location.
     *
     * @param chunkPos a packed {@link ChunkPos} of the location
     * @param defo     the default value if not stored
     * @return the UUID, or {@code defo} if not stored
     */
    public @NotNull UUID getAreaUuid(long chunkPos, UUID defo) {
        return areas.getOrDefault(chunkPos, defo);
    }

    /**
     * Removes entries associated with chunks beyond a certain distance.
     *
     * @param position the position to base the distance from
     * @param distance a distance beyond which entries will be removed;
     *                 only chunks whose centers are at most {@code distance}
     *                 units away from {@code position} will be retained.
     */
    public void prune(Vec3d position, double distance) {
        LongList pending = new LongArrayList();
        for (Long2ObjectMap.Entry<UUID> e : areas.long2ObjectEntrySet()) {
            long pos = e.getLongKey();
            double x = 16 * ChunkPos.getPackedX(pos) + 8;
            double z = 16 * ChunkPos.getPackedZ(pos) + 8;
            double dx = position.x - x;
            double dz = position.z - z;
            if (dx * dx + dz * dz > distance) {
                pending.add(pos);
            }
        }
        for (long pos : pending) {
            areas.remove(pos);
        }
    }

    public Area getCurrentArea() {
        return currentArea;
    }

    public void setCurrentArea(Area currentArea) {
        this.currentArea = currentArea;
    }

    public void clear() {
        areas.clear();
        currentArea = null;
    }
}
