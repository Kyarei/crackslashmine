package flirora.crackslashmine.generation;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mapdb.Serializer;

import flirora.crackslashmine.core.storage.CsmDatabase;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.Vec3d;

/**
 * A structure that manages areas on the server.
 */
public class AreaManager {
  private final Executor EXECUTOR =
      Executors.newCachedThreadPool((runnable) -> {
        Thread t = new Thread(runnable);
        t.setName("Area manager thread");
        t.setDaemon(true);
        return t;
      });

  private final ConcurrentMap<UUID, Area> areasByUuid;
  private final ConcurrentMap<Long, UUID> areasByChunk;
  private final AreaFactory areaFactory;
  private final MinecraftServer server;

  /**
   * Constructs a new {@link AreaManager} for a {@link ServerWorld}.
   *
   * @param w a {@link ServerWorld} to construct it for
   */
  public AreaManager(ServerWorld w) {
    CsmDatabase db = CsmDatabase.fromServerWorld(w);
    Identifier dimensionId = w.getRegistryKey().getValue();
    areasByUuid =
        db.getDb()
            .hashMap("areas." + dimensionId.getNamespace() + "."
                + dimensionId.getPath(), Serializer.UUID, Area.SERIALIZER)
            .create();
    areasByChunk =
        db.getDb()
            .hashMap("areasByChunk." + dimensionId.getNamespace() + "."
                + dimensionId.getPath(), Serializer.LONG, Serializer.UUID)
            .create();
    areaFactory = new AreaFactory(w);
    server = w.getServer();
  }

  private @Nullable Area getByUuid(UUID uuid) {
    return areasByUuid.get(uuid);
  }

  private @Nullable UUID getUuid(ChunkPos chunkPos) {
    return areasByChunk.get(chunkPos.toLong());
  }

  private @Nullable Area get(ChunkPos chunkPos) {
    UUID uuid = getUuid(chunkPos);
    if (uuid == null)
      return null;
    return getByUuid(uuid);
  }

  private @NotNull Area getOrCreate(ChunkPos chunkPos) {
    // TODO: sort out concurrency
    Area area = get(chunkPos);
    if (area == null) {
      // server.getProfiler().push("createArea");
      area = areaFactory.createArea(chunkPos,
          (uuid) -> !areasByUuid.containsKey(uuid));
      areasByUuid.put(area.getUuid(), area);
      for (long l : area.getChunks()) {
        areasByChunk.put(l, area.getUuid());
      }
      // server.getProfiler().pop();
    }
    return area;
  }

  /**
   * Get an area by chunk position, or create it if it does not exist. If this
   * is called on a thread other than the main server thread, then it will queue
   * the task on the main server thread.
   *
   * @param chunkPos the position of the chunk to get the area for
   * @return the {@link Area} returned
   */
  public Area getOrCreateSync(ChunkPos chunkPos) {
    return CompletableFuture.supplyAsync(() -> getOrCreate(chunkPos), EXECUTOR)
        .join();
  }

  /**
   * Get an area by chunk position, or create it if it does not exist.
   *
   * @param pos the position of an object in the chunk to get an area for
   * @return the {@link Area} returned
   * @see AreaManager#getOrCreateSync(ChunkPos)
   */
  public Area getOrCreateSync(Vec3d pos) {
    return getOrCreateSync(new ChunkPos(new BlockPos(pos)));
  }

  /**
   * Get the {@link AreaManager} associated with a {@link ServerWorld}.
   *
   * @param w a {@link ServerWorld}
   * @return the {@link AreaManager} associated with {@code w}
   */
  public static AreaManager getFor(ServerWorld w) {
    return ((AreaManagerProvider) w).getAreaManager();
  }

  /**
   * Return what this manager's area factory would have returned for a chunk.
   * <p>
   * This is used in the
   * {@link flirora.crackslashmine.generation.gui.AreaVisGui}, which was made to
   * debug area generation.
   *
   * @param x the chunk X-coordinate
   * @param z the chunk Z-coordinate
   * @return an {@link Area} created at the specified chunk coordinates
   */
  public Area debugArea(int x, int z) {
    return areaFactory.createArea(new ChunkPos(x, z), uuid -> true);
  }
}
