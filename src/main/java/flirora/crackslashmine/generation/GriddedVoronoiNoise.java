package flirora.crackslashmine.generation;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import net.minecraft.util.crash.CrashException;
import net.minecraft.util.crash.CrashReport;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.Vec3d;

import java.util.Random;
import java.util.concurrent.ExecutionException;

/**
 * A generator for multiplicatively weighted Voronoi noise that divides the
 * space into square cells and assigns one site to each cell.
 */
public class GriddedVoronoiNoise {
    private static final long XOR_MASK = 0x424A_9765_B148_A751L;
    private static final long XOR_MASK_X = 0x429F_BCE4_E2DA_5CEEL;
    private static final long XOR_MASK_Z = 0x5880_D019_9EF1_DB33L;

    private static class CellPoint {
        public double x, y, z, w;

        public CellPoint(double x, double y, double z, double w) {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }
    }

    private final double cellSize;
    private final long seed;
    private final double weightRandomness;
    // the ChunkPos is actually a cell position
    // TODO: does this speed things up?
    private final LoadingCache<ChunkPos, CellPoint> cachedCellPoints;

    /**
     * Constructs a new {@link GriddedVoronoiNoise}.
     *
     * @param cellSize         the side length of one cell
     * @param seed             the random seed to use for this generator
     * @param weightRandomness a number such that site weights are generated
     *                         uniformly in the range [1, 1 + weightRandomness)
     */
    public GriddedVoronoiNoise(double cellSize, long seed, double weightRandomness) {
        this.cellSize = cellSize;
        this.seed = seed ^ XOR_MASK;
        this.weightRandomness = weightRandomness;
        this.cachedCellPoints = CacheBuilder.newBuilder()
                .maximumSize(2000)
                .build(CacheLoader.from(this::calculatePoint));
    }

    private CellPoint calculatePoint(ChunkPos cellPos) {
        long cellSeed = seed ^ (XOR_MASK_X * cellPos.x) ^ (XOR_MASK_Z * cellPos.z);
        Random r = new Random(cellSeed);
        double xOffset = r.nextFloat();
        double zOffset = r.nextFloat();
        double weight = 1 + weightRandomness * r.nextFloat();
        return new CellPoint(
                (cellPos.x + xOffset) * cellSize,
                0,
                (cellPos.z + zOffset) * cellSize,
                weight);
    }

    private CellPoint getPoint(ChunkPos cellPos) {
        try {
            return cachedCellPoints.get(cellPos);
        } catch (ExecutionException ex) {
            throw new CrashException(new CrashReport("Generating noise", ex));
        }
    }

    /**
     * Get the position of the cell that contains the point {@code pos}.
     *
     * @param pos the point to query the noise for
     * @return a {@link ChunkPos} that describes the cell coordinates. This is
     * used only as a way to hold two integer coordinates and should not be
     * confused with actual chunk positions.
     */
    public ChunkPos getCellPosAt(Vec3d pos) {
        // In case the cell coordinates don't fit in 32 bits, wrap around
        ChunkPos cellPos = new ChunkPos(
                (int) (long) Math.floor(pos.x / cellSize),
                (int) (long) Math.floor(pos.z / cellSize));
        CellPoint[] points = new CellPoint[25];
        for (int i = 0; i < 5; ++i) {
            for (int j = 0; j < 5; ++j) {
                int index = 5 * i + j;
                ChunkPos at = new ChunkPos(cellPos.x + i - 2, cellPos.z + j - 2);
                points[index] = getPoint(at);
            }
        }
        double bestDistance = Double.POSITIVE_INFINITY;
        int best = -1;
        for (int i = 0; i < 25; ++i) {
            CellPoint p = points[i];
            double distance = pos.distanceTo(new Vec3d(p.x, p.y, p.z)) * p.w;
            if (distance < bestDistance) {
                bestDistance = distance;
                best = i;
            }
        }
        return new ChunkPos(cellPos.x + (best / 5) - 2, cellPos.z + (best % 5) - 2);
    }

    /**
     * Get the position of the cell that contains the point {@code pos}, as
     * a long integer. Equivalent to {@code getCellPosAt(pos).toLong()}.
     *
     * @param pos the point to query the noise for
     * @return a long that describes the cell coordinates
     * @see GriddedVoronoiNoise#getCellPosAt(Vec3d)
     */
    public long getCellPosAtAsLong(Vec3d pos) {
        return getCellPosAt(pos).toLong();
    }
}
