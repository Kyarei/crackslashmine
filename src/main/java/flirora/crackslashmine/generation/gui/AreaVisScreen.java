package flirora.crackslashmine.generation.gui;

import io.github.cottonmc.cotton.gui.GuiDescription;
import io.github.cottonmc.cotton.gui.client.CottonClientScreen;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

@Environment(EnvType.CLIENT)
public class AreaVisScreen extends CottonClientScreen {
    public AreaVisScreen(GuiDescription description) {
        super(description);
    }
}
