package flirora.crackslashmine.generation.gui;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.generation.Area;
import flirora.crackslashmine.gui.ClippedWrapper;
import flirora.crackslashmine.gui.ScrollableArea2D;
import io.github.cottonmc.cotton.gui.client.LightweightGuiDescription;
import io.github.cottonmc.cotton.gui.widget.WGridPanel;
import io.github.cottonmc.cotton.gui.widget.WText;
import io.netty.buffer.Unpooled;
import net.fabricmc.fabric.api.network.ClientSidePacketRegistry;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;
import net.minecraft.util.Pair;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.biome.Biome;

public class AreaVisGui extends LightweightGuiDescription {
    public static final Identifier OPEN_GUI_S2C = new Identifier(CrackSlashMineMod.MOD_ID, "open_area_vis_gui");
    public static final Identifier REQUEST_AREA_INFO_C2S = new Identifier(CrackSlashMineMod.MOD_ID, "request_area_info");
    public static final Identifier RETURN_AREA_INFO_S2C = new Identifier(CrackSlashMineMod.MOD_ID, "return_area_info");

    // The area is requested through the network
    // TODO: keep a cache of chunks?
    private Area area;
    private int mousedChunkX, mousedChunkZ;

    public AreaVisGui() {
        WGridPanel root = new WGridPanel();
        setRootPanel(root);

        ScrollableArea2D visualizer = new ScrollableArea2D(200, 200, 0.0, 0.0, 25.0) {
            @Override
            public void onMouseMove(int x, int y) {
                double logicalMouseX = this.pixelToAbstractX(x);
                double logicalMouseY = this.pixelToAbstractY(y);
                int myChunkX = (int) Math.floor(logicalMouseX);
                int myChunkZ = (int) Math.floor(logicalMouseY);
                if (mousedChunkX != myChunkX || mousedChunkZ != myChunkZ) {
                    // Changed; request a new one
                    mousedChunkX = myChunkX;
                    mousedChunkZ = myChunkZ;
                    PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
                    buf.writeInt(mousedChunkX);
                    buf.writeInt(mousedChunkZ);
                    ClientSidePacketRegistry.INSTANCE.sendToServer(REQUEST_AREA_INFO_C2S, buf);
                }
            }

            @Override
            protected void myPaint(MatrixStack matrices, double logicalMouseX, double logicalMouseY) {
                if (area != null) {
                    for (long chunk : area.getChunks()) {
                        int chunkX = ChunkPos.getPackedX(chunk);
                        int chunkZ = ChunkPos.getPackedZ(chunk);
                        DrawableHelper.fill(matrices,

                                chunkX, chunkZ,
                                chunkX + 1, chunkZ + 1,
                                0xFF88FF88);
                    }
                }
                DrawableHelper.fill(matrices,
                        mousedChunkX, mousedChunkZ,
                        mousedChunkX + 1, mousedChunkZ + 1,
                        0xFFFF8888);
            }
        };
        ClippedWrapper clippedVisualizer = new ClippedWrapper();
        clippedVisualizer.add(visualizer);
        root.add(clippedVisualizer, 0, 0);

        WText text = new WText(new LiteralText("")) {
            @Override
            public void tick() {
                super.tick();
                if (area != null && MinecraftClient.getInstance().world != null) {
                    Registry<Biome> biomes = MinecraftClient.getInstance().world.getRegistryManager().get(Registry.BIOME_KEY);
                    Pair<Text, Text> texts = area.getTitleAndSubtitle(biomes);
                    this.setText(new TranslatableText(
                            "command.csm.areavis.tooltip",
                            mousedChunkX,
                            mousedChunkZ,
                            texts.getLeft(),
                            area.size()
                    ));
                }
            }
        };
        root.add(text, 0, 0);
        text.setLocation(text.getX() + 205, text.getY());
        text.setSize(100, 200);

        root.validate(this);
    }

    public void setArea(Area area) {
        this.area = area;
    }
}
