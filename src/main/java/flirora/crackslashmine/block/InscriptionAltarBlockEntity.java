package flirora.crackslashmine.block;

import org.jetbrains.annotations.Nullable;

import flirora.crackslashmine.block.gui.InscriptionAltarGui;
import flirora.crackslashmine.core.stat.Stat;
import flirora.crackslashmine.item.equipment.CompatibleItemDescription;
import flirora.crackslashmine.item.equipment.CompatibleItemManager;
import flirora.crackslashmine.item.equipment.EquipmentComponent;
import flirora.crackslashmine.item.equipment.EquipmentUtils;
import flirora.crackslashmine.item.essence.tonat.TonatComponent;
import flirora.crackslashmine.item.essence.tonat.TonatItem;
import flirora.crackslashmine.item.essence.tonat.VödDescription;
import flirora.crackslashmine.item.essence.tonat.VödManager;
import it.unimi.dsi.fastutil.objects.Object2LongMap;
import it.unimi.dsi.fastutil.objects.Object2LongOpenHashMap;
import net.fabricmc.fabric.api.block.entity.BlockEntityClientSerializable;
import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerFactory;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.Direction;
import net.minecraft.util.registry.Registry;

public class InscriptionAltarBlockEntity extends BlockEntity
    implements ImplementedInventory, SidedInventory,
    ExtendedScreenHandlerFactory, BlockEntityClientSerializable {
  public static final int MAX_TONATS = 24;
  public static final int INVENTORY_SIZE = 1 + MAX_TONATS;
  private final DefaultedList<ItemStack> items =
      DefaultedList.ofSize(INVENTORY_SIZE, ItemStack.EMPTY);

  public InscriptionAltarBlockEntity() {
    super(CsmBlocks.INSCRIPTION_ALTAR_BLOCK_ENTITY);
  }

  @Override
  public DefaultedList<ItemStack> getItems() {
    return items;
  }

  @Override
  public void fromTag(BlockState state, CompoundTag tag) {
    super.fromTag(state, tag);
    Inventories.fromTag(tag, items);
  }

  @Override
  public CompoundTag toTag(CompoundTag tag) {
    Inventories.toTag(tag, items);
    return super.toTag(tag);
  }

  public static String getVöd(Inventory inventory) {
    StringBuilder builder = new StringBuilder();
    for (int i = 1; i < 1 + MAX_TONATS; ++i) {
      Item item = inventory.getStack(i).getItem();
      if (item instanceof TonatItem) {
        builder.append(((TonatItem) item).getLetter());
      }
    }
    return builder.toString();
  }

  public static InscriptionResult dryRun(Inventory inventory) {
    ItemStack equipment = inventory.getStack(0);
    EquipmentComponent component = EquipmentComponent.getFor(equipment);
    if (component == null)
      return InscriptionResult.fail("notCompatible");
    EquipmentComponent.Meta meta = component.getMeta();
    EquipmentUtils.VödData existing = component.getVödData();
    if (existing != null)
      return InscriptionResult.fail("alreadyInscribed");
    int maxLen = meta.getMaxVödLength();
    String word = getVöd(inventory);
    if (word.length() > maxLen)
      return InscriptionResult.fail("tooLong");
    VödDescription pending = VödManager.SERVER.getByWord(word);
    if (pending == null)
      return InscriptionResult.fail("noSuchWord");

    int equipmentLevel = meta.getLevel();
    int totalLevel = 0, totalPower = 0;
    for (int i = 1; i < 1 + MAX_TONATS; ++i) {
      ItemStack stack = inventory.getStack(i);
      Item item = stack.getItem();
      if (item instanceof TonatItem) {
        TonatComponent tc = TonatComponent.getFor(stack);
        if (tc.getLevel() == 0 || tc.getLevel() > equipmentLevel)
          return InscriptionResult.fail("tooHighLevel");
        totalLevel += tc.getLevel();
        totalPower += tc.getPower();
      }
    }
    double averageLevel = ((double) totalLevel) / word.length();
    return InscriptionResult.success(pending, averageLevel, totalPower);
  }

  public boolean inscribe() {
    InscriptionResult result = dryRun(this);
    if (!result.isSuccessful())
      return false;

    double averageLevel = result.getLevel();
    int totalPower = result.getPower();
    VödDescription pending = result.getDescription();

    Object2LongMap<Stat> stats = new Object2LongOpenHashMap<>();
    for (VödDescription.Entry entry : pending.getEntries()) {
      long value = Math.round(entry.getCurveByLevel().value(averageLevel)
          * (1 + entry.getBonusPerPower() * totalPower));
      stats.put(entry.getStat(), value);
    }
    EquipmentUtils.VödData newVd = new EquipmentUtils.VödData(pending, stats);
    EquipmentComponent component = EquipmentComponent.getFor(items.get(0));
    if (component != null)
      component.setVödData(newVd);

    for (int i = 1; i < 1 + MAX_TONATS; ++i) {
      ItemStack stack = items.get(i);
      Item item = stack.getItem();
      if (item instanceof TonatItem) {
        stack.decrement(1);
      }
    }
    return true;
  }

  @Override
  public boolean isValid(int slot, ItemStack stack) {
    if (slot == 0) {
      CompatibleItemDescription desc = CompatibleItemManager.INSTANCE
          .get(Registry.ITEM.getId(stack.getItem()));
      return desc != null;
    } else {
      return stack.getItem() instanceof TonatItem;
    }
  }

  // Prevent hopper interaction

  private static final int[] EMPTY = new int[0];

  @Override
  public int[] getAvailableSlots(Direction side) {
    return EMPTY;
  }

  @Override
  public boolean canInsert(int slot, ItemStack stack, @Nullable Direction dir) {
    return false;
  }

  @Override
  public boolean canExtract(int slot, ItemStack stack, Direction dir) {
    return false;
  }

  @Override
  public Text getDisplayName() {
    return new TranslatableText("block.crackslashmine.inscription_altar");
  }

  @Override
  public @Nullable ScreenHandler createMenu(int syncId, PlayerInventory inv,
      PlayerEntity player) {
    return new InscriptionAltarGui(syncId, inv,
        ScreenHandlerContext.create(world, pos));
  }

  @Override
  public void writeScreenOpeningData(ServerPlayerEntity player,
      PacketByteBuf buf) {
    buf.writeBlockPos(pos);
  }

  @Override
  public void fromClientTag(CompoundTag compoundTag) {
    assert world != null;
    fromTag(world.getBlockState(pos), compoundTag);
  }

  @Override
  public CompoundTag toClientTag(CompoundTag compoundTag) {
    return toTag(compoundTag);
  }

  @Override
  public void markDirty() {
    super.markDirty();
    if (world != null && !world.isClient)
      this.sync();
  }
}
