package flirora.crackslashmine.block;

import flirora.crackslashmine.block.gui.MixingBowlBlockGui;
import flirora.crackslashmine.item.equipment.CompatibleItemDescription;
import flirora.crackslashmine.item.equipment.CompatibleItemManager;
import flirora.crackslashmine.item.equipment.EquipmentComponent;
import flirora.crackslashmine.item.essence.CrystallizedChromaItem;
import flirora.crackslashmine.item.recipe.MixingRecipe;
import io.github.cottonmc.cotton.gui.PropertyDelegateHolder;
import net.fabricmc.fabric.api.block.entity.BlockEntityClientSerializable;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.recipe.Recipe;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;
import net.minecraft.util.Tickable;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.Direction;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.explosion.Explosion;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class MixingBowlBlockEntity extends BlockEntity
        implements ImplementedInventory, SidedInventory, Tickable,
        NamedScreenHandlerFactory, PropertyDelegateHolder,
        BlockEntityClientSerializable {
    private final DefaultedList<ItemStack> items =
            DefaultedList.ofSize(3, ItemStack.EMPTY);
    private final WorkstationTier.MixingBowl tier;
    private double colX, colY;
    private double reserve;
    private Entity lastInserter;
    private int progress, maxProgress;
    private MixingRecipe currentRecipe;

    private final PropertyDelegate propertyDelegate = new PropertyDelegate() {
        @Override
        public int get(int index) {
            if (index == 0) return (int) Math.round(colX * 1000);
            if (index == 1) return (int) Math.round(colY * 1000);
            if (index == 2) return progress;
            if (index == 3) return maxProgress;
            return -1;
        }

        @Override
        public void set(int index, int value) {
            if (index == 0) colX = 1e-3 * value;
            else if (index == 1) colY = 1e-3 * value;
            else if (index == 2) progress = value;
            else if (index == 3) maxProgress = value;
        }

        @Override
        public int size() {
            return 4;
        }
    };

    public MixingBowlBlockEntity(WorkstationTier.MixingBowl tier) {
        super(CsmBlocks.ADVENTURERS_MIXING_BOWL_BLOCK_ENTITY);
        this.tier = tier;
    }

    @Override
    public DefaultedList<ItemStack> getItems() {
        return items;
    }

    private static final int[] TOP_SLOTS = {0};
    private static final int[] SIDE_SLOTS = {1};
    private static final int[] BOTTOM_SLOTS = {2};

    @Override
    public int[] getAvailableSlots(Direction side) {
        switch (side) {
            case DOWN:
                return BOTTOM_SLOTS;
            case UP:
                return TOP_SLOTS;
            default:
                return SIDE_SLOTS;
        }
    }

    private boolean isCsmCompatibleItem(ItemStack stack) {
        CompatibleItemDescription desc = CompatibleItemManager.INSTANCE.get(
                Registry.ITEM.getId(stack.getItem()));
        return desc != null;
    }

    @Override
    public boolean canInsert(int slot, ItemStack stack, @Nullable Direction dir) {
        if (dir == Direction.UP)
            return slot == 0 && isCsmCompatibleItem(stack);
        if (dir == Direction.DOWN)
            return false;
        return slot == 1 && stack.getItem() instanceof CrystallizedChromaItem;
    }

    @Override
    public boolean canExtract(int slot, ItemStack stack, Direction dir) {
        return dir == Direction.DOWN && slot == 2;
    }

    @Override
    public boolean isValid(int slot, ItemStack stack) {
        if (stack.getItem() instanceof CrystallizedChromaItem) {
            return slot == 1;
        }
        return slot != 1;
    }

    @Override
    public void onOpen(PlayerEntity player) {
        lastInserter = player;
    }

    private boolean hasChroma() {
        return Math.hypot(colX, colY) >= 0.01;
    }

    private double[] getDecrement() {
        double len = Math.hypot(colX, colY);
        if (len == 0) return new double[]{0, 0};
        double decrementPerTick = tier.getDecrementPerTick();
        double dx = colX * decrementPerTick / len;
        double dy = colY * decrementPerTick / len;
        // Make sure we don't overshoot
        if ((colX - dx) * colX < 0) dx = colX;
        if ((colY - dy) * colY < 0) dy = colY;
        return new double[]{dx, dy};
    }

    @Override
    public void tick() {
        if (world == null || world.isClient) return;
        // Consume chroma items
        ItemStack fuel = items.get(1);
        if (!fuel.isEmpty() && fuel.getItem() instanceof CrystallizedChromaItem) {
            CrystallizedChromaItem item = (CrystallizedChromaItem) fuel.getItem();
            fuel.decrement(1);
            int hueIndex = item.getColor();
            if (hueIndex == -1) {
                if (!hasChroma()) {
                    // No good
                    if (world.getBlockState(pos).getBlock() instanceof MixingBowlBlock) {
                        world.removeBlock(pos, false);
                    }
                    world.createExplosion(
                            null,
                            new DivideByZeroDamageSource(lastInserter),
                            null,
                            pos.getX() + 0.5, pos.getY() + 0.5, pos.getZ() + 0.5,
                            tier.getMisuseExplosionStrength(),
                            false, Explosion.DestructionType.DESTROY);
                    return;
                }
                double len = Math.hypot(colX, colY);
                colX += colX / len;
                colY += colY / len;
            } else {
                double hueInRadians = 2 * Math.PI * hueIndex / 24;
                hueInRadians += (2 * Math.PI / 360) * tier.getBaseSpread() * world.random.nextGaussian();
                colX += Math.cos(hueInRadians);
                colY += Math.sin(hueInRadians);
            }
            this.markDirty();
        }
        ItemStack gear = items.get(0);
        EquipmentComponent component = EquipmentComponent.getFor(gear);
        if (!gear.isEmpty() && component != null && items.get(2).isEmpty()) {
            EquipmentComponent.Meta meta = component.getMeta();
            double[] dd = getDecrement();
            double dx = dd[0], dy = dd[1];
            double hueInDegrees = meta.getHue();
            double hueInRadians = Math.toRadians(hueInDegrees);
            double gcx = Math.cos(hueInRadians);
            double gcy = Math.sin(hueInRadians);
            // within the range [-DECREMENT_PER_TICK, DECREMENT_PER_TICK]
            double dot = dx * gcx + dy * gcy;
            double durabilityBonus = tier.getDurabilityPerPoint() * dot + reserve;
            int pointsAdded = (int) Math.floor(durabilityBonus);
            reserve = durabilityBonus - pointsAdded;
            // Don't go past the minimum or maximum durability
            int maxPointsAdded = gear.getDamage();
            int maxPointsSubtracted = gear.getMaxDamage() - gear.getDamage();
            if (pointsAdded > maxPointsAdded)
                pointsAdded = maxPointsAdded;
            if (pointsAdded < -maxPointsSubtracted)
                pointsAdded = -maxPointsSubtracted;
            gear.setDamage(gear.getDamage() - pointsAdded);
            if (pointsAdded != 0) {
                colX -= dx;
                colY -= dy;
                progress = gear.getMaxDamage() - gear.getDamage();
                maxProgress = gear.getMaxDamage();
                this.markDirty();
            }
            if ((!gear.isDamaged() && durabilityBonus >= 0) || !hasChroma()) {
                transferToOutput();
                this.markDirty();
            }
        } else if (!gear.isEmpty()) {
            if (currentRecipe == null || !currentRecipe.matches(this, this.getWorld())) {
                assert world != null;
                List<MixingRecipe> recipes = world.getRecipeManager().getAllMatches(
                        MixingRecipe.Type.INSTANCE, this, world);
                MixingRecipe best = null;
                double bestScore = Double.NEGATIVE_INFINITY;
                for (MixingRecipe r : recipes) {
                    double score = r.getEffectiveness(this);
                    if (score > bestScore) {
                        bestScore = score;
                        best = r;
                    }
                }
                currentRecipe = best;
                progress = 0;
                maxProgress = currentRecipe == null ? 1 : (int) currentRecipe.getMagnitude();
                reserve = 0.0;
                this.markDirty();
            }
            if (currentRecipe != null) {
                double[] dd = getDecrement();
                double dx = dd[0], dy = dd[1];
                double hueInRadians = Math.toRadians(currentRecipe.getHue());
                double gcx = Math.cos(hueInRadians);
                double gcy = Math.sin(hueInRadians);
                double dot = dx * gcx + dy * gcy;
                double progressBonus = dot + reserve;
                int pointsAdded = (int) Math.floor(progressBonus);
                reserve = progressBonus - pointsAdded;
                progress += progressBonus;
                if (progress >= maxProgress && canAcceptOutput()) {
                    gear.decrement(1);
                    ItemStack outputSlot = items.get(2);
                    if (outputSlot.isEmpty()) {
                        items.set(2, currentRecipe.craft(this));
                    } else {
                        outputSlot.increment(currentRecipe.getOutput().getCount());
                    }
                    progress = 0;
                    reserve = 0.0;
                }
                if (progress > maxProgress) progress = maxProgress;
                if (progress < 0) progress = 0;
                colX -= dx;
                colY -= dy;
                this.markDirty();
            } else {
                progress = 0;
                reserve = 0.0;
                this.markDirty();
            }
        } else {
            progress = 0;
            reserve = 0.0;
            this.markDirty();
        }
        this.sync();
    }

    private boolean canAcceptOutput() {
        ItemStack outputInBlock = items.get(2);
        if (outputInBlock.isEmpty()) return true;
        if (currentRecipe == null) return false;
        ItemStack recipeOutput = currentRecipe.getOutput();
        if (recipeOutput.getItem() != outputInBlock.getItem()) return false;
        return recipeOutput.getCount() + outputInBlock.getCount() <= outputInBlock.getMaxCount();
    }

    private void transferToOutput() {
        items.set(2, items.get(0));
        items.set(0, ItemStack.EMPTY);
        progress = 0;
        reserve = 0.0;
    }

    @Override
    public void fromTag(BlockState state, CompoundTag tag) {
        super.fromTag(state, tag);
        Inventories.fromTag(tag, items);
        colX = tag.getDouble("col_x");
        colY = tag.getDouble("col_y");
        reserve = tag.getDouble("reserve");
        progress = tag.getInt("progress");
        maxProgress = tag.getInt("max_progress");
        Identifier id = Identifier.tryParse(tag.getString("recipe"));
        if (id != null && world != null) {
            Recipe<?> r = world.getRecipeManager().get(id).orElse(null);
            if (r instanceof MixingRecipe)
                currentRecipe = (MixingRecipe) r;
        }
    }

    @Override
    public CompoundTag toTag(CompoundTag tag) {
        Inventories.toTag(tag, items);
        tag.putDouble("col_x", colX);
        tag.putDouble("col_y", colY);
        tag.putDouble("reserve", reserve);
        tag.putInt("progress", progress);
        tag.putInt("max_progress", maxProgress);
        if (currentRecipe != null)
            tag.putString("recipe", currentRecipe.getId().toString());
        return super.toTag(tag);
    }

    @Override
    public void fromClientTag(CompoundTag tag) {
        super.fromTag(this.getCachedState(), tag);
        for (int i = 0; i < items.size(); ++i) {
            items.set(i, ItemStack.EMPTY);
        }
        Inventories.fromTag(tag, items);
        colX = tag.getDouble("col_x");
        colY = tag.getDouble("col_y");
        reserve = tag.getDouble("reserve");
    }

    @Override
    public CompoundTag toClientTag(CompoundTag tag) {
        Inventories.toTag(tag, items);
        tag.putDouble("col_x", colX);
        tag.putDouble("col_y", colY);
        return super.toTag(tag);
    }

    @Override
    public Text getDisplayName() {
        return new TranslatableText("block.crackslashmine.adventurers_mixing_bowl");
    }

    @Override
    public @Nullable
    ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player) {
        return new MixingBowlBlockGui(syncId, inv, ScreenHandlerContext.create(world, pos));
    }

    @Override
    public PropertyDelegate getPropertyDelegate() {
        return propertyDelegate;
    }

    public double getColX() {
        return colX;
    }

    public double getColY() {
        return colY;
    }
}
