package flirora.crackslashmine.block;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.EntityDamageSource;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import org.jetbrains.annotations.Nullable;

public class DivideByZeroDamageSource extends EntityDamageSource {
    public DivideByZeroDamageSource(@Nullable Entity source) {
        super("csm.divideByZero", source);
        this.setExplosive();
    }

    @Override
    public Text getDeathMessage(LivingEntity entity) {
        if (source == null || source == entity) {
            return new TranslatableText("death.csm.divideByZero", entity.getDisplayName());
        }
        return new TranslatableText("death.csm.divideByZero.by", entity.getDisplayName(), source);
    }
}
