package flirora.crackslashmine.block.gui;

import java.text.NumberFormat;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.gui.CsmScreenHandlers;
import io.github.cottonmc.cotton.gui.SyncedGuiDescription;
import io.github.cottonmc.cotton.gui.client.ScreenDrawing;
import io.github.cottonmc.cotton.gui.widget.TooltipBuilder;
import io.github.cottonmc.cotton.gui.widget.WGridPanel;
import io.github.cottonmc.cotton.gui.widget.WItemSlot;
import io.github.cottonmc.cotton.gui.widget.WSprite;
import io.github.cottonmc.cotton.gui.widget.WWidget;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;

public class MixingBowlBlockGui extends SyncedGuiDescription {
  private static final Identifier WHEEL =
      new Identifier(CrackSlashMineMod.MOD_ID, "textures/gui/mixing_bowl.png");

  public MixingBowlBlockGui(int syncId, PlayerInventory playerInventory,
      ScreenHandlerContext context) {
    super(CsmScreenHandlers.MIXING_BOWL, syncId, playerInventory,
        getBlockInventory(context, 3), getBlockPropertyDelegate(context, 4));

    WGridPanel root = new WGridPanel();
    setRootPanel(root);

    WItemSlot itemSlot = WItemSlot.of(blockInventory, 0);
    root.add(itemSlot, 0, 1);

    WItemSlot outputSlot = WItemSlot.of(blockInventory, 2);
    root.add(outputSlot, 0, 6);

    WItemSlot fuelSlot = WItemSlot.of(blockInventory, 1);
    root.add(fuelSlot, 8, 6);

    WSprite bg = new WSprite(WHEEL, 0, 0, 216f / 256, 216f / 256);
    root.add(bg, 0, 0);
    bg.setLocation(4 * 18 + 9 - 54, 18);
    bg.setSize(108, 108);

    WSprite marker = new WSprite(WHEEL, 216f / 256, 0, 232f / 256, 16f / 256) {
      private double colX, colY;
      private NumberFormat nf = NumberFormat.getNumberInstance();

      {
        nf.setMinimumFractionDigits(3);
        nf.setMaximumFractionDigits(3);
      }

      @Override
      public void paint(MatrixStack matrices, int x, int y, int mouseX,
          int mouseY) {
        colX = propertyDelegate.get(0) * 1e-3;
        colY = propertyDelegate.get(1) * 1e-3;
        double nx = colX / 20;
        double ny = colY / 20;
        double len = Math.hypot(nx, ny);
        if (len > 1) {
          nx /= len;
          ny /= len;
        }
        int wx = bg.getX() + 54 + (int) Math.round(54 * nx);
        int wy = bg.getY() + 54 - (int) Math.round(54 * ny);
        setLocation(wx - 8, wy - 8);
        super.paint(matrices, x, y, mouseX, mouseY);
      }

      @Override
      public void addTooltip(TooltipBuilder tooltip) {
        tooltip.add(new TranslatableText("gui.csm.mixingBowl.coordinates",
            nf.format(colX), nf.format(colY)));
      }
    };
    root.add(marker, 0, 0);
    marker.setSize(16, 16);

    WWidget arrow = new WWidget() {
      {
        setSize(16, 64);
      }

      @Override
      public void paint(MatrixStack matrices, int x, int y, int mouseX,
          int mouseY) {
        int progress = propertyDelegate.get(2);
        int maxProgress = propertyDelegate.get(3);
        int pixels = progress == 0 ? 0 : height * progress / maxProgress;
        ScreenDrawing.texturedRect(x, y, width, pixels, WHEEL, 232.0f / 256,
            16.0f / 256, 248.0f / 256, (16.0f + pixels) / 256, -1);
        ScreenDrawing.texturedRect(x, y + pixels, width, height - pixels, WHEEL,
            216.0f / 256, (16.0f + pixels) / 256, 232.0f / 256, 80.0f / 256,
            -1);
      }
    };
    root.add(arrow, 0, 2);
    arrow.setLocation(arrow.getX() + 1, arrow.getY() + 4);

    root.add(this.createPlayerInventoryPanel(), 0, 7);

    root.validate(this);
  }
}
