package flirora.crackslashmine.block.gui;

import io.github.cottonmc.cotton.gui.client.CottonInventoryScreen;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.text.Text;

public class InscriptionAltarScreen extends CottonInventoryScreen<InscriptionAltarGui> {
    public InscriptionAltarScreen(InscriptionAltarGui description, PlayerEntity player, Text title) {
        super(description, player, title);
    }
}
