package flirora.crackslashmine.block.gui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.block.InscriptionAltarBlockEntity;
import flirora.crackslashmine.block.InscriptionResult;
import flirora.crackslashmine.core.Beautify;
import flirora.crackslashmine.gui.CsmScreenHandlers;
import flirora.crackslashmine.item.equipment.EquipmentComponent;
import flirora.crackslashmine.item.essence.tonat.VödDescription;
import flirora.crackslashmine.item.essence.tonat.VödManager;
import io.github.cottonmc.cotton.gui.SyncedGuiDescription;
import io.github.cottonmc.cotton.gui.widget.TooltipBuilder;
import io.github.cottonmc.cotton.gui.widget.WButton;
import io.github.cottonmc.cotton.gui.widget.WGridPanel;
import io.github.cottonmc.cotton.gui.widget.WItemSlot;
import io.github.cottonmc.cotton.gui.widget.WLabel;
import io.github.cottonmc.cotton.gui.widget.WListPanel;
import io.github.cottonmc.cotton.gui.widget.WText;
import io.netty.buffer.Unpooled;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.network.ClientSidePacketRegistry;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;

public class InscriptionAltarGui extends SyncedGuiDescription {
  public static final Identifier USE_C2S =
      new Identifier(CrackSlashMineMod.MOD_ID, "use_inscription_altar");

  private static class VödText extends WText {
    private VödDescription description;

    public VödText() {
      super(new LiteralText(""));
    }

    public void setDescription(VödDescription description) {
      this.description = description;
      this.text = new LiteralText(description.getWord());
    }

    @Override
    public void addTooltip(TooltipBuilder tooltip) {
      super.addTooltip(tooltip);
      tooltip.add(
          new LiteralText(description.getWord()).formatted(Formatting.AQUA));
      ArrayList<Text> out = new ArrayList<>();
      description.addFlavorText(out);
      tooltip.add(out.toArray(new Text[0]));
      for (VödDescription.Entry e : description.getEntries()) {
        Text name = e.getStat().getName();
        Text poly =
            new LiteralText(Beautify.beautifyPolynomial(e.getCurveByLevel()))
                .formatted(Formatting.GOLD);
        Text bonus =
            new LiteralText(Beautify.beautifyPercentage(e.getBonusPerPower()))
                .formatted(Formatting.GREEN);
        tooltip.add(
            new TranslatableText("tooltip.csm.voed.stat", name, poly, bonus));
      }
    }
  }

  private static class VödList extends WListPanel<VödDescription, VödText> {

    private final List<VödDescription> descriptions;

    public VödList() {
      super(Collections.emptyList(), VödText::new,
          (desc, text) -> text.setDescription(desc));
      this.setSize(3 * 18, 5 * 18);
      this.setListItemHeight(12);
      descriptions = new ArrayList<>(VödManager.CLIENT.getEntries().values());
      descriptions.sort(Comparator.comparing(VödDescription::getWord));
    }

    @Environment(EnvType.CLIENT)
    public void update(String prefix, int maxLength) {
      this.data = descriptions.stream().filter(desc -> {
        String word = desc.getWord();
        return word.length() <= maxLength && word.startsWith(prefix);
      }).collect(Collectors.toList());
      this.layout();
    }

    @Environment(EnvType.CLIENT)
    public void reset() {
      this.data = Collections.emptyList();
      this.layout();
    }

    @Override
    public boolean canResize() {
      return false;
    }
  }

  private static class InscribeButton extends WButton {
    private InscriptionResult result = InscriptionResult.fail("unknown");
    private final BlockPos pos;

    public InscribeButton(BlockPos pos) {
      super(new TranslatableText("gui.csm.inscriptionAltar.confirm"));
      setSize(54, 16);
      setEnabled(false);
      this.pos = pos;
    }

    public void setResult(InscriptionResult result) {
      this.result = result;
      this.setEnabled(result.isSuccessful());
    }

    @Override
    public void addTooltip(TooltipBuilder tooltip) {
      super.addTooltip(tooltip);
      Text text = result.getErrorText();
      if (text != null)
        tooltip.add(text);
    }

    @Override
    public boolean canResize() {
      return false;
    }

    @Override
    public void onClick(int x, int y, int button) {
      super.onClick(x, y, button);
      PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
      buf.writeBlockPos(pos);
      if (this.isEnabled())
        ClientSidePacketRegistry.INSTANCE.sendToServer(USE_C2S, buf);
    }
  }

  public InscriptionAltarGui(int syncId, PlayerInventory playerInventory,
      ScreenHandlerContext context) {
    super(CsmScreenHandlers.INSCRIPTION_ALTAR, syncId, playerInventory,
        getBlockInventory(context, InscriptionAltarBlockEntity.INVENTORY_SIZE),
        getBlockPropertyDelegate(context));

    WGridPanel root = new WGridPanel();
    setRootPanel(root);

    WItemSlot equipment = WItemSlot.of(blockInventory, 0, 1, 1);
    root.add(equipment, 3, 1);

    WItemSlot tonats = WItemSlot.of(blockInventory, 1, 6, 4);
    root.add(tonats, 3, 2);

    VödList list = new VödList();
    root.add(list, 0, 1);

    BlockPos blockPos =
        context.run((world, position) -> position, BlockPos.ORIGIN);
    InscribeButton button = new InscribeButton(blockPos);
    root.add(button, 6, 0);

    WLabel label = new WLabel("");
    root.add(label, 4, 1);
    label.setLocation(label.getX() + 2, label.getY() + 4);
    label.setSize(80, 16);

    WItemSlot.ChangeListener changeListener =
        (slot, inventory, index, stack) -> {
          if (!world.isClient())
            return;
          InscriptionResult result =
              InscriptionAltarBlockEntity.dryRun(blockInventory);
          button.setResult(result);
          ItemStack gear = blockInventory.getStack(0);
          EquipmentComponent component = EquipmentComponent.getFor(gear);
          if (component != null && component.getVödData() == null) {
            int maxLen = component.getMeta().getMaxVödLength();
            String prefix = InscriptionAltarBlockEntity.getVöd(blockInventory);
            list.update(prefix, maxLen);
            label.setText(new LiteralText(prefix));
          } else {
            list.reset();
            label.setText(new LiteralText(""));
          }
        };
    equipment.addChangeListener(changeListener);
    tonats.addChangeListener(changeListener);

    root.add(this.createPlayerInventoryPanel(), 0, 6);

    root.validate(this);
  }
}
