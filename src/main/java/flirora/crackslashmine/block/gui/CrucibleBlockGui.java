package flirora.crackslashmine.block.gui;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.gui.CsmScreenHandlers;
import io.github.cottonmc.cotton.gui.SyncedGuiDescription;
import io.github.cottonmc.cotton.gui.client.ScreenDrawing;
import io.github.cottonmc.cotton.gui.widget.WGridPanel;
import io.github.cottonmc.cotton.gui.widget.WItemSlot;
import io.github.cottonmc.cotton.gui.widget.WWidget;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.util.Identifier;

public class CrucibleBlockGui extends SyncedGuiDescription {
    private static final Identifier ARROW = new Identifier(CrackSlashMineMod.MOD_ID, "textures/gui/crucible.png");

    public CrucibleBlockGui(int syncId, PlayerInventory playerInventory, ScreenHandlerContext context) {
        super(
                CsmScreenHandlers.CRUCIBLE, syncId, playerInventory,
                getBlockInventory(context, 18),
                getBlockPropertyDelegate(context, 2));

        WGridPanel root = new WGridPanel();
        setRootPanel(root);

        WItemSlot inputs = WItemSlot.of(blockInventory, 0, 3, 3);
        root.add(inputs, 0, 1);

        WItemSlot outputs = WItemSlot.of(blockInventory, 9, 3, 3);
        root.add(outputs, 6, 1);

        root.add(this.createPlayerInventoryPanel(), 0, 5);

        WWidget arrow = new WWidget() {
            @Override
            @Environment(EnvType.CLIENT)
            public void paint(
                    MatrixStack matrices, int x, int y, int mouseX, int mouseY) {
                float p = progress();
                int pixels = (int) (48 * p);
                p = pixels / 48.0f;
                ScreenDrawing.texturedRect(
                        x, y, pixels, 16,
                        ARROW, 0, 0.5f, p, 1.0f, -1);
                ScreenDrawing.texturedRect(
                        x + pixels, y, 48 - pixels, 16,
                        ARROW, p, 0, 1.0f, 0.5f, -1);
            }
        };

        root.add(arrow, 3, 2);
        arrow.setLocation(arrow.getX() + 4, arrow.getY() + 1);

        root.validate(this);
    }

    private float progress() {
        return ((float) propertyDelegate.get(0)) / propertyDelegate.get(1);
    }
}
