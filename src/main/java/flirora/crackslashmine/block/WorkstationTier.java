package flirora.crackslashmine.block;

public class WorkstationTier {
    public WorkstationTier(Crucible crucibleProperties, MixingBowl mixingBowlProperties) {
        this.crucibleProperties = crucibleProperties;
        this.mixingBowlProperties = mixingBowlProperties;
    }

    public static class Crucible {
        private final int smeltTime;
        private final double avgExpectedProducts;

        public Crucible(int smeltTime, double avgExpectedProducts) {
            this.smeltTime = smeltTime;
            this.avgExpectedProducts = avgExpectedProducts;
        }

        public int getSmeltTime() {
            return smeltTime;
        }

        public double getAvgExpectedProducts() {
            return avgExpectedProducts;
        }
    }

    public static class MixingBowl {
        private final double decrementPerTick;
        private final double durabilityPerPoint;
        private final double baseSpread;
        private final float misuseExplosionStrength;

        public MixingBowl(double decrementPerTick, double durabilityPerPoint, double baseSpread, float misuseExplosionStrength) {
            this.decrementPerTick = decrementPerTick;
            this.durabilityPerPoint = durabilityPerPoint;
            this.baseSpread = baseSpread;
            this.misuseExplosionStrength = misuseExplosionStrength;
        }

        public double getDecrementPerTick() {
            return decrementPerTick;
        }

        public double getDurabilityPerPoint() {
            return durabilityPerPoint;
        }

        public double getBaseSpread() {
            return baseSpread;
        }

        public float getMisuseExplosionStrength() {
            return misuseExplosionStrength;
        }
    }

    private final Crucible crucibleProperties;
    private final MixingBowl mixingBowlProperties;

    public Crucible getCrucibleProperties() {
        return crucibleProperties;
    }

    public MixingBowl getMixingBowlProperties() {
        return mixingBowlProperties;
    }

    public static final WorkstationTier ADVENTURERS = new WorkstationTier(
            new Crucible(
                    300,
                    2.5
            ),
            new MixingBowl(
                    0.01,
                    15.8,
                    10.0,
                    4.0f)
    );
}
