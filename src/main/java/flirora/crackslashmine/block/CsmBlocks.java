package flirora.crackslashmine.block;

import flirora.crackslashmine.CrackSlashMineMod;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.tool.attribute.v1.FabricToolTags;
import net.minecraft.block.Block;
import net.minecraft.block.Material;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class CsmBlocks {
    public static final Block ADVENTURERS_CRUCIBLE = new CrucibleBlock(WorkstationTier.ADVENTURERS);
    public static BlockEntityType<CrucibleBlockEntity> ADVENTURERS_CRUCIBLE_BLOCK_ENTITY;
    public static final Block ADVENTURERS_MIXING_BOWL = new MixingBowlBlock(WorkstationTier.ADVENTURERS);
    public static BlockEntityType<MixingBowlBlockEntity> ADVENTURERS_MIXING_BOWL_BLOCK_ENTITY;

    public static final Block INSCRIPTION_ALTAR = new InscriptionAltarBlock();
    public static BlockEntityType<InscriptionAltarBlockEntity> INSCRIPTION_ALTAR_BLOCK_ENTITY;

    public static final Block TERRIBLE_HACK_BLOCK = new Block(
            FabricBlockSettings.of(Material.AIR).nonOpaque().noCollision().breakInstantly().dropsNothing());
    public static final Block CITRINE_BLOCK = new CitrineBlock();
    public static final Block CITRINE_ORE = new CsmOreBlock(
            FabricBlockSettings.of(Material.STONE)
                    .requiresTool()
                    .breakByTool(FabricToolTags.PICKAXES, 1)
                    .strength(3.0F, 3.0F),
            0, 2);

    private static void registerBlockWithItem(Identifier id, Block block, Item.Settings settings) {
        Registry.register(Registry.BLOCK, id, block);
        Registry.register(Registry.ITEM, id, new BlockItem(block, settings));
    }

    public static void registerBlocks() {
        registerBlockWithItem(CrucibleBlock.ID, ADVENTURERS_CRUCIBLE, new Item.Settings().group(ItemGroup.DECORATIONS));
        ADVENTURERS_CRUCIBLE_BLOCK_ENTITY = Registry.register(
                Registry.BLOCK_ENTITY_TYPE,
                CrucibleBlock.ID,
                BlockEntityType.Builder
                        .create(() -> new CrucibleBlockEntity(WorkstationTier.ADVENTURERS.getCrucibleProperties()), ADVENTURERS_CRUCIBLE)
                        .build(null));
        registerBlockWithItem(MixingBowlBlock.ID, ADVENTURERS_MIXING_BOWL, new Item.Settings().group(ItemGroup.DECORATIONS));
        ADVENTURERS_MIXING_BOWL_BLOCK_ENTITY = Registry.register(
                Registry.BLOCK_ENTITY_TYPE,
                MixingBowlBlock.ID,
                BlockEntityType.Builder
                        .create(() -> new MixingBowlBlockEntity(WorkstationTier.ADVENTURERS.getMixingBowlProperties()), ADVENTURERS_MIXING_BOWL)
                        .build(null));
        registerBlockWithItem(InscriptionAltarBlock.ID, INSCRIPTION_ALTAR, new Item.Settings().group(ItemGroup.DECORATIONS));
        INSCRIPTION_ALTAR_BLOCK_ENTITY = Registry.register(
                Registry.BLOCK_ENTITY_TYPE,
                InscriptionAltarBlock.ID,
                BlockEntityType.Builder
                        .create(InscriptionAltarBlockEntity::new)
                        .build(null));
        Registry.register(Registry.BLOCK,
                new Identifier(CrackSlashMineMod.MOD_ID, "terrible_hack_block_do_not_use_this"),
                TERRIBLE_HACK_BLOCK);
        registerBlockWithItem(CitrineBlock.ID, CITRINE_BLOCK,
                new Item.Settings().group(ItemGroup.BUILDING_BLOCKS));
        registerBlockWithItem(new Identifier(CrackSlashMineMod.MOD_ID, "citrine_ore"), CITRINE_ORE,
                new Item.Settings().group(ItemGroup.BUILDING_BLOCKS));
    }
}
