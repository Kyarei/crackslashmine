package flirora.crackslashmine.block;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.advancement.CsmCriteria;
import flirora.crackslashmine.item.ChromaticUtils;
import flirora.crackslashmine.item.equipment.EquipmentComponent;
import flirora.crackslashmine.network.RubbedItemS2C;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.tool.attribute.v1.FabricToolTags;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Material;
import net.minecraft.block.MaterialColor;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.TextColor;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.Identifier;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class CitrineBlock extends Block {
    public static final Identifier ID =
            new Identifier(CrackSlashMineMod.MOD_ID, "citrine_block");

    public CitrineBlock() {
        super(FabricBlockSettings.of(Material.METAL, MaterialColor.ORANGE)
                .requiresTool()
                .breakByTool(FabricToolTags.PICKAXES, 1)
                .strength(5.0F, 6.0F)
                .sounds(BlockSoundGroup.METAL));
    }

    private boolean tryRub(
            BlockPos pos,
            World world,
            PlayerEntity player,
            Hand hand) {
        if (world.isClient) return false;
        ItemStack stack = player.getStackInHand(hand);
        EquipmentComponent component = EquipmentComponent.getFor(stack);
        if (component == null) return false;
        EquipmentComponent.Meta meta = component.getMeta();
        double hue = meta.getHue();
        int color = ChromaticUtils.fromHue(hue);
        String colorKey = Double.isNaN(hue) ?
                "chat.csm.color.none" :
                "chat.csm.color." + Math.floorMod(Math.round(hue / 15.0), 24);
        player.sendMessage(
                new TranslatableText(
                        "chat.csm.rubEquipment",
                        stack.getName(),
                        new TranslatableText(colorKey).styled(s -> s.withColor(TextColor.fromRgb(color)))),
                true);
        RubbedItemS2C.create(world, pos, hue);
        world.playSound(
                pos.getX() + 0.5, pos.getY() + 1.0, pos.getZ() + 0.5,
                SoundEvents.ENTITY_FIREWORK_ROCKET_BLAST, SoundCategory.AMBIENT,
                1.0f, 1.0f, false);
        if (stack.isDamageable() && !player.isCreative()) {
            int amt = world.getRandom().nextInt(5) + 3;
            stack.damage(amt, player, p -> p.sendToolBreakStatus(hand));
        }
        if (player instanceof ServerPlayerEntity)
            CsmCriteria.RUB_EQUIPMENT.trigger((ServerPlayerEntity) player, stack);
        return true;
    }

    @Override
    public ActionResult onUse(BlockState state,
                              World world,
                              BlockPos pos,
                              PlayerEntity player,
                              Hand hand,
                              BlockHitResult hit) {
        if (tryRub(pos, world, player, hand)) return ActionResult.SUCCESS;
        return super.onUse(state, world, pos, player, hand, hit);
    }
}
