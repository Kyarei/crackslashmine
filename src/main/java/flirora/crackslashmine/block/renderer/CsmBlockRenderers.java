package flirora.crackslashmine.block.renderer;

import flirora.crackslashmine.block.CsmBlocks;
import net.fabricmc.fabric.api.client.rendereregistry.v1.BlockEntityRendererRegistry;

public class CsmBlockRenderers {
    public static void register() {
        BlockEntityRendererRegistry.INSTANCE.register(
                CsmBlocks.ADVENTURERS_MIXING_BOWL_BLOCK_ENTITY,
                MixingBowlBlockEntityRenderer::new);
    }
}
