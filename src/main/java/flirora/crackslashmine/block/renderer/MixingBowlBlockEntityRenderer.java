package flirora.crackslashmine.block.renderer;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.block.MixingBowlBlockEntity;
import flirora.crackslashmine.item.ChromaticUtils;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.*;
import net.minecraft.client.render.block.BlockModelRenderer;
import net.minecraft.client.render.block.entity.BlockEntityRenderDispatcher;
import net.minecraft.client.render.block.entity.BlockEntityRenderer;
import net.minecraft.client.render.model.BakedModel;
import net.minecraft.client.render.model.json.ModelTransformation;
import net.minecraft.client.util.ModelIdentifier;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.client.util.math.Vector3f;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;

import java.util.Objects;

public class MixingBowlBlockEntityRenderer extends BlockEntityRenderer<MixingBowlBlockEntity> {
    private final BakedModel liquid;
    private final BlockModelRenderer renderer;

    public static final ModelIdentifier LIQUID_MODEL = new ModelIdentifier(new Identifier(
            CrackSlashMineMod.MOD_ID, "terrible_hack_block_do_not_use_this"), "");

    public MixingBowlBlockEntityRenderer(BlockEntityRenderDispatcher dispatcher) {
        super(dispatcher);
        this.liquid = MinecraftClient.getInstance().getBakedModelManager().getModel(LIQUID_MODEL);
        this.renderer = MinecraftClient.getInstance().getBlockRenderManager().getModelRenderer();
    }

    private static final float COLOR_BOOST = 0.4f;

    @Override
    public void render(MixingBowlBlockEntity entity,
                       float tickDelta,
                       MatrixStack matrices,
                       VertexConsumerProvider vertexConsumers,
                       int light,
                       int overlay) {
        matrices.push();
        int lightAbove = WorldRenderer.getLightmapCoordinates(
                Objects.requireNonNull(entity.getWorld()), entity.getPos().up());
        double colX = entity.getColX();
        double colY = entity.getColY();
        double amt = Math.hypot(colX, colY);
        if (amt >= 0.01) {
            // The inside of the bowl is the inner 12x12 region, with the bottom
            // of the interior of the bowl being 4 pixels high.
            double hue = Math.toDegrees(Math.atan2(colY, colX)) % 360.0;
            if (hue < 0) hue += 360.0;
            int liquidColor = ChromaticUtils.fromHue(hue);
            float red = (liquidColor >> 16) / 255.0f;
            float green = ((liquidColor >> 8) & 255) / 255.0f;
            float blue = (liquidColor & 255) / 255.0f;
            red = COLOR_BOOST + (1 - COLOR_BOOST) * red;
            green = COLOR_BOOST + (1 - COLOR_BOOST) * green;
            blue = COLOR_BOOST + (1 - COLOR_BOOST) * blue;
            double level = 6.0 * Math.min(0.98, amt / 64.0) / 16.0;
            matrices.translate(0, level, 0);
            RenderLayer renderLayer = RenderLayers.getBlockLayer(entity.getCachedState());
            VertexConsumer vertexConsumer = vertexConsumers.getBuffer(renderLayer);
            renderer.render(matrices.peek(), vertexConsumer, null, liquid, red, green, blue, 0xF000F0, overlay);
            matrices.translate(0, -level, 0);
        }
        ItemStack gear = entity.getStack(0);
        if (!gear.isEmpty()) {
            matrices.translate(0.5, 0.625, 0.5);
            matrices.multiply(Vector3f.POSITIVE_X.getDegreesQuaternion(90.0f));

            MinecraftClient.getInstance().getItemRenderer().renderItem(
                    gear, ModelTransformation.Mode.FIXED, lightAbove, overlay, matrices, vertexConsumers);
        }
        matrices.pop();
    }
}
