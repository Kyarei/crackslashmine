package flirora.crackslashmine.block;

import flirora.crackslashmine.block.gui.CrucibleBlockGui;
import flirora.crackslashmine.item.equipment.EquipmentUtils;
import io.github.cottonmc.cotton.gui.PropertyDelegateHolder;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.ItemScatterer;
import net.minecraft.util.Tickable;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.Direction;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

public class CrucibleBlockEntity extends BlockEntity
        implements ImplementedInventory, SidedInventory, Tickable,
        PropertyDelegateHolder, NamedScreenHandlerFactory {
    private final DefaultedList<ItemStack> items =
            DefaultedList.ofSize(18, ItemStack.EMPTY);
    private final WorkstationTier.Crucible tier;
    private short smeltProgress;

    private final PropertyDelegate propertyDelegate = new PropertyDelegate() {
        @Override
        public int get(int index) {
            if (index == 0) return smeltProgress;
            if (index == 1) return getSmeltTime();
            return -1;
        }

        @Override
        public void set(int index, int value) {
            if (index == 0) smeltProgress = (short) value;
        }

        @Override
        public int size() {
            return 2;
        }
    };

    public CrucibleBlockEntity(WorkstationTier.Crucible tier) {
        super(CsmBlocks.ADVENTURERS_CRUCIBLE_BLOCK_ENTITY);
        this.tier = tier;
    }

    private int getSmeltTime() {
        return tier.getSmeltTime();
    }

    @Override
    public DefaultedList<ItemStack> getItems() {
        return items;
    }

    @Override
    public void fromTag(BlockState state, CompoundTag tag) {
        super.fromTag(state, tag);
        Inventories.fromTag(tag, items);
        smeltProgress = tag.getShort("smeltProgress");
    }

    @Override
    public CompoundTag toTag(CompoundTag tag) {
        tag.putShort("smeltProgress", smeltProgress);
        Inventories.toTag(tag, items);
        return super.toTag(tag);
    }

    @Override
    public int[] getAvailableSlots(Direction side) {
        int offset = side == Direction.DOWN ? 9 : 0;
        int[] slots = new int[9];
        for (int i = 0; i < 9; ++i)
            slots[i] = i + offset;
        return slots;
    }

    @Override
    public boolean canInsert(int slot, ItemStack stack, @Nullable Direction dir) {
        return dir != Direction.DOWN && slot < 9;
    }

    @Override
    public boolean canExtract(int slot, ItemStack stack, Direction dir) {
        return dir == Direction.DOWN && slot >= 9;
    }

    private int pendingSlot() {
        for (int i = 0; i < 9; ++i) {
            ItemStack stack = items.get(i);
            if (!stack.isEmpty() && EquipmentUtils.canSalvage(stack)) {
                return i;
            }
        }
        return -1;
    }

    private void insertToOutput(ItemStack stack) {
        int count = stack.getCount();
        int maxCount = stack.getMaxCount();
        for (int i = 9; i < 18; ++i) {
            if (count == 0) return;
            ItemStack slot = items.get(i);
            if (slot.isEmpty()) {
                int deducted = Math.min(count, maxCount);
                items.set(i, new ItemStack(stack.getItem(), deducted));
                count -= deducted;
            } else if (slot.getItem() == stack.getItem()) {
                int deducted = Math.min(count, maxCount - slot.getCount());
                slot.increment(deducted);
                count -= deducted;
            }
        }
        if (count > 0) {
            DefaultedList<ItemStack> list = DefaultedList.of();
            list.add(new ItemStack(stack.getItem(), count));
            // Could not find any empty slot; scatter into the world
            ItemScatterer.spawn(world, pos, list);
        }
    }

    @Override
    public void tick() {
        int slot = pendingSlot();
        if (slot == -1) {
            if (smeltProgress != 0)
                this.markDirty();
            smeltProgress = 0;
        } else {
            ++smeltProgress;
            this.markDirty();
        }
        if (smeltProgress >= getSmeltTime()) {
            boolean succeeded = EquipmentUtils.salvage(
                    items.get(slot),
                    tier.getAvgExpectedProducts(),
                    this::insertToOutput,
                    Objects.requireNonNull(this.getWorld()).getRandom());
            if (succeeded) {
                items.get(slot).decrement(1);
                smeltProgress = 0;
            }
            this.markDirty();
        }
        assert this.world != null;
        this.world.setBlockState(this.pos, this.world.getBlockState(pos).with(CrucibleBlock.ACTIVE, slot != -1));
    }

    @Override
    public PropertyDelegate getPropertyDelegate() {
        return propertyDelegate;
    }

    @Override
    public Text getDisplayName() {
        return new TranslatableText("block.crackslashmine.adventurers_crucible");
    }

    @Override
    public @Nullable ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player) {
        return new CrucibleBlockGui(syncId, inv, ScreenHandlerContext.create(world, pos));
    }
}
