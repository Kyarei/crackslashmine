package flirora.crackslashmine.block;

import org.jetbrains.annotations.Nullable;

import flirora.crackslashmine.CrackSlashMineMod;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.tool.attribute.v1.FabricToolTags;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.BlockWithEntity;
import net.minecraft.block.Material;
import net.minecraft.block.ShapeContext;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.Identifier;
import net.minecraft.util.ItemScatterer;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.util.shape.VoxelShapes;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;

public class InscriptionAltarBlock extends BlockWithEntity {
  public static final Identifier ID =
      new Identifier(CrackSlashMineMod.MOD_ID, "inscription_altar");

  protected InscriptionAltarBlock() {
    super(FabricBlockSettings.of(Material.METAL)
        .breakByTool(FabricToolTags.PICKAXES, 1).requiresTool().hardness(5)
        .resistance(5).nonOpaque());
  }

  @Override
  public BlockRenderType getRenderType(BlockState state) {
    return BlockRenderType.MODEL;
  }

  @Override
  public @Nullable BlockEntity createBlockEntity(BlockView world) {
    return new InscriptionAltarBlockEntity();
  }

  @Override
  public void onStateReplaced(BlockState state, World world, BlockPos pos,
      BlockState newState, boolean moved) {
    if (!state.isOf(newState.getBlock())) {
      BlockEntity blockEntity = world.getBlockEntity(pos);
      if (blockEntity instanceof Inventory) {
        ItemScatterer.spawn(world, pos, (Inventory) blockEntity);
        world.updateComparators(pos, this);
      }

      super.onStateReplaced(state, world, pos, newState, moved);
    }
  }

  @Override
  public ActionResult onUse(BlockState state, World world, BlockPos pos,
      PlayerEntity player, Hand hand, BlockHitResult hit) {
    player.openHandledScreen(state.createScreenHandlerFactory(world, pos));
    return ActionResult.SUCCESS;
  }

  @Override
  public boolean isTranslucent(BlockState state, BlockView world,
      BlockPos pos) {
    return true;
  }

  @Override
  public VoxelShape getVisualShape(BlockState state, BlockView world,
      BlockPos pos, ShapeContext context) {
    return VoxelShapes.empty();
  }

  @Override
  @Environment(EnvType.CLIENT)
  public boolean isSideInvisible(BlockState state, BlockState stateFrom,
      Direction direction) {
    return stateFrom.isOf(this)
        || super.isSideInvisible(state, stateFrom, direction);
  }
}
