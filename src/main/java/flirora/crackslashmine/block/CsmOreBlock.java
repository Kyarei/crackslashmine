package flirora.crackslashmine.block;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;

public class CsmOreBlock extends Block {
    private final int minXp, maxXp;

    public CsmOreBlock(AbstractBlock.Settings settings, int minXp, int maxXp) {
        super(settings);
        this.minXp = minXp;
        this.maxXp = maxXp;
    }

    @Override
    public void onStacksDropped(BlockState state, ServerWorld world, BlockPos pos, ItemStack stack) {
        super.onStacksDropped(state, world, pos, stack);
        if (EnchantmentHelper.getLevel(Enchantments.SILK_TOUCH, stack) == 0) {
            int xp = MathHelper.nextInt(world.random, minXp, maxXp);
            if (xp > 0) {
                this.dropExperience(world, pos, xp);
            }
        }
    }
}
