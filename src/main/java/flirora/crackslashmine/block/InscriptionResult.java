package flirora.crackslashmine.block;

import flirora.crackslashmine.item.essence.tonat.VödDescription;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import org.jetbrains.annotations.Nullable;

public interface InscriptionResult {
    @Nullable Text getErrorText();

    default boolean isSuccessful() {
        return getErrorText() == null;
    }

    double getLevel();

    int getPower();

    VödDescription getDescription();

    class Success implements InscriptionResult {
        private final VödDescription desc;
        private final double level;
        private final int power;

        public Success(VödDescription desc, double level, int power) {
            this.desc = desc;
            this.level = level;
            this.power = power;
        }

        @Override
        public double getLevel() {
            return level;
        }

        @Override
        public int getPower() {
            return power;
        }

        @Override
        public VödDescription getDescription() {
            return desc;
        }

        @Override
        public @Nullable Text getErrorText() {
            return null;
        }
    }

    class Failure implements InscriptionResult {
        private final String code;

        public Failure(String code) {
            this.code = code;
        }

        @Override
        public @Nullable Text getErrorText() {
            return new TranslatableText("block.crackslashmine.inscription_altar.fail." + code)
                    .formatted(Formatting.RED);
        }

        @Override
        public double getLevel() {
            return 0;
        }

        @Override
        public int getPower() {
            return 0;
        }

        @Override
        public VödDescription getDescription() {
            return null;
        }

        public String getCode() {
            return code;
        }
    }

    static InscriptionResult success(VödDescription desc, double level, int power) {
        return new Success(desc, level, power);
    }

    static InscriptionResult fail(String code) {
        return new Failure(code);
    }
}
