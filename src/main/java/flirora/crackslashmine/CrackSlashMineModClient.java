package flirora.crackslashmine;

import flirora.crackslashmine.block.CsmBlocks;
import flirora.crackslashmine.block.gui.CrucibleBlockGui;
import flirora.crackslashmine.block.gui.CrucibleBlockScreen;
import flirora.crackslashmine.block.gui.InscriptionAltarGui;
import flirora.crackslashmine.block.gui.InscriptionAltarScreen;
import flirora.crackslashmine.block.gui.MixingBowlBlockGui;
import flirora.crackslashmine.block.gui.MixingBowlBlockScreen;
import flirora.crackslashmine.block.renderer.CsmBlockRenderers;
import flirora.crackslashmine.config.CsmConfigGuiUtils;
import flirora.crackslashmine.core.Beautify;
import flirora.crackslashmine.core.CsmRegistries;
import flirora.crackslashmine.core.EntityDamageUtils;
import flirora.crackslashmine.core.xse.ExtendedStatusEffect;
import flirora.crackslashmine.entity.CsmEntities;
import flirora.crackslashmine.entity.spell.BallEntity;
import flirora.crackslashmine.entity.spell.BallEntityRenderer;
import flirora.crackslashmine.generation.Area;
import flirora.crackslashmine.generation.ClientAreaManager;
import flirora.crackslashmine.generation.PalettedAreaMap;
import flirora.crackslashmine.generation.gui.AreaVisGui;
import flirora.crackslashmine.generation.gui.AreaVisScreen;
import flirora.crackslashmine.gui.AreaChangeListener;
import flirora.crackslashmine.gui.CsmKeybinds;
import flirora.crackslashmine.gui.CsmScreenHandlers;
import flirora.crackslashmine.gui.ItemHudRenderers;
import flirora.crackslashmine.gui.LevelUpListener;
import flirora.crackslashmine.gui.TextParticle;
import flirora.crackslashmine.item.ChromaticUtils;
import flirora.crackslashmine.item.CsmItems;
import flirora.crackslashmine.item.essence.CrystallizedChromaItem;
import flirora.crackslashmine.item.essence.tonat.VödManager;
import flirora.crackslashmine.item.food.CompatibleFoodManager;
import flirora.crackslashmine.item.spell.WandGui;
import flirora.crackslashmine.item.spell.WandItem;
import flirora.crackslashmine.item.spell.WandScreen;
import flirora.crackslashmine.item.spell.data.SpellDataManager;
import flirora.crackslashmine.network.DamageSplatS2C;
import flirora.crackslashmine.network.HealSplatS2C;
import flirora.crackslashmine.network.MiscSplatS2C;
import flirora.crackslashmine.network.RubbedItemS2C;
import flirora.crackslashmine.network.XpAwardSplatS2C;
import flirora.crackslashmine.network.entity.CsmEntitySpawnS2CPacket;
import flirora.crackslashmine.network.entity.EntitySpawnPacketRegistry;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.fabricmc.fabric.api.client.rendereregistry.v1.EntityRendererRegistry;
import net.fabricmc.fabric.api.client.rendering.v1.ColorProviderRegistry;
import net.fabricmc.fabric.api.client.rendering.v1.HudRenderCallback;
import net.fabricmc.fabric.api.client.screenhandler.v1.ScreenRegistry;
import net.fabricmc.fabric.api.event.client.ClientSpriteRegistryCallback;
import net.fabricmc.fabric.api.network.ClientSidePacketRegistry;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.entity.LightningEntityRenderer;
import net.minecraft.client.texture.SpriteAtlasTexture;
import net.minecraft.entity.Entity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.text.LiteralText;
import net.minecraft.text.TextColor;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.registry.Registry;

public class CrackSlashMineModClient implements ClientModInitializer {
  public static SpriteAtlasTexture extendedStatusEffectIconAtlas;

  public static AreaVisGui areaVisGui;

  // TODO: replace this with dedicated textures for each crystallized chroma
  private static int getCrystallizedChromaColor(ItemStack stack, int layer) {
    if (!(stack.getItem() instanceof CrystallizedChromaItem))
      return -1;
    int c = ((CrystallizedChromaItem) stack.getItem()).getColor();
    return ChromaticUtils.fromHue(15 * c);
  }

  @Override
  public void onInitializeClient() {
    CsmConfigGuiUtils.initialize();

    ClientSpriteRegistryCallback
        .event(new Identifier("textures/atlas" + "/mob_effects.png"))
        .register((atlasTexture, registry) -> {
          for (ExtendedStatusEffect effect : CsmRegistries.EXTENDED_STATUS_EFFECT) {
            registry.register(effect.getTextureWithoutExtension());
          }
          extendedStatusEffectIconAtlas = atlasTexture;
        });

    EntityRendererRegistry.INSTANCE.register(CsmEntities.BALL,
        (dispatcher, context) -> new BallEntityRenderer(dispatcher));
    EntityRendererRegistry.INSTANCE.register(CsmEntities.LIGHTNING,
        (dispatcher, context) -> new LightningEntityRenderer(dispatcher));
    EntitySpawnPacketRegistry.INSTANCE.register(CsmEntities.BALL,
        (packet, world) -> new BallEntity(world, packet));

    HudRenderCallback.EVENT
        .register(new AreaChangeListener(MinecraftClient.getInstance()));
    HudRenderCallback.EVENT
        .register(new LevelUpListener(MinecraftClient.getInstance()));

    CsmBlockRenderers.register();

    ColorProviderRegistry.ITEM.register(
        CrackSlashMineModClient::getCrystallizedChromaColor,
        CsmItems.COLORED_CHROMAS.toArray(new Item[0]));

    BlockRenderLayerMap.INSTANCE.putBlock(CsmBlocks.INSCRIPTION_ALTAR,
        RenderLayer.getCutout());

    ClientSidePacketRegistry.INSTANCE.register(DamageSplatS2C.ID,
        (context, data) -> {
          DamageSplatS2C splat =
              DamageSplatS2C.read(data, context.getPlayer().getEntityWorld());
          context.getTaskQueue().execute(() -> {
            MinecraftClient client = MinecraftClient.getInstance();
            int eid = splat.getEntityId();
            assert client.world != null;
            Entity e = client.world.getEntityById(eid);
            if (e == null)
              return;
            Vec3d pos =
                EntityDamageUtils.getPositionForSplat(e, context.getPlayer());
            client.particleManager.addParticle(new TextParticle(client.world,
                splat.getDamage().asText(), pos.getX(), pos.getY(), pos.getZ(),
                0.0f, 0.8f, 0.0f, 0.04f));
          });
        });
    ClientSidePacketRegistry.INSTANCE.register(MiscSplatS2C.ID,
        (context, data) -> {
          MiscSplatS2C splat = MiscSplatS2C.read(data);
          context.getTaskQueue().execute(() -> {
            MinecraftClient client = MinecraftClient.getInstance();
            int eid = splat.getEntityId();
            assert client.world != null;
            Entity e = client.world.getEntityById(eid);
            if (e == null)
              return;
            Vec3d pos =
                EntityDamageUtils.getPositionForSplat(e, context.getPlayer());
            client.particleManager.addParticle(new TextParticle(client.world,
                splat.getType().asText(), pos.getX(), pos.getY(), pos.getZ(),
                0.0f, 0.8f, 0.0f, 0.04f));
          });
        });
    ClientSidePacketRegistry.INSTANCE.register(XpAwardSplatS2C.ID,
        (context, data) -> {
          XpAwardSplatS2C splat = XpAwardSplatS2C.read(data);
          context.getTaskQueue().execute(() -> {
            MinecraftClient client = MinecraftClient.getInstance();
            int eid = splat.getEntityId();
            assert client.world != null;
            Entity e = client.world.getEntityById(eid);
            if (e == null)
              return;
            Vec3d pos =
                EntityDamageUtils.getPositionForSplat(e, context.getPlayer());
            client.particleManager.addParticle(new TextParticle(client.world,
                new TranslatableText("csm.hud.splat.xpAward",
                    Beautify.beautify(splat.getXp())),
                pos.getX(), pos.getY() + 0.5, pos.getZ(), 0.0f, 0.2f, 0.0f,
                0.05f));
          });
        });
    ClientSidePacketRegistry.INSTANCE.register(HealSplatS2C.ID,
        (context, data) -> {
          HealSplatS2C splat = HealSplatS2C.read(data);
          context.getTaskQueue().execute(() -> {
            MinecraftClient client = MinecraftClient.getInstance();
            int eid = splat.getEntityId();
            assert client.world != null;
            Entity e = client.world.getEntityById(eid);
            if (e == null)
              return;
            Vec3d pos =
                EntityDamageUtils.getPositionForSplat(e, context.getPlayer());
            client.particleManager.addParticle(new TextParticle(client.world,
                new LiteralText(Beautify.beautify(splat.getHealAmt()))
                    .styled(s -> s.withColor(TextColor.fromRgb(0x6FEC55))),
                pos.getX(), pos.getY(), pos.getZ(), 0.0f, 0.8f, 0.0f, 0.04f));
          });
        });

    ClientSidePacketRegistry.INSTANCE.register(CompatibleFoodManager.PACKET_ID,
        CompatibleFoodManager.CLIENT::fillDataFrom);
    ClientSidePacketRegistry.INSTANCE.register(VödManager.PACKET_ID,
        VödManager.CLIENT::fillDataFrom);
    ClientSidePacketRegistry.INSTANCE.register(SpellDataManager.PACKET_ID,
        SpellDataManager.CLIENT::fillDataFrom);

    ClientSidePacketRegistry.INSTANCE.register(AreaVisGui.OPEN_GUI_S2C,
        (context, data) -> {
          context.getTaskQueue().execute(() -> {
            areaVisGui = new AreaVisGui();
            MinecraftClient.getInstance()
                .openScreen(new AreaVisScreen(areaVisGui));
          });
        });

    ClientSidePacketRegistry.INSTANCE.register(AreaVisGui.RETURN_AREA_INFO_S2C,
        (context, data) -> {
          Area a = Area.readFromPacket(data);
          context.getTaskQueue().execute(() -> {
            if (areaVisGui != null)
              areaVisGui.setArea(a);
          });
        });

    ClientSidePacketRegistry.INSTANCE.register(PalettedAreaMap.SYNC_S2C,
        (context, data) -> {
          PalettedAreaMap map = PalettedAreaMap.readFromPacket(data);
          Area current = Area.readFromPacket(data);
          context.getTaskQueue().execute(() -> {
            PalettedAreaMap.receivePacket(map, current,
                ((ClientAreaManager.Provider) MinecraftClient.getInstance())
                    .getClientAreaManager());
          });
        });

    ClientSidePacketRegistry.INSTANCE.register(RubbedItemS2C.ID,
        (context, data) -> {
          RubbedItemS2C packet = RubbedItemS2C.read(data);
          context.getTaskQueue().execute(() -> {
            int color = ChromaticUtils.fromHue(packet.getHue());
            BlockPos pos = packet.getPos();
            CompoundTag tag = new CompoundTag();
            ListTag explosions = new ListTag();
            tag.put("Explosions", explosions);
            CompoundTag explosion = new CompoundTag();
            explosion.putByte("type", (byte) 0);
            explosion.putIntArray("Colors", new int[] { color });
            explosion.putIntArray("FadeColors", new int[] { color });
            explosions.add(explosion);
            context.getPlayer().getEntityWorld().addFireworkParticle(
                pos.getX() + 0.5, pos.getY() + 1, pos.getZ() + 0.5, 0, 0, 0,
                tag);
          });
        });

    ClientSidePacketRegistry.INSTANCE.register(CsmEntitySpawnS2CPacket.ID,
        CsmEntitySpawnS2CPacket::receive);

    CsmKeybinds.load();

    ScreenRegistry.<CrucibleBlockGui, CrucibleBlockScreen>register(
        CsmScreenHandlers.CRUCIBLE, (gui, inventory,
            title) -> new CrucibleBlockScreen(gui, inventory.player, title));
    ScreenRegistry.<MixingBowlBlockGui, MixingBowlBlockScreen>register(
        CsmScreenHandlers.MIXING_BOWL, (gui, inventory,
            title) -> new MixingBowlBlockScreen(gui, inventory.player, title));
    ScreenRegistry.<WandGui, WandScreen>register(CsmScreenHandlers.WAND, (gui,
        inventory, title) -> new WandScreen(gui, inventory.player, title));
    ScreenRegistry.<InscriptionAltarGui, InscriptionAltarScreen>register(
        CsmScreenHandlers.INSCRIPTION_ALTAR, (gui, inventory,
            title) -> new InscriptionAltarScreen(gui, inventory.player, title));

    for (Item item : Registry.ITEM) {
      if (item instanceof WandItem) {
        ItemHudRenderers.INSTANCE.register(item, WandItem.HUD_RENDERER);
      }
    }
  }
}
