package flirora.crackslashmine.command;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import flirora.crackslashmine.generation.gui.AreaVisGui;
import io.netty.buffer.Unpooled;
import net.fabricmc.fabric.api.network.ServerSidePacketRegistry;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.command.ServerCommandSource;

import static net.minecraft.server.command.CommandManager.literal;

public class AreaVisCommand {
    public static void register(CommandDispatcher<ServerCommandSource> dispatcher) {
        dispatcher.register(literal("csm")
                .then(literal("areavis")
                        .requires(source -> source.hasPermissionLevel(4))
                        .executes(ctx -> {
                            ServerCommandSource source = ctx.getSource();
                            ServerSidePacketRegistry.INSTANCE.sendToPlayer(
                                    source.getPlayer(),
                                    AreaVisGui.OPEN_GUI_S2C,
                                    new PacketByteBuf(Unpooled.buffer()));
                            return Command.SINGLE_SUCCESS;
                        })));
    }
}
