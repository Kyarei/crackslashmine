package flirora.crackslashmine.command;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType;
import flirora.crackslashmine.item.equipment.LootUtils;
import flirora.crackslashmine.item.equipment.Origin;
import net.minecraft.command.argument.ItemStackArgument;
import net.minecraft.command.argument.ItemStackArgumentType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.TranslatableText;

import static com.mojang.brigadier.arguments.IntegerArgumentType.integer;
import static net.minecraft.command.argument.ItemStackArgumentType.itemStack;
import static net.minecraft.server.command.CommandManager.argument;
import static net.minecraft.server.command.CommandManager.literal;

public class CsmGiveCommand {
    public static void register(CommandDispatcher<ServerCommandSource> dispatcher) {
        dispatcher.register(literal("csm")
                .then(literal("give")
                        .requires(source -> source.hasPermissionLevel(2))
                        .then(argument("item", itemStack())
                                .executes((ctx) -> give(
                                        ctx.getSource(),
                                        ItemStackArgumentType.getItemStackArgument(ctx, "item"),
                                        -1))
                                .then(argument("level", integer(-1))
                                        .executes((ctx) -> give(
                                                ctx.getSource(),
                                                ItemStackArgumentType.getItemStackArgument(ctx, "item"),
                                                IntegerArgumentType.getInteger(ctx, "level")
                                        ))))));
    }

    private static int give(
            ServerCommandSource source,
            ItemStackArgument stackArg,
            int level)
            throws CommandSyntaxException {
        PlayerEntity e = source.getPlayer();
        ItemStack stack = stackArg.createStack(1, false);
        boolean succeeded = LootUtils.generateItem(
                stack.getItem(),
                stack,
                source.getWorld(),
                e,
                level,
                Origin.LOOTED
        );
        if (!succeeded) {
            throw new SimpleCommandExceptionType(
                    new TranslatableText("command.csm.give.fail"))
                    .create();
        }
        source.sendFeedback(new TranslatableText("command.csm.give.success", stack.getName(), e.getDisplayName()), true);
        e.inventory.insertStack(stack);
        return Command.SINGLE_SUCCESS;
    }
}
