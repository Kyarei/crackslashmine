package flirora.crackslashmine.command;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType;
import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.item.essence.tonat.TonatItem;
import net.minecraft.command.argument.ItemStackArgument;
import net.minecraft.command.argument.ItemStackArgumentType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.TranslatableText;

import static com.mojang.brigadier.arguments.IntegerArgumentType.integer;
import static net.minecraft.command.argument.ItemStackArgumentType.itemStack;
import static net.minecraft.server.command.CommandManager.argument;
import static net.minecraft.server.command.CommandManager.literal;

public class CsmGiveTonatCommand {
    private static final SimpleCommandExceptionType BAD_ITEM = new SimpleCommandExceptionType(
            new TranslatableText("command.csm.giveTonat.badItem"));

    public static void register(CommandDispatcher<ServerCommandSource> dispatcher) {
        dispatcher.register(literal("csm")
                .then(literal("give_tonat")
                        .requires(source -> source.hasPermissionLevel(2))
                        .then(argument("tonat", itemStack())
                                .executes((ctx) -> give(
                                        ctx.getSource(),
                                        ItemStackArgumentType.getItemStackArgument(ctx, "tonat"),
                                        -1))
                                .then(argument("level", integer(-1))
                                        .executes((ctx) -> give(
                                                ctx.getSource(),
                                                ItemStackArgumentType.getItemStackArgument(ctx, "tonat"),
                                                IntegerArgumentType.getInteger(ctx, "level")
                                        ))))));
    }

    private static int give(
            ServerCommandSource source,
            ItemStackArgument item,
            int level)
            throws CommandSyntaxException {
        PlayerEntity e = source.getPlayer();
        if (!(item.getItem() instanceof TonatItem)) {
            throw BAD_ITEM.create();
        }
        ItemStack tonat = ((TonatItem) item.getItem()).createWithRandomStats(
                source.getWorld().getRandom(),
                level == -1 ? LivingEntityStats.getFor(source.getPlayer()).getLevel() : level);
        source.sendFeedback(new TranslatableText("command.csm.give.success", tonat.getName(), e.getDisplayName()), true);
        e.inventory.insertStack(tonat);
        return Command.SINGLE_SUCCESS;
    }
}
