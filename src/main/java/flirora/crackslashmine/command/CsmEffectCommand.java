package flirora.crackslashmine.command;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.arguments.LongArgumentType;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import flirora.crackslashmine.core.Beautify;
import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.core.xse.ExtendedStatusEffect;
import flirora.crackslashmine.core.xse.ExtendedStatusEffectInstance;
import net.minecraft.command.argument.EntityArgumentType;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.TranslatableText;

import static com.mojang.brigadier.arguments.LongArgumentType.longArg;
import static flirora.crackslashmine.command.ExtendedStatusEffectArgumentType.extendedStatusEffect;
import static net.minecraft.command.argument.EntityArgumentType.entity;
import static net.minecraft.command.argument.TimeArgumentType.time;
import static net.minecraft.server.command.CommandManager.argument;
import static net.minecraft.server.command.CommandManager.literal;

public class CsmEffectCommand {
    public static void register(CommandDispatcher<ServerCommandSource> dispatcher) {
        dispatcher.register(literal("csm")
                .then(literal("effect")
                        .requires(source -> source.hasPermissionLevel(2))
                        .then(argument("target", entity())
                                .then(argument("effect", extendedStatusEffect())
                                        .then(argument("intensity", longArg())
                                                .then(argument("duration", time())
                                                        .executes(ctx ->
                                                                effect(
                                                                        ctx.getSource(),
                                                                        EntityArgumentType.getEntity(ctx, "target"),
                                                                        ExtendedStatusEffectArgumentType.getEffect(ctx, "effect"),
                                                                        LongArgumentType.getLong(ctx, "intensity"),
                                                                        IntegerArgumentType.getInteger(ctx, "duration"))
                                                        )))))));
    }

    private static int effect(
            ServerCommandSource source,
            Entity e,
            ExtendedStatusEffect effect,
            long intensity,
            int duration) throws CommandSyntaxException {
        if (e == null) e = source.getEntity();
        if (e == null) {
            throw ShowStatsCommand.NOT_LIVING_ENTITY.create("null");
        }
        if (!(e instanceof LivingEntity)) {
            throw ShowStatsCommand.NOT_LIVING_ENTITY.create(e.getName());
        }
        LivingEntity target = (LivingEntity) e;
        LivingEntityStats stats = LivingEntityStats.getFor(target);
        stats.applyEffect(new ExtendedStatusEffectInstance(
                effect,
                duration,
                intensity,
                null
        ));
        source.sendFeedback(new TranslatableText(
                "command.csm.effect.success",
                effect.getStatusEffectName(),
                intensity,
                e.getDisplayName(),
                Beautify.beautifyTime(duration)
        ), true);
        return Command.SINGLE_SUCCESS;
    }
}
