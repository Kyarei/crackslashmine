package flirora.crackslashmine.command;

import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.CompletableFuture;

import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.arguments.ArgumentType;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.DynamicCommandExceptionType;
import com.mojang.brigadier.suggestion.Suggestions;
import com.mojang.brigadier.suggestion.SuggestionsBuilder;

import flirora.crackslashmine.item.spell.Spell;
import flirora.crackslashmine.item.spell.data.SpellDataManager;
import net.minecraft.command.CommandSource;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;

public class SpellArgumentType implements ArgumentType<Spell> {
  private static final Collection<String> EXAMPLES =
      Arrays.asList("spooky", "effect");
  public static final DynamicCommandExceptionType INVALID_SPELL_EXCEPTION =
      new DynamicCommandExceptionType(
          o -> new TranslatableText("command.csm.spellNotFound", o));

  public static SpellArgumentType spell() {
    return new SpellArgumentType();
  }

  public static Spell getSpell(
      CommandContext<ServerCommandSource> commandContext, String string)
      throws CommandSyntaxException {
    return commandContext.getArgument(string, Spell.class);
  }

  @Override
  public Spell parse(StringReader reader) throws CommandSyntaxException {
    Identifier id = Identifier.fromCommandInput(reader);
    Spell result = SpellDataManager.SERVER.getSpell(id);
    if (id == null)
      throw INVALID_SPELL_EXCEPTION.create(id);
    return result;
  }

  @Override
  public <S> CompletableFuture<Suggestions> listSuggestions(
      CommandContext<S> context, SuggestionsBuilder builder) {
    return CommandSource
        .suggestIdentifiers(SpellDataManager.SERVER.getAllSpellIds(), builder);
  }

  @Override
  public Collection<String> getExamples() {
    return EXAMPLES;
  }
}
