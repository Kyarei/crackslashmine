package flirora.crackslashmine.command;

import static com.mojang.brigadier.arguments.IntegerArgumentType.integer;
import static net.minecraft.command.argument.ItemStackArgumentType.itemStack;
import static net.minecraft.server.command.CommandManager.argument;
import static net.minecraft.server.command.CommandManager.literal;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType;

import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.item.baggim.LootBagItem;
import net.minecraft.command.argument.ItemStackArgument;
import net.minecraft.command.argument.ItemStackArgumentType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.TranslatableText;

public class CsmGiveLootBagCommand {
  private static final SimpleCommandExceptionType BAD_ITEM =
      new SimpleCommandExceptionType(
          new TranslatableText("command.csm.giveLootBag.badItem"));

  public static void register(
      CommandDispatcher<ServerCommandSource> dispatcher) {
    dispatcher
        .register(
            literal("csm").then(literal("give_loot_bag")
                .requires(source -> source.hasPermissionLevel(2))
                .then(argument("loot_bag", itemStack())
                    .executes((ctx) -> give(ctx.getSource(),
                        ItemStackArgumentType.getItemStackArgument(ctx,
                            "loot_bag"),
                        -1))
                    .then(argument("level", integer(-1))
                        .executes((ctx) -> give(ctx.getSource(),
                            ItemStackArgumentType.getItemStackArgument(ctx,
                                "loot_bag"),
                            IntegerArgumentType.getInteger(ctx, "level")))))));
  }

  private static int give(ServerCommandSource source, ItemStackArgument bag,
      int level) throws CommandSyntaxException {
    PlayerEntity e = source.getPlayer();
    Item item = bag.getItem();
    if (!(item instanceof LootBagItem)) {
      throw BAD_ITEM.create();
    }
    ItemStack stack = ((LootBagItem) item).create(e.getRandom(),
        level == -1 ? LivingEntityStats.getFor(e).getLevel() : level);
    source.sendFeedback(new TranslatableText("command.csm.give.success",
        stack.getName(), e.getDisplayName()), true);
    e.inventory.insertStack(stack);
    return Command.SINGLE_SUCCESS;
  }

}
