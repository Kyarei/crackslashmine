package flirora.crackslashmine.command;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import flirora.crackslashmine.generation.Area;
import flirora.crackslashmine.generation.AreaManager;
import net.minecraft.command.argument.Vec3ArgumentType;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;
import net.minecraft.util.Pair;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.registry.MutableRegistry;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.biome.Biome;

import static net.minecraft.command.argument.Vec3ArgumentType.vec3;
import static net.minecraft.server.command.CommandManager.argument;
import static net.minecraft.server.command.CommandManager.literal;

public class AreaCommand {
    public static void register(CommandDispatcher<ServerCommandSource> dispatcher) {
        dispatcher.register(literal("csm")
                .then(literal("area")
                        .requires(source -> source.hasPermissionLevel(2))
                        .then(argument("location", vec3())
                                .executes(ctx -> areaInfo(ctx.getSource(), Vec3ArgumentType.getVec3(ctx, "location"))))
                        .executes(ctx -> areaInfo(ctx.getSource(), ctx.getSource().getPosition()))));
    }

    private static int areaInfo(ServerCommandSource source, Vec3d location) {
        ServerWorld world = source.getWorld();
        AreaManager manager = AreaManager.getFor(world);
        Area area = manager.getOrCreateSync(location);
        MutableRegistry<Biome> biomes = world.getRegistryManager().get(Registry.BIOME_KEY);
        Pair<Text, Text> texts = area.getTitleAndSubtitle(
                biomes);
        source.sendFeedback(texts.getLeft(), false);
        source.sendFeedback(texts.getRight(), false);
        source.sendFeedback(new TranslatableText("command.csm.area.uuid", area.getUuid()), false);
        source.sendFeedback(new TranslatableText("command.csm.area.numChunks", area.size()), false);
        Identifier biomeId = biomes.getId(area.getBiome(biomes));
        if (biomeId == null) {
            source.sendFeedback(new TranslatableText(
                            "command.csm.area.biome",
                            new TranslatableText("command.csm.area.biome.unknown")),
                    false);
        } else {
            source.sendFeedback(new TranslatableText(
                            "command.csm.area.biome",
                            new TranslatableText("biome." + biomeId.getNamespace() + "." + biomeId.getPath())),
                    false);
        }
        source.sendFeedback(new TranslatableText("command.csm.area.nameVersion", area.getNameVersion()), false);
        source.sendFeedback(new TranslatableText("command.csm.area.nameSeed", area.getNameSeed()), false);
        return Command.SINGLE_SUCCESS;
    }
}
