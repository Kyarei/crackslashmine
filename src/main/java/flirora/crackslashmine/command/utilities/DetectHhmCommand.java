package flirora.crackslashmine.command.utilities;

import com.google.common.collect.ImmutableSet;
import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import net.fabricmc.loader.api.ModContainer;
import net.fabricmc.loader.api.metadata.ModMetadata;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.TranslatableText;

import java.util.Set;

import static net.minecraft.server.command.CommandManager.literal;

public class DetectHhmCommand {
    private static final Set<String> NOT_HHM;

    static {
        ImmutableSet.Builder<String> builder = ImmutableSet.builder();
        // These are all the licenses from https://github.com/spdx/license-list-data/blob/master/json/licenses.json
        // with osiApproved = true
        builder.add("0BSD");
        builder.add("AAL");
        builder.add("AFL-1.1");
        builder.add("AFL-1.2");
        builder.add("AFL-2.0");
        builder.add("AFL-2.1");
        builder.add("AFL-3.0");
        builder.add("AGPL-3.0");
        builder.add("AGPL-3.0-only");
        builder.add("AGPL-3.0-or-later");
        builder.add("APL-1.0");
        builder.add("APSL-1.0");
        builder.add("APSL-1.1");
        builder.add("APSL-1.2");
        builder.add("APSL-2.0");
        builder.add("Apache-1.1");
        builder.add("Apache-2.0");
        builder.add("Artistic-1.0");
        builder.add("Artistic-1.0-Perl");
        builder.add("Artistic-1.0-cl8");
        builder.add("Artistic-2.0");
        builder.add("BSD-1-Clause");
        builder.add("BSD-2-Clause");
        builder.add("BSD-2-Clause-Patent");
        builder.add("BSD-3-Clause");
        builder.add("BSD-3-Clause-LBNL");
        builder.add("BSL-1.0");
        builder.add("CAL-1.0");
        builder.add("CAL-1.0-Combined-Work-Exception");
        builder.add("CATOSL-1.1");
        builder.add("CDDL-1.0");
        builder.add("CECILL-2.1");
        builder.add("CNRI-Python");
        builder.add("CPAL-1.0");
        builder.add("CPL-1.0");
        builder.add("CUA-OPL-1.0");
        builder.add("ECL-1.0");
        builder.add("ECL-2.0");
        builder.add("EFL-1.0");
        builder.add("EFL-2.0");
        builder.add("EPL-1.0");
        builder.add("EPL-2.0");
        builder.add("EUDatagrid");
        builder.add("EUPL-1.1");
        builder.add("EUPL-1.2");
        builder.add("Entessa");
        builder.add("Fair");
        builder.add("Frameworx-1.0");
        builder.add("GPL-2.0");
        builder.add("GPL-2.0+");
        builder.add("GPL-2.0-only");
        builder.add("GPL-2.0-or-later");
        builder.add("GPL-3.0");
        builder.add("GPL-3.0+");
        builder.add("GPL-3.0-only");
        builder.add("GPL-3.0-or-later");
        builder.add("GPL-3.0-with-GCC-exception");
        builder.add("HPND");
        builder.add("IPA");
        builder.add("IPL-1.0");
        builder.add("ISC");
        builder.add("Intel");
        builder.add("LGPL-2.0");
        builder.add("LGPL-2.0+");
        builder.add("LGPL-2.0-only");
        builder.add("LGPL-2.0-or-later");
        builder.add("LGPL-2.1");
        builder.add("LGPL-2.1+");
        builder.add("LGPL-2.1-only");
        builder.add("LGPL-2.1-or-later");
        builder.add("LGPL-3.0");
        builder.add("LGPL-3.0+");
        builder.add("LGPL-3.0-only");
        builder.add("LGPL-3.0-or-later");
        builder.add("LPL-1.0");
        builder.add("LPL-1.02");
        builder.add("LPPL-1.3c");
        builder.add("LiLiQ-P-1.1");
        builder.add("LiLiQ-R-1.1");
        builder.add("LiLiQ-Rplus-1.1");
        builder.add("MIT");
        builder.add("MIT-0");
        builder.add("MPL-1.0");
        builder.add("MPL-1.1");
        builder.add("MPL-2.0");
        builder.add("MPL-2.0-no-copyleft-exception");
        builder.add("MS-PL");
        builder.add("MS-RL");
        builder.add("MirOS");
        builder.add("Motosoto");
        builder.add("MulanPSL-2.0");
        builder.add("Multics");
        builder.add("NASA-1.3");
        builder.add("NCSA");
        builder.add("NGPL");
        builder.add("NPOSL-3.0");
        builder.add("NTP");
        builder.add("Naumen");
        builder.add("Nokia");
        builder.add("OCLC-2.0");
        builder.add("OFL-1.1");
        builder.add("OFL-1.1-RFN");
        builder.add("OFL-1.1-no-RFN");
        builder.add("OGTSL");
        builder.add("OSET-PL-2.1");
        builder.add("OSL-1.0");
        builder.add("OSL-2.0");
        builder.add("OSL-2.1");
        builder.add("OSL-3.0");
        builder.add("PHP-3.0");
        builder.add("PHP-3.01");
        builder.add("PostgreSQL");
        builder.add("Python-2.0");
        builder.add("QPL-1.0");
        builder.add("RPL-1.1");
        builder.add("RPL-1.5");
        builder.add("RPSL-1.0");
        builder.add("RSCPL");
        builder.add("SISSL");
        builder.add("SPL-1.0");
        builder.add("SimPL-2.0");
        builder.add("Sleepycat");
        builder.add("UCL-1.0");
        builder.add("UPL-1.0");
        builder.add("Unlicense");
        builder.add("VSL-1.0");
        builder.add("W3C");
        builder.add("Watcom-1.0");
        builder.add("Xnet");
        builder.add("ZPL-2.0");
        builder.add("Zlib");
        // Additions:
        // Trinkets library uses this as the license for some reason; it's not
        // listed explicitly in the file mentioned as a non-OSI-approved
        // license, so I've added it here.
        builder.add("MIT-1.0");
        NOT_HHM = builder.build();
    }

    public static void register(CommandDispatcher<ServerCommandSource> dispatcher) {
        dispatcher.register(literal("detecthhm")
                .executes(ctx -> {
                    boolean hhm = false;
                    outer:
                    for (ModContainer mod : net.fabricmc.loader.api.FabricLoader.getInstance().getAllMods()) {
                        ModMetadata metadata = mod.getMetadata();
                        for (String license : metadata.getLicense()) {
                            // System.err.println(metadata.getId() + " " + license);
                            if (!NOT_HHM.contains(license)) {
                                hhm = true;
                                break outer;
                            }
                        }
                    }
                    if (hhm) {
                        ctx.getSource().sendFeedback(new TranslatableText("command.csm.detecthhm.hhmDetected"), false);
                        return Command.SINGLE_SUCCESS;
                    } else {
                        ctx.getSource().sendFeedback(new TranslatableText("command.csm.detecthhm.hhmNotDetected"), false);
                        return 0;
                    }
                }));
    }
}
