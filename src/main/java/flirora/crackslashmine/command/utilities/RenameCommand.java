package flirora.crackslashmine.command.utilities;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.LiteralText;
import net.minecraft.text.TranslatableText;

import static com.mojang.brigadier.arguments.StringArgumentType.greedyString;
import static net.minecraft.server.command.CommandManager.argument;
import static net.minecraft.server.command.CommandManager.literal;

public class RenameCommand {
    public static void register(CommandDispatcher<ServerCommandSource> dispatcher) {
        dispatcher.register(literal("rename")
                .then(argument("name", greedyString())
                        .requires(source -> source.hasPermissionLevel(2))
                        .executes(ctx -> {
                            ServerCommandSource source = ctx.getSource();
                            PlayerEntity player = source.getPlayer();
                            if (player == null) {
                                throw new SimpleCommandExceptionType(
                                        new TranslatableText("command.rename.noPlayer"))
                                        .create();
                            }
                            ItemStack item = player.getMainHandStack();
                            if (item == null) {
                                throw new SimpleCommandExceptionType(
                                        new TranslatableText("command.rename.noItemInHand"))
                                        .create();
                            }
                            item.setCustomName(new LiteralText(StringArgumentType.getString(ctx, "name")));
                            return Command.SINGLE_SUCCESS;
                        })));
    }
}
