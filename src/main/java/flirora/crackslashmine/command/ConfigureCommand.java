package flirora.crackslashmine.command;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType;
import com.mojang.brigadier.tree.LiteralCommandNode;
import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.item.traits.Configurable;
import net.minecraft.item.ItemStack;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.TranslatableText;

import static net.minecraft.server.command.CommandManager.literal;

public class ConfigureCommand {
    private static final SimpleCommandExceptionType BAD_ITEM = new SimpleCommandExceptionType(
            new TranslatableText("command.csm.configure.badItem"));

    public static void register(CommandDispatcher<ServerCommandSource> dispatcher) {
        LiteralCommandNode<ServerCommandSource> configure = dispatcher.register(literal("configure")
                .executes(ctx -> {
                    ServerCommandSource source = ctx.getSource();
                    ServerPlayerEntity player = source.getPlayer();
                    ItemStack stack = player.getMainHandStack();
                    int slot = player.inventory.selectedSlot;
                    if (!(stack.getItem() instanceof Configurable)) {
                        throw BAD_ITEM.create();
                    }
                    ((Configurable) stack.getItem()).configure(stack, player, source.getWorld(), slot);
                    return Command.SINGLE_SUCCESS;
                }));
        String alias = CrackSlashMineMod.config.configureAlias;
        if (!alias.isEmpty())
            dispatcher.register(literal(alias)
                    .redirect(configure));
    }
}
