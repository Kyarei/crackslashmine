package flirora.crackslashmine.command;

import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.CompletableFuture;

import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.arguments.ArgumentType;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.DynamicCommandExceptionType;
import com.mojang.brigadier.suggestion.Suggestions;
import com.mojang.brigadier.suggestion.SuggestionsBuilder;

import flirora.crackslashmine.core.CsmRegistries;
import flirora.crackslashmine.core.xse.ExtendedStatusEffect;
import net.minecraft.command.CommandSource;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;

public class ExtendedStatusEffectArgumentType
    implements ArgumentType<ExtendedStatusEffect> {
  private static final Collection<String> EXAMPLES =
      Arrays.asList("spooky", "effect");
  public static final DynamicCommandExceptionType INVALID_EFFECT_EXCEPTION =
      new DynamicCommandExceptionType(
          o -> new TranslatableText("effect.effectNotFound", o));

  public static ExtendedStatusEffectArgumentType extendedStatusEffect() {
    return new ExtendedStatusEffectArgumentType();
  }

  public static ExtendedStatusEffect getEffect(
      CommandContext<ServerCommandSource> commandContext, String string)
      throws CommandSyntaxException {
    return commandContext.getArgument(string, ExtendedStatusEffect.class);
  }

  @Override
  public ExtendedStatusEffect parse(StringReader reader)
      throws CommandSyntaxException {
    Identifier id = Identifier.fromCommandInput(reader);
    return CsmRegistries.EXTENDED_STATUS_EFFECT.getOrEmpty(id)
        .orElseThrow(() -> INVALID_EFFECT_EXCEPTION.create(id));
  }

  @Override
  public <S> CompletableFuture<Suggestions> listSuggestions(
      CommandContext<S> context, SuggestionsBuilder builder) {
    return CommandSource.suggestIdentifiers(
        CsmRegistries.EXTENDED_STATUS_EFFECT.getIds(), builder);
  }

  @Override
  public Collection<String> getExamples() {
    return EXAMPLES;
  }
}
