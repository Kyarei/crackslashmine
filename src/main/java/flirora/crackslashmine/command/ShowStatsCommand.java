package flirora.crackslashmine.command;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.DynamicCommandExceptionType;
import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.core.stat.Stat;
import flirora.crackslashmine.core.stat.Stats;
import net.minecraft.command.argument.EntityArgumentType;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;

import static net.minecraft.server.command.CommandManager.argument;
import static net.minecraft.server.command.CommandManager.literal;

public class ShowStatsCommand {
    public static final DynamicCommandExceptionType NOT_LIVING_ENTITY =
            new DynamicCommandExceptionType(entityName ->
                    new TranslatableText(
                            "command.csm.showstats.notLivingEntity",
                            entityName));

    private static int showStats(ServerCommandSource source, Entity e, boolean cooked)
            throws CommandSyntaxException {
        if (e == null) e = source.getEntity();
        if (e == null) {
            throw NOT_LIVING_ENTITY.create("null");
        }
        if (!(e instanceof LivingEntity)) {
            throw NOT_LIVING_ENTITY.create(e.getName());
        }
        LivingEntityStats stats = LivingEntityStats.getFor((LivingEntity) e);
        for (Stat stat : Stats.TOPOLOGICAL_ORDER) {
            long amt = stats.getStat(stat);
            Text message = cooked ?
                    stat.format(amt) :
                    new TranslatableText(
                            "command.csm.showstats.line",
                            stat.getId(), amt);
            source.sendFeedback(message, false);
        }
        return Command.SINGLE_SUCCESS;
    }

    public static void register(CommandDispatcher<ServerCommandSource> dispatcher) {
        dispatcher.register(literal("csm")
                .then(literal("showstats")
                        .requires(source -> source.hasPermissionLevel(2))
                        .then(argument("entity", EntityArgumentType.entity())
                                .then(literal("cooked").executes(ctx -> showStats(
                                        ctx.getSource(),
                                        EntityArgumentType.getEntity(ctx, "entity"), true)))
                                .executes(ctx ->
                                        showStats(
                                                ctx.getSource(),
                                                EntityArgumentType.getEntity(ctx, "entity"), false)))
                        .then(literal("cooked").executes(ctx -> showStats(
                                ctx.getSource(), null, true)))
                        .executes(ctx -> showStats(ctx.getSource(), null, false))));
    }
}
