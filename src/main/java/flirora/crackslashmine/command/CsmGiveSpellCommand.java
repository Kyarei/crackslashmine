package flirora.crackslashmine.command;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.item.spell.Spell;
import flirora.crackslashmine.item.spell.SpellItem;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.TranslatableText;

import static com.mojang.brigadier.arguments.IntegerArgumentType.integer;
import static flirora.crackslashmine.command.SpellArgumentType.spell;
import static net.minecraft.server.command.CommandManager.argument;
import static net.minecraft.server.command.CommandManager.literal;

public class CsmGiveSpellCommand {
    public static void register(CommandDispatcher<ServerCommandSource> dispatcher) {
        dispatcher.register(literal("csm")
                .then(literal("give_spell")
                        .requires(source -> source.hasPermissionLevel(2))
                        .then(argument("spell", spell())
                                .executes((ctx) -> give(
                                        ctx.getSource(),
                                        SpellArgumentType.getSpell(ctx, "spell"),
                                        -1))
                                .then(argument("level", integer(-1))
                                        .executes((ctx) -> give(
                                                ctx.getSource(),
                                                SpellArgumentType.getSpell(ctx, "spell"),
                                                IntegerArgumentType.getInteger(ctx, "level")
                                        ))))));
    }

    private static int give(
            ServerCommandSource source,
            Spell spell,
            int level)
            throws CommandSyntaxException {
        PlayerEntity e = source.getPlayer();
        ItemStack stack = SpellItem.createStack(
                spell,
                level == -1 ? LivingEntityStats.getFor(e).getLevel() : level,
                source.getWorld().getRandom());
        source.sendFeedback(new TranslatableText("command.csm.give.success", stack.getName(), e.getDisplayName()), true);
        e.inventory.insertStack(stack);
        return Command.SINGLE_SUCCESS;
    }
}
