package flirora.crackslashmine.config;

import org.apache.commons.math3.analysis.polynomials.PolynomialFunction;

import me.sargunvohra.mcmods.autoconfig1u.ConfigData;
import me.sargunvohra.mcmods.autoconfig1u.annotation.Config;
import me.sargunvohra.mcmods.autoconfig1u.annotation.ConfigEntry;

@Config(name = "crackslashmine")
public class CsmConfig implements ConfigData {
  @ConfigEntry.Category("general")
  public int maxLevel = 256;

  @ConfigEntry.Category("general")
  @ConfigEntry.Gui.Tooltip(count = 2)
  public long csmToVanillaHealthRatio = 10;

  @ConfigEntry.Category("general")
  @ConfigEntry.Gui.Tooltip
  public double csmMobToEnvironmentalDamageRatio = 1.5;

  @ConfigEntry.Category("general")
  @ConfigEntry.Gui.Tooltip(count = 2)
  public double sprintEnergyCostScaling = 12.0;

  @ConfigEntry.Gui.PrefixText
  @ConfigEntry.Category("general")
  public PolynomialFunction weaponDamageScaling =
      new PolynomialFunction(new double[] { 22, 2.1, 0.001 });

  @ConfigEntry.Category("general")
  public PolynomialFunction weaponStaminaScaling =
      new PolynomialFunction(new double[] { 2.3, 0.24, 0.0011 });

  @ConfigEntry.Category("general")
  public PolynomialFunction environmentalDamageScaling =
      new PolynomialFunction(new double[] { 10, 0.6, 0.06 });

  @ConfigEntry.Category("general")
  public PolynomialFunction armorHealthScaling =
      new PolynomialFunction(new double[] { 1, 0.07, 0.0006 });

  @ConfigEntry.Category("general")
  public PolynomialFunction armorDamageScaling =
      new PolynomialFunction(new double[] { 1, 0.05 });

  @ConfigEntry.Category("general")
  public PolynomialFunction xpCurve =
      new PolynomialFunction(new double[] { 100, 10, 1 });

  @ConfigEntry.Category("general")
  @ConfigEntry.Gui.Tooltip(count = 3)
  public PolynomialFunction goldCurve =
      new PolynomialFunction(new double[] { 0.2, 0.02, 0.0001 });

  @ConfigEntry.Category("general")
  @ConfigEntry.Gui.Tooltip(count = 4)
  public PolynomialFunction zombiesToNextLevel =
      new PolynomialFunction(new double[] { 4, 0.98, 0.02 });

  @ConfigEntry.Category("general")
  @ConfigEntry.Gui.Tooltip(count = 3)
  public double minimumPlayerDamageProportion = 0.5;

  @ConfigEntry.Category("client")
  @ConfigEntry.Gui.Tooltip(count = 3)
  public int potionShift = 160;

  public static class EffectConfig {
    public double minProportionDeducted;
    public double maxProportionDeducted;
    public int scaleNumerator;
    public int scaleDenominator;
    public int minDuration;
    public int maxDuration;

    public EffectConfig() {
      this(0.15, 0.35, 4, 3, 80, 160);
    }

    public EffectConfig(double minProportionDeducted,
        double maxProportionDeducted, int scaleNumerator, int scaleDenominator,
        int minDuration, int maxDuration) {
      this.minProportionDeducted = minProportionDeducted;
      this.maxProportionDeducted = maxProportionDeducted;
      this.scaleNumerator = scaleNumerator;
      this.scaleDenominator = scaleDenominator;
      this.minDuration = minDuration;
      this.maxDuration = maxDuration;
    }
  }

  @ConfigEntry.Category("general")
  @ConfigEntry.Gui.CollapsibleObject
  public EffectConfig dotEffectConfig =
      new EffectConfig(0.15, 0.35, 4, 3, 80, 160);

  @ConfigEntry.Category("general")
  @ConfigEntry.Gui.CollapsibleObject
  public EffectConfig delayEffectConfig =
      new EffectConfig(0.15, 0.35, 7, 5, 80, 160);

  @ConfigEntry.Category("general")
  @ConfigEntry.Gui.Tooltip(count = 2)
  public double miscDropChance = 0.05;

  @ConfigEntry.Category("worldgen")
  @ConfigEntry.Gui.Tooltip(count = 3)
  @ConfigEntry.Gui.RequiresRestart
  public double voronoiCellSize = 400.0;

  @ConfigEntry.Category("worldgen")
  @ConfigEntry.Gui.Tooltip(count = 5)
  @ConfigEntry.Gui.RequiresRestart
  public double voronoiWeightRandomness = 0.5;

  @ConfigEntry.Category("commands")
  public String configureAlias = "k";

  @ConfigEntry.Category("technical")
  @ConfigEntry.Gui.Tooltip(count = 2)
  public boolean enableDbTransaction = true;
  @ConfigEntry.Category("technical")
  @ConfigEntry.Gui.Tooltip(count = 4)
  public boolean enableDbMmap = true;

  @ConfigEntry.Category("technical")
  @ConfigEntry.Gui.Tooltip(count = 3)
  public boolean syncStats = true;
}
