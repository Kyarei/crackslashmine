package flirora.crackslashmine.config;

import com.google.common.collect.ImmutableList;
import me.sargunvohra.mcmods.autoconfig1u.AutoConfig;
import me.sargunvohra.mcmods.autoconfig1u.gui.registry.GuiRegistry;
import me.sargunvohra.mcmods.autoconfig1u.util.Utils;
import me.shedaniel.clothconfig2.api.ConfigEntryBuilder;
import me.shedaniel.clothconfig2.gui.entries.DoubleListListEntry;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.text.TranslatableText;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.math3.analysis.polynomials.PolynomialFunction;

import java.util.Collections;
import java.util.List;

@Environment(EnvType.CLIENT)
public class CsmConfigGuiUtils {
    private static final ConfigEntryBuilder ENTRY_BUILDER = ConfigEntryBuilder.create();

    private static List<Double> polyToList(PolynomialFunction poly) {
        return ImmutableList.copyOf(ArrayUtils.toObject(poly.getCoefficients()));
    }

    public static void initialize() {
        GuiRegistry registry = AutoConfig.getGuiRegistry(CsmConfig.class);
        registry.registerTypeProvider((i13n, field, config, defaults, reg) -> {
            DoubleListListEntry entry = ENTRY_BUILDER.startDoubleList(
                    new TranslatableText(i13n),
                    polyToList(Utils.<PolynomialFunction>getUnsafely(field, config)))
                    .setDefaultValue(() -> polyToList(Utils.<PolynomialFunction>getUnsafely(field, defaults)))
                    .setSaveConsumer(newValue -> Utils.setUnsafely(field, config, new PolynomialFunction(
                            ArrayUtils.toPrimitive(newValue.toArray(new Double[0])))))
                    .build();
            return Collections.singletonList(entry);
        }, PolynomialFunction.class);
    }
}
