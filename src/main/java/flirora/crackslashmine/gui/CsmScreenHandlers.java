package flirora.crackslashmine.gui;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.block.gui.CrucibleBlockGui;
import flirora.crackslashmine.block.gui.InscriptionAltarGui;
import flirora.crackslashmine.block.gui.MixingBowlBlockGui;
import flirora.crackslashmine.item.spell.WandGui;
import net.fabricmc.fabric.api.screenhandler.v1.ScreenHandlerRegistry;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.util.Identifier;

public class CsmScreenHandlers {
  public static final ScreenHandlerType<CrucibleBlockGui> CRUCIBLE =
      ScreenHandlerRegistry.registerSimple(
          new Identifier(CrackSlashMineMod.MOD_ID, "crucible"),
          (syncId, inventory) -> new CrucibleBlockGui(syncId, inventory,
              ScreenHandlerContext.EMPTY));
  public static final ScreenHandlerType<MixingBowlBlockGui> MIXING_BOWL =
      ScreenHandlerRegistry.registerSimple(
          new Identifier(CrackSlashMineMod.MOD_ID, "mixing_bowl"),
          (syncId, inventory) -> new MixingBowlBlockGui(syncId, inventory,
              ScreenHandlerContext.EMPTY));
  public static final ScreenHandlerType<WandGui> WAND = ScreenHandlerRegistry
      .registerSimple(new Identifier(CrackSlashMineMod.MOD_ID, "wand"),
          (syncId, inventory) -> new WandGui(syncId, inventory, null, null,
              inventory.selectedSlot));
  public static final ScreenHandlerType<InscriptionAltarGui> INSCRIPTION_ALTAR =
      ScreenHandlerRegistry.registerExtended(
          new Identifier(CrackSlashMineMod.MOD_ID, "inscription_altar"),
          (syncId, inventory, buf) -> new InscriptionAltarGui(syncId, inventory,
              ScreenHandlerContext.create(inventory.player.getEntityWorld(),
                  buf.readBlockPos())));

  public static void load() {
    // just load the class
  }
}
