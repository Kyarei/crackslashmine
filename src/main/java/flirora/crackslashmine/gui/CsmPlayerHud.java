package flirora.crackslashmine.gui;

import com.google.common.collect.Lists;
import com.mojang.blaze3d.systems.RenderSystem;
import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.CrackSlashMineModClient;
import flirora.crackslashmine.core.Beautify;
import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.core.player.Money;
import flirora.crackslashmine.core.player.PlayerProfile;
import flirora.crackslashmine.core.xse.ExtendedStatusEffect;
import flirora.crackslashmine.core.xse.ExtendedStatusEffectInstance;
import flirora.crackslashmine.item.traits.ItemHudRenderer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.rendering.v1.HudRenderCallback;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.texture.Sprite;
import net.minecraft.client.texture.SpriteAtlasTexture;
import net.minecraft.client.texture.StatusEffectSpriteManager;
import net.minecraft.client.util.Window;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffectType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.MathHelper;
import org.jetbrains.annotations.NotNull;

import java.text.NumberFormat;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Environment(EnvType.CLIENT)
public class CsmPlayerHud extends DrawableHelper implements HudRenderCallback {
    private static final Identifier HUD_TEXTURE =
            new Identifier(CrackSlashMineMod.MOD_ID, "textures/gui/hud.png");

    private final MinecraftClient client;

    public CsmPlayerHud(MinecraftClient client) {
        this.client = client;
    }

    private void drawText(MatrixStack matrixStack, Text text, int x, int y, int color, boolean right) {
        Text smallText = text.copy().styled(s -> s.withFont(CrackSlashMineMod.SMALL_FONT));
        int adjustedX = right ? x - client.textRenderer.getWidth(smallText) : x;
        client.textRenderer.drawWithShadow(matrixStack, smallText, adjustedX, y, color);
    }

    private void drawStatusBars(MatrixStack matrixStack) {
        this.client.getTextureManager().bindTexture(HUD_TEXTURE);
        PlayerEntity player = client.player;
        if (player == null) return;
        LivingEntityStats stats = LivingEntityStats.getFor(player);
        Window window = this.client.getWindow();
        int width = window.getScaledWidth();
        int height = window.getScaledHeight();
        int leftEdge = width / 2 - 91, rightEdge = width / 2 + 91;
        int rightBarX = rightEdge - 80;
        int healthBarY = height - 39;
        int manaBarY = healthBarY - 10;
        // Health
        drawTexture(matrixStack, leftEdge, healthBarY, 0, 0, 80, 9);
        int nHealthPixels = (int) (80 * stats.getHealth() / stats.getMaxHealth());
        drawTexture(matrixStack, leftEdge, healthBarY, 0, 9, nHealthPixels, 9);
        // Mana
        drawTexture(matrixStack, rightBarX, manaBarY, 0, 0, 80, 9);
        int nManaPixels = (int) (80 * stats.getMana() / stats.getMaxMana());
        drawTexture(matrixStack, rightEdge - nManaPixels, manaBarY, 80 - nManaPixels, 18, nManaPixels, 9);
        // Stamina
        drawTexture(matrixStack, rightBarX, healthBarY, 0, 0, 80, 9);
        int nStaminaPixels = (int) (80 * stats.getStamina() / stats.getMaxStamina());
        drawTexture(matrixStack, rightEdge - nStaminaPixels, healthBarY, 80 - nStaminaPixels, 27, nStaminaPixels, 9);
        // Air
        int air = player.getAir();
        int maxAir = player.getMaxAir();
        boolean shouldShowAir = air < maxAir || player.isSubmergedInWater();
        if (shouldShowAir) {
            drawTexture(matrixStack, leftEdge, manaBarY, 0, 0, 80, 9);
            int nAirPixels = 80 * air / maxAir;
            drawTexture(matrixStack, leftEdge, manaBarY, 0, 36, nAirPixels, 9);
        }
        // Text
        drawText(
                matrixStack,
                new TranslatableText(
                        "csm.hud.hp",
                        Beautify.beautify0(stats.getHealth()),
                        Beautify.beautify0(stats.getMaxHealth())),
                leftEdge + 4,
                healthBarY,
                0xFFFFFF,
                false);
        drawText(
                matrixStack,
                new TranslatableText(
                        "csm.hud.mp",
                        Beautify.beautify0(stats.getMana()),
                        Beautify.beautify0(stats.getMaxMana())),
                rightEdge - 3,
                manaBarY,
                0xFFFFFF,
                true);
        drawText(
                matrixStack,
                new TranslatableText(
                        "csm.hud.st",
                        Beautify.beautify0(stats.getStamina()),
                        Beautify.beautify0(stats.getMaxStamina())),
                rightEdge - 3,
                healthBarY,
                0xFFFFFF,
                true);
        if (shouldShowAir) {
            drawText(
                    matrixStack,
                    new TranslatableText("csm.hud.air", Beautify.beautifyTimeS(air)),
                    leftEdge + 4,
                    manaBarY,
                    0xFFFFFF,
                    false);
        }
    }

    private void drawCxpBar(MatrixStack stack) {
        this.client.getTextureManager().bindTexture(HUD_TEXTURE);
        PlayerEntity player = client.player;
        if (player == null) return;
        LivingEntityStats stats = LivingEntityStats.getFor(player);
        int level = stats.getLevel();
        double xpPercent = stats.getXpPercent();
        Window window = this.client.getWindow();
        int width = window.getScaledWidth();
        int height = window.getScaledHeight();
        int leftEdge = width / 2 - 91;
        int barY = height - 29;
        drawTexture(stack, leftEdge, barY, 0, 45, 182, 5);
        int pixels = (int) (xpPercent * 183);
        if (pixels > 0)
            drawTexture(stack, leftEdge, barY, 0, 50, pixels, 5);
        String text = "" + level;
        int textX = (width - client.textRenderer.getWidth(text)) / 2;
        int textY = height - 35;
        client.textRenderer.draw(stack, text, textX - 1, textY, 0);
        client.textRenderer.draw(stack, text, textX + 1, textY, 0);
        client.textRenderer.draw(stack, text, textX, textY - 1, 0);
        client.textRenderer.draw(stack, text, textX, textY + 1, 0);
        client.textRenderer.draw(stack, text, textX, textY, 0xFF4BF5);
    }

    private void drawExp(MatrixStack stack) {
        this.client.getTextureManager().bindTexture(HUD_TEXTURE);
        PlayerEntity player = client.player;
        if (player == null) return;
        Window window = this.client.getWindow();
        int width = window.getScaledWidth();
        int height = window.getScaledHeight();
        int rightEdge = width / 2 + 91;
        drawTexture(stack, rightEdge + 15, height - 16, 80, 0, 9, 9);
        double total = player.experienceLevel + player.experienceProgress;
        NumberFormat nf = NumberFormat.getPercentInstance();
        nf.setMinimumFractionDigits(0);
        nf.setMaximumFractionDigits(0);
        String disp = nf.format(total);
        client.textRenderer.drawWithShadow(stack, disp, rightEdge + 27, height - 15, 0xFFFFFF);
    }

    private static class EffectOverlayEntry implements Comparable<EffectOverlayEntry> {
        public Sprite sprite;
        public int count;
        public int duration;
        public boolean ambient;
        public boolean beneficial;

        public EffectOverlayEntry(Sprite sprite, int count, int duration, boolean ambient, boolean beneficial) {
            this.sprite = sprite;
            this.count = count;
            this.duration = duration;
            this.ambient = ambient;
            this.beneficial = beneficial;
        }

        @Override
        public int compareTo(@NotNull CsmPlayerHud.EffectOverlayEntry that) {
            int res;
            if ((res = Boolean.compare(this.ambient, that.ambient)) != 0)
                return res;
            if ((res = Integer.compare(this.duration, that.duration)) != 0)
                return res;
            if ((res = Integer.compare(this.count, that.count)) != 0)
                return res;
            return 0;
        }
    }

    private float getEffectIconAlpha(int duration) {
        if (duration > 200) return 1.0f;
        int secondsAfterFade = 10 - duration / 20;
        return MathHelper.clamp(duration / 10.0F / 5.0F * 0.5F, 0.0F, 0.5F) +
                MathHelper.cos(duration * (float) Math.PI / 5.0F) *
                        MathHelper.clamp(secondsAfterFade / 10.0F * 0.25F, 0.0F, 0.25F);
    }

    private static Stream<EffectOverlayEntry> createEffectOverlayEntries(
            Collection<StatusEffectInstance> basicEffects,
            StatusEffectSpriteManager spriteManager,
            List<ExtendedStatusEffectInstance> extendedEffects,
            SpriteAtlasTexture statusEffectIconAtlas
    ) {
        Stream<EffectOverlayEntry> basic = basicEffects.stream()
                .filter(StatusEffectInstance::shouldShowIcon)
                .map(effect -> new EffectOverlayEntry(
                        spriteManager.getSprite(effect.getEffectType()),
                        1,
                        effect.getDuration(),
                        effect.isAmbient(),
                        effect.getEffectType().isBeneficial()
                ));
        Stream<EffectOverlayEntry> extended = extendedEffects.stream()
                .collect(Collectors.groupingBy(ExtendedStatusEffectInstance::getEffect))
                .entrySet().stream()
                .map(entry -> {
                    ExtendedStatusEffect effectType = entry.getKey();
                    List<ExtendedStatusEffectInstance> instances = entry.getValue();
                    return new EffectOverlayEntry(
                            statusEffectIconAtlas.getSprite(effectType.getTextureWithoutExtension()),
                            instances.size(),
                            instances.stream().mapToInt(ExtendedStatusEffectInstance::getDuration).max().orElse(0),
                            false,
                            effectType.getType() == StatusEffectType.BENEFICIAL
                    );
                });
        return Stream.concat(basic, extended).sorted();
    }

    // Reproduce vanilla status effect overlay plus our own stuff.
    private void renderStatusEffectOverlay(MatrixStack matrices) {
        if (client.player == null) return;
        LivingEntityStats stats = LivingEntityStats.getFor(client.player);
        RenderSystem.enableBlend();
        Window window = this.client.getWindow();
        int width = window.getScaledWidth();
        int height = window.getScaledHeight();
        Collection<StatusEffectInstance> basicEffects = client.player.getStatusEffects();
        List<ExtendedStatusEffectInstance> extendedEffects = stats.getEffects();
        {
            int nBeneficial = 0;
            int nHarmful = 0;
            StatusEffectSpriteManager basicSpriteManager = client.getStatusEffectSpriteManager();
            List<Runnable> runnables = Lists.newArrayListWithExpectedSize(basicEffects.size());
            client.getTextureManager().bindTexture(HandledScreen.BACKGROUND_TEXTURE);

            List<EffectOverlayEntry> entries =
                    createEffectOverlayEntries(basicEffects, basicSpriteManager, extendedEffects, CrackSlashMineModClient.extendedStatusEffectIconAtlas)
                            .collect(Collectors.toList());
            for (EffectOverlayEntry entry : entries) {
                int iconX = width;
                int iconY = 1;
                if (this.client.isDemo()) {
                    iconY += 15;
                }

                if (entry.beneficial) {
                    ++nBeneficial;
                    iconX -= 25 * nBeneficial;
                } else {
                    ++nHarmful;
                    iconX -= 25 * nHarmful;
                    iconY += 26;
                }

                RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
                final float alpha = entry.ambient ? 1.0F : getEffectIconAlpha(entry.duration);
                if (entry.ambient) {
                    this.drawTexture(matrices, iconX, iconY, 165, 166, 24, 24);
                } else {
                    this.drawTexture(matrices, iconX, iconY, 141, 166, 24, 24);
                }

                Sprite sprite = entry.sprite;
                int finalIconX = iconX;
                int finalIconY = iconY;
                int count = entry.count;

                runnables.add(() -> {
                    this.client.getTextureManager().bindTexture(sprite.getAtlas().getId());
                    RenderSystem.color4f(1.0F, 1.0F, 1.0F, alpha);
                    drawSprite(matrices, finalIconX + 3, finalIconY + 3, this.getZOffset(), 18, 18, sprite);
                    if (count > 1) {
                        client.textRenderer.drawWithShadow(
                                matrices, "" + count,
                                finalIconX + 15, finalIconY + 12,
                                0xFFFFFF);
                    }
                });
            }

            runnables.forEach(Runnable::run);
        }
    }

    private void drawPlayerProfileInfo(MatrixStack matrices) {
        if (client.player == null) return;
        TextRenderer textRenderer = client.textRenderer;
        PlayerProfile profile = PlayerProfile.getFor(client.player);
        Window window = this.client.getWindow();
        int width = window.getScaledWidth();
        int height = window.getScaledHeight();
        int rightEdge = width / 2 + 91;
        textRenderer.drawWithShadow(
                matrices,
                Money.format(profile.getMoney(), Money.Context.HELD),
                rightEdge + 27, height - 27, 0xFFFFFF);
    }

    private void drawItemHud(MatrixStack matrices, float delta) {
        if (client.player == null) return;
        ItemStack stack = client.player.getMainHandStack();
        ItemHudRenderer renderer = ItemHudRenderers.INSTANCE.get(stack.getItem());
        if (renderer != null) {
            renderer.onHudRender(stack, client.player, matrices, delta);
        }
    }

    @Override
    public void onHudRender(MatrixStack matrixStack, float delta) {
        client.getProfiler().push("csmHud");
        RenderSystem.enableDepthTest();
        RenderSystem.disableBlend();
        RenderSystem.defaultBlendFunc();
        RenderSystem.defaultAlphaFunc();
        matrixStack.push();
        matrixStack.translate(0, 0, -50);
        drawStatusBars(matrixStack);
        drawCxpBar(matrixStack);
        drawExp(matrixStack);
        renderStatusEffectOverlay(matrixStack);
        drawPlayerProfileInfo(matrixStack);
        drawItemHud(matrixStack, delta);
        matrixStack.pop();
        client.getProfiler().pop();
    }
}
