package flirora.crackslashmine.gui;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.particle.ParticleTextureSheet;
import net.minecraft.client.render.Camera;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.util.math.Vector3f;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.text.Text;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Matrix4f;
import net.minecraft.util.math.Quaternion;
import net.minecraft.util.math.Vec3d;

/**
 * A particle that displays text.
 */
public class TextParticle extends Particle {
    private final Text text;
    private final float scale;

    /**
     * Constructs a new {@link TextParticle}.
     * <p>
     * Do not put any formatting in {@code text} other than color, or the
     * particle's shadow will not display properly.
     *
     * @param clientWorld the world to spawn this particle in
     * @param text        the text that should be displayed on this particle
     * @param x           the x-coordinate of the particle
     * @param y           the y-coordinate of the particle
     * @param z           the z-coordinate of the particle
     * @param vx          the x-component of the particle's velocity
     * @param vy          the y-component of the particle's velocity
     * @param vz          the z-component of the particle's velocity
     * @param scale       the scaling factor of this particle
     */
    public TextParticle(ClientWorld clientWorld, Text text,
                        double x, double y, double z,
                        double vx, double vy, double vz,
                        float scale) {
        super(clientWorld, x, y, z, vx, vy, vz);
        this.text = text;
        this.scale = scale;
        this.setPos(x, y, z);
        this.setMaxAge(20);
    }

    public float getSize(float tickDelta) {
        return this.scale;
    }

    @Override
    public void buildGeometry(VertexConsumer vertexConsumer, Camera camera, float tickDelta) {
        Vec3d cameraPos = camera.getPos();
        float lerpedX = (float) (MathHelper.lerp(tickDelta, prevPosX, x) - cameraPos.getX());
        float lerpedY = (float) (MathHelper.lerp(tickDelta, prevPosY, y) - cameraPos.getY());
        float lerpedZ = (float) (MathHelper.lerp(tickDelta, prevPosZ, z) - cameraPos.getZ());
        Quaternion cameraRotation;
        if (this.angle == 0.0f) {
            cameraRotation = camera.getRotation();
        } else {
            cameraRotation = new Quaternion(camera.getRotation());
            float lerpedAngle = MathHelper.lerp(tickDelta, this.prevAngle, this.angle);
            cameraRotation.hamiltonProduct(Vector3f.POSITIVE_Z.getRadialQuaternion(lerpedAngle));
        }

        MinecraftClient client = MinecraftClient.getInstance();
        TextRenderer textRenderer = client.textRenderer;
        float textWidth = (float) textRenderer.getWidth(text);
        float textHeight = (float) textRenderer.fontHeight;
        float actualScale = this.getSize(tickDelta);

        Matrix4f transform = Matrix4f.translate(lerpedX, lerpedY, lerpedZ);
        transform.multiply(cameraRotation);
        transform.multiply(Matrix4f.scale(-actualScale, -actualScale, actualScale));
        Matrix4f transformShadow = transform.copy();
        transformShadow.multiply(Matrix4f.translate(0, 0, 0.03f));

        VertexConsumerProvider.Immediate provider = MinecraftClient.getInstance()
                .getBufferBuilders().getEntityVertexConsumers();

        textRenderer.draw(
                text,
                -textWidth / 2, -textHeight / 2,
                -1, false, transform, provider,
                false, 0, 0xF000F0);
        // XXX: this does not work with non-color formatting such as bold
        // or italics! Obfuscated text is also a big problem, as we can't just
        // enable the shadow flag in the text drawing call (otherwise, it
        // would sometimes draw the shadow in front of the text).
        textRenderer.draw(
                text.asTruncatedString(Integer.MAX_VALUE),
                -textWidth / 2 + 1, -textHeight / 2 + 1,
                0x404040, false, transformShadow, provider,
                false, 0, 0xF000F0);

        provider.draw();
    }

    @Override
    public ParticleTextureSheet getType() {
        return ParticleTextureSheet.CUSTOM;
    }
}
