package flirora.crackslashmine.gui;

import io.github.cottonmc.cotton.gui.widget.WWidget;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.client.util.math.Vector4f;

/**
 * A widget for a 2D area that can be scrolled, panned, and zoomed.
 * <p>
 * Positions on the widget are mapped to "abstract" or "logical" coordinates,
 * which can be used to refer to locations on the area without regard to
 * the pan or zoom settings.
 * <p>
 * There are some parameters that describe how abstract and pixel coordinates
 * are related:
 * <ul>
 *     <li>{@code centerX} and {@code centerY} describe the abstract coordinates
 *     of the center point of the widget.</li>
 *     <li>{@code scale} is the number of pixels associated with an
 *     abstract coordinate unit.</li>
 *     <li>{@code sensitivity} is the sensitivity of this widget to mouse
 *     scroll actions.</li>
 * </ul>
 */
public abstract class ScrollableArea2D extends WWidget {
    private double centerX, centerY;
    private double scale; // How many pixels per abstract unit?
    private double dragX, dragY;
    private double originalCenterX, originalCenterY;
    private double sensitivity;

    public ScrollableArea2D(int width, int height, double centerX, double centerY, double scale) {
        setSize(width, height);
        this.centerX = centerX;
        this.centerY = centerY;
        this.scale = scale;
        this.sensitivity = 0.2;
        dragX = 0;
        dragY = 0;
    }

    /**
     * Convert a pixel x-coordinate (relative to the widget) to an abstract one.
     *
     * @param x the pixel x-coordinate
     * @return the abstract x-coordinate
     */
    public double pixelToAbstractX(int x) {
        double fromCenter = x - width / 2.0;
        return centerX + fromCenter / scale;
    }

    /**
     * Convert a pixel y-coordinate (relative to the widget) to an abstract one.
     *
     * @param y the pixel y-coordinate
     * @return the abstract y-coordinate
     */
    public double pixelToAbstractY(int y) {
        double fromCenter = y - width / 2.0;
        return centerY + fromCenter / scale;
    }

    /**
     * Convert an abstract x-coordinate to a pixel one (relative to the widget).
     *
     * @param x the abstract x-coordinate
     * @return the pixel x-coordinate
     */
    public int abstractToPixelX(double x) {
        double fromCenter = (x - centerX) * scale;
        return (int) Math.round(fromCenter + width / 2.0);
    }

    /**
     * Convert an abstract y-coordinate to a pixel one (relative to the widget).
     *
     * @param y the abstract y-coordinate
     * @return the pixel y-coordinate
     */
    public int abstractToPixelY(double y) {
        double fromCenter = (y - centerY) * scale;
        return (int) Math.round(fromCenter + height / 2.0);
    }

    private boolean isPressingShift() {
        return Screen.hasShiftDown();
    }

    private boolean isPressingCtrl() {
        return Screen.hasControlDown();
    }

    @Override
    @Environment(EnvType.CLIENT)
    public WWidget onMouseDown(int x, int y, int button) {
        dragX = pixelToAbstractX(x);
        dragY = pixelToAbstractY(y);
        originalCenterX = centerX;
        originalCenterY = centerY;
        return super.onMouseDown(x, y, button);
    }

    @Override
    @Environment(EnvType.CLIENT)
    public void onMouseDrag(int x, int y, int button) {
        double dx = pixelToAbstractX(x) - dragX;
        double dy = pixelToAbstractY(y) - dragY;
        centerX = originalCenterX - dx;
        centerY = originalCenterY - dy;
        super.onMouseDrag(x, y, button);
    }

    @Override
    @Environment(EnvType.CLIENT)
    public void onMouseScroll(int x, int y, double amount) {
        if (isPressingCtrl()) {
            scale *= Math.exp(sensitivity * amount);
        } else if (isPressingShift()) {
            // Scroll horizontally
            centerX += sensitivity * amount * scale;
            originalCenterX += sensitivity * amount * scale;
        } else {
            // Scroll vertically
            centerY += sensitivity * amount * scale;
            originalCenterY += sensitivity * amount * scale;
        }
        super.onMouseScroll(x, y, amount);
    }

    @Override
    @Environment(EnvType.CLIENT)
    public void paint(MatrixStack matrices, int x, int y, int mouseX, int mouseY) {
        super.paint(matrices, x, y, mouseX, mouseY);
        matrices.push();
        // the center point is (x + width / 2, y + height / 2)
        matrices.translate(x + width / 2.0, y + height / 2.0, 1);
        // logical unit is `scale` scaled pixels
        matrices.scale((float) scale, (float) scale, 1.0f);
        matrices.translate(-centerX, -centerY, 0);
        Vector4f a = new Vector4f(0.0f, 0.0f, 0.0f, 1.0f);
        a.transform(matrices.peek().getModel());
        Vector4f b = new Vector4f(1.0f, 1.0f, 0.0f, 1.0f);
        b.transform(matrices.peek().getModel());
        myPaint(matrices, pixelToAbstractX(mouseX - x), pixelToAbstractY(mouseY - y));
        matrices.pop();
    }

    /**
     * A method called by {@link ScrollableArea2D#paint}.
     *
     * @param matrices      a {@link MatrixStack}. This has been set up so that
     *                      drawing with this matrix stack will perform the
     *                      drawing in abstract coordinates.
     * @param logicalMouseX the abstract x-coordinate of the cursor
     * @param logicalMouseY the abstract y-coordinate of the cursor
     */
    @Environment(EnvType.CLIENT)
    protected abstract void myPaint(
            MatrixStack matrices,
            double logicalMouseX,
            double logicalMouseY);

    public double getCenterX() {
        return centerX;
    }

    public void setCenterX(double centerX) {
        this.centerX = centerX;
    }

    public double getCenterY() {
        return centerY;
    }

    public void setCenterY(double centerY) {
        this.centerY = centerY;
    }

    public double getScale() {
        return scale;
    }

    public void setScale(double scale) {
        this.scale = scale;
    }

    public double getSensitivity() {
        return sensitivity;
    }

    public void setSensitivity(double sensitivity) {
        this.sensitivity = sensitivity;
    }

    public double getLeftEdgeX() {
        return centerX - width / (2 * scale);
    }

    public double getRightEdgeX() {
        return centerX + width / (2 * scale);
    }

    public double getTopEdgeY() {
        return centerY - height / (2 * scale);
    }

    public double getBottomEdgeY() {
        return centerY + height / (2 * scale);
    }
}
