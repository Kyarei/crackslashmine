package flirora.crackslashmine.gui;

import io.github.cottonmc.cotton.gui.widget.WClippedPanel;
import io.github.cottonmc.cotton.gui.widget.WWidget;

/**
 * A wrapper around {@link WClippedPanel} that provides a method to add widgets.
 */
public class ClippedWrapper extends WClippedPanel {
    public void add(WWidget w) {
        children.add(w);
        w.setParent(this);
    }
}
