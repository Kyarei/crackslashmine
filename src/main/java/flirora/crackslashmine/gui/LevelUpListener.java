package flirora.crackslashmine.gui;

import flirora.crackslashmine.core.LivingEntityStats;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.rendering.v1.HudRenderCallback;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.KeybindText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;

@Environment(EnvType.CLIENT)
public class LevelUpListener implements HudRenderCallback {
  private final MinecraftClient client;
  private int previousLevel = -1;
  private static final Text TITLE = new TranslatableText("csm.hud.levelUp");

  public LevelUpListener(MinecraftClient client) {
    this.client = client;
  }

  private void updateWith(int level) {
    if (previousLevel > 0 && level > previousLevel) {
      previousLevel = level;
      client.inGameHud.setTitles(TITLE, null, 10, 70, 20);
      client.inGameHud
          .setTitles(null,
              new TranslatableText("csm.hud.levelUp.subtitle",
                  new KeybindText("key.crackslashmine.openProfile")),
              10, 70, 20);
    }
    previousLevel = level;
  }

  @Override
  public void onHudRender(MatrixStack matrixStack, float v) {
    if (client.player == null)
      return;
    LivingEntityStats stats = LivingEntityStats.getFor(client.player);
    int level = stats.getLevel();
    updateWith(level);
  }
}
