package flirora.crackslashmine.gui;

import org.jetbrains.annotations.NotNull;

import flirora.crackslashmine.generation.Area;
import flirora.crackslashmine.generation.ClientAreaManager;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.rendering.v1.HudRenderCallback;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.util.Window;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.Text;
import net.minecraft.util.Pair;
import net.minecraft.util.registry.Registry;

@Environment(EnvType.CLIENT)
public class AreaChangeListener implements HudRenderCallback {
  private static final int DEFAULT_AREA_TITLE_COOLDOWN = 200;
  private final MinecraftClient client;
  private Area previousArea;
  private int areaTitleCooldown;

  public AreaChangeListener(MinecraftClient client) {
    this.client = client;
  }

  private void updateWith(@NotNull Area newArea) {
    if (client.world == null) {
      previousArea = null;
      return;
    }
    if (previousArea == null || (areaTitleCooldown <= 0
        && !previousArea.getUuid().equals(newArea.getUuid()))) {
      previousArea = newArea;
      areaTitleCooldown = DEFAULT_AREA_TITLE_COOLDOWN;
      Pair<Text, Text> title = newArea.getTitleAndSubtitle(
          client.world.getRegistryManager().get(Registry.BIOME_KEY));
      client.inGameHud.setTitles(title.getLeft(), null, 10, 70, 20);
      client.inGameHud.setTitles(null, title.getRight(), 10, 70, 20);
    }
  }

  private void renderLowerLeftCorner(MatrixStack matrices) {
    if (client.world == null) {
      return;
    }
    TextRenderer textRenderer = client.textRenderer;
    Window window = this.client.getWindow();
    int width = window.getScaledWidth();
    int height = window.getScaledHeight();
    Pair<Text, Text> title = previousArea.getTitleAndSubtitle(
        client.world.getRegistryManager().get(Registry.BIOME_KEY));
    textRenderer.drawWithShadow(matrices, title.getLeft(), 10, height - 27,
        0xFFFFFF);
    textRenderer.drawWithShadow(matrices, title.getRight(), 10, height - 15,
        0xFFFFFF);
  }

  @Override
  public void onHudRender(MatrixStack matrixStack, float v) {
    ClientAreaManager manager =
        ((ClientAreaManager.Provider) client).getClientAreaManager();
    Area area = manager.getCurrentArea();
    if (area != null) {
      updateWith(area);
      renderLowerLeftCorner(matrixStack);
    }
    if (areaTitleCooldown > 0)
      --areaTitleCooldown;
  }
}
