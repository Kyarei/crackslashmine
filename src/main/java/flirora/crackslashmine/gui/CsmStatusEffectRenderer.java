package flirora.crackslashmine.gui;

import com.mojang.blaze3d.systems.RenderSystem;
import flirora.crackslashmine.CrackSlashMineModClient;
import flirora.crackslashmine.core.Beautify;
import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.core.xse.ExtendedStatusEffect;
import flirora.crackslashmine.core.xse.ExtendedStatusEffectInstance;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.texture.Sprite;
import net.minecraft.client.texture.SpriteAtlasTexture;
import net.minecraft.client.texture.StatusEffectSpriteManager;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.text.LiteralText;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Renders status effects in the inventory.
 */
@Environment(EnvType.CLIENT)
public class CsmStatusEffectRenderer {
    private static class InventoryEffectEntry {
        public Sprite sprite;
        public Text headLine;
        public List<Text> lines;

        public InventoryEffectEntry(Sprite sprite, Text headLine, List<Text> lines) {
            this.sprite = sprite;
            this.headLine = headLine;
            this.lines = lines;
        }

        public int getSkip() {
            return Math.max(21,
                    10 + 3 + 10 * lines.size());
        }
    }

    private static Text statusEffectName(StatusEffectInstance effect) {
        MutableText t = new TranslatableText(effect.getEffectType().getTranslationKey());
        if (effect.getAmplifier() > 0) {
            t = t.append(" ").append(new TranslatableText("enchantment.level." + (effect.getAmplifier() + 1)));
        }
        return t;
    }

    private static List<Text> statusEffectLines(StatusEffectInstance effect) {
        return Collections.singletonList(
                new LiteralText(Beautify.beautifyTime(effect.getDuration())));
    }

    private static Stream<InventoryEffectEntry> createInventoryEffectEntries(
            Collection<StatusEffectInstance> basicEffects,
            StatusEffectSpriteManager spriteManager,
            List<ExtendedStatusEffectInstance> extendedEffects,
            SpriteAtlasTexture statusEffectIconAtlas
    ) {
        Stream<InventoryEffectEntry> basic = basicEffects.stream()
                .filter(StatusEffectInstance::shouldShowIcon)
                .map(effect -> new InventoryEffectEntry(
                        spriteManager.getSprite(effect.getEffectType()),
                        statusEffectName(effect),
                        statusEffectLines(effect)
                ));
        Stream<InventoryEffectEntry> extended = extendedEffects.stream()
                .collect(Collectors.groupingBy(ExtendedStatusEffectInstance::getEffect))
                .entrySet().stream()
                .map(entry -> {
                    ExtendedStatusEffect effectType = entry.getKey();
                    List<ExtendedStatusEffectInstance> instances = entry.getValue();
                    return new InventoryEffectEntry(
                            statusEffectIconAtlas.getSprite(effectType.getTextureWithoutExtension()),
                            effectType.getStatusEffectName(),
                            instances.stream()
                                    .map(ExtendedStatusEffectInstance::formatInHud)
                                    .collect(Collectors.toList())
                    );
                });
        return Stream.concat(basic, extended);
    }

    public static void drawStatusEffects(
            MatrixStack matrices,
            MinecraftClient client,
            int invScreenX,
            int invScreenY,
            int invScreenZ) {
        assert client.player != null;
        int x = invScreenX - 124;
        Collection<StatusEffectInstance> collection = client.player.getStatusEffects();
        List<InventoryEffectEntry> effects = createInventoryEffectEntries(
                client.player.getStatusEffects(),
                client.getStatusEffectSpriteManager(),
                LivingEntityStats.getFor(client.player).getEffects(),
                CrackSlashMineModClient.extendedStatusEffectIconAtlas
        ).collect(Collectors.toList());
        if (!effects.isEmpty()) {
            RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
            drawStatusEffectSprites(
                    matrices, client, x, invScreenY, invScreenZ, effects);
            drawStatusEffectDescriptions(
                    matrices, client, x, invScreenY, invScreenZ, effects);
        }
    }

    private static void drawStatusEffectSprites(
            MatrixStack matrices,
            MinecraftClient client,
            int x, int y, int z,
            List<InventoryEffectEntry> effects) {
        for (InventoryEffectEntry entry : effects) {
            client.getTextureManager().bindTexture(entry.sprite.getAtlas().getId());
            DrawableHelper.drawSprite(
                    matrices,
                    x + 6, y + 2, z,
                    18, 18,
                    entry.sprite);
            y += entry.getSkip();
        }
    }

    private static void drawStatusEffectDescriptions(
            MatrixStack matrices,
            MinecraftClient client,
            int x, int y, int z,
            List<InventoryEffectEntry> effects) {
        matrices.push();
        matrices.translate(0, 0, z);
        TextRenderer textRenderer = client.textRenderer;
        for (InventoryEffectEntry entry : effects) {
            int rightEdge = x + 121;
            textRenderer.drawWithShadow(
                    matrices,
                    entry.headLine,
                    rightEdge - textRenderer.getWidth(entry.headLine),
                    y,
                    0xFFFFFF
            );
            int lineY = y + 10;
            for (Text line : entry.lines) {
                textRenderer.drawWithShadow(
                        matrices,
                        line,
                        rightEdge - textRenderer.getWidth(line),
                        lineY,
                        0xCCCCCC // Can't say that in the P2W game!
                );
                lineY += 10;
            }
            y += entry.getSkip();
        }
        matrices.pop();
    }
}
