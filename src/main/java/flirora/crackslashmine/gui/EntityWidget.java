package flirora.crackslashmine.gui;

import io.github.cottonmc.cotton.gui.client.ScreenDrawing;
import io.github.cottonmc.cotton.gui.widget.WWidget;
import net.minecraft.client.gui.screen.ingame.InventoryScreen;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.LivingEntity;

public class EntityWidget extends WWidget {
    private final LivingEntity entity;

    public EntityWidget(LivingEntity entity) {
        this.entity = entity;
        this.setSize(51, 75);
    }

    @Override
    public void paint(MatrixStack matrices, int x, int y, int mouseX,
                      int mouseY) {
        super.paint(matrices, x, y, mouseX, mouseY);
        ScreenDrawing.coloredRect(x, y, width, height, 0xFF000000);
        int cx = x + width / 2;
        int adjustedHeight = height * 9 / 10;
        int cy = y + adjustedHeight;
        int cy2 = y + adjustedHeight / 3;
        int scale = Math.min(width * 30 / 51, height * 30 / 75);
        InventoryScreen.drawEntity(
                cx, cy, scale,
                cx - mouseX + this.getAbsoluteX(),
                cy2 - mouseY + this.getAbsoluteY(),
                entity);
    }
}
