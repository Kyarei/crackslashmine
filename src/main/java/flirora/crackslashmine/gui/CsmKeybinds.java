package flirora.crackslashmine.gui;

import flirora.crackslashmine.gui.profile.PlayerProfileGui;
import flirora.crackslashmine.gui.profile.PlayerProfileScreen;
import flirora.crackslashmine.network.UserKeyC2S;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.minecraft.client.options.KeyBinding;
import net.minecraft.client.util.InputUtil;
import org.lwjgl.glfw.GLFW;

@Environment(EnvType.CLIENT)
public class CsmKeybinds {
    public static final int NUM_USER_KEYS = 4;

    private static KeyBinding openProfile;

    private static KeyBinding[] userKeys = new KeyBinding[NUM_USER_KEYS];

    private static final int[] userKeycodes = {
            GLFW.GLFW_KEY_Z,
            GLFW.GLFW_KEY_X,
            GLFW.GLFW_KEY_C,
            GLFW.GLFW_KEY_V,
    };

    public static void load() {
        openProfile = KeyBindingHelper.registerKeyBinding(
                new KeyBinding("key.crackslashmine.openProfile",
                        InputUtil.Type.KEYSYM,
                        GLFW.GLFW_KEY_H,
                        "category.crackslashmine.general"));

        for (int i = 0; i < NUM_USER_KEYS; ++i) {
            userKeys[i] = KeyBindingHelper.registerKeyBinding(
                    new KeyBinding("key.crackslashmine.user." + i,
                            InputUtil.Type.KEYSYM,
                            userKeycodes[i],
                            "category.crackslashmine.general"));
        }

        ClientTickEvents.END_CLIENT_TICK.register(client -> {
            if (openProfile.wasPressed()) {
                client.openScreen(new PlayerProfileScreen(new PlayerProfileGui(client.player)));
            }
            for (int i = 0; i < NUM_USER_KEYS; ++i) {
                if (userKeys[i].wasPressed()) {
                    UserKeyC2S.send(i);
                }
            }
        });
    }
}
