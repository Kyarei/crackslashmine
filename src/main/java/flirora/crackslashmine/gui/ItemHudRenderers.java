package flirora.crackslashmine.gui;

import flirora.crackslashmine.item.traits.ItemHudRenderer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.item.Item;
import net.minecraft.util.registry.Registry;

import java.util.HashMap;
import java.util.Map;

@Environment(EnvType.CLIENT)
public class ItemHudRenderers {
    public static final ItemHudRenderers INSTANCE = new ItemHudRenderers();

    private final Map<Item, ItemHudRenderer> renderers;

    private ItemHudRenderers() {
        renderers = new HashMap<>();
    }

    public ItemHudRenderer get(Item item) {
        return renderers.get(item);
    }

    public void register(Item item, ItemHudRenderer renderer) {
        if (renderers.containsKey(item)) {
            throw new UnsupportedOperationException(
                    "Tried to register item " + Registry.ITEM.getId(item) + " twice");
        }
        renderers.put(item, renderer);
    }
}
