package flirora.crackslashmine.gui.profile;

import flirora.crackslashmine.core.stat.StatDisplayType;
import flirora.crackslashmine.core.stat.Stats;
import net.minecraft.entity.player.PlayerEntity;

public class SundryProfilePanel extends BaseProfilePanel {
  public SundryProfilePanel(PlayerEntity player) {
    super(player);

    createStatWidget(Stats.AIR_TIME, StatDisplayType.TIME, 4, 4, 11, 1);
    createStatWidget(Stats.LOOT_FIND, StatDisplayType.MULTI, 4, 26, 12, 1);
    createStatWidget(Stats.GOLD_FIND, StatDisplayType.MULTI, 4, 44, 13, 1);
    createStatWidget(Stats.BONUS_FORTUNE_CHANCE, StatDisplayType.MULTI, 4, 62,
        15, 2);
  }

}
