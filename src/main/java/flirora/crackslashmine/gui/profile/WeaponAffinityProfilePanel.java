package flirora.crackslashmine.gui.profile;

import flirora.crackslashmine.core.stat.StatDisplayType;
import flirora.crackslashmine.core.stat.Stats;
import net.minecraft.entity.LivingEntity;

public class WeaponAffinityProfilePanel extends BaseProfilePanel {
  public WeaponAffinityProfilePanel(LivingEntity player) {
    super(player);

    createStatWidget(Stats.MELEE_WEAPON_BONUS, StatDisplayType.MULTI, 4, 4, 10,
        0);
    createStatWidget(Stats.RANGED_WEAPON_BONUS, StatDisplayType.MULTI, 129, 4,
        10, 2);

    createStatWidget(Stats.SWORD_BONUS, StatDisplayType.MULTI, 12, 26, 11, 0);
    createStatWidget(Stats.AXE_BONUS, StatDisplayType.MULTI, 12, 44, 0, 1);

    createStatWidget(Stats.BOW_BONUS, StatDisplayType.MULTI, 137, 26, 0, 2);
    createStatWidget(Stats.CROSSBOW_BONUS, StatDisplayType.MULTI, 137, 44, 1,
        2);
  }
}
