package flirora.crackslashmine.gui.profile;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.Beautify;
import flirora.crackslashmine.core.Fundamental;
import flirora.crackslashmine.core.stat.Stat;
import flirora.crackslashmine.core.stat.StatDisplayType;
import flirora.crackslashmine.core.stat.Stats;
import flirora.crackslashmine.gui.EntityWidget;
import io.github.cottonmc.cotton.gui.widget.TooltipBuilder;
import io.github.cottonmc.cotton.gui.widget.WButton;
import io.github.cottonmc.cotton.gui.widget.WSprite;
import io.github.cottonmc.cotton.gui.widget.WText;
import io.github.cottonmc.cotton.gui.widget.data.HorizontalAlignment;
import io.netty.buffer.Unpooled;
import net.fabricmc.fabric.api.network.ClientSidePacketRegistry;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.text.LiteralText;
import net.minecraft.text.MutableText;
import net.minecraft.text.Style;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;

public class MainProfilePanel extends BaseProfilePanel {
  public MainProfilePanel(PlayerEntity player) {
    super(player);

    MutableText levelText =
        new TranslatableText("csm.hud.level.short", stats.getLevel())
            .formatted(Formatting.DARK_PURPLE)
            .styled(s -> s.withFont(CrackSlashMineMod.SMALL_FONT));
    Text playerName = levelText
        .append(player.getDisplayName().copy().formatted(Formatting.RESET)
            .styled(s -> s.withFont(Style.DEFAULT_FONT_ID)));
    WText playerNameWidget = new WText(playerName);
    this.add(playerNameWidget, 0, 0);
    playerNameWidget.setSize(150, 10);

    EntityWidget playerWidget = new EntityWidget(player);
    this.add(playerWidget, 0, 16);

    createStatPointWidget();

    createBaseStatWidget(Stats.HEALTH_BONUS, Stats.HEALTH_REGEN, 71, 16, 0, 0);
    createBaseStatWidget(Stats.MANA_BONUS, Stats.MANA_REGEN, 71, 34, 1, 0);
    createBaseStatWidget(Stats.STAMINA_BONUS, Stats.STAMINA_REGEN, 71, 52, 2,
        0);
    createFundamentalStatWidget(Fundamental.STRENGTH, Stats.STRENGTH, 71, 70, 3,
        0);
    createFundamentalStatWidget(Fundamental.DEXTERITY, Stats.DEXTERITY, 71, 88,
        4, 0);
    createFundamentalStatWidget(Fundamental.INTELLECT, Stats.INTELLECT, 71, 106,
        5, 0);
    createFundamentalStatWidget(Fundamental.WISDOM, Stats.WISDOM, 171, 70, 6,
        0);
    createFundamentalStatWidget(Fundamental.ENDURANCE, Stats.ENDURANCE, 171, 88,
        7, 0);
    createFundamentalStatWidget(Fundamental.FORTITUDE, Stats.FORTITUDE, 171,
        106, 8, 0);
  }

  private void createBaseStatWidget(Stat stat, Stat regenStat, int x, int y,
      int u, int v) {
    WSprite sprite = PlayerProfileGui.getAtlasSprite(stat, u, v);
    WText text = new WText(new LiteralText("???")) {
      @Override
      public void paint(MatrixStack matrices, int x, int y, int mouseX,
          int mouseY) {
        this.setText(new TranslatableText("gui.csm.baseWithRegen",
            Beautify.beautify(stats.getStat(stat)),
            StatDisplayType.HUNDREDTHS.format(stats.getStat(regenStat))));
        super.paint(matrices, x, y, mouseX, mouseY);
      }

      @Override
      public void addTooltip(TooltipBuilder builder) {
        PlayerProfileGui.addTooltipAboutStat(builder, stat);
      }
    };
    this.add(sprite, x, y);
    sprite.setSize(16, 16);
    this.add(text, x + 18, y + 4);
    text.setSize(150, 10);
  }

  private void createFundamentalStatWidget(Fundamental fundamental, Stat stat,
      int x, int y, int u, int v) {
    WSprite sprite = PlayerProfileGui.getAtlasSprite(stat, u, v);
    WText text = new WText(new LiteralText("???")) {
      @Override
      public void paint(MatrixStack matrices, int x, int y, int mouseX,
          int mouseY) {
        long base = stats.getIntrinsicFundamental(fundamental);
        long actual = stats.getStat(stat);
        this.setText(new LiteralText(Beautify.beautify(base))
            .append(new TranslatableText("gui.csm.fundamentalBoost",
                Beautify.beautify(actual - base))
                    .formatted(Formatting.DARK_GREEN)));
        super.paint(matrices, x, y, mouseX, mouseY);
      }

      @Override
      public void addTooltip(TooltipBuilder builder) {
        PlayerProfileGui.addTooltipAboutStat(builder, stat);
      }
    };
    WButton increment = new WButton(Text.of("+")) {
      @Override
      public void onClick(int x, int y, int button) {
        super.onClick(x, y, button);
        PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
        buf.writeByte((byte) fundamental.ordinal());
        ClientSidePacketRegistry.INSTANCE
            .sendToServer(PlayerProfileGui.INCREMENT_STAT_C2S, buf);
      }
    };
    this.add(sprite, x, y);
    sprite.setSize(16, 16);
    this.add(text, x + 18, y + 4);
    text.setSize(80, 10);
    this.add(increment, x - 18, y - 1);
    increment.setAlignment(HorizontalAlignment.CENTER);
    increment.setSize(16, 16);
  }

  private void createStatPointWidget() {
    WSprite sprite = new WSprite(PlayerProfileGui.PROFILE_ATLAS, 9 / 16.0f,
        0 / 16.0f, 10 / 16.0f, 1 / 16.0f) {
      @Override
      public void addTooltip(TooltipBuilder builder) {
        builder.add(new TranslatableText("gui.csm.statPoints"));
      }
    };
    WText text = new WText(new LiteralText("???")) {
      @Override
      public void paint(MatrixStack matrices, int x, int y, int mouseX,
          int mouseY) {
        this.setText(
            new LiteralText(Beautify.beautify(stats.statPointsRemaining())));
        super.paint(matrices, x, y, mouseX, mouseY);
      }

      @Override
      public void addTooltip(TooltipBuilder builder) {
        builder.add(new TranslatableText("gui.csm.statPoints"));
      }
    };
    this.add(sprite, 25 - 8, 91 + 5);
    sprite.setSize(16, 16);
    this.add(text, 0, 91 + 23);
    text.setHorizontalAlignment(HorizontalAlignment.CENTER);
    text.setSize(50, 10);
  }

}
