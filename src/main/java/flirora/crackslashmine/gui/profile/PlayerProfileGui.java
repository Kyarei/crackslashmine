package flirora.crackslashmine.gui.profile;

import org.jetbrains.annotations.NotNull;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.stat.Stat;
import flirora.crackslashmine.item.CsmItems;
import io.github.cottonmc.cotton.gui.client.LightweightGuiDescription;
import io.github.cottonmc.cotton.gui.widget.TooltipBuilder;
import io.github.cottonmc.cotton.gui.widget.WSprite;
import io.github.cottonmc.cotton.gui.widget.WTabPanel;
import io.github.cottonmc.cotton.gui.widget.icon.ItemIcon;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;

public class PlayerProfileGui extends LightweightGuiDescription {
  public static final Identifier INCREMENT_STAT_C2S =
      new Identifier(CrackSlashMineMod.MOD_ID, "increment_stat");

  public static final Identifier PROFILE_ATLAS =
      new Identifier(CrackSlashMineMod.MOD_ID, "textures/gui/profile.png");

  public PlayerProfileGui(PlayerEntity player) {

    WTabPanel root = new WTabPanel();
    setRootPanel(root);

    root.add(new MainProfilePanel(player),
        tab -> tab.icon(new ItemIcon(new ItemStack(Items.DIAMOND_SWORD)))
            .tooltip(new TranslatableText("tooltip.csm.profile.tab.main")));
    root.add(new ElementalAffinityProfilePanel(player),
        tab -> tab.icon(new ItemIcon(new ItemStack(CsmItems.CITRINE_WAND)))
            .tooltip(new TranslatableText("tooltip.csm.profile.tab.affinity")));
    root.add(new WeaponAffinityProfilePanel(player),
        tab -> tab.icon(new ItemIcon(new ItemStack(Items.CROSSBOW))).tooltip(
            new TranslatableText("tooltip.csm.profile.tab.weaponAffinity")));
    root.add(new MiscCombatProfilePanel(player),
        tab -> tab.icon(new ItemIcon(new ItemStack(Items.IRON_HELMET))).tooltip(
            new TranslatableText("tooltip.csm.profile.tab.miscCombat")));
    root.add(new SundryProfilePanel(player),
        tab -> tab.icon(new ItemIcon(new ItemStack(Items.GOLD_INGOT)))
            .tooltip(new TranslatableText("tooltip.csm.profile.tab.sundry")));

    root.validate(this);
  }

  @Override
  public void addPainters() {
  }

  @NotNull
  public static WSprite getAtlasSprite(Stat stat, int u, int v) {
    return new WSprite(PROFILE_ATLAS, u / 16.0f, v / 16.0f, (u + 1) / 16.0f,
        (v + 1) / 16.0f) {
      @Override
      public void addTooltip(TooltipBuilder builder) {
        addTooltipAboutStat(builder, stat);
      }
    };
  }

  public static void addTooltipAboutStat(TooltipBuilder builder, Stat stat) {
    builder.add(stat.getName());
    builder.add(stat.getDescription());
  }

  @NotNull
  public static WSprite getAtlasSprite(String keyPrefix, int u, int v) {
    return new WSprite(PROFILE_ATLAS, u / 16.0f, v / 16.0f, (u + 1) / 16.0f,
        (v + 1) / 16.0f) {
      @Override
      public void addTooltip(TooltipBuilder builder) {
        builder.add(new TranslatableText(keyPrefix));
        builder.add(new TranslatableText(keyPrefix + ".description")
            .formatted(Formatting.AQUA, Formatting.ITALIC));
      }
    };
  }

  @NotNull
  public static WSprite getAtlasSpriteWithoutDescription(String keyPrefix,
      int u, int v) {
    return new WSprite(PROFILE_ATLAS, u / 16.0f, v / 16.0f, (u + 1) / 16.0f,
        (v + 1) / 16.0f) {
      @Override
      public void addTooltip(TooltipBuilder builder) {
        builder.add(new TranslatableText(keyPrefix));
      }
    };
  }
}
