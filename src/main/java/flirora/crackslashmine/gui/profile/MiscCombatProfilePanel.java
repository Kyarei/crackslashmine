package flirora.crackslashmine.gui.profile;

import flirora.crackslashmine.core.stat.StatDisplayType;
import flirora.crackslashmine.core.stat.Stats;
import io.github.cottonmc.cotton.gui.widget.WSprite;
import net.minecraft.entity.player.PlayerEntity;

public class MiscCombatProfilePanel extends BaseProfilePanel {
  public MiscCombatProfilePanel(PlayerEntity player) {
    super(player);

    WSprite criticalSprite = PlayerProfileGui
        .getAtlasSprite("tooltip.csm.profile.statClass.critical", 15, 0);
    this.add(criticalSprite, 4, 4);
    criticalSprite.setSize(16, 16);

    createStatWidget(Stats.CRITICAL_RATE, StatDisplayType.MULTI, 22, 4, 14, 0);
    createStatWidget(Stats.CRITICAL_DAMAGE, StatDisplayType.MULTI, 22, 22, 11,
        0);

    WSprite kritikhälhSprite = PlayerProfileGui
        .getAtlasSprite("tooltip.csm.profile.statClass.kritikhaelh", 15, 1);
    this.add(kritikhälhSprite, 4, 40);
    kritikhälhSprite.setSize(16, 16);

    createStatWidget(Stats.KRITIKHÄLH_RATE, StatDisplayType.MULTI, 22, 40, 14,
        0);
    createStatWidget(Stats.KRITIKHÄLH_DAMAGE, StatDisplayType.MULTI, 22, 58, 11,
        0);

    createStatWidget(Stats.DODGE_RATING, StatDisplayType.FLAT, 4, 80, 14, 1);
    createStatWidget(Stats.ACCURACY, StatDisplayType.FLAT, 4, 98, 14, 2);

    createStatWidget(Stats.OUTGOING_HEAL_MULTI, StatDisplayType.FLAT, 4, 120, 9,
        1);
    createStatWidget(Stats.INCOMING_HEAL_MULTI, StatDisplayType.FLAT, 4, 138,
        10, 1);
  }

}
