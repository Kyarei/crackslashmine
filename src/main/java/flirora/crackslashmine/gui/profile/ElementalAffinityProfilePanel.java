package flirora.crackslashmine.gui.profile;

import flirora.crackslashmine.core.DamageType;
import flirora.crackslashmine.core.stat.Stat;
import flirora.crackslashmine.core.stat.StatDisplayType;
import flirora.crackslashmine.core.stat.Stats;
import io.github.cottonmc.cotton.gui.widget.WSprite;
import io.github.cottonmc.cotton.gui.widget.WText;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.text.LiteralText;

// TODO: we will eventually need multiple panels of this kind
public class ElementalAffinityProfilePanel extends BaseProfilePanel {
  public ElementalAffinityProfilePanel(PlayerEntity player) {
    super(player);

    addHeaders();
    addTexts();
  }

  private void addHeaders() {
    WSprite damageBonusSprite = PlayerProfileGui
        .getAtlasSprite("tooltip.csm.profile.statClass.damageBonus", 10, 0);
    WSprite damageMultiSprite = PlayerProfileGui
        .getAtlasSprite("tooltip.csm.profile.statClass.damageMulti", 11, 0);
    WSprite resistanceRatingSprite = PlayerProfileGui.getAtlasSprite(
        "tooltip.csm.profile.statClass.resistanceRating", 12, 0);
    WSprite manaCostSprite = PlayerProfileGui
        .getAtlasSprite("tooltip.csm.profile.statClass.manaCostMulti", 13, 0);
    this.add(damageBonusSprite, 24, 2);
    this.add(damageMultiSprite, 74, 2);
    this.add(resistanceRatingSprite, 124, 2);
    this.add(manaCostSprite, 174, 2);
    damageBonusSprite.setSize(16, 16);
    damageMultiSprite.setSize(16, 16);
    resistanceRatingSprite.setSize(16, 16);
    manaCostSprite.setSize(16, 16);

    for (DamageType dt : DamageType.values()) {
      int ordinal = dt.ordinal();
      WSprite sprite = PlayerProfileGui.getAtlasSpriteWithoutDescription(
          "tooltip.csm.profile.damageType." + dt.getName(), ordinal, 1);
      this.add(sprite, 2, 18 + 16 * ordinal);
      sprite.setSize(16, 16);
    }
  }

  private void addTexts() {
    for (DamageType dt : DamageType.values()) {
      int ordinal = dt.ordinal();
      int y = 18 + 16 * ordinal + 5;

      WText damageBonusText =
          createStatText(Stats.DAMAGE_BONUS.get(dt), StatDisplayType.FLAT);
      this.add(damageBonusText, 24, y);
      damageBonusText.setSize(50, 10);

      WText damageMultiText =
          createStatText(Stats.DAMAGE_MULTI.get(dt), StatDisplayType.MULTI);
      this.add(damageMultiText, 74, y);
      damageMultiText.setSize(50, 10);

      WText resistanceRatingText =
          createStatText(Stats.RESISTANCE_RATING.get(dt), StatDisplayType.FLAT);
      this.add(resistanceRatingText, 124, y);
      resistanceRatingText.setSize(50, 10);

      WText manaCostMultiText =
          createStatText(Stats.MANA_COST_MULTI.get(dt), StatDisplayType.MULTI);
      this.add(manaCostMultiText, 174, y);
      manaCostMultiText.setSize(50, 10);
    }
  }

  private WText createStatText(Stat stat, StatDisplayType display) {
    return new WText(new LiteralText("???")) {
      {
        this.setSize(40, 10);
      }

      @Override
      public void paint(MatrixStack matrices, int x, int y, int mouseX,
          int mouseY) {
        long amt = stats.getStat(stat);
        this.setText(new LiteralText(display.format(amt)));
        super.paint(matrices, x, y, mouseX, mouseY);
      }
    };
  }
}
