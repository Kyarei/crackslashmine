package flirora.crackslashmine.gui.profile;

import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.core.stat.Stat;
import flirora.crackslashmine.core.stat.StatDisplayType;
import io.github.cottonmc.cotton.gui.widget.TooltipBuilder;
import io.github.cottonmc.cotton.gui.widget.WPlainPanel;
import io.github.cottonmc.cotton.gui.widget.WSprite;
import io.github.cottonmc.cotton.gui.widget.WText;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.LivingEntity;
import net.minecraft.text.LiteralText;

public class BaseProfilePanel extends WPlainPanel {
  protected final LivingEntityStats stats;

  public BaseProfilePanel(LivingEntity player) {
    this.stats = LivingEntityStats.getFor(player);

    this.setSize(250, 165);
  }

  protected void createStatWidget(Stat stat, StatDisplayType display, int x,
      int y, int u, int v) {
    WSprite sprite = PlayerProfileGui.getAtlasSprite(stat, u, v);
    WText text = new WText(new LiteralText("???")) {
      @Override
      public void paint(MatrixStack matrices, int x, int y, int mouseX,
          int mouseY) {
        this.setText(new LiteralText(display.format(stats.getStat(stat))));
        super.paint(matrices, x, y, mouseX, mouseY);
      }

      @Override
      public void addTooltip(TooltipBuilder builder) {
        PlayerProfileGui.addTooltipAboutStat(builder, stat);
      }
    };
    this.add(sprite, x, y);
    sprite.setSize(16, 16);
    this.add(text, x + 18, y + 4);
    text.setSize(150, 10);
  }
}
