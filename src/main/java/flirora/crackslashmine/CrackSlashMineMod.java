package flirora.crackslashmine;

import java.util.Objects;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dev.emi.trinkets.api.SlotGroups;
import dev.emi.trinkets.api.Slots;
import dev.emi.trinkets.api.TrinketSlots;
import flirora.crackslashmine.advancement.CsmCriteria;
import flirora.crackslashmine.block.CsmBlocks;
import flirora.crackslashmine.block.InscriptionAltarBlockEntity;
import flirora.crackslashmine.block.gui.InscriptionAltarGui;
import flirora.crackslashmine.command.AreaCommand;
import flirora.crackslashmine.command.AreaVisCommand;
import flirora.crackslashmine.command.ConfigureCommand;
import flirora.crackslashmine.command.CsmEffectCommand;
import flirora.crackslashmine.command.CsmGiveCommand;
import flirora.crackslashmine.command.CsmGiveLootBagCommand;
import flirora.crackslashmine.command.CsmGiveSpellCommand;
import flirora.crackslashmine.command.CsmGiveTonatCommand;
import flirora.crackslashmine.command.ShowStatsCommand;
import flirora.crackslashmine.command.utilities.DetectHhmCommand;
import flirora.crackslashmine.command.utilities.RenameCommand;
import flirora.crackslashmine.config.CsmConfig;
import flirora.crackslashmine.core.EntityDamageUtils;
import flirora.crackslashmine.core.Fundamental;
import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.core.stat.Stats;
import flirora.crackslashmine.core.stat.modifier.ModifierManager;
import flirora.crackslashmine.core.xse.ExtendedStatusEffects;
import flirora.crackslashmine.entity.CsmEntities;
import flirora.crackslashmine.generation.Area;
import flirora.crackslashmine.generation.AreaManagerProvider;
import flirora.crackslashmine.generation.PalettedAreaMap;
import flirora.crackslashmine.generation.PlayerLastChunkStore;
import flirora.crackslashmine.generation.dimension.DimensionConfigManager;
import flirora.crackslashmine.generation.feature.condition.FeatureConditions;
import flirora.crackslashmine.generation.gui.AreaVisGui;
import flirora.crackslashmine.gui.CsmKeybinds;
import flirora.crackslashmine.gui.CsmScreenHandlers;
import flirora.crackslashmine.gui.profile.PlayerProfileGui;
import flirora.crackslashmine.item.CsmItems;
import flirora.crackslashmine.item.equipment.CompatibleItemManager;
import flirora.crackslashmine.item.equipment.EquipmentUtils;
import flirora.crackslashmine.item.equipment.types.EquipmentTypes;
import flirora.crackslashmine.item.essence.tonat.VödManager;
import flirora.crackslashmine.item.food.CompatibleFoodManager;
import flirora.crackslashmine.item.recipe.MixingRecipe;
import flirora.crackslashmine.item.recipe.MixingRecipeSerializer;
import flirora.crackslashmine.item.spell.data.BuiltinSpellSerializers;
import flirora.crackslashmine.item.spell.data.SpellDataManager;
import flirora.crackslashmine.item.traits.UserKeyListener;
import flirora.crackslashmine.loot.CsmLootFunctionTypes;
import flirora.crackslashmine.network.AttackAttemptedC2S;
import flirora.crackslashmine.network.UserKeyC2S;
import io.netty.buffer.Unpooled;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.command.v1.CommandRegistrationCallback;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerTickEvents;
import net.fabricmc.fabric.api.event.player.AttackEntityCallback;
import net.fabricmc.fabric.api.network.ServerSidePacketRegistry;
import net.fabricmc.fabric.api.resource.ResourceManagerHelper;
import net.fabricmc.fabric.api.server.PlayerStream;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.MessageType;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.resource.ResourceType;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;

public class CrackSlashMineMod implements ModInitializer {

  public static Logger LOGGER = LogManager.getLogger();

  public static final String MOD_ID = "crackslashmine";
  public static final String MOD_NAME = "Crack/Mine";

  public static final Identifier SMALL_FONT = new Identifier(MOD_ID, "small");

  public static CsmConfig config;

  @Override
  public void onInitialize() {
    log(Level.INFO, "ḋ");

    // Load appropriate classes
    Stats.load();
    EquipmentTypes.load();
    ExtendedStatusEffects.load(); // Must be loaded after stats, or stat boost
                                  // effects will not be registered!
    CsmScreenHandlers.load();
    CsmCriteria.load();
    CsmEntities.load();
    CsmLootFunctionTypes.load();
    BuiltinSpellSerializers.load();

    // Register trinket types
    TrinketSlots.addSlot(SlotGroups.HAND, Slots.RING, new Identifier("trinkets",
        "textures/item/empty_trinket_slot_ring.png"));
    TrinketSlots.addSlot(SlotGroups.OFFHAND, Slots.RING, new Identifier(
        "trinkets", "textures/item/empty_trinket_slot_ring.png"));
    TrinketSlots.addSlot(SlotGroups.HAND, Slots.GLOVES, new Identifier(
        "trinkets", "textures/item/empty_trinket_slot_gloves.png"));
    TrinketSlots.addSlot(SlotGroups.CHEST, Slots.NECKLACE, new Identifier(
        "trinkets", "textures/item/empty_trinket_slot_necklace.png"));

    // Register managers
    ResourceManagerHelper helper =
        ResourceManagerHelper.get(ResourceType.SERVER_DATA);
    helper.registerReloadListener(ModifierManager.INSTANCE);
    helper.registerReloadListener(CompatibleItemManager.INSTANCE);
    helper.registerReloadListener(CompatibleFoodManager.SERVER);
    helper.registerReloadListener(DimensionConfigManager.INSTANCE);
    helper.registerReloadListener(VödManager.SERVER);
    helper.registerReloadListener(SpellDataManager.SERVER);

    // Register blocks
    CsmBlocks.registerBlocks();
    // Register items
    CsmItems.registerItems();

    Registry.register(Registry.RECIPE_SERIALIZER, MixingRecipeSerializer.ID,
        MixingRecipeSerializer.INSTANCE);
    Registry.register(Registry.RECIPE_TYPE,
        new Identifier(MOD_ID, MixingRecipe.Type.ID),
        MixingRecipe.Type.INSTANCE);

    // Register commands
    CommandRegistrationCallback.EVENT.register((dispatcher, dedicated) -> {
      ShowStatsCommand.register(dispatcher);
      CsmGiveCommand.register(dispatcher);
      RenameCommand.register(dispatcher);
      CsmEffectCommand.register(dispatcher);
      AreaCommand.register(dispatcher);
      AreaVisCommand.register(dispatcher);
      DetectHhmCommand.register(dispatcher);
      CsmGiveSpellCommand.register(dispatcher);
      ConfigureCommand.register(dispatcher);
      CsmGiveTonatCommand.register(dispatcher);
      CsmGiveLootBagCommand.register(dispatcher);
    });

    // Register stamina check when attacking
    AttackEntityCallback.EVENT
        .register(((player, world, hand, entity, hitResult) -> {
          if (player.isCreative() || player.isSpectator())
            return ActionResult.PASS;
          LivingEntityStats stats = LivingEntityStats.getFor(player);
          long staminaCost = EquipmentUtils.getStaminaCostForAttacking(
              player.getMainHandStack(), stats.getLevel(), true);
          if (stats.getStamina() < staminaCost) {
            return ActionResult.FAIL;
          }
          return ActionResult.PASS;
        }));

    // Register packet handlers
    ServerSidePacketRegistry.INSTANCE.register(AttackAttemptedC2S.ID,
        (context, data) -> {
          AttackAttemptedC2S packet = AttackAttemptedC2S.read(data);
          context.getTaskQueue().execute(() -> {
            EntityDamageUtils.attemptAttack(context.getPlayer(),
                packet.isSuccessful());
          });
        });

    ServerSidePacketRegistry.INSTANCE.register(AreaVisGui.REQUEST_AREA_INFO_C2S,
        (context, data) -> {
          if (!context.getPlayer().hasPermissionLevel(4)) {
            context.getTaskQueue()
                .execute(() -> iAmAHacker(context.getPlayer()));
            return;
          }
          int x = data.readInt();
          int z = data.readInt();
          context.getTaskQueue().execute(() -> {
            Area a =
                ((AreaManagerProvider) context.getPlayer().getEntityWorld())
                    .getAreaManager().debugArea(x, z);
            PacketByteBuf areaData = new PacketByteBuf(Unpooled.buffer());
            a.writeToPacket(areaData);
            ServerSidePacketRegistry.INSTANCE.sendToPlayer(context.getPlayer(),
                AreaVisGui.RETURN_AREA_INFO_S2C, areaData);
          });
        });

    ServerSidePacketRegistry.INSTANCE
        .register(PlayerProfileGui.INCREMENT_STAT_C2S, (context, data) -> {
          Fundamental f = Fundamental.values()[data.readByte()];
          context.getTaskQueue().execute(() -> {
            LivingEntityStats.getFor(context.getPlayer())
                .incrementFundamental(f);
          });
        });

    ServerSidePacketRegistry.INSTANCE.register(UserKeyC2S.ID,
        (context, data) -> {
          PlayerEntity player = context.getPlayer();
          int num = data.readByte();
          if (num < 0 || num >= CsmKeybinds.NUM_USER_KEYS) {
            iAmAHacker(player);
            return;
          }
          context.getTaskQueue().execute(() -> {
            if (player instanceof ServerPlayerEntity) {
              ServerPlayerEntity serverPlayer = (ServerPlayerEntity) player;
              ServerWorld world = serverPlayer.getServerWorld();
              ItemStack stack = serverPlayer.getMainHandStack();
              if (stack.getItem() instanceof UserKeyListener) {
                ((UserKeyListener) stack.getItem()).onKey(stack, serverPlayer,
                    world, num);
              }
            }
          });
        });

    ServerSidePacketRegistry.INSTANCE.register(InscriptionAltarGui.USE_C2S,
        (context, data) -> {
          BlockPos pos = data.readBlockPos();
          context.getTaskQueue().execute(() -> {
            PlayerEntity player = context.getPlayer();
            if (player.squaredDistanceTo(pos.getX() + 0.5, pos.getY() + 0.5,
                pos.getZ() + 0.5) > 64) {
              iAmAHacker(player);
              return;
            }
            BlockEntity be = player.getEntityWorld().getBlockEntity(pos);
            if (!(be instanceof InscriptionAltarBlockEntity)) {
              iAmAHacker(player);
              return;
            }
            ((InscriptionAltarBlockEntity) be).inscribe();
          });
        });

    // Register data pack reload events
    ServerLifecycleEvents.END_DATA_PACK_RELOAD
        .register((server, resourceManager, success) -> {
          CompatibleFoodManager.SERVER.syncToClients(PlayerStream.all(server));
          VödManager.SERVER.syncToClients(PlayerStream.all(server));
          SpellDataManager.SERVER.syncToClients(PlayerStream.all(server));
        });

    // Register area syncing handlers
    ServerTickEvents.END_SERVER_TICK.register(server -> {
      if (server.getTicks() % 40 == 0) {
        PalettedAreaMap.sendAreasToAllPlayers(server,
            ((PlayerLastChunkStore.Provider) server.getPlayerManager())
                .getPlayerLastChunkStore());
      }
    });

    // Register worldgen stuff
    FeatureConditions.register();
  }

  public void iAmAHacker(PlayerEntity player) {
    Text text = new TranslatableText("chat.type.text", player.getDisplayName(),
        "I am a hacker!");
    Objects.requireNonNull(player.getServer()).getPlayerManager()
        .broadcastChatMessage(text, MessageType.CHAT, player.getUuid());
  }

  public static void log(Level level, String message) {
    LOGGER.log(level, "[" + MOD_NAME + "] " + message);
  }

  public static void logException(String message, Throwable e) {
    LOGGER.error(message, e);
  }

}
