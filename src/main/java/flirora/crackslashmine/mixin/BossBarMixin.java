package flirora.crackslashmine.mixin;

import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.entity.boss.CsmBossBar;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.boss.BossBar;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(BossBar.class)
public abstract class BossBarMixin implements CsmBossBar {
    protected long csmHealth, csmMaxHealth;
    @Shadow
    protected float percent;

    @Shadow
    public abstract void setPercent(float f);

    @Override
    public long getCsmHealth() {
        return csmHealth;
    }

    @Override
    public long getCsmMaxHealth() {
        return csmMaxHealth;
    }

    @Override
    public void setCsmHealth(long val) {
        this.csmHealth = val;
    }

    @Override
    public void setCsmMaxHealth(long val) {
        this.csmMaxHealth = val;
    }

    @Override
    public void updateHealthFromEntity(LivingEntity e) {
        LivingEntityStats stats = LivingEntityStats.getFor(e);
        csmHealth = stats.getHealth();
        csmMaxHealth = stats.getMaxHealth();
        setPercent((float) ((double) csmHealth) / csmMaxHealth);
    }
}
