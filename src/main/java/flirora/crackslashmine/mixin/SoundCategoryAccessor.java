package flirora.crackslashmine.mixin;

import java.util.Map;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

import net.minecraft.sound.SoundCategory;

@Mixin(SoundCategory.class)
public interface SoundCategoryAccessor {
  @Accessor(value = "BY_NAME")
  static Map<String, SoundCategory> getByName() {
    throw new UnsupportedOperationException("untransformed accessor");
  }
}
