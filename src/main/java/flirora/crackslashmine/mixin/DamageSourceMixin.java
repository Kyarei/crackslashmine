package flirora.crackslashmine.mixin;

import flirora.crackslashmine.core.EntityDamageUtils;
import net.minecraft.entity.damage.DamageSource;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(DamageSource.class)
public abstract class DamageSourceMixin implements EntityDamageUtils.DamageSourceProtectedSetterAccessor {
    @Shadow
    protected abstract DamageSource setFire();

    @Shadow
    protected abstract DamageSource setUnblockable();

    @Shadow
    protected abstract DamageSource setBypassesArmor();

    @Shadow
    protected abstract DamageSource setOutOfWorld();

    @Override
    public void setFlags(boolean fire, boolean unblockable, boolean bypassesArmor, boolean outOfWorld) {
        if (fire) this.setFire();
        if (unblockable) this.setUnblockable();
        if (bypassesArmor) this.setBypassesArmor();
        if (outOfWorld) this.setOutOfWorld();
    }
}
