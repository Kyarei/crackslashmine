package flirora.crackslashmine.mixin;

import flirora.crackslashmine.entity.boss.CsmBossBar;
import net.minecraft.entity.boss.ServerBossBar;
import net.minecraft.entity.boss.dragon.EnderDragonEntity;
import net.minecraft.entity.boss.dragon.EnderDragonFight;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin(EnderDragonFight.class)
public abstract class EnderDragonFightMixin {

    @Redirect(method = "updateFight", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/boss/ServerBossBar;setPercent(F)V"))
    private void bossBarPercentProxy(ServerBossBar bossBar, float f, EnderDragonEntity me) {
        ((CsmBossBar) bossBar).updateHealthFromEntity(me);
    }
}
