package flirora.crackslashmine.mixin;

import flirora.crackslashmine.core.LivingEntityStats;
import net.minecraft.server.network.ServerPlayNetworkHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin(ServerPlayNetworkHandler.class)
public class ServerPlayNetworkHandlerMixin {
    // Fix player not being able to respawn
    @Redirect(method = "onClientStatus", at = @At(value = "INVOKE", target = "Lnet/minecraft/server/network/ServerPlayerEntity;getHealth()F"))
    public float getHealthProxy(ServerPlayerEntity player) {
        return LivingEntityStats.getFor(player).getHealth() > 0 ? 1.0f : -1.0f;
    }
}
