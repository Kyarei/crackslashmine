package flirora.crackslashmine.mixin;

import flirora.crackslashmine.item.equipment.EquipmentComponent;
import flirora.crackslashmine.item.equipment.EquipmentUtils;
import flirora.crackslashmine.item.food.FoodUtils;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import java.util.List;

@Mixin(ItemStack.class)
public abstract class ItemStackClientMixin {
    // TODO: only hide this on C/M-compatible items?
    @Inject(method = "isSectionHidden", at = @At(value = "HEAD"), cancellable = true)
    private static void onSectionHidden(int i, ItemStack.TooltipSection tooltipSection, CallbackInfoReturnable<Boolean> cir) {
        if (tooltipSection == ItemStack.TooltipSection.MODIFIERS) {
            cir.setReturnValue(false);
        }
    }

    @Inject(method = "getTooltip", at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/item/Item;appendTooltip(Lnet/minecraft/item/ItemStack;Lnet/minecraft/world/World;Ljava/util/List;Lnet/minecraft/client/item/TooltipContext;)V",
            shift = At.Shift.AFTER),
            locals = LocalCapture.CAPTURE_FAILHARD)
    private void afterAppendTooltip(@Nullable PlayerEntity playerEntity, TooltipContext tooltipContext, CallbackInfoReturnable<List<Text>> cir, List<Text> list, int i) {
        EquipmentUtils.addCsmTooltip((ItemStack) (Object) this, list, playerEntity);
        FoodUtils.appendFoodInfo((ItemStack) (Object) this, list);
    }

    // Note that we can call client-only methods from here.
    @Inject(method = "getName", at = @At(value = "RETURN", ordinal = 1), cancellable = true)
    private void onGetName(CallbackInfoReturnable<Text> cir) {
        EquipmentComponent component = EquipmentComponent.getFor((ItemStack) (Object) this);
        if (component != null) {
            cir.setReturnValue(component.getName().asText());
        }
    }
}
