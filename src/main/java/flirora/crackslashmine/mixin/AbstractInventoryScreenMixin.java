package flirora.crackslashmine.mixin;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.gui.CsmStatusEffectRenderer;
import net.minecraft.client.gui.screen.ingame.AbstractInventoryScreen;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.text.Text;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.*;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Collection;

@Mixin(AbstractInventoryScreen.class)
public abstract class AbstractInventoryScreenMixin<T extends ScreenHandler> extends HandledScreen<T> {
    public AbstractInventoryScreenMixin(T handler, PlayerInventory inventory, Text title) {
        super(handler, inventory, title);
    }

    @ModifyConstant(method = "applyStatusEffectOffset", constant = @Constant(intValue = 160))
    private int getConstant(int old) {
        return CrackSlashMineMod.config.potionShift;
    }

    @Redirect(method = "applyStatusEffectOffset", at = @At(value = "INVOKE", target = "Ljava/util/Collection;isEmpty()Z"))
    private boolean shouldntShowStatusEffects(Collection<StatusEffectInstance> collection) {
        return collection.isEmpty() &&
                (this.client == null || this.client.player == null ||
                        LivingEntityStats.getFor(this.client.player).getEffects().isEmpty());
    }

    @Inject(method = "drawStatusEffects", at = @At(value = "HEAD"), cancellable = true)
    private void onDrawStatusEffects(MatrixStack matrixStack, CallbackInfo ci) {
        assert this.client != null;
        CsmStatusEffectRenderer.drawStatusEffects(
                matrixStack,
                this.client,
                this.x,
                this.y,
                this.getZOffset()
        );
        ci.cancel();
    }
}
