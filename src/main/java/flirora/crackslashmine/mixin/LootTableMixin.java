package flirora.crackslashmine.mixin;

import java.util.function.Consumer;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import flirora.crackslashmine.core.Levels;
import flirora.crackslashmine.item.equipment.CompatibleItemDescription;
import flirora.crackslashmine.item.equipment.CompatibleItemManager;
import flirora.crackslashmine.item.equipment.LootUtils;
import flirora.crackslashmine.item.equipment.Origin;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.context.LootContextParameters;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.registry.Registry;

@Mixin(LootTable.class)
public abstract class LootTableMixin {

  @Unique
  private static Consumer<ItemStack> processCsmCompatibles(
      Consumer<ItemStack> consumer, LootContext ctx) {
    return (stack) -> {
      consumer.accept(stack);
      CompatibleItemDescription desc = CompatibleItemManager.INSTANCE
          .get(Registry.ITEM.getId(stack.getItem()));
      if (desc != null) {
        Entity e = ctx.get(LootContextParameters.THIS_ENTITY);
        LivingEntity l = e instanceof LivingEntity ? (LivingEntity) e : null;
        Vec3d location = ctx.get(LootContextParameters.ORIGIN);
        int level = location == null ? 0
            : Levels.positionToLevel(ctx.getWorld(), location);
        LootUtils.generateItem(stack.getItem(), stack, ctx.getWorld(), l, level,
            Origin.LOOTED);
      }
    };
  }

  @Redirect(
      method = "generateLoot(Lnet/minecraft/loot/context/LootContext;Ljava/util/function/Consumer;)V",
      at = @At(
          value = "INVOKE",
          target = "Lnet/minecraft/loot/LootTable;processStacks(Ljava/util/function/Consumer;)Ljava/util/function/Consumer;"
      )
  )
  public Consumer<ItemStack> modifyConsumer(Consumer<ItemStack> consumer,
      LootContext lootContext) {
    return LootTable
        .processStacks(processCsmCompatibles(consumer, lootContext));
  }
}
