package flirora.crackslashmine.mixin;

import flirora.crackslashmine.core.EntityDamageUtils;
import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.entity.projectile.ProjectileComponent;
import flirora.crackslashmine.item.equipment.EquipmentUtils;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.item.*;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin(BowItem.class)
public abstract class BowItemMixin extends RangedWeaponItem implements Vanishable {
    public BowItemMixin(Settings settings) {
        super(settings);
    }

    @Inject(method = "onStoppedUsing", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/World;spawnEntity(Lnet/minecraft/entity/Entity;)Z"), cancellable = true, locals = LocalCapture.CAPTURE_FAILHARD)
    private void beforeSpawningProjectile(
            ItemStack stack, World world, LivingEntity user, int remainingUseTicks, CallbackInfo ci,
            PlayerEntity playerEntity, boolean bl, ItemStack itemStack, int i, float pullProgress, boolean bl2, ArrowItem arrow, PersistentProjectileEntity projectile) {
        LivingEntityStats stats = LivingEntityStats.getFor(user);
        long staminaCost = EquipmentUtils.getStaminaCostForAttacking(stack, stats.getLevel(), true);
        long netStaminaCost = Math.round(pullProgress * staminaCost);
        if (stats.getStamina() < netStaminaCost) {
            if (!user.getEntityWorld().isClient())
                EntityDamageUtils.playExhaustedSoundEffect(playerEntity);
            // We might still be able to shoot with less pull
            pullProgress = (float) (((double) stats.getStamina()) / staminaCost);
            netStaminaCost = stats.getStamina();
            if (pullProgress < 0.1f) {
                // Nope.
                ci.cancel();
                return;
            }
            projectile.setProperties(playerEntity, playerEntity.pitch, playerEntity.yaw, 0.0F, pullProgress * 3.0F, 1.0F);
        }
        ProjectileComponent.getFor(projectile).setBowProgress(
                pullProgress *
                        (1.0f + 0.1f * EnchantmentHelper.getLevel(Enchantments.POWER, stack)));
        stats.setStamina(stats.getStamina() - netStaminaCost);
    }
}
