package flirora.crackslashmine.mixin;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.CsmPrimaryAttackStat;
import flirora.crackslashmine.core.DamageType;
import flirora.crackslashmine.core.Levels;
import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.entity.projectile.ProjectileComponent;
import flirora.crackslashmine.entity.projectile.ProjectileDivision;
import flirora.crackslashmine.item.equipment.EquipmentComponent;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import org.apache.logging.log4j.Level;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(ProjectileEntity.class)
public abstract class ProjectileEntityMixin extends Entity {
    public ProjectileEntityMixin(EntityType<?> type, World world) {
        super(type, world);
    }

    /*
        Save stats of owner at the time the projectile is fired.

        Note that when this method is called, the shooter still has the
        respective item in their hand, so it's safe to get their main hand
        stack, even if the projectile is e.g. a trident or a snowball.
     */
    @Inject(method = "setOwner", at = @At(value = "RETURN"))
    private void onSetOwner(@Nullable Entity entity, CallbackInfo ci) {
        if (entity != null) {
            if (entity instanceof LivingEntity) {
                LivingEntity attacker = (LivingEntity) entity;
                LivingEntityStats attackerStats = LivingEntityStats.getFor(attacker);
                ProjectileDivision division = ProjectileDivision.getFor((ProjectileEntity) (Object) this);
                ProjectileComponent component = ProjectileComponent.getFor((ProjectileEntity) (Object) this);
                if (component.getAttack() != null) return;
                if (division == ProjectileDivision.PASSTHROUGH) return;
                ItemStack weapon = attacker.getMainHandStack();
                // TODO: make each UNARMED projectile have its own type of damage
                CsmPrimaryAttackStat attack = new CsmPrimaryAttackStat(new CsmPrimaryAttackStat.Entry(
                        DamageType.PHYSICAL,
                        Math.round(Levels.getHealthScaleFactor(attackerStats.getLevel())),
                        Math.round(Levels.getHealthScaleFactor(attackerStats.getLevel()))));
                if (division == ProjectileDivision.ARMED) {
                    EquipmentComponent equipmentComponent = EquipmentComponent.getFor(weapon);
                    CsmPrimaryAttackStat stat = equipmentComponent != null ?
                            equipmentComponent.getWeaponBaseStat() : null;
                    int requiredLevel = equipmentComponent != null ?
                            equipmentComponent.getMeta().getLevel() : 0;
                    if (stat != null && attackerStats.getLevel() >= requiredLevel) {
                        attack = stat;
                    }
                }
                component.init(attack, attacker, attackerStats);
            } else {
                CrackSlashMineMod.log(Level.WARN, entity + " shot a projectile as a non-LivingEntity");
            }
        }
    }
}
