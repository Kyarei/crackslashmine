package flirora.crackslashmine.mixin;

import flirora.crackslashmine.item.equipment.LootUtils;
import net.minecraft.entity.Entity;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.world.ChunkRegion;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(ChunkRegion.class)
public abstract class ChunkRegionMixin {
    // We have to inject our behavior here, since doing so on MobEntity#init()
    // might cause it to occur before a mob has gained all of its equipment.
    @Inject(method = "spawnEntity", at = @At("RETURN"))
    private void onSpawnEntity(Entity entity, CallbackInfoReturnable<Boolean> cir) {
        if (entity instanceof MobEntity) {
            LootUtils.populateEquipmentOnMob((MobEntity) entity);
        }
    }
}
