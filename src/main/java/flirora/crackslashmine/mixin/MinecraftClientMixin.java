package flirora.crackslashmine.mixin;

import flirora.crackslashmine.generation.ClientAreaManager;
import flirora.crackslashmine.network.AttackAttemptedC2S;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.RunArgs;
import net.minecraft.client.WindowEventHandler;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.options.GameOptions;
import net.minecraft.util.hit.HitResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.snooper.SnooperListener;
import net.minecraft.util.thread.ReentrantThreadExecutor;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Environment(EnvType.CLIENT)
@Mixin(MinecraftClient.class)
public abstract class MinecraftClientMixin
        extends ReentrantThreadExecutor<Runnable>
        implements SnooperListener, WindowEventHandler, ClientAreaManager.Provider {
    @Shadow
    @Nullable
    public HitResult crosshairTarget;

    @Shadow
    @Nullable
    public ClientPlayerEntity player;
    @Shadow
    @Final
    public GameOptions options;
    
    private ClientAreaManager manager;
    private Vec3d playerPositionSinceLastPrune;

    public MinecraftClientMixin(String string) {
        super(string);
    }

    @Inject(method = "<init>", at = @At(value = "RETURN"))
    private void onInit(RunArgs args, CallbackInfo ci) {
        manager = new ClientAreaManager();
        playerPositionSinceLastPrune = Vec3d.ZERO;
    }

    @Inject(method = "doAttack", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/network/ClientPlayerEntity;swingHand(Lnet/minecraft/util/Hand;)V"))
    private void onDoAttack(CallbackInfo ci) {
        if (this.crosshairTarget == null) return;
        HitResult.Type type = this.crosshairTarget.getType();
        if (type != HitResult.Type.BLOCK) {
            AttackAttemptedC2S.create(type == HitResult.Type.ENTITY);
        }
    }

    @Override
    public ClientAreaManager getClientAreaManager() {
        return manager;
    }

    @Inject(method = "tick", at = @At(value = "RETURN"))
    private void onTick(CallbackInfo ci) {
        if (this.player == null) return;
        // Prune the client area manager if the player has traveled at least
        // 64 blocks away from where they were the last time it was pruned
        if (playerPositionSinceLastPrune.equals(Vec3d.ZERO) ||
                playerPositionSinceLastPrune.squaredDistanceTo(this.player.getPos()) > 4096) {
            manager.prune(this.player.getPos(), 16 * (this.options.viewDistance + 8));
            playerPositionSinceLastPrune = this.player.getPos();
        }
    }
}
