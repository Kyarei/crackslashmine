package flirora.crackslashmine.mixin;

import net.minecraft.entity.LivingEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(LivingEntity.class)
public class LivingEntityClientMixin {
    @Inject(method = "shouldRenderName", at = @At(value = "HEAD"), cancellable = true)
    private void onShouldRenderName(CallbackInfoReturnable<Boolean> cir) {
        cir.setReturnValue(true);
    }
}
