package flirora.crackslashmine.mixin;

import flirora.crackslashmine.entity.boss.CsmBossBar;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.boss.BossBar;
import net.minecraft.network.Packet;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.listener.ClientPlayPacketListener;
import net.minecraft.network.packet.s2c.play.BossBarS2CPacket;
import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(BossBarS2CPacket.class)
public abstract class BossBarS2CPacketMixin implements Packet<ClientPlayPacketListener>, CsmBossBar {
    protected long csmHealth, csmMaxHealth;

    @Inject(method = "<init>(Lnet/minecraft/network/packet/s2c/play/BossBarS2CPacket$Type;Lnet/minecraft/entity/boss/BossBar;)V", at = @At(value = "RETURN"))
    private void onInit(BossBarS2CPacket.Type type, BossBar bossBar, CallbackInfo ci) {
        csmHealth = ((CsmBossBar) bossBar).getCsmHealth();
        csmMaxHealth = ((CsmBossBar) bossBar).getCsmMaxHealth();
    }

    // TODO: is there a more robust way to send and receive the extra data?

    @Inject(method = "read", at = @At(value = "FIELD", target = "Lnet/minecraft/network/packet/s2c/play/BossBarS2CPacket;percent:F", opcode = Opcodes.PUTFIELD, shift = At.Shift.AFTER))
    private void onWritePercent(PacketByteBuf packetByteBuf, CallbackInfo ci) {
        this.csmHealth = packetByteBuf.readLong();
        this.csmMaxHealth = packetByteBuf.readLong();
    }

    @Inject(method = "write", at = @At(value = "FIELD", target = "Lnet/minecraft/network/packet/s2c/play/BossBarS2CPacket;percent:F", opcode = Opcodes.GETFIELD, shift = At.Shift.BY, by = 2))
    private void onReadPercent(PacketByteBuf packetByteBuf, CallbackInfo ci) {
        packetByteBuf.writeLong(this.csmHealth);
        packetByteBuf.writeLong(this.csmMaxHealth);
    }

    @Override
    public long getCsmHealth() {
        return csmHealth;
    }

    @Override
    public long getCsmMaxHealth() {
        return csmMaxHealth;
    }

    @Override
    public void setCsmHealth(long val) {
        this.csmHealth = val;
    }

    @Override
    public void setCsmMaxHealth(long val) {
        this.csmMaxHealth = val;
    }

    @Override
    public void updateHealthFromEntity(LivingEntity e) {
        throw new UnsupportedOperationException("haha i'm lazy");
    }
}
