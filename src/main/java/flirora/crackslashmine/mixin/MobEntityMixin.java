package flirora.crackslashmine.mixin;

import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.item.equipment.LootUtils;
import flirora.crackslashmine.network.XpAwardSplatS2C;
import net.minecraft.entity.*;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.LocalDifficulty;
import net.minecraft.world.ServerWorldAccess;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(MobEntity.class)
public abstract class MobEntityMixin extends LivingEntity {
    @Override
    @Shadow
    public abstract ItemStack getEquippedStack(EquipmentSlot equipmentSlot);

    protected MobEntityMixin(EntityType<? extends LivingEntity> entityType,
                             World world) {
        super(entityType, world);
    }

    @Inject(method = "initialize", at = @At(value = "RETURN"))
    private void onInitialize(ServerWorldAccess serverWorldAccess,
                              LocalDifficulty localDifficulty,
                              SpawnReason spawnReason,
                              @Nullable EntityData entityData,
                              @Nullable CompoundTag compoundTag,
                              CallbackInfoReturnable<EntityData> cir) {
        // initialize stats
        LivingEntityStats stats =
                LivingEntityStats.getFor((LivingEntity) (Object) this);
        stats.initMob();
    }

    @Inject(method = "dropLoot", at = @At(value = "RETURN"))
    private void onDropLoot(DamageSource source, boolean causedByPlayer, CallbackInfo ci) {
        LivingEntity attacker = this.getAttacker();
        if (!(attacker instanceof PlayerEntity)) return;
        long xp = LootUtils.dropExtras(
                this.getRandom(),
                (MobEntity) (Object) this,
                (PlayerEntity) attacker,
                this::dropStack);
        if (xp > 0) {
            LivingEntityStats.getFor(this.attackingPlayer).addXp(xp);
            XpAwardSplatS2C.create(
                    (PlayerEntity) attacker, this.getEntityWorld(), this, xp);
        }
    }
}
