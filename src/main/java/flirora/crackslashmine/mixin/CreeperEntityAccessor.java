package flirora.crackslashmine.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.mob.CreeperEntity;

@Mixin(CreeperEntity.class)
public interface CreeperEntityAccessor {
  @Accessor("CHARGED")
  static TrackedData<Boolean> getChargedTrackedData() {
    throw new UnsupportedOperationException("untransformed access");
  }
}
