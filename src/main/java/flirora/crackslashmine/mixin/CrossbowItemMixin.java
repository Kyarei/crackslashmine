package flirora.crackslashmine.mixin;

import flirora.crackslashmine.core.EntityDamageUtils;
import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.core.MathUtils;
import flirora.crackslashmine.item.equipment.EquipmentUtils;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.CrossbowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.RangedWeaponItem;
import net.minecraft.item.Vanishable;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(CrossbowItem.class)
public abstract class CrossbowItemMixin extends RangedWeaponItem implements Vanishable {
    public CrossbowItemMixin(Settings settings) {
        super(settings);
    }

    /*
        This is likely to be wonky.
     */
    @Inject(method = "usageTick", at = @At(value = "HEAD"), cancellable = true)
    private void onUsageTick(World world, LivingEntity user, ItemStack stack, int remainingUseTicks, CallbackInfo ci) {
        if (!world.isClient() && user instanceof PlayerEntity) {
            if (((PlayerEntity) user).isCreative() || user.isSpectator())
                return;
            LivingEntityStats stats = LivingEntityStats.getFor(user);
            int pullTime = CrossbowItem.getPullTime(stack);
            long staminaCost = EquipmentUtils.getStaminaCostForAttacking(stack, stats.getLevel(), true);
            int current = stack.getMaxUseTime() - remainingUseTicks;
            long staminaCostForThisTick = current < pullTime ?
                    MathUtils.clock(staminaCost, pullTime, current) :
                    0;
            if (stats.getStamina() < staminaCostForThisTick) {
                EntityDamageUtils.playExhaustedSoundEffect((PlayerEntity) user);
                user.stopUsingItem();
                ci.cancel();
                return;
            }
            stats.setStamina(stats.getStamina() - staminaCostForThisTick);
        }
    }
}
