package flirora.crackslashmine.mixin;

import net.minecraft.item.Item;
import net.minecraft.item.ItemConvertible;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(Item.class)
public abstract class ItemClientMixin implements ItemConvertible {
}
