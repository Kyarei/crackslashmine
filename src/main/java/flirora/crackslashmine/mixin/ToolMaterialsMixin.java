package flirora.crackslashmine.mixin;

import net.minecraft.item.ToolMaterials;
import net.minecraft.recipe.Ingredient;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.function.Supplier;

@Mixin(ToolMaterials.class)
public class ToolMaterialsMixin {
    @Mutable
    @Shadow
    @Final
    private int itemDurability;

    @Inject(method = "<init>", at = @At("RETURN"))
    private void onInit(String enumName, int ordinal, int j, int k, float f, float g, int l, Supplier<Ingredient> supplier, CallbackInfo ci) {
        if (k == 32) this.itemDurability = 666;
    }
}
