package flirora.crackslashmine.mixin;

import flirora.crackslashmine.core.EntityDamageUtils;
import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.item.equipment.EquipmentUtils;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.TridentItem;
import net.minecraft.item.Vanishable;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(TridentItem.class)
public abstract class TridentItemMixin extends Item implements Vanishable {
    public TridentItemMixin(Settings settings) {
        super(settings);
    }

    @Inject(method = "onStoppedUsing", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/World;spawnEntity(Lnet/minecraft/entity/Entity;)Z"), cancellable = true)
    private void beforeProjectileSpawn(ItemStack stack, World world, LivingEntity user, int remainingUseTicks, CallbackInfo ci) {
        LivingEntityStats stats = LivingEntityStats.getFor(user);
        long staminaCost = EquipmentUtils.getStaminaCostForAttacking(stack, stats.getLevel(), true);
        if (stats.getStamina() < staminaCost) {
            EntityDamageUtils.playExhaustedSoundEffect((PlayerEntity) user); // always safe
            ci.cancel();
            return;
        }
        stats.setStamina(stats.getStamina() - staminaCost);
    }
}
