package flirora.crackslashmine.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.loot.context.LootContext;

@Mixin(LivingEntity.class)
public interface LivingEntityInvoker {
  @Invoker(value = "getLootContextBuilder")
  LootContext.Builder callGetLootContextBuilder(boolean causedByPlayer,
      DamageSource source);
}
