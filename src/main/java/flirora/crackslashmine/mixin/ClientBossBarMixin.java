package flirora.crackslashmine.mixin;

import flirora.crackslashmine.entity.boss.CsmBossBar;
import net.minecraft.client.gui.hud.ClientBossBar;
import net.minecraft.entity.boss.BossBar;
import net.minecraft.network.packet.s2c.play.BossBarS2CPacket;
import net.minecraft.text.Text;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.UUID;

@Mixin(ClientBossBar.class)
public abstract class ClientBossBarMixin extends BossBar {
    public ClientBossBarMixin(UUID uUID, Text text, Color color, Style style) {
        super(uUID, text, color, style);
    }

    @Inject(method = "<init>", at = @At(value = "RETURN"))
    private void onInit(BossBarS2CPacket bossBarS2CPacket, CallbackInfo ci) {
        CsmBossBar me = (CsmBossBar) (Object) this;
        CsmBossBar packet = (CsmBossBar) bossBarS2CPacket;
        me.setCsmHealth(packet.getCsmHealth());
        me.setCsmMaxHealth(packet.getCsmMaxHealth());
    }
}
