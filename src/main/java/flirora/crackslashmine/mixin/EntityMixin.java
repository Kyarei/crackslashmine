package flirora.crackslashmine.mixin;

import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.core.stat.Stats;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(Entity.class)
public class EntityMixin {
    /*
        Override the max air ticks of this entity.

        Note that dolphins override this method, so they won't get the change.
     */
    @Inject(method = "getMaxAir", at = @At(value = "HEAD"), cancellable = true)
    private void onGetMaxAir(CallbackInfoReturnable<Integer> cir) {
        Entity me = (Entity) (Object) this;
        if (me instanceof LivingEntity) {
            LivingEntity living = (LivingEntity) me;
            /*
                For some reason, getMaxAir() is called in the Entity
                constructor. If this is the case, then this entity's equipment
                slots will hold null objects, so we will crash if we continue.
             */
            // XXX: do any living entities actually have a standing eye
            // height of 0?
            if (me.getStandingEyeHeight() == 0.0) {
                return;
            }
            LivingEntityStats stats = LivingEntityStats.getFor(living);
            cir.setReturnValue((int) Math.min(
                    Integer.MAX_VALUE,
                    stats.getStat(Stats.AIR_TIME) *
                            (1 + EnchantmentHelper.getRespiration(living))));
        }
    }
}
