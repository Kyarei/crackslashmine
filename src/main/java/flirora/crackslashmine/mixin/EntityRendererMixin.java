package flirora.crackslashmine.mixin;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.mojang.blaze3d.systems.RenderSystem;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.Beautify;
import flirora.crackslashmine.core.LivingEntityStats;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.EntityRenderDispatcher;
import net.minecraft.client.render.entity.EntityRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.text.MutableText;
import net.minecraft.text.Style;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.math.Matrix4f;

@Mixin(EntityRenderer.class)
public abstract class EntityRendererMixin<T extends Entity> {
  @Shadow
  @Final
  protected EntityRenderDispatcher dispatcher;

  @Shadow
  public abstract TextRenderer getFontRenderer();

  @Shadow
  public abstract EntityRenderDispatcher getRenderManager();

  @Inject(
      method = "renderLabelIfPresent", at = @At(value = "HEAD"),
      cancellable = true
  )
  private void onRenderLabelIfPresent(T entity, Text text,
      MatrixStack matrixStack, VertexConsumerProvider vertexConsumerProvider,
      int i, CallbackInfo ci) {
    if (entity instanceof LivingEntity) {
      renderLivingEntityLabelIfPresent((LivingEntity) entity, text, matrixStack,
          vertexConsumerProvider, i);
      ci.cancel();
    }
  }

  private static final int HEALTH_BAR_WIDTH = 80;

  protected void renderLivingEntityLabelIfPresent(LivingEntity entity,
      Text nameText, MatrixStack matrixStack,
      VertexConsumerProvider vertexConsumerProvider, int i) {
    double squaredDistance = this.dispatcher.getSquaredDistanceToCamera(entity);
    if (squaredDistance <= 4096.0D) {
      float yOffset = entity.getHeight() + 0.5F;
      int additionalYOffset = "deadmau5".equals(nameText.getString()) ? -10 : 0;
      LivingEntityStats stats = LivingEntityStats.getFor(entity);
      matrixStack.push();
      matrixStack.translate(0.0D, yOffset, 0.0D);
      matrixStack.multiply(this.dispatcher.getRotation());
      matrixStack.scale(-0.025F, -0.025F, 0.025F);
      Matrix4f matrix4f = matrixStack.peek().getModel();
      float bgAlpha =
          MinecraftClient.getInstance().options.getTextBackgroundOpacity(0.25F);
      int bgColor = (int) (bgAlpha * 255.0F) << 24;
      TextRenderer textRenderer = this.getFontRenderer();
      MutableText levelText =
          new TranslatableText("csm.hud.level.short", stats.getLevel())
              .formatted(Formatting.YELLOW)
              .styled(s -> s.withFont(CrackSlashMineMod.SMALL_FONT));
      Text levelAndNameText =
          levelText.append(nameText.copy().formatted(Formatting.WHITE)
              .styled(s -> s.withFont(Style.DEFAULT_FONT_ID)));
      float healthPercentage =
          Math.max(0.0f, ((float) stats.getHealth()) / stats.getMaxHealth());
      float healthBarPixels = HEALTH_BAR_WIDTH * healthPercentage;
      Text hpText = Text.of(Beautify.beautify(stats.getHealth()));
      textRenderer.draw(levelAndNameText,
          -textRenderer.getWidth(levelAndNameText) / 2.0f,
          additionalYOffset - 12, -1, false, matrix4f, vertexConsumerProvider,
          false, bgColor, i);
      int x1 = -HEALTH_BAR_WIDTH / 2;
      int x2 = x1 + Math.round(healthBarPixels);
      int barColor = 0x80800000;
      RenderSystem.enableDepthTest();
      DrawableHelper.fill(matrixStack, x1, additionalYOffset - 1, x2,
          additionalYOffset + 9, barColor);
      RenderSystem.disableDepthTest();
      textRenderer.draw(hpText,
          HEALTH_BAR_WIDTH / 2.0f - 2 - textRenderer.getWidth(hpText),
          additionalYOffset, -1, false, matrix4f, vertexConsumerProvider, false,
          bgColor, i);
      matrixStack.pop();
    }
  }
}
