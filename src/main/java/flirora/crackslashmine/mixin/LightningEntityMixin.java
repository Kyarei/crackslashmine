package flirora.crackslashmine.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import flirora.crackslashmine.entity.EntityDamageMethods;
import flirora.crackslashmine.entity.spell.CsmLightningEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LightningEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.world.World;

@Mixin(LightningEntity.class)
public abstract class LightningEntityMixin extends Entity {

  public LightningEntityMixin(EntityType<?> type, World world) {
    super(type, world);
    // TODO Auto-generated constructor stub
  }

  @Redirect(
      method = "tick",
      at = @At(
          value = "INVOKE",
          target = "Lnet/minecraft/entity/Entity;onStruckByLightning(Lnet/minecraft/server/world/ServerWorld;Lnet/minecraft/entity/LightningEntity;)V"
      )
  )
  public void smiteEntity(Entity e, ServerWorld world,
      LightningEntity lightning) {
    if (lightning instanceof CsmLightningEntity && e instanceof LivingEntity) {
      CsmLightningEntity csmLightning = (CsmLightningEntity) lightning;
      EntityDamageMethods.onStruckByLightning((LivingEntity) e,
          csmLightning.getAttacker(), world, csmLightning,
          csmLightning.getAttack());
    } else {
      e.onStruckByLightning(world, lightning);
    }
  }
}
