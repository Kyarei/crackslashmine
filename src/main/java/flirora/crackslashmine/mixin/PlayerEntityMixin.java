package flirora.crackslashmine.mixin;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.*;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.damage.EntityDamageSource;
import net.minecraft.entity.player.HungerManager;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.world.GameRules;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(PlayerEntity.class)
public abstract class PlayerEntityMixin extends LivingEntity {
    @Shadow
    public abstract HungerManager getHungerManager();

    @Shadow
    public abstract boolean isCreative();

    @Override
    @Shadow
    public abstract boolean isSpectator();

    @Shadow
    protected HungerManager hungerManager;

    private int exhaustedTicks = 0;

    protected PlayerEntityMixin(
            EntityType<? extends LivingEntity> entityType, World world) {
        super(entityType, world);
    }

    @Inject(method = "tickMovement", at = @At(value = "HEAD"))
    private void onTickMovement(CallbackInfo ci) {
        if (isDead()) return;
        LivingEntityStats stats =
                LivingEntityStats.getFor((LivingEntity) (Object) this);
        if (!this.world.isClient() &&
                this.world.getGameRules().getBoolean(GameRules.NATURAL_REGENERATION)) {
            // Regenerate resources
            stats.regenerate(this.age % 20);
        }
        if (!this.isCreative() && !this.isSpectator()) {
            long staminaCost = MathUtils.clock(
                    Math.round(CrackSlashMineMod.config.sprintEnergyCostScaling *
                            EntityDamageUtils.getUnarmedEnergyCost(stats.getLevel())),
                    20,
                    this.age % 20);
            if (stats.getStamina() < staminaCost && exhaustedTicks <= 0) {
                exhaustedTicks = 40;
                EntityDamageUtils.playExhaustedSoundEffect((PlayerEntity) (Object) this);
            }
            if (!this.world.isClient() && this.isSprinting()) {
                stats.setStamina(Math.max(0, stats.getStamina() - staminaCost));
            }
            // Prevent sprinting when the player does not have enough stamina.
            // We set hunger to 19 instead of 20 so players can always eat.
            getHungerManager().setFoodLevel(exhaustedTicks <= 0 ? 19 : 6);
        } else {
            // Force the game to ignore hunger
            getHungerManager().setFoodLevel(19);
        }
        if (exhaustedTicks > 0) --exhaustedTicks;
    }

    /*
        Modify applyDamage() when the source is environmental
     */

    @Inject(method = "applyDamage", at = @At(value = "HEAD"))
    private void onApplyDamage(DamageSource damageSource, float f,
                               CallbackInfo ci) {
        if (damageSource instanceof EntityDamageSource && ((EntityDamageSource) damageSource).getAttacker() instanceof LivingEntity) {
            throw new UnsupportedOperationException("Do not use applyDamage()" +
                    " for damage from mobs or players!");
        }
    }

    // Scale environmental damage to area level
    @Redirect(method = "applyDamage", at = @At(value = "INVOKE", target =
            "Lnet/minecraft/entity/player/PlayerEntity;setHealth(F)V"))
    public void ourSetHealth(PlayerEntity e, float f, DamageSource damageSource) {
        LivingEntityStats stats =
                LivingEntityStats.getFor((LivingEntity) (Object) this);
        float current = e.getHealth();
        float damage = current - f;
        // also will always succeed
        int level = Levels.positionToLevel(
                (ServerWorld) e.getEntityWorld(), e.getPos());
        double scaledDamage = Levels.getHealthScaleFactor(level) * damage;
        CsmAttack attack = new CsmAttack(
                EntityDamageUtils.isInfallible(damageSource),
                new CsmAttack.Entry(
                        EntityDamageUtils.getIntrinsicDamageTypeFor(damageSource),
                        Math.round(scaledDamage)));
        stats.applyDamage(null, attack, damageSource);
    }

    /*
        Adjust hardcoded getHealth() > 0 check to check for C/M health
     */
    @Redirect(method = "tickMovement",
            at = @At(
                    value = "INVOKE",
                    target = "Lnet/minecraft/entity/player/PlayerEntity;getHealth()F",
                    ordinal = 1))
    public float getCsmHealth(PlayerEntity player) {
        return (float) LivingEntityStats.getFor(player).getHealth();
    }
}
