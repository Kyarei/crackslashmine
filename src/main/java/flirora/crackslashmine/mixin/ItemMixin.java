package flirora.crackslashmine.mixin;

import flirora.crackslashmine.item.equipment.LootUtils;
import flirora.crackslashmine.item.equipment.Origin;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemConvertible;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(Item.class)
public abstract class ItemMixin implements ItemConvertible {
    @Inject(method = "onCraft", at = @At(value = "HEAD"))
    private void handleOnCraft(ItemStack itemStack, World world, PlayerEntity playerEntity, CallbackInfo ci) {
        Item me = (Item) (Object) this; // So anyway I started casting
        LootUtils.generateItem(
                me, itemStack, world, playerEntity, -1, Origin.CRAFTED);
    }

}
