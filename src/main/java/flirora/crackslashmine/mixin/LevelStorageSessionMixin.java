package flirora.crackslashmine.mixin;

import flirora.crackslashmine.core.storage.CsmDatabase;
import flirora.crackslashmine.core.storage.CsmDatabaseProvider;
import net.minecraft.world.level.storage.LevelStorage;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.io.IOException;
import java.nio.file.Path;

@Mixin(LevelStorage.Session.class)
public abstract class LevelStorageSessionMixin implements CsmDatabaseProvider, AutoCloseable {
    @Shadow
    @Final
    private Path directory;
    private CsmDatabase csmDatabase;

    @Inject(method = "<init>", at = @At(value = "RETURN"))
    private void onInit(LevelStorage outer, String directoryName, CallbackInfo ci) throws IOException {
        csmDatabase = new CsmDatabase(
                // this.directory.resolve("csmdata.db.temp").toFile(),
                this.directory.resolve("csmdata.db"));
    }

    @Inject(method = "close", at = @At(value = "RETURN"))
    private void onClose(CallbackInfo ci) throws IOException {
        csmDatabase.close();
    }

    @Override
    public CsmDatabase getDatabase() {
        return csmDatabase;
    }
}
