package flirora.crackslashmine.mixin;

import flirora.crackslashmine.generation.PlayerLastChunkStore;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.PlayerManager;
import net.minecraft.util.registry.DynamicRegistryManager;
import net.minecraft.world.WorldSaveHandler;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(PlayerManager.class)
public abstract class PlayerManagerMixin implements PlayerLastChunkStore.Provider {
    private PlayerLastChunkStore store;

    @Inject(method = "<init>", at = @At(value = "RETURN"))
    private void onInit(MinecraftServer server, DynamicRegistryManager.Impl registryManager, WorldSaveHandler saveHandler, int maxPlayers, CallbackInfo ci) {
        store = new PlayerLastChunkStore();
    }

    @Override
    public PlayerLastChunkStore getPlayerLastChunkStore() {
        return store;
    }
}
