package flirora.crackslashmine.mixin;

import flirora.crackslashmine.entity.boss.CsmBossBar;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.gui.hud.BossBarHud;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.boss.BossBar;
import net.minecraft.text.Text;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(BossBarHud.class)
public abstract class BossBarHudMixin extends DrawableHelper {
    @Shadow
    @Final
    private MinecraftClient client;

    @Inject(method = "renderBossBar", at = @At(value = "RETURN"))
    private void afterRenderingBossBar(MatrixStack matrixStack, int x, int y, BossBar bossBar, CallbackInfo ci) {
        int rightMargin = (int) (x + 183.0f - 3);
        Text text = ((CsmBossBar) bossBar).getHealthText();
        int width = this.client.textRenderer.getWidth(text);
        int textX = rightMargin - width;
        int textY = y + 2;
        this.client.textRenderer.drawWithShadow(matrixStack, text, textX, textY, 0xFFFFFF);
    }
}
