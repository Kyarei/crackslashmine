package flirora.crackslashmine.mixin;

import flirora.crackslashmine.gui.CsmPlayerHud;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.hud.InGameHud;
import net.minecraft.client.util.math.MatrixStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

/**
 * A mixin to roadblock any of {@link InGameHud}'s HUD elements that we want to
 * replace with our own.
 */
@Mixin(InGameHud.class)
public abstract class InGameHudMixin {
    private CsmPlayerHud csmHud;

    @Inject(method = "<init>", at = @At(value = "RETURN"))
    private void onInit(MinecraftClient client, CallbackInfo ci) {
        csmHud = new CsmPlayerHud(client);
    }

    @Inject(method = "renderStatusBars", at = @At(value = "HEAD"), cancellable = true)
    private void onRenderStatusBars(MatrixStack matrixStack, CallbackInfo ci) {
        ci.cancel(); // Did Notch deserve this? Find out more at 11.
    }

    @Inject(method = "renderExperienceBar", at = @At(value = "HEAD"), cancellable = true)
    private void onRenderExperienceBar(MatrixStack matrices, int x, CallbackInfo ci) {
        ci.cancel();
    }

    @Inject(method = "renderStatusEffectOverlay", at = @At(value = "HEAD"), cancellable = true)
    private void onRenderStatusEffectOverlay(MatrixStack matrices, CallbackInfo ci) {
        ci.cancel();
    }

    @Inject(method = "render",
            at = @At(
                    value = "INVOKE_STRING",
                    target = "Lnet/minecraft/util/profiler/Profiler;push(Ljava/lang/String;)V",
                    args = {"ldc=bossHealth"}))
    private void beforeBossBar(MatrixStack matrices, float tickDelta, CallbackInfo ci) {
        csmHud.onHudRender(matrices, tickDelta);
    }
}
