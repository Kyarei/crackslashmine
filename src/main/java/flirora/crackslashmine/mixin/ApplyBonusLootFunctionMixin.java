package flirora.crackslashmine.mixin;

import java.util.Random;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.core.stat.Stats;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.condition.LootCondition;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.context.LootContextParameters;
import net.minecraft.loot.function.ApplyBonusLootFunction;
import net.minecraft.loot.function.ConditionalLootFunction;
import net.minecraft.util.math.MathHelper;

@Mixin(ApplyBonusLootFunction.class)
public abstract class ApplyBonusLootFunctionMixin
    extends ConditionalLootFunction {
  @Shadow
  @Final
  private Enchantment enchantment;

  public ApplyBonusLootFunctionMixin(LootCondition[] conditions) {
    super(conditions);
    // TODO Auto-generated constructor stub
  }

  @Unique
  private int csm_adjustedEnchantmentLevel(LootContext context, int oldLevel) {
    if (enchantment == Enchantments.FORTUNE) {
      Entity e = context.get(LootContextParameters.THIS_ENTITY);
      if (e instanceof LivingEntity) {
        LivingEntityStats stats = LivingEntityStats.getFor((LivingEntity) e);
        long chance = stats.getStat(Stats.BONUS_FORTUNE_CHANCE);
        if (chance > 0) {
          int bonusLevels = (int) MathHelper.clamp(chance / 10000, 0,
              Integer.MAX_VALUE)
              + (context.getRandom().nextInt(10000) < chance % 10000 ? 1 : 0);
          return oldLevel + bonusLevels;
        }
      }
    }
    return oldLevel;
  }

  @Redirect(
      method = "process",
      at = @At(
          value = "INVOKE",
          target = "Lnet/minecraft/loot/function/ApplyBonusLootFunction$Formula;getValue(Ljava/util/Random;II)I"
      )
  )
  private int getValueProxy(ApplyBonusLootFunction.Formula formula, Random r,
      int count, int oldLevel, ItemStack stack, LootContext context) {
    return formula.getValue(r, count,
        csm_adjustedEnchantmentLevel(context, oldLevel));
  }

}
