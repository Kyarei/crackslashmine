package flirora.crackslashmine.mixin;

import flirora.crackslashmine.item.traits.CustomPickup;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stat.Stats;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.UUID;

@Mixin(ItemEntity.class)
public abstract class ItemEntityMixin extends Entity {
    @Shadow
    public abstract ItemStack getStack();

    @Shadow
    private int pickupDelay;

    @Shadow
    private UUID owner;

    @Shadow
    public abstract void setStack(ItemStack stack);

    public ItemEntityMixin(EntityType<?> type, World world) {
        super(type, world);
    }

    @Inject(method = "onPlayerCollision", at = @At(value = "HEAD"),
            cancellable = true)
    private void handlePlayerCollision(PlayerEntity player, CallbackInfo ci) {
        if (!this.world.isClient()) {
            ItemStack itemStack = this.getStack();
            Item item = itemStack.getItem();
            int count = itemStack.getCount();

            if (item instanceof CustomPickup) {
                CustomPickup cp = (CustomPickup) item;
                if (this.pickupDelay == 0 &&
                        (this.owner == null || this.owner.equals(player.getUuid())) &&
                        cp.canPickUp(player, itemStack)) {
                    ItemStack remainder = cp.doPickUp(player, itemStack);
                    player.sendPickup(this, count);
                    if (remainder.isEmpty()) {
                        this.remove();
                    } else {
                        this.setStack(remainder);
                    }

                    player.increaseStat(
                            Stats.PICKED_UP.getOrCreateStat(item), count);
                    player.method_29499((ItemEntity) (Object) this);
                }
                ci.cancel();
            }
        }
    }
}
