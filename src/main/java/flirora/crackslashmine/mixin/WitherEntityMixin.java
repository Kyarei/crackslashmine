package flirora.crackslashmine.mixin;

import flirora.crackslashmine.entity.boss.CsmBossBar;
import net.minecraft.entity.boss.ServerBossBar;
import net.minecraft.entity.boss.WitherEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin(WitherEntity.class)
public abstract class WitherEntityMixin {
    @Redirect(method = "mobTick", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/boss/ServerBossBar;setPercent(F)V"))
    private void bossBarPercentProxy(ServerBossBar bossBar, float f) {
        WitherEntity me = (WitherEntity) (Object) this;
        ((CsmBossBar) bossBar).updateHealthFromEntity(me);
    }
}
