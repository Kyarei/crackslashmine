package flirora.crackslashmine.mixin;

import java.util.Random;

import org.apache.logging.log4j.Level;
import org.jetbrains.annotations.Nullable;
import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyConstant;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.Slice;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.AttackContext;
import flirora.crackslashmine.core.CsmAttack;
import flirora.crackslashmine.core.CsmPrimaryAttackStat;
import flirora.crackslashmine.core.DamageType;
import flirora.crackslashmine.core.EntityDamageUtils;
import flirora.crackslashmine.core.Levels;
import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.core.RawDamageable;
import flirora.crackslashmine.entity.projectile.ProjectileComponent;
import flirora.crackslashmine.entity.projectile.ProjectileDivision;
import flirora.crackslashmine.item.equipment.EquipmentComponent;
import flirora.crackslashmine.item.food.CompatibleFoodDescription;
import flirora.crackslashmine.item.food.CompatibleFoodManager;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityGroup;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.attribute.AttributeContainer;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.damage.DamageTracker;
import net.minecraft.entity.damage.EntityDamageSource;
import net.minecraft.entity.damage.ProjectileDamageSource;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.entity.projectile.TridentEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.world.World;

@Mixin(LivingEntity.class)
public abstract class LivingEntityMixin extends Entity
    implements RawDamageable {
  @Shadow
  protected abstract void applyDamage(DamageSource damageSource, float f);

  @Shadow
  protected float lastDamageTaken;

  @Shadow
  public abstract float getHealth();

  @Shadow
  public abstract ItemStack getMainHandStack();

  @Shadow
  public abstract Random getRandom();

  @Shadow
  public abstract AttributeContainer getAttributes();

  @Shadow
  public abstract boolean isDead();

  @Shadow
  @Final
  private DamageTracker damageTracker;

  @Shadow
  public abstract DamageTracker getDamageTracker();

  @Shadow
  public abstract float getMaxHealth();

  @Shadow
  public abstract EntityGroup getGroup();

  private boolean rawmode;
  private float dealt;

  public LivingEntityMixin(EntityType<?> entityType, World world) {
    super(entityType, world);
  }

  @Inject(method = "tick", at = @At(value = "HEAD"))
  private void onTick(CallbackInfo ci) {
    /*
     * This should work to prevent undesired effects of having <= 0 vanilla
     * health but > 0 C/M health but produces undesirable damage animations when
     * regenerating health and immediately 'snapping back'. A more refined way
     * of doing this could get rid of the problem.
     */
    // this.setHealth(1);
    LivingEntityStats.getFor((LivingEntity) (Object) this).tick();
  }

  /*
   * Override isDead() and isAlive() to use our health system instead of vanilla
   * health
   */

  @Inject(method = "isDead", at = @At(value = "HEAD"), cancellable = true)
  private void onIsDead(CallbackInfoReturnable<Boolean> cir) {
    LivingEntityStats stats =
        LivingEntityStats.getFor((LivingEntity) (Object) this);
    cir.setReturnValue(stats.getHealth() <= 0);
  }

  @Inject(method = "isAlive", at = @At(value = "HEAD"), cancellable = true)
  private void onIsAlive(CallbackInfoReturnable<Boolean> cir) {
    LivingEntityStats stats =
        LivingEntityStats.getFor((LivingEntity) (Object) this);
    cir.setReturnValue(!this.removed && stats.getHealth() > 0);
  }

  /*
   * Modify applyDamage() when the source is environmental
   */

  // Dummy out these two calls; we have our own armor system

  @Redirect(
      method = "applyDamage",
      at = @At(
          value = "INVOKE",
          target = "Lnet/minecraft/entity/LivingEntity;applyArmorToDamage(Lnet/minecraft/entity/damage/DamageSource;F)F"
      )
  )
  public float applyArmorToDamageProxy(LivingEntity livingEntity,
      DamageSource damageSource, float f) {
    return f;
  }

  @Redirect(
      method = "applyDamage",
      at = @At(
          value = "INVOKE",
          target = "Lnet/minecraft/entity/LivingEntity;applyEnchantmentsToDamage(Lnet/minecraft/entity/damage/DamageSource;F)F"
      )
  )
  public float applyEnchantmentsToDamageProxy(LivingEntity livingEntity,
      DamageSource damageSource, float f) {
    return f;
  }

  @Inject(method = "applyDamage", at = @At(value = "HEAD"))
  private void onApplyDamage(DamageSource damageSource, float f,
      CallbackInfo ci) {
    if (damageSource instanceof EntityDamageSource
        && damageSource.getAttacker() instanceof LivingEntity) {
      throw new UnsupportedOperationException(
          "Do not use applyDamage()" + " for damage from mobs or players!");
    }
  }

  // Scale environmental damage to area level
  @Redirect(
      method = "applyDamage",
      at = @At(
          value = "INVOKE",
          target = "Lnet/minecraft/entity/LivingEntity;setHealth(F)V"
      )
  )
  public void ourSetHealth(LivingEntity e, float f, DamageSource damageSource) {
    LivingEntityStats stats =
        LivingEntityStats.getFor((LivingEntity) (Object) this);
    float current = e.getHealth();
    float damage = current - f;
    // cast is always safe since damage() returns early on the client
    int level =
        Levels.positionToLevel((ServerWorld) e.getEntityWorld(), e.getPos());
    double scaledDamage = Levels.getHealthScaleFactor(level) * damage;
    // System.err.printf("env damage: %f -> %f\n", damage, scaledDamage);
    CsmAttack attack =
        new CsmAttack(EntityDamageUtils.isInfallible(damageSource),
            new CsmAttack.Entry(
                EntityDamageUtils.getIntrinsicDamageTypeFor(damageSource),
                Math.round(scaledDamage)));
    stats.applyDamage(null, attack, damageSource);
  }

  /*
   * Handle damage from living entities
   */

  /*
   * @Redirect(method = "damage", at = @At( value = "FIELD", opcode =
   * Opcodes.GETFIELD, target =
   * "Lnet/minecraft/entity/LivingEntity;lastDamageTaken:F"), slice = @Slice(
   * from = @At(value = "FIELD", opcode = Opcodes.GETFIELD, target =
   * "Lnet/minecraft/entity/LivingEntity;timeUntilRegen:I"), to = @At(value =
   * "INVOKE", target =
   * "Lnet/minecraft/entity/LivingEntity;applyDamage(Lnet/minecraft/entity/damage/DamageSource;F)V",
   * ordinal = 0)), allow = 2, require = 2) public float
   * onGetLastDamageTaken(LivingEntity livingEntity, DamageSource damageSource,
   * float originalAmount) { return damageSource instanceof EntityDamageSource ?
   * 0.0f : this.lastDamageTaken; }
   * 
   * @Redirect(method = "damage", at = @At( value = "FIELD", opcode =
   * Opcodes.PUTFIELD, target =
   * "Lnet/minecraft/entity/LivingEntity;lastDamageTaken:F"), allow = 2, require
   * = 2 ) public void onSetLastDamageTaken(LivingEntity livingEntity, float
   * value, DamageSource damageSource, float originalDamage) { if
   * (!(damageSource instanceof EntityDamageSource)) this.lastDamageTaken =
   * value; }
   */

  @ModifyConstant(
      method = "damage", constant = @Constant(floatValue = 10.0f),
      slice = @Slice(
          to = @At(
              value = "INVOKE",
              target = "Lnet/minecraft/entity/LivingEntity;applyDamage(Lnet/minecraft/entity/damage/DamageSource;F)V",
              ordinal = 0
          )
      ), allow = 1, require = 1
  )
  public float proxy10Ticks(float old, DamageSource damageSource,
      float netDamage) {
    // Make the less-than check always pass for EntityDamageSource sources
    return damageSource instanceof EntityDamageSource ? Float.POSITIVE_INFINITY
        : old;
  }

  @Redirect(
      method = "damage",
      at = @At(
          value = "FIELD", opcode = Opcodes.PUTFIELD,
          target = "Lnet/minecraft/entity/LivingEntity;lastDamageTaken:F"
      ), allow = 2, require = 2
  )
  public void onSetLastDamageTaken(LivingEntity livingEntity, float value,
      DamageSource damageSource, float originalDamage) {
    if (!(damageSource instanceof EntityDamageSource))
      this.lastDamageTaken = value;
  }

  @Redirect(
      method = "damage",
      at = @At(
          value = "INVOKE",
          target = "Lnet/minecraft/entity/LivingEntity;applyDamage(Lnet/minecraft/entity/damage/DamageSource;F)V"
      )
  )
  public void ourApplyDamage(LivingEntity me, DamageSource damageSource,
      float netDamage) {
    if (rawmode) {
      dealt = netDamage;
      return;
    }
    if (!(damageSource instanceof EntityDamageSource)) {
      this.applyDamage(damageSource, netDamage);
      return;
    }
    EntityDamageSource source = (EntityDamageSource) damageSource;
    Entity baseAttacker = source.getAttacker();
    Entity projectile =
        source instanceof ProjectileDamageSource ? source.getSource() : null;
    if ((baseAttacker != null && !(baseAttacker instanceof LivingEntity))
        || (projectile != null && !(projectile instanceof ProjectileEntity))) {
      CrackSlashMineMod.log(Level.ERROR, String.format(
          "%s tried to attack with projectile %s, but they are of the wrong types",
          baseAttacker, projectile));
      return;
    }
    csm_applyDamageFromLivingEntity(source, (LivingEntity) baseAttacker,
        netDamage, (ProjectileEntity) projectile);
  }

  /**
   * Apply damage from a living entity, to be called by
   * {@link LivingEntity#damage(DamageSource, float)} when the damage source is
   * an {@link EntityDamageSource}.
   *
   * @param damageSource   The {@link EntityDamageSource} that would have been
   *                       used
   * @param attacker       The attacker, or null if e.g. the damage came from a
   *                       dispensed projectile
   * @param originalAmount The amount of vanilla damage that would have been
   *                       dealt; usually ignored.
   * @param projectile     The projectile that dealt the damage, or null if a
   *                       projectile was not involved.
   */
  @Unique
  private void csm_applyDamageFromLivingEntity(EntityDamageSource damageSource,
      @Nullable LivingEntity attacker, float originalAmount,
      @Nullable ProjectileEntity projectile) {
    // TODO: need to account for splash damage from Riptide-enchanted tridents
    boolean thorns = damageSource.isThorns(),
        explosive = damageSource.isExplosive(), magic = damageSource.getMagic();
    if (thorns || explosive || attacker == null) { // Treat these types of
                                                   // damage as environmental
      this.getDamageTracker().onDamage(damageSource, this.getHealth(),
          originalAmount);
      ourSetHealth((LivingEntity) (Object) this,
          this.getHealth() - originalAmount, damageSource);
      return;
    }
    LivingEntityStats defenderStats =
        LivingEntityStats.getFor((LivingEntity) (Object) this);
    // TODO: account for weapon enchantments
    if (projectile == null) {
      // Melee damage
      LivingEntityStats attackerStats = LivingEntityStats.getFor(attacker);
      ItemStack meleeWeapon = attacker.getMainHandStack();
      EquipmentComponent component = EquipmentComponent.getFor(meleeWeapon);
      CsmPrimaryAttackStat stat =
          component != null ? component.getWeaponBaseStat() : null;
      int requiredLevel =
          component != null ? component.getMeta().getLevel() : 0;
      CsmAttack baseAttack;
      if (stat == null || attackerStats.getLevel() < requiredLevel) {
        // Not holding a C/M-compatible weapon, or too unskilled for it.
        if (attacker instanceof PlayerEntity) {
          baseAttack =
              new CsmAttack(new CsmAttack.Entry(DamageType.PHYSICAL, 1));
        } else {
          // TODO: add support for assigning intrinsic damage types for mobs
          AttributeContainer container = this.getAttributes();
          double damage =
              container.hasAttribute(EntityAttributes.GENERIC_ATTACK_DAMAGE)
                  ? container
                      .getBaseValue(EntityAttributes.GENERIC_ATTACK_DAMAGE)
                  : 1.0;
          baseAttack = new CsmAttack(new CsmAttack.Entry(DamageType.PHYSICAL,
              Math.round(damage
                  * Levels.getHealthScaleFactor(attackerStats.getLevel())
                  * CrackSlashMineMod.config.csmMobToEnvironmentalDamageRatio)));
        }
      } else {
        baseAttack = stat.roll(this.getRandom(), attacker);
      }
      CsmAttack modifiedAttack = baseAttack
          .withAppliedBonuses(attackerStats.getStats(), this.getRandom(),
              new AttackContext(attacker), meleeWeapon)
          .applyMeleeWeaponEnchants(meleeWeapon, this.getGroup());
      defenderStats.applyDamage(attacker, modifiedAttack, damageSource);
      // Don't forget to do this for death messages!
      getDamageTracker().onDamage(damageSource, getHealth(),
          modifiedAttack.totalDamage() * getMaxHealth()
              / defenderStats.getMaxHealth());
    } else {
      // Projectile damage – what types of projectiles precisely
      // fall under our system?
      switch (ProjectileDivision.getFor(projectile)) {
      case PASSTHROUGH:
        this.applyDamage(EntityDamageUtils.eraseAttacker(damageSource),
            originalAmount);
        break;
      case UNARMED:
      case ARMED:
        LivingEntityStats attackerStats = LivingEntityStats.getFor(attacker);
        ProjectileComponent component = ProjectileComponent.getFor(projectile);
        double bowProgress = component.getBowProgress();
        if (projectile instanceof TridentEntity) {
          // Hardcode trident logic. Nasty.
          bowProgress *= (1.0 + 0.1 * EnchantmentHelper.getAttackDamage(
              ((TridentEntityInvoker) projectile).callAsItemStack(),
              this.getGroup()));
        }
        CsmAttack attack = component.getAttack().scaled(bowProgress);
        defenderStats.applyDamage(attacker, attack, damageSource);
        getDamageTracker().onDamage(damageSource, getHealth(),
            attack.totalDamage() * getMaxHealth()
                / defenderStats.getMaxHealth());
        break;
      }
    }
  }

  @Override
  public float damageRaw(DamageSource source, float amount) {
    rawmode = true;
    boolean success = damage(source, amount);
    rawmode = false;
    return success ? dealt : 0.0f;
  }

  @Inject(method = "kill", at = @At(value = "HEAD"), cancellable = true)
  private void onKill(CallbackInfo ci) {
    LivingEntityStats.dealDamage((LivingEntity) (Object) this, null,
        new CsmAttack(true,
            new CsmAttack.Entry(DamageType.PHYSICAL, 1_000_000_000_000L)),
        DamageSource.OUT_OF_WORLD);
    ci.cancel();
  }

  /*
   * Changes related to air time.
   * 
   * Minecraft uses RNG to determine whether air time should be deducted each
   * tick if the entity has Respiration-enchanted armor. This is undesirable for
   * showing the remaining air time on the HUD, so we replace the calculations
   * with deterministic ones.
   */

  /*
   * Always deduct one air tick; this is compensated for by increasing the
   * maximum number of air ticks when one has Respiration.
   */
  @Inject(
      method = "getNextAirUnderwater", at = @At(value = "HEAD"),
      cancellable = true
  )
  private void onGetNextAirUnderwater(int air,
      CallbackInfoReturnable<Integer> cir) {
    cir.setReturnValue(air - 1);
  }

  /*
   * Scale air regain on land to max air ticks.
   */
  @Inject(
      method = "getNextAirOnLand", at = @At(value = "HEAD"), cancellable = true
  )
  private void onGetNextAirOnLand(int air,
      CallbackInfoReturnable<Integer> cir) {
    cir.setReturnValue(Math.min(getMaxAir(), air + getMaxAir() * 4 / 300));
  }

  /*
   * If the entity's air ticks exceed its max air ticks, then bring it back in
   * range. This can happen, for instance, if it unequips Respiration-enchanted
   * armor.
   */
  @Inject(method = "baseTick", at = @At(value = "HEAD"))
  private void onBaseTick(CallbackInfo ci) {
    if (this.getAir() > this.getMaxAir())
      this.setAir(this.getMaxAir());
  }

  /*
   * Handle compatible foods.
   */
  @Inject(
      method = "applyFoodEffects", at = @At(value = "HEAD"), cancellable = true
  )
  private void onApplyFoodEffects(ItemStack stack, World world,
      LivingEntity targetEntity, CallbackInfo ci) {
    if (world.isClient())
      return;
    CompatibleFoodDescription desc = CompatibleFoodManager.SERVER.get(stack);
    if (desc != null) {
      desc.apply(targetEntity);
      if (desc.isDisableVanillaEffects()) {
        ci.cancel();
        return;
      }
    }
  }
}
