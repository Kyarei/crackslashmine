package flirora.crackslashmine.mixin;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.stream.Stream;

import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import flirora.crackslashmine.core.storage.CsmDatabase;
import flirora.crackslashmine.core.storage.CsmDatabaseProvider;
import flirora.crackslashmine.generation.AreaManager;
import flirora.crackslashmine.generation.AreaManagerProvider;
import flirora.crackslashmine.generation.PlayerLastChunkStore;
import flirora.crackslashmine.item.equipment.LootUtils;
import flirora.crackslashmine.item.essence.tonat.VödManager;
import flirora.crackslashmine.item.food.CompatibleFoodManager;
import flirora.crackslashmine.item.spell.data.SpellDataManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.WorldGenerationProgressListener;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.gen.Spawner;
import net.minecraft.world.gen.chunk.ChunkGenerator;
import net.minecraft.world.level.ServerWorldProperties;
import net.minecraft.world.level.storage.LevelStorage;

@Mixin(ServerWorld.class)
public abstract class ServerWorldMixin
    implements CsmDatabaseProvider, AreaManagerProvider {
  @Shadow
  public @NotNull abstract MinecraftServer getServer();

  private CsmDatabase csmDatabase;
  private AreaManager areaManager;

  @Inject(method = "<init>", at = @At(value = "RETURN"))
  private void onInit(MinecraftServer server, Executor workerExecutor,
      LevelStorage.Session session, ServerWorldProperties properties,
      RegistryKey<World> registryKey, DimensionType dimensionType,
      WorldGenerationProgressListener worldGenerationProgressListener,
      ChunkGenerator chunkGenerator, boolean bl, long l, List<Spawner> list,
      boolean bl2, CallbackInfo ci) {
    csmDatabase = ((CsmDatabaseProvider) session).getDatabase();
    areaManager = new AreaManager((ServerWorld) (Object) this);
  }

  @Inject(method = "spawnEntity", at = @At(value = "RETURN"))
  private void onSpawnEntity(Entity entity,
      CallbackInfoReturnable<Boolean> cir) {
    if (entity instanceof MobEntity) {
      LootUtils.populateEquipmentOnMob((MobEntity) entity);
    }
  }

  // Temporary mixin until player connect event is merged
  @Inject(method = "onPlayerConnected", at = @At(value = "RETURN"))
  private void handleOnPlayerConnected(ServerPlayerEntity player,
      CallbackInfo ci) {
    // Sync data that needs to
    CompatibleFoodManager.SERVER.syncToClients(Stream.of(player));
    VödManager.SERVER.syncToClients(Stream.of(player));
    SpellDataManager.SERVER.syncToClients(Stream.of(player));
    // Erase position in player last chunk store
    ((PlayerLastChunkStore.Provider) this.getServer().getPlayerManager())
        .getPlayerLastChunkStore().remove(player);
  }

  @Override
  public CsmDatabase getDatabase() {
    return csmDatabase;
  }

  @Override
  public AreaManager getAreaManager() {
    return areaManager;
  }
}
