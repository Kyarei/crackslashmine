package flirora.crackslashmine.entity;

import flirora.crackslashmine.core.CsmAttack;
import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.mixin.CreeperEntityAccessor;
import net.minecraft.entity.LightningEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.mob.CreeperEntity;
import net.minecraft.server.world.ServerWorld;

public class EntityDamageMethods {
  public static void onStruckByLightning(LivingEntity e, LivingEntity attacker,
      ServerWorld world, LightningEntity lightning, CsmAttack attack) {
    e.setFireTicks(e.getFireTicks() + 1);
    if (e.getFireTicks() == 0) {
      e.setOnFireFor(8);
    }

    LivingEntityStats.dealDamage(e, attacker, attack,
        DamageSource.LIGHTNING_BOLT);

    if (e instanceof CreeperEntity) {
      e.getDataTracker().set(CreeperEntityAccessor.getChargedTrackedData(),
          true);
    }
  }
}
