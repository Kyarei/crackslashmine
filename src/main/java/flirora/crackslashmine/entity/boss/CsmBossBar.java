package flirora.crackslashmine.entity.boss;

import net.minecraft.entity.LivingEntity;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;

/**
 * An interface that provides additional boss bar functionality;
 * {@link net.minecraft.entity.boss.BossBar} is made to implement this
 * interface in this mod.
 */
public interface CsmBossBar {
    long getCsmHealth();

    void setCsmHealth(long val);

    long getCsmMaxHealth();

    void setCsmMaxHealth(long val);

    void updateHealthFromEntity(LivingEntity e);

    default Text getHealthText() {
        return new TranslatableText("bossbar.hp_numeric", getCsmHealth(), getCsmMaxHealth());
    }
}
