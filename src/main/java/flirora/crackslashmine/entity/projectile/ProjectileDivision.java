package flirora.crackslashmine.entity.projectile;

import net.minecraft.entity.projectile.FireworkRocketEntity;
import net.minecraft.entity.projectile.FishingBobberEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.entity.projectile.thrown.SnowballEntity;
import net.minecraft.entity.projectile.thrown.ThrownItemEntity;

/**
 * Represents a type of projectile as seen by Crack/Mine's damage system.
 */
public enum ProjectileDivision {
    /**
     * Projctiles that deal no damage and can be safely passed to vanilla
     * damage functionality.
     */
    PASSTHROUGH,
    /**
     * Projectiles that should be handled by Crack/Mine's custom damage
     * handlers.
     */
    ARMED,
    /**
     * Same as {@link ProjectileDivision#ARMED}, but always treat this
     * projectile as being shot without a weapon.
     */
    UNARMED,
    ;

    /**
     * Get the {@link ProjectileDivision} of this projectile.
     * <p>
     * Note that this is set up for only vanilla projectiles at this time.
     *
     * @param projectile the projectile
     * @return the division
     */
    public static ProjectileDivision getFor(ProjectileEntity projectile) {
        if (projectile instanceof ThrownItemEntity) {
            if (projectile instanceof SnowballEntity) return UNARMED;
            return PASSTHROUGH;
        }
        if (projectile instanceof FishingBobberEntity) {
            return PASSTHROUGH;
        }
        if (projectile instanceof FireworkRocketEntity) return UNARMED;
        return ARMED;
    }

}
