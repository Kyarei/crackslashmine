package flirora.crackslashmine.entity.projectile;

import dev.onyxstudios.cca.api.v3.component.ComponentV3;
import flirora.crackslashmine.core.AttackContext;
import flirora.crackslashmine.core.CrackSlashMineComponents;
import flirora.crackslashmine.core.CsmAttack;
import flirora.crackslashmine.core.CsmPrimaryAttackStat;
import flirora.crackslashmine.core.CsmRegistries;
import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.core.stat.Stat;
import flirora.crackslashmine.core.stat.StatContainer;
import flirora.crackslashmine.core.stat.StatView;
import net.fabricmc.fabric.api.util.NbtType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.util.Identifier;

public class ProjectileComponent implements ComponentV3 {
  private final ProjectileEntity projectile;
  private CsmAttack attack;
  private StatContainer attackerStats;
  // Stores the pull progress of the bow that shot an arrow.
  private float bowProgress = 1.0f;

  public ProjectileComponent(ProjectileEntity projectile) {
    this.projectile = projectile;
    attackerStats = new StatContainer();
  }

  public CsmAttack getAttack() {
    return attack;
  }

  public void setAttack(CsmAttack attack) {
    this.attack = attack;
  }

  public StatView getAttackerStats() {
    return attackerStats.frozen();
  }

  public float getBowProgress() {
    return bowProgress;
  }

  public void setBowProgress(float bowProgress) {
    this.bowProgress = bowProgress;
  }

  public void init(CsmPrimaryAttackStat attack, LivingEntity attackerEntity,
      LivingEntityStats attacker) {
    init(attack.roll(attackerEntity.getRandom(), attackerEntity),
        attackerEntity, attacker);
  }

  private void init(CsmAttack attack, LivingEntity attackerEntity,
      LivingEntityStats attacker) {
    this.attack = attack.withAppliedBonuses(attacker.getStats(),
        projectile.getEntityWorld().getRandom(),
        new AttackContext(attackerEntity), attackerEntity.getMainHandStack());
    this.attackerStats = attacker.getStats().copy();
  }

  @Override
  public void readFromNbt(CompoundTag tag) {
    // nothing yet
    this.attack =
        CsmAttack.fromTag(tag.getCompound("attack"), projectile.world);
    attackerStats.clear();
    ListTag statsTag = tag.getList("stats", NbtType.COMPOUND);
    for (Tag t : statsTag) {
      CompoundTag statTag = (CompoundTag) t;
      Stat stat =
          CsmRegistries.STAT.get(Identifier.tryParse(statTag.getString("id")));
      if (stat == null)
        continue;
      attackerStats.set(stat, statTag.getLong("amt"));
    }
    this.bowProgress = tag.getFloat("bowProgress");
  }

  @Override
  public void writeToNbt(CompoundTag tag) {
    tag.put("attack", attack.toTag());
    ListTag statsTag = new ListTag();
    for (StatContainer.Entry e : attackerStats) {
      CompoundTag statTag = new CompoundTag();
      statTag.putString("id", e.getKey().getId().toString());
      statTag.putLong("amt", e.getValue());
      statsTag.add(statTag);
    }
    tag.put("stats", statsTag);
    tag.putFloat("bowProgress", bowProgress);
  }

  public static ProjectileComponent getFor(ProjectileEntity e) {
    return CrackSlashMineComponents.PROJECTILE_COMPONENT.get(e);
  }
}
