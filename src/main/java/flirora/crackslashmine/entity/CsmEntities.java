package flirora.crackslashmine.entity;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.entity.spell.BallEntity;
import flirora.crackslashmine.entity.spell.CsmLightningEntity;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class CsmEntities {
  public static final EntityType<BallEntity> BALL = Registry.register(
      Registry.ENTITY_TYPE, new Identifier(CrackSlashMineMod.MOD_ID, "ball"),
      FabricEntityTypeBuilder.<BallEntity>create(SpawnGroup.MISC)
          .dimensions(EntityDimensions.fixed(0.5f, 0.5f)).build());
  public static final EntityType<CsmLightningEntity> LIGHTNING =
      Registry.register(Registry.ENTITY_TYPE,
          new Identifier(CrackSlashMineMod.MOD_ID, "lightning"),
          FabricEntityTypeBuilder.<CsmLightningEntity>create(SpawnGroup.MISC)
              .disableSaving().dimensions(EntityDimensions.fixed(0.0f, 0.0f))
              .trackRangeChunks(16).trackedUpdateRate(Integer.MAX_VALUE)
              .build());

  public static void load() {
    //
  }
}
