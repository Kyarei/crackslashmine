package flirora.crackslashmine.entity.spell;

import flirora.crackslashmine.core.CsmPrimaryAttackStat;
import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.entity.EntityUtils;
import flirora.crackslashmine.entity.projectile.ProjectileComponent;
import flirora.crackslashmine.item.spell.SpellParameters;
import flirora.crackslashmine.item.spell.data.Scrolllet;
import flirora.crackslashmine.item.spell.data.factory.ProjectileEntityFactory;
import flirora.crackslashmine.item.spell.data.target.SpellTargetContext;
import flirora.crackslashmine.network.entity.CsmEntitySpawnS2CPacket;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.entity.projectile.ProjectileUtil;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.Packet;
import net.minecraft.network.packet.s2c.play.EntitySpawnS2CPacket;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.EntityHitResult;
import net.minecraft.util.hit.HitResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public abstract class CsmBaseProjectileEntity extends ProjectileEntity {
  protected Scrolllet onCollideWithEntity, onCollideWithBlock;
  private SpellParameters params;

  public CsmBaseProjectileEntity(
      EntityType<? extends ProjectileEntity> entityType, World world,
      EntitySpawnS2CPacket packet) {
    super(entityType, world);
    this.setOwner(world.getEntityById(packet.getEntityData()));
    this.setPos(packet.getX(), packet.getY(), packet.getZ());
    this.setVelocity(packet.getVelocityX(), packet.getVelocityY(),
        packet.getVelocityZ());
    this.yaw = packet.getYaw();
    this.pitch = packet.getPitch();
  }

  public CsmBaseProjectileEntity(
      EntityType<? extends ProjectileEntity> entityType, World world,
      CsmPrimaryAttackStat attack, LivingEntity owner, Entity center,
      ProjectileEntityFactory.CreationContext context, SpellParameters params) {
    super(entityType, world);
    EntityUtils.initFromContext(this, center, context.base);
    ProjectileComponent csmProperties = ProjectileComponent.getFor(this);
    csmProperties.init(attack, owner, LivingEntityStats.getFor(owner));
    this.setOwner(owner);
    this.onCollideWithEntity = context.proj.onCollideWithEntity;
    this.onCollideWithBlock = context.proj.onCollideWithBlock;
    this.params = params;
  }

  @Override
  protected void initDataTracker() {
  }

  @Override
  public Packet<?> createSpawnPacket() {
    Entity entity = this.getOwner();
    int ownerId = entity == null ? 0 : entity.getEntityId();
    return CsmEntitySpawnS2CPacket
        .fromVanilla(new EntitySpawnS2CPacket(this, ownerId));
  }

  @Override
  public void tick() {
    super.tick();

    HitResult hitResult = ProjectileUtil.getCollision(this, this::method_26958);
    if (hitResult != null) {
      this.onCollision(hitResult);
    }

    Vec3d velocity = this.getVelocity();
    double newX = this.getX() + velocity.x;
    double newY = this.getY() + velocity.y;
    double newZ = this.getZ() + velocity.z;
    this.method_26962();
    this.updatePosition(newX, newY, newZ);
  }

  @Override
  public boolean collides() {
    return true;
  }

  @Override
  protected void onEntityHit(EntityHitResult entityHitResult) {
    super.onEntityHit(entityHitResult);
    Entity entity = entityHitResult.getEntity();
    entity.damage(DamageSource.magic(this, getOwner()), 1.0f);
    if (world instanceof ServerWorld && getOwner() instanceof LivingEntity) {
      SpellTargetContext context =
          new SpellTargetContext((LivingEntity) this.getOwner(), entity, this);
      onCollideWithEntity.execute(context, (ServerWorld) world, params);
    }
    this.destroy();
  }

  @Override
  protected void onBlockHit(BlockHitResult blockHitResult) {
    super.onBlockHit(blockHitResult);
    if (world instanceof ServerWorld && getOwner() instanceof LivingEntity) {
      SpellTargetContext context =
          new SpellTargetContext((LivingEntity) this.getOwner(), this, this);
      onCollideWithBlock.execute(context, (ServerWorld) world, params);
    }
    this.destroy();
  }

  @Override
  protected void readCustomDataFromTag(CompoundTag tag) {
    super.readCustomDataFromTag(tag);
    this.onCollideWithEntity = Scrolllet.SERIALIZER
        .deserialize(tag.getCompound("onCollideWithEntity"));
    this.onCollideWithBlock =
        Scrolllet.SERIALIZER.deserialize(tag.getCompound("onCollideWithBlock"));
  }

  @Override
  protected void writeCustomDataToTag(CompoundTag tag) {
    super.writeCustomDataToTag(tag);
    tag.put("onCollideWithEntity",
        Scrolllet.SERIALIZER.serialize(new CompoundTag(), onCollideWithEntity));
    tag.put("onCollideWithBlock",
        Scrolllet.SERIALIZER.serialize(new CompoundTag(), onCollideWithBlock));
  }
}
