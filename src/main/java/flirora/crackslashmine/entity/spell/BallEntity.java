package flirora.crackslashmine.entity.spell;

import flirora.crackslashmine.core.CsmPrimaryAttackStat;
import flirora.crackslashmine.core.DamageType;
import flirora.crackslashmine.entity.CsmEntities;
import flirora.crackslashmine.item.spell.SpellParameters;
import flirora.crackslashmine.item.spell.data.factory.ProjectileEntityFactory;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.data.TrackedDataHandlerRegistry;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.packet.s2c.play.EntitySpawnS2CPacket;
import net.minecraft.world.World;

public class BallEntity extends CsmBaseProjectileEntity {
  private static final TrackedData<Byte> DAMAGE_TYPE = DataTracker
      .registerData(BallEntity.class, TrackedDataHandlerRegistry.BYTE);

  public BallEntity(World world, EntitySpawnS2CPacket packet) {
    super(CsmEntities.BALL, world, packet);
  }

  public BallEntity(World world, LivingEntity owner, Entity center,
      DamageType type, CsmPrimaryAttackStat attack,
      ProjectileEntityFactory.CreationContext context, SpellParameters params) {
    super(CsmEntities.BALL, world, attack, owner, center, context, params);
    this.setDamageType(type);
  }

  public BallEntity(World world, LivingEntity owner, Entity center,
      DamageType type, long amountMin, long amountMax,
      ProjectileEntityFactory.CreationContext context, SpellParameters params) {
    this(world, owner, center, type,
        new CsmPrimaryAttackStat(
            new CsmPrimaryAttackStat.Entry(type, amountMin, amountMax)),
        context, params);
  }

  public static BallEntity spawn(World world, LivingEntity owner, Entity center,
      DamageType type, CsmPrimaryAttackStat attack,
      ProjectileEntityFactory.CreationContext context, SpellParameters params) {
    BallEntity b =
        new BallEntity(world, owner, center, type, attack, context, params);
    world.spawnEntity(b);
    return b;
  }

  public static BallEntity spawn(World world, LivingEntity owner, Entity center,
      DamageType type, long amountMin, long amountMax,
      ProjectileEntityFactory.CreationContext context, SpellParameters params) {
    BallEntity b = new BallEntity(world, owner, center, type, amountMin,
        amountMax, context, params);
    world.spawnEntity(b);
    return b;
  }

  @Override
  protected void initDataTracker() {
    super.initDataTracker();
    dataTracker.startTracking(DAMAGE_TYPE, (byte) 0);
  }

  public DamageType getDamageType() {
    return DamageType.values()[dataTracker.get(DAMAGE_TYPE)];
  }

  public void setDamageType(DamageType damageType) {
    dataTracker.set(DAMAGE_TYPE, (byte) damageType.ordinal());
  }

  @Override
  protected void writeCustomDataToTag(CompoundTag tag) {
    super.writeCustomDataToTag(tag);
    tag.putByte("damage_type", (byte) getDamageType().ordinal());
  }

  @Override
  protected void readCustomDataFromTag(CompoundTag tag) {
    super.readCustomDataFromTag(tag);
    int id = tag.getByte("damage_type");
    setDamageType(DamageType
        .values()[(id >= 0 && id < DamageType.values().length) ? id : 0]);
  }
}
