package flirora.crackslashmine.entity.spell;

import flirora.crackslashmine.CrackSlashMineMod;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.render.OverlayTexture;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.EntityRenderDispatcher;
import net.minecraft.client.render.entity.EntityRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.client.util.math.Vector3f;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Matrix3f;
import net.minecraft.util.math.Matrix4f;

@Environment(EnvType.CLIENT)
public class BallEntityRenderer extends EntityRenderer<BallEntity> {
    private static final Identifier TEXTURE = new Identifier(
            // Can't say that in Wizard101!
            CrackSlashMineMod.MOD_ID, "textures/entity/csm_spells/balls.png");
    private static final RenderLayer LAYER = RenderLayer.getEntityCutoutNoCull(TEXTURE);

    public BallEntityRenderer(EntityRenderDispatcher entityRenderDispatcher) {
        super(entityRenderDispatcher);
    }

    @Override
    public Identifier getTexture(BallEntity entity) {
        return TEXTURE;
    }

    @Override
    protected int getBlockLight(BallEntity entity, BlockPos blockPos) {
        return 15;
    }

    @Override
    public void render(BallEntity entity,
                       float yaw,
                       float tickDelta,
                       MatrixStack matrices,
                       VertexConsumerProvider vertexConsumers,
                       int light) {
        matrices.push();
        matrices.scale(2.0F, 2.0F, 2.0F);
        matrices.multiply(this.dispatcher.getRotation());
        matrices.multiply(Vector3f.POSITIVE_Y.getDegreesQuaternion(180.0F));
        MatrixStack.Entry top = matrices.peek();
        Matrix4f model = top.getModel();
        Matrix3f normal = top.getNormal();
        VertexConsumer vertexConsumer = vertexConsumers.getBuffer(LAYER);
        int type = entity.getDamageType().ordinal();
        produceVertex(vertexConsumer, model, normal, light, 0.0F, 0, type, 0, 1);
        produceVertex(vertexConsumer, model, normal, light, 1.0F, 0, type, 1, 1);
        produceVertex(vertexConsumer, model, normal, light, 1.0F, 1, type, 1, 0);
        produceVertex(vertexConsumer, model, normal, light, 0.0F, 1, type, 0, 0);
        matrices.pop();
        super.render(entity, yaw, tickDelta, matrices, vertexConsumers, light);
    }

    private static void produceVertex(VertexConsumer vertexConsumer,
                                      Matrix4f modelMatrix,
                                      Matrix3f normalMatrix,
                                      int light,
                                      float x,
                                      int y,
                                      int type,
                                      int textureU,
                                      int textureV) {
        float newV = (type + textureV) / 16.0f;
        vertexConsumer.vertex(modelMatrix, x - 0.5F, (float) y - 0.25F, 0.0F)
                .color(255, 255, 255, 255)
                .texture(textureU, newV)
                .overlay(OverlayTexture.DEFAULT_UV)
                .light(light)
                .normal(normalMatrix, 0.0F, 1.0F, 0.0F)
                .next();
    }
}
