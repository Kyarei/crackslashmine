package flirora.crackslashmine.entity.spell;

import flirora.crackslashmine.core.AttackContext;
import flirora.crackslashmine.core.CsmAttack;
import flirora.crackslashmine.core.LivingEntityStats;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LightningEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.world.World;

// LightningEntityMixin checks for this particular class
// TODO: currently we use the entity spawn packet for the vanilla version of 
// this entity, but if we wanted to add more rendering features to this, 
// we could create our own
public class CsmLightningEntity extends LightningEntity {
  private LivingEntity attacker;
  private CsmAttack attack;

  public CsmLightningEntity(EntityType<? extends LightningEntity> entityType,
      World world, LivingEntity attacker, CsmAttack attack) {
    super(entityType, world);
    LivingEntityStats attackerStats = LivingEntityStats.getFor(attacker);
    this.attacker = attacker;
    this.attack = attack.withAppliedBonuses(attackerStats.getStats(),
        attacker.getRandom(), new AttackContext(attacker));
  }

  public CsmAttack getAttack() {
    return attack;
  }

  public LivingEntity getAttacker() {
    return attacker;
  }
}
