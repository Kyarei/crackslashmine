package flirora.crackslashmine.entity;

import java.util.List;
import java.util.function.Predicate;

import org.jetbrains.annotations.NotNull;

import flirora.crackslashmine.item.spell.data.factory.BaseEntityFactory;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;

public class EntityUtils {
  public static List<@NotNull LivingEntity> getAround(Entity e, double x,
      double y, double z, Predicate<? super LivingEntity> predicate) {
    Vec3d pos = e.getPos();
    return e.getEntityWorld().getEntitiesByClass(
        LivingEntity.class, new Box(pos.getX() - x, pos.getY() - y,
            pos.getZ() - z, pos.getX() + x, pos.getY() + y, pos.getZ() + z),
        predicate);
  }

  public static boolean isInRange(Entity which, LivingEntity target,
      double distance, double maxAngleDiff) {
    if (which == target)
      return false;
    if (target.squaredDistanceTo(which) > distance * distance)
      return false;
    double angleToTarget = Math.toDegrees(MathHelper
        .atan2(-(target.getX() - which.getX()), target.getZ() - which.getZ()));
    double angleDiff =
        MathHelper.wrapDegrees(angleToTarget - which.getHeadYaw());
    return Math.abs(angleDiff) <= maxAngleDiff;
  }

  public static void initFromContext(ProjectileEntity projectile, Entity center,
      BaseEntityFactory.Context context) {
    projectile.yaw = center.yaw + context.yawOffset;
    projectile.pitch = center.pitch + context.pitchOffset;
    double cy = Math.cos(Math.toRadians(projectile.yaw));
    double sy = Math.sin(Math.toRadians(projectile.yaw));
    double xoff = context.frontOff * cy + context.sideOff * sy;
    double zoff = context.frontOff * sy - context.sideOff * cy;
    projectile.setPos(center.getX() + xoff, center.getEyeY() + context.yOff,
        center.getZ() + zoff);
    projectile.setProperties(center, projectile.pitch, projectile.yaw, 0.0f,
        context.speed, context.divergence);
  }
}
