package flirora.crackslashmine.rei;

import com.google.common.collect.ImmutableList;
import flirora.crackslashmine.item.recipe.MixingRecipe;
import me.shedaniel.rei.api.EntryStack;
import me.shedaniel.rei.api.TransferRecipeDisplay;
import me.shedaniel.rei.server.ContainerInfo;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.Ingredient;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.util.Identifier;

import java.util.List;
import java.util.Optional;

public class MixingRecipeDisplay implements TransferRecipeDisplay {
    private final MixingRecipe recipe;
    private final List<List<EntryStack>> input;
    private final List<List<EntryStack>> output;

    public MixingRecipeDisplay(MixingRecipe recipe) {
        this.recipe = recipe;
        this.input = ImmutableList.of(EntryStack.ofIngredient(recipe.getInput()));
        this.output = ImmutableList.of(ImmutableList.of(
                EntryStack.create(recipe.getOutput())));
    }

    @Override
    public int getWidth() {
        return 1;
    }

    @Override
    public int getHeight() {
        return 1;
    }

    @Override
    public List<List<EntryStack>> getOrganisedInputEntries(ContainerInfo<ScreenHandler> containerInfo, ScreenHandler container) {
        return input;
    }

    @Override
    public List<List<EntryStack>> getInputEntries() {
        return input;
    }

    @Override
    public List<List<EntryStack>> getRequiredEntries() {
        return input;
    }

    @Override
    public List<List<EntryStack>> getResultingEntries() {
        return output;
    }

    @Override
    public Identifier getRecipeCategory() {
        return null;
    }

    @Override
    public Optional<Identifier> getRecipeLocation() {
        return Optional.ofNullable(recipe).map(MixingRecipe::getId);
    }

    public Ingredient getInput() {
        return recipe.getInput();
    }

    public ItemStack getOutput() {
        return recipe.getOutput();
    }

    public double getHue() {
        return recipe.getHue();
    }

    public double getMagnitude() {
        return recipe.getMagnitude();
    }
}
