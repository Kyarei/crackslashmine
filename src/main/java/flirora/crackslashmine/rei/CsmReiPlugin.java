package flirora.crackslashmine.rei;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.block.MixingBowlBlock;
import flirora.crackslashmine.item.recipe.MixingRecipe;
import me.shedaniel.rei.api.EntryStack;
import me.shedaniel.rei.api.RecipeHelper;
import me.shedaniel.rei.api.plugins.REIPluginV0;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class CsmReiPlugin implements REIPluginV0 {
  public static final Identifier PLUGIN =
      new Identifier(CrackSlashMineMod.MOD_ID, "rei_plugin");
  public static final Identifier MIXING =
      new Identifier(CrackSlashMineMod.MOD_ID, "mixing");

  @Override
  public Identifier getPluginIdentifier() {
    return PLUGIN;
  }

  @Override
  public void registerPluginCategories(RecipeHelper recipeHelper) {
    REIPluginV0.super.registerPluginCategories(recipeHelper);
    recipeHelper.registerCategory(new MixingRecipeCategory());
  }

  @Override
  public void registerRecipeDisplays(RecipeHelper recipeHelper) {
    REIPluginV0.super.registerRecipeDisplays(recipeHelper);
    recipeHelper.registerRecipes(MIXING, MixingRecipe.class,
        MixingRecipeDisplay::new);
  }

  @Override
  public void registerOthers(RecipeHelper recipeHelper) {
    REIPluginV0.super.registerOthers(recipeHelper);
    recipeHelper.registerWorkingStations(MIXING,
        EntryStack.create(Registry.ITEM.get(MixingBowlBlock.ID)));
    recipeHelper.removeAutoCraftButton(MIXING);
  }

}
