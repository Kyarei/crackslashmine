package flirora.crackslashmine.rei;

import com.google.common.collect.Lists;
import flirora.crackslashmine.block.MixingBowlBlock;
import it.unimi.dsi.fastutil.ints.IntList;
import me.shedaniel.math.Point;
import me.shedaniel.math.Rectangle;
import me.shedaniel.rei.api.EntryStack;
import me.shedaniel.rei.api.TransferRecipeCategory;
import me.shedaniel.rei.api.widgets.Widgets;
import me.shedaniel.rei.gui.widget.Widget;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.resource.language.I18n;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import java.text.NumberFormat;
import java.util.List;

public class MixingRecipeCategory implements TransferRecipeCategory<MixingRecipeDisplay> {
    @Override
    public List<Widget> setupDisplay(MixingRecipeDisplay display, Rectangle bounds) {
        Point startPoint = new Point(bounds.getCenterX() - 41, bounds.y + 10);
        NumberFormat df = NumberFormat.getIntegerInstance();
        double magnitude = display.getMagnitude();
        double hue = display.getHue();
        List<Widget> widgets = Lists.newArrayList();
        widgets.add(Widgets.createRecipeBase(bounds));
        widgets.add(Widgets.createResultSlotBackground(new Point(startPoint.x + 61, startPoint.y + 5)));
        widgets.add(Widgets.createLabel(new Point(bounds.x + 10, bounds.y + 40),
                new TranslatableText("category.crackslashmine.mixing.label", df.format(magnitude), df.format(hue)))
                .noShadow()
                .leftAligned()
                .color(0xFF404040, 0xFFBBBBBB));
        widgets.add(Widgets.createArrow(new Point(startPoint.x + 24, startPoint.y + 4)).animationDurationTicks(magnitude / 0.01));
        widgets.add(Widgets.createSlot(new Point(startPoint.x + 1, startPoint.y + 5)).entries(display.getInputEntries().get(0)).markInput());
        widgets.add(Widgets.createSlot(new Point(startPoint.x + 61, startPoint.y + 5)).entries(display.getResultingEntries().get(0)).disableBackground().markOutput());
        return widgets;
    }

    @Override
    public void renderRedSlots(MatrixStack matrices, List<Widget> widgets, Rectangle bounds, MixingRecipeDisplay display, IntList redSlots) {
        Point startPoint = new Point(bounds.getCenterX() - 41, bounds.getCenterY() - 27);
        matrices.push();
        matrices.translate(0, 0, 400);
        if (redSlots.contains(0)) {
            DrawableHelper.fill(
                    matrices,
                    startPoint.x + 1, startPoint.y + 5,
                    startPoint.x + 1 + 16, startPoint.y + 5 + 16,
                    1090453504);
        }
        matrices.pop();
    }

    @Override
    public Identifier getIdentifier() {
        return CsmReiPlugin.MIXING;
    }

    @Override
    public EntryStack getLogo() {
        return EntryStack.create(Registry.BLOCK.get(MixingBowlBlock.ID));
    }

    @Override
    public String getCategoryName() {
        return I18n.translate("category.crackslashmine.mixing");
    }

    @Override
    public int getDisplayHeight() {
        return 54;
    }
}
