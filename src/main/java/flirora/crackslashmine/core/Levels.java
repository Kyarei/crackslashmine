package flirora.crackslashmine.core;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.generation.AreaManager;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.Vec3d;

public class Levels {
    public static int positionToLevel(ServerWorld w, Vec3d position) {
        return AreaManager.getFor(w).getOrCreateSync(position).getLevel();
    }

    public static double getHealthScaleFactor(int level) {
        return CrackSlashMineMod.config.environmentalDamageScaling.value(level);
    }

    public static long getXpForNextLevel(int currentLevel) {
        return Math.round(CrackSlashMineMod.config.xpCurve.value(currentLevel));
    }
}
