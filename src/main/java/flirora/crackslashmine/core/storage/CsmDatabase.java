package flirora.crackslashmine.core.storage;

import flirora.crackslashmine.CrackSlashMineMod;
import net.minecraft.server.world.ServerWorld;
import org.apache.logging.log4j.Level;
import org.mapdb.DB;
import org.mapdb.DBMaker;

import java.io.Closeable;
import java.io.IOException;
import java.nio.file.Path;

// TODO: extract MapDB-related functionality into a separate library mod
// TODO: Data here is saved, even without an explicit save operation.
// Revisit when snapshotting is supported (MapDB issue #831).
public class CsmDatabase implements Closeable {
    private final DB db;

    public CsmDatabase(Path file) throws IOException {
        DBMaker.Maker maker = DBMaker.fileDB(file.toFile())
                .cleanerHackEnable();

        if (CrackSlashMineMod.config.enableDbTransaction) {
            maker.transactionEnable();
        }

        if (CrackSlashMineMod.config.enableDbMmap) {
            maker.fileMmapEnableIfSupported();
        }

        this.db = maker.make();
    }

    public DB getDb() {
        if (db.isClosed())
            throw new IllegalStateException("Attempt to access database after closing");
        return db;
    }

    @Override
    public void close() throws IOException {
        db.close();
        CrackSlashMineMod.log(Level.INFO, "Closed database file");
    }

    public static CsmDatabase fromServerWorld(ServerWorld w) {
        return ((CsmDatabaseProvider) w).getDatabase();
    }
}
