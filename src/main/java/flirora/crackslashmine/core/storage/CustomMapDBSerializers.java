package flirora.crackslashmine.core.storage;

import net.minecraft.util.Identifier;
import org.jetbrains.annotations.NotNull;
import org.mapdb.DataInput2;
import org.mapdb.DataOutput2;
import org.mapdb.Serializer;

import java.io.IOException;

public class CustomMapDBSerializers {
    public static final Serializer<Identifier> IDENTIFIER = new Serializer<Identifier>() {
        @Override
        public void serialize(@NotNull DataOutput2 out, @NotNull Identifier value) throws IOException {
            Serializer.STRING.serialize(out, value.getNamespace());
            Serializer.STRING.serialize(out, value.getPath());
        }

        @Override
        public Identifier deserialize(@NotNull DataInput2 input, int available) throws IOException {
            return new Identifier(
                    Serializer.STRING.deserialize(input, available),
                    Serializer.STRING.deserialize(input, available)
            );
        }
    };
}
