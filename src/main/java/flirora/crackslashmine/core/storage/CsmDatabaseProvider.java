package flirora.crackslashmine.core.storage;

public interface CsmDatabaseProvider {
    CsmDatabase getDatabase();
}
