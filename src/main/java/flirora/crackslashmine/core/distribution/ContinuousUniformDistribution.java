package flirora.crackslashmine.core.distribution;

import java.util.Random;

public class ContinuousUniformDistribution implements ContinuousDistribution {
  private final double min, max;

  public ContinuousUniformDistribution(double min, double max) {
    super();
    this.min = min;
    this.max = max;
  }

  @Override
  public double sample(Random r) {
    return min + (max - min) * r.nextDouble();
  }

  public double getMin() {
    return min;
  }

  public double getMax() {
    return max;
  }
}
