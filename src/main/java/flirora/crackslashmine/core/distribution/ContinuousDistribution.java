package flirora.crackslashmine.core.distribution;

import java.util.Random;

public interface ContinuousDistribution {
  double sample(Random r);
}
