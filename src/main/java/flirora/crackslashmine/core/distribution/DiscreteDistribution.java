package flirora.crackslashmine.core.distribution;

import java.util.Random;

public interface DiscreteDistribution {
  long sample(Random r);
}
