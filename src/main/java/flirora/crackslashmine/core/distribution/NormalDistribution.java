package flirora.crackslashmine.core.distribution;

import java.util.Random;

public class NormalDistribution implements ContinuousDistribution {
  private final double mean, sd;

  public NormalDistribution(double mean, double sd) {
    super();
    this.mean = mean;
    this.sd = sd;
  }

  public double getMean() {
    return mean;
  }

  public double getSd() {
    return sd;
  }

  @Override
  public double sample(Random r) {
    return mean + sd * r.nextGaussian();
  }

}
