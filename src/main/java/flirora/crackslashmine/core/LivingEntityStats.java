package flirora.crackslashmine.core;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Level;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import dev.emi.trinkets.api.TrinketComponent;
import dev.emi.trinkets.api.TrinketSlots;
import dev.emi.trinkets.api.TrinketsApi;
import dev.onyxstudios.cca.api.v3.component.ComponentV3;
import dev.onyxstudios.cca.api.v3.entity.PlayerComponent;
import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.advancement.CsmCriteria;
import flirora.crackslashmine.core.stat.Stat;
import flirora.crackslashmine.core.stat.StatContainer;
import flirora.crackslashmine.core.stat.StatView;
import flirora.crackslashmine.core.stat.Stats;
import flirora.crackslashmine.core.xse.ExtendedStatusEffectInstance;
import flirora.crackslashmine.core.xse.StatBoostExtendedStatusEffect;
import flirora.crackslashmine.item.equipment.CompatibleItemDescription;
import flirora.crackslashmine.item.equipment.CompatibleItemManager;
import flirora.crackslashmine.item.equipment.EquipmentComponent;
import flirora.crackslashmine.item.equipment.EquipmentUtils;
import flirora.crackslashmine.network.DamageSplatS2C;
import flirora.crackslashmine.network.HealSplatS2C;
import flirora.crackslashmine.network.MiscSplatS2C;
import it.unimi.dsi.fastutil.objects.Object2LongMap;
import nerdhub.cardinal.components.api.util.sync.EntitySyncedComponent;
import net.fabricmc.fabric.api.util.NbtType;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;

/**
 * Data about a living entity.
 * <p>
 * Note that some of the information in this class is not used by mobs.
 */
public class LivingEntityStats implements ResourceLimitComponent, ComponentV3,
    EntitySyncedComponent, PlayerComponent<LivingEntityStats> {
  private final LivingEntity e;
  private CsmCharacter character;
  private StatContainer stats;
  private long health, mana, stamina;
  private final EnumMap<EquipmentSlot, ItemStack> equipmentStacks =
      new EnumMap<>(EquipmentSlot.class);
  private final Map<TrinketSlots.Slot, ItemStack> trinketStacks;
  private double playerDamage, nonPlayerDamage;
  private final ArrayList<ExtendedStatusEffectInstance> currentEffects;
  private boolean initialized = false;
  private boolean shouldSync = false;

  /**
   * Partially construct a component. The {@link LivingEntityStats#initMob()}
   * method must be called afterwards once the entity's position has been
   * determined.
   *
   * @param e         the entity that this component describes
   * @param character default character until overwritten by
   *                  {@link LivingEntityStats#initMob()}
   */
  public LivingEntityStats(LivingEntity e, CsmCharacter character) {
    this.e = e;
    this.character = character;
    this.health = 1;
    this.currentEffects = new ArrayList<>();
    this.stats = new StatContainer();
    trinketStacks = e instanceof PlayerEntity ? new HashMap<>() : null;
  }

  public LivingEntityStats(LivingEntity e) {
    this(e, new CsmCharacter());
  }

  private void scheduleSync() {
    shouldSync = true;
  }

  /**
   * Initialize things that couldn't be done in the constructor.
   */
  public void initMob() { // blursed two-phase initialization
    if (e.getEntityWorld().isClient()) {
      initialized = true;
      recalculateStats();
      initialized = false;
      return; // wait for sync packet instead
    }
    initialized = true;
    character = CsmCharacter.fromEntity(e);
    recalculateStats();
    this.health = getMaxHealth();
    this.mana = getMaxMana();
    this.stamina = getMaxStamina();
  }

  private void recalculateStats() {
    if (!initialized) {
      CrackSlashMineMod.log(Level.WARN,
          "Tried to recalculate stats when initMob() has not been " + "called");
      return;
    }
    StatContainer effectBonuses = new StatContainer();
    for (ExtendedStatusEffectInstance inst : currentEffects) {
      if (inst.getEffect() instanceof StatBoostExtendedStatusEffect) {
        StatBoostExtendedStatusEffect effect =
            (StatBoostExtendedStatusEffect) inst.getEffect();
        effectBonuses.add(effect.getStat(), inst.getIntensity());
      }
    }
    StatContainer result = new StatContainer();
    EnumMap<EquipmentSlot, Object2LongMap<Stat>> equipments =
        new EnumMap<>(EquipmentSlot.class);
    for (EquipmentSlot slot : EquipmentSlot.values()) {
      ItemStack equipment = e.getEquippedStack(slot);
      EquipmentComponent component = EquipmentComponent.getFor(equipment);
      CompatibleItemDescription desc = CompatibleItemManager.INSTANCE
          .get(Registry.ITEM.getId(equipment.getItem()));
      boolean slotIsValid = desc == null || desc.getType().isValidInSlot(slot);
      if (component == null
          || character.getLevel() < component.getMeta().getLevel()
          || !slotIsValid || equipment.isEmpty()) {
        equipments.put(slot, EquipmentUtils.NO_STATS);
      } else {
        Object2LongMap<Stat> equipmentStats = component.getNetStatBonuses();
        equipments.put(slot, equipmentStats);
      }
      equipmentStacks.put(slot, equipment);
    }
    Map<TrinketSlots.Slot, Object2LongMap<Stat>> trinkets = new HashMap<>();
    if (e instanceof PlayerEntity) {
      TrinketComponent c = TrinketsApi.getTrinketComponent((PlayerEntity) e);
      for (TrinketSlots.Slot s : TrinketSlots.getAllSlots()) {
        String group = s.getSlotGroup().getName();
        String slot = s.getName();
        ItemStack equipment = c.getStack(group, slot);
        EquipmentComponent component = EquipmentComponent.getFor(equipment);
        CompatibleItemDescription desc = CompatibleItemManager.INSTANCE
            .get(Registry.ITEM.getId(equipment.getItem()));
        boolean slotIsValid =
            desc == null || desc.getType().isValidInTrinketSlot(group, slot);
        if (component == null
            || character.getLevel() < component.getMeta().getLevel()
            || !slotIsValid || equipment.isEmpty()) {
          trinkets.put(s, EquipmentUtils.NO_STATS);
        } else {
          Object2LongMap<Stat> equipmentStats = component.getNetStatBonuses();
          trinkets.put(s, equipmentStats);
        }
        trinketStacks.put(s, equipment);
      }
    }
    for (Stat stat : Stats.TOPOLOGICAL_ORDER) {
      // Get stat bonuses from equipment
      long value = stat.getBase(character, e);
      for (EquipmentSlot slot : EquipmentSlot.values()) {
        Object2LongMap<Stat> equipmentStats = equipments.get(slot);
        value += equipmentStats.getOrDefault(stat, 0);
      }
      if (e instanceof PlayerEntity) {
        for (TrinketSlots.Slot s : TrinketSlots.getAllSlots()) {
          Object2LongMap<Stat> trinketStats = trinkets.get(s);
          value += trinketStats.getOrDefault(stat, 0);
        }
      }
      value += effectBonuses.get(stat);
      value = stat.postprocess(value, result);
      result.set(stat, value);
    }
    stats = result;
    health = Math.min(health, getMaxHealth());
    mana = Math.min(mana, getMaxMana());
    stamina = Math.min(stamina, getMaxStamina());
    scheduleSync();
  }

  private boolean isRecalculationNeeded() {
    for (EquipmentSlot slot : EquipmentSlot.values()) {
      ItemStack equipment = e.getEquippedStack(slot);
      ItemStack prevEquipment = equipmentStacks.get(slot);
      if (!ItemStack.areEqual(equipment, prevEquipment)) {
        return true;
      }
    }
    if (e instanceof PlayerEntity) {
      TrinketComponent c = TrinketsApi.getTrinketComponent((PlayerEntity) e);
      for (TrinketSlots.Slot s : TrinketSlots.getAllSlots()) {
        String group = s.getSlotGroup().getName();
        String slot = s.getName();
        ItemStack equipment = c.getStack(group, slot);
        ItemStack prevEquipment = trinketStacks.get(s);
        if (!ItemStack.areEqual(equipment, prevEquipment)) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Runs tasks that should be done each tick.
   */
  public void tick() {
    if (shouldSync || e.age % 20 == 0) {
      sync();
      shouldSync = false;
    }
    if (getHealth() <= 0)
      return; // You're dead, man! Dead!
    if (!e.getEntityWorld().isClient()) {
      if (isRecalculationNeeded())
        recalculateStats();
      for (ExtendedStatusEffectInstance effect : currentEffects) {
        effect.tick(e, this);
      }
    } else {
      for (ExtendedStatusEffectInstance effect : currentEffects) {
        effect.tickClient();
      }
    }
    if (currentEffects.removeIf(effect -> effect.getDuration() <= 0)) {
      recalculateStats();
    }
  }

  @Override
  public long getMaxHealth() {
    return stats.get(Stats.HEALTH_BONUS);
  }

  @Override
  public long getMaxMana() {
    return stats.get(Stats.MANA_BONUS);
  }

  @Override
  public long getMaxStamina() {
    return stats.get(Stats.STAMINA_BONUS);
  }

  public long getHealth() {
    return health;
  }

  public void setHealth(long health) {
    health = Math.min(health, getMaxHealth());
    boolean shouldSync = health != this.health;
    this.health = health;
    if (shouldSync)
      scheduleSync();
  }

  private void applyDamage(long amt) {
    setHealth(health - amt);
    scheduleSync();
  }

  /**
   * Applies damage to the entity.
   *
   * @param attacker the {@link LivingEntity} that dealt the damage, or null if
   *                 the damage was environmental
   * @param attack   the {@link CsmAttack} that describes the type and amount
   * @param source   the {@link DamageSource} for this damage
   */
  public void applyDamage(@Nullable LivingEntity attacker, CsmAttack attack,
      DamageSource source) {
    World world = e.getEntityWorld();
    if (!attack.isInfallible()) {
      int level = attacker != null
          ? LivingEntityStats.getFor(attacker).character.getLevel()
          : Levels.positionToLevel((ServerWorld) e.getEntityWorld(),
              e.getPos());
      long dodgeRating = stats.get(Stats.DODGE_RATING);
      long accuracy = attacker != null
          ? LivingEntityStats.getFor(attacker).getStat(Stats.ACCURACY)
          : 0;
      long netDodgeRating = dodgeRating - accuracy;
      if (netDodgeRating > 0 && MathUtils.nextLong(e.getRandom(),
          10000 + netDodgeRating + 100 * level) < netDodgeRating) {
        MiscSplatS2C.create(world, e, MiscSplatS2C.Type.DODGED);
        return;
      }
    }
    CsmAttack withResist = attack.withAppliedResistances(stats.frozen())
        .scaled(1.0 / (1.0 + 0.03 * EnchantmentHelper
            .getProtectionAmount(e.getArmorItems(), source)));
    long total = withResist.getEntries().stream()
        .mapToLong(CsmAttack.Entry::getAmount).sum();
    applyDamage(total);
    DamageSplatS2C.create(world, e, attack);
    for (ExtendedStatusEffectInstance effect : attack.getEffects()) {
      applyEffect(effect);
    }
    if (attacker instanceof PlayerEntity)
      playerDamage += total;
    else
      nonPlayerDamage += total;
  }

  /**
   * Heals the entity.
   *
   * @param amt         the amount to heal
   * @param applyBoosts whether boosts on the target should be applied; e.g.
   *                    this is false for natural regeneration
   */
  public void heal(long amt, boolean applyBoosts) {
    long total = amt; // don't touch this yet; I have bigger plannim
    if (applyBoosts) {
      total = amt * (10000 + getStat(Stats.INCOMING_HEAL_MULTI)) / 10000;
    }
    setHealth(getHealth() + total);
    HealSplatS2C.create(e.getEntityWorld(), e, total);
    if (shouldSync)
      scheduleSync();
  }

  /**
   * Heals the entity.
   *
   * @param healerStats the {@link LivingEntityStats} of the healer
   * @param amt         the amount to heal
   */
  public void healFrom(LivingEntityStats healerStats, long amt) {
    long adjusted =
        amt * (10000 + healerStats.getStat(Stats.OUTGOING_HEAL_MULTI)) / 10000;
    heal(adjusted, true);
  }

  /**
   * Get the proportion of damage that was dealt by a player, rather than by a
   * mob or from anothersource.
   *
   * @return the proportion
   */
  public double getProportionOfDamageFromPlayer() {
    return playerDamage / (playerDamage + nonPlayerDamage);
  }

  public long getMana() {
    return mana;
  }

  public void setMana(long mana) {
    mana = Math.min(mana, getMaxMana());
    boolean shouldSync = mana != this.mana;
    this.mana = mana;
    if (shouldSync)
      scheduleSync();
  }

  public long getStamina() {
    return stamina;
  }

  public void setStamina(long stamina) {
    stamina = Math.min(stamina, getMaxStamina());
    boolean shouldSync = stamina != this.stamina;
    this.stamina = stamina;
    if (shouldSync)
      scheduleSync();
  }

  @Override
  public int getLevel() {
    return character.getLevel();
  }

  public long getXp() {
    return character.getXp();
  }

  public void addXp(long xp) {
    int oldLevel = getLevel();
    character.addXp(xp);
    if (getLevel() != oldLevel)
      recalculateStats();
    if (e instanceof ServerPlayerEntity) {
      CsmCriteria.COMBAT_LEVEL.trigger((ServerPlayerEntity) e);
    }
    scheduleSync();
  }

  public double getXpPercent() {
    return ((double) character.getXp()) / Levels.getXpForNextLevel(getLevel());
  }

  public long getIntrinsicFundamental(Fundamental f) {
    return character.getIntrinsicFundamental(f);
  }

  public boolean incrementFundamental(Fundamental f) {
    boolean success = character.incrementFundamental(f);
    if (success)
      recalculateStats();
    return success;
  }

  public int statPointsRemaining() {
    return character.statPointsRemaining();
  }

  public void applyEffect(ExtendedStatusEffectInstance effect) {
    currentEffects.add(effect);
    scheduleSync();
  }

  public List<ExtendedStatusEffectInstance> getEffects() {
    return currentEffects;
  }

  @Override
  public @NotNull Entity getEntity() {
    return e;
  }

  // Important: check the next six methods whenever you add a field to
  // this class!

  @Override
  public void readFromNbt(CompoundTag tag) {
    character.readFromTag(tag);
    health = tag.getLong("health");
    mana = tag.getLong("mana");
    stamina = tag.getLong("stamina");
    playerDamage = tag.getDouble("playerDamage");
    nonPlayerDamage = tag.getDouble("nonPlayerDamage");
    currentEffects.clear();
    ListTag effectTag = tag.getList("effects", NbtType.COMPOUND);
    for (Tag t : effectTag) {
      CompoundTag effect = (CompoundTag) t;
      currentEffects.add(
          ExtendedStatusEffectInstance.fromTag(effect, e.getEntityWorld()));
    }
    // We need to store this anyway, since bee data is stored as NBT
    // in beehives and a bee won't have an initialized LivingEntityStats
    // when it pops out of its hive for the first time.
    // We could use some other approach (such as initializing bee stats
    // when the hive generates), but this approach is more likely to
    // work with modded things that store mob data in NBT.
    initialized = tag.getBoolean("initialized");
    if (initialized)
      recalculateStats();
  }

  @Override
  public void writeToNbt(CompoundTag tag) {
    if (character != null)
      character.writeToTag(tag);
    tag.putLong("health", health);
    tag.putLong("mana", mana);
    tag.putLong("stamina", stamina);
    tag.putDouble("playerDamage", playerDamage);
    tag.putDouble("nonPlayerDamage", nonPlayerDamage);
    ListTag effectTag = new ListTag();
    for (ExtendedStatusEffectInstance effect : currentEffects) {
      effectTag.add(effect.toTag());
    }
    tag.put("effects", effectTag);
    tag.putBoolean("initialized", initialized);
  }

  @Override
  public boolean shouldCopyForRespawn(boolean lossless, boolean keepInventory) {
    return true;
  }

  @Override
  public void copyForRespawn(LivingEntityStats original, boolean lossless,
      boolean keepInventory) {
    System.err.println("copyForRespawn");
    this.character = original.character;
    recalculateStats();
    this.health = getMaxHealth();
    this.mana = getMaxMana();
    this.stamina = getMaxStamina();
  }

  @Override
  public void writeToPacket(PacketByteBuf buf) {
    character.write(buf);
    buf.writeLong(health);
    buf.writeLong(mana);
    buf.writeLong(stamina);
    buf.writeInt(currentEffects.size());
    for (ExtendedStatusEffectInstance effect : currentEffects) {
      effect.writeToPacket(buf);
    }
    boolean syncStats = CrackSlashMineMod.config.syncStats;
    buf.writeBoolean(syncStats);
    if (syncStats) {
      buf.writeInt(stats.size());
      for (StatContainer.Entry e : stats) {
        buf.writeInt(CsmRegistries.STAT.getRawId(e.getKey()));
        buf.writeLong(e.getValue());
      }
    }
  }

  @Override
  public void readFromPacket(PacketByteBuf buf) {
    character.read(buf);
    health = buf.readLong();
    mana = buf.readLong();
    stamina = buf.readLong();
    int nEffects = buf.readInt();
    currentEffects.clear();
    for (int i = 0; i < nEffects; ++i) {
      currentEffects.add(
          ExtendedStatusEffectInstance.readFromPacket(buf, e.getEntityWorld()));
    }
    initialized = true;
    boolean syncStats = buf.readBoolean();
    if (syncStats) {
      int n = buf.readInt();
      for (int i = 0; i < n; ++i) {
        Stat stat = CsmRegistries.STAT.get(buf.readInt());
        long value = buf.readLong();
        if (stat == null)
          continue;
        stats.set(stat, value);
      }
    } else {
      recalculateStats();
    }
  }

  /**
   * Gets the {@link LivingEntityStats} associated with a {@link LivingEntity}.
   *
   * @param e the entity
   * @return the component
   */
  public static LivingEntityStats getFor(LivingEntity e) {
    LivingEntityStats res = CrackSlashMineComponents.LIVING_ENTITY_STATS.get(e);
    if (!res.initialized)
      res.initMob();
    return res;
  }

  public long getStat(Stat stat) {
    return stats.get(stat);
  }

  public void regenerate(int tickno) {
    heal(MathUtils.clock(getStat(Stats.HEALTH_REGEN), 2000, tickno), false);
    setMana(mana + MathUtils.clock(getStat(Stats.MANA_REGEN), 2000, tickno));
    setStamina(
        stamina + MathUtils.clock(getStat(Stats.STAMINA_REGEN), 2000, tickno));
  }

  public StatView getStats() {
    return stats.frozen();
  }

  public static void dealDamage(LivingEntity e, @Nullable LivingEntity attacker,
      CsmAttack attack, DamageSource source) {
    if (e.isDead())
      return;
    LivingEntityStats stats = LivingEntityStats.getFor(e);
    float amt = attack.totalDamage() * e.getMaxHealth() / stats.getMaxHealth();
    float adjusted = ((RawDamageable) e).damageRaw(source, amt);
    if (adjusted <= 0.0f)
      return;
    e.getDamageTracker().onDamage(source, e.getHealth(), adjusted);
    stats.applyDamage(attacker, attack.scaled(adjusted / amt), source);
    if (e.isDead()) {
      e.onDeath(source);
    }
  }
}
