package flirora.crackslashmine.core;

/**
 * A fundamental stat. Players can increase the base value of these stats
 * by spending attribute points.
 */
public enum Fundamental {
    STRENGTH("strength"),
    DEXTERITY("dexterity"),
    INTELLECT("intellect"),
    WISDOM("wisdom"),
    ENDURANCE("endurance"),
    FORTITUDE("fortitude"),
    ;

    private final String name;

    Fundamental(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
