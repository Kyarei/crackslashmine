package flirora.crackslashmine.core;

import java.util.EnumMap;
import java.util.Map;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;

/**
 * A class of damage, either physical or elemental.
 */
public enum DamageType {
  PHYSICAL("physical", 0xFFFFFF, Double.NaN), FIRE("fire", 0xFC9E23, 0.0),
  WATER("water", 0x23ACFC, 240.0), EARTH("earth", 0x36D167, 120.0),
  ICE("ice", 0xB0F4ED, 180.0), LIGHTNING("lightning", 0xE0A4F9, 300.0),
  WIND("wind", 0xDEEFEC, 60.0), LIGHT("light", 0xF8F9C5, 90.0),
  DARKNESS("darkness", 0x884ACE, 270.0),;

  private final String name;
  private final int color;
  private final double hue;

  DamageType(String name, int color, double hue) {
    this.name = name;
    this.color = color;
    this.hue = hue;
  }

  public String getName() {
    return name;
  }

  public int getColor() {
    return color;
  }

  public double getHue() {
    return hue;
  }

  public static DamageType randomElemental(Random r) {
    DamageType[] values = DamageType.values();
    return values[1 + r.nextInt(values.length - 1)];
  }

  public static <T> EnumMap<DamageType, T> forEachDamageType(
      Function<DamageType, T> f) {
    EnumMap<DamageType, T> res = new EnumMap<>(DamageType.class);
    for (DamageType d : DamageType.values()) {
      res.put(d, f.apply(d));
    }
    return res;
  }

  public static final Map<String, DamageType> BY_NAME =
      Stream.of(DamageType.values())
          .collect(Collectors.toMap(DamageType::getName, d -> d));

  public static final Codec<DamageType> CODEC =
      Codec.STRING.comapFlatMap(name -> {
        DamageType type = BY_NAME.get(name);
        return type == null ? DataResult.error("unknown damage type: " + name)
            : DataResult.success(type);
      }, DamageType::getName);
}
