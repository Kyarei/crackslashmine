package flirora.crackslashmine.core;

import dev.onyxstudios.cca.api.v3.component.ComponentKey;
import dev.onyxstudios.cca.api.v3.component.ComponentRegistryV3;
import dev.onyxstudios.cca.api.v3.entity.EntityComponentFactoryRegistry;
import dev.onyxstudios.cca.api.v3.entity.EntityComponentInitializer;
import dev.onyxstudios.cca.api.v3.item.ItemComponentFactoryRegistry;
import dev.onyxstudios.cca.api.v3.item.ItemComponentInitializer;
import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.player.PlayerProfile;
import flirora.crackslashmine.entity.projectile.ProjectileComponent;
import flirora.crackslashmine.item.equipment.EquipmentComponent;
import flirora.crackslashmine.item.essence.tonat.TonatComponent;
import flirora.crackslashmine.item.essence.tonat.TonatItem;
import flirora.crackslashmine.item.spell.SpellComponent;
import flirora.crackslashmine.item.spell.SpellItem;
import flirora.crackslashmine.item.spell.WandComponent;
import flirora.crackslashmine.item.spell.WandItem;
import nerdhub.cardinal.components.api.ComponentRegistry;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.util.Identifier;

public class CrackSlashMineComponents
    implements EntityComponentInitializer, ItemComponentInitializer {

  public static final ComponentKey<LivingEntityStats> LIVING_ENTITY_STATS =
      ComponentRegistryV3.INSTANCE.getOrCreate(
          new Identifier(CrackSlashMineMod.MOD_ID, "living_entity_stats"),
          LivingEntityStats.class);
  public static final ComponentKey<ProjectileComponent> PROJECTILE_COMPONENT =
      ComponentRegistryV3.INSTANCE.getOrCreate(
          new Identifier(CrackSlashMineMod.MOD_ID, "projectile_component"),
          ProjectileComponent.class);
  public static final ComponentKey<PlayerProfile> PLAYER_PROFILE =
      ComponentRegistryV3.INSTANCE.getOrCreate(
          new Identifier(CrackSlashMineMod.MOD_ID, "player_profile"),
          PlayerProfile.class);

  public static final ComponentKey<EquipmentComponent> EQUIPMENT_COMPONENT =
      ComponentRegistry.INSTANCE.registerStatic(
          new Identifier(CrackSlashMineMod.MOD_ID, "equipment_component"),
          EquipmentComponent.class);
  public static final ComponentKey<SpellComponent> SPELL_COMPONENT =
      ComponentRegistry.INSTANCE.registerStatic(
          new Identifier(CrackSlashMineMod.MOD_ID, "spell_component"),
          SpellComponent.class);
  public static final ComponentKey<TonatComponent> TONAT_COMPONENT =
      ComponentRegistry.INSTANCE.registerStatic(
          new Identifier(CrackSlashMineMod.MOD_ID, "tonat_component"),
          TonatComponent.class);
  public static final ComponentKey<WandComponent> WAND_COMPONENT =
      ComponentRegistry.INSTANCE.registerStatic(
          new Identifier(CrackSlashMineMod.MOD_ID, "wand_component"),
          WandComponent.class);

  @Override
  public void registerEntityComponentFactories(
      EntityComponentFactoryRegistry registry) {
    registry.registerFor(LivingEntity.class, LIVING_ENTITY_STATS,
        LivingEntityStats::new);
    registry.registerFor(ProjectileEntity.class, PROJECTILE_COMPONENT,
        ProjectileComponent::new);
    registry.registerFor(PlayerEntity.class, PLAYER_PROFILE,
        PlayerProfile::new);
  }

  @Override
  public void registerItemComponentFactories(
      ItemComponentFactoryRegistry registry) {
    registry.registerFor(item -> item.getMaxCount() == 1, EQUIPMENT_COMPONENT,
        (item, stack) -> new EquipmentComponent(stack));
    registry.registerFor(SpellItem.ID, SPELL_COMPONENT,
        (item, stack) -> new SpellComponent());
    registry.registerFor(item -> item instanceof TonatItem, TONAT_COMPONENT,
        (item, stack) -> new TonatComponent());
    registry.registerFor(item -> item instanceof WandItem, WAND_COMPONENT,
        (item, stack) -> new WandComponent());
  }
}
