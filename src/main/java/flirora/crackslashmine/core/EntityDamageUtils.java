package flirora.crackslashmine.core;

import com.google.common.collect.ImmutableMap;
import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.item.equipment.EquipmentUtils;
import net.minecraft.entity.Entity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.damage.EntityDamageSource;
import net.minecraft.entity.damage.ProjectileDamageSource;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.Vec3d;

import java.util.Map;

public class EntityDamageUtils {
    private static Map<String, DamageType> buildDamageTypesBySourceName() {
        ImmutableMap.Builder<String, DamageType> builder = ImmutableMap.builder();
        builder.put("inFire", DamageType.FIRE);
        builder.put("lightningBolt", DamageType.LIGHTNING);
        builder.put("onFire", DamageType.FIRE);
        builder.put("lava", DamageType.FIRE);
        builder.put("hotFloor", DamageType.FIRE);
        builder.put("inWall", DamageType.PHYSICAL);
        builder.put("cramming", DamageType.PHYSICAL);
        builder.put("drown", DamageType.WATER);
        builder.put("starve", DamageType.DARKNESS);
        builder.put("cactus", DamageType.EARTH);
        builder.put("fall", DamageType.WIND);
        builder.put("flyIntoWall", DamageType.WIND);
        builder.put("outOfWorld", DamageType.DARKNESS);
        builder.put("generic", DamageType.PHYSICAL);
        // crazy, I know, but we don't have a generic 'magic' type
        builder.put("magic", DamageType.PHYSICAL);
        builder.put("wither", DamageType.DARKNESS);
        builder.put("anvil", DamageType.PHYSICAL);
        builder.put("fallingBlock", DamageType.PHYSICAL);
        builder.put("dragonBreath", DamageType.LIGHTNING);
        builder.put("dryout", DamageType.LIGHTNING);
        builder.put("sweetBerryBush", DamageType.EARTH);
        builder.put("arrow", DamageType.PHYSICAL);
        builder.put("trident", DamageType.WATER);
        builder.put("fireworks", DamageType.FIRE);
        builder.put("witherSkull", DamageType.DARKNESS);
        builder.put("explosion", DamageType.WIND);
        return builder.build();
    }

    public static final Map<String, DamageType> DAMAGE_TYPES_BY_SOURCE_NAME =
            buildDamageTypesBySourceName();

    public static DamageType getIntrinsicDamageTypeFor(DamageSource source) {
        DamageType val = DAMAGE_TYPES_BY_SOURCE_NAME.get(source.getName());
        if (val != null) return val;
        return DamageType.PHYSICAL; // fallback
    }

    // TODO: optimal place to spawn the splat?
    public static Vec3d getPositionForSplat(Entity defender, Entity viewer) {
        return defender.getPos().add(0, defender.getHeight() * 0.6, 0);
    }

    public static boolean isInfallible(DamageSource damageSource) {
        if (damageSource instanceof EntityDamageSource) return false;
        return damageSource != DamageSource.ANVIL;
    }

    public static double getUnarmedEnergyCost(int level) {
        return CrackSlashMineMod.config.weaponStaminaScaling.value(level);
    }

    public interface DamageSourceProtectedSetterAccessor {
        void setFlags(
                boolean fire,
                boolean unblockable,
                boolean bypassesArmor,
                boolean outOfWorld);
    }

    private static void copyParams(EntityDamageSource dest, EntityDamageSource src) {
        if (src.isThorns()) dest.setThorns();
        if (src.isExplosive()) dest.setExplosive();
        if (src.isProjectile()) dest.setProjectile();
        if (src.isScaledWithDifficulty()) dest.setScaledWithDifficulty();
        if (src.getMagic()) dest.setUsesMagic();
        ((DamageSourceProtectedSetterAccessor) dest).setFlags(
                src.isFire(), src.isUnblockable(),
                src.bypassesArmor(), src.isOutOfWorld()
        );
    }

    public static EntityDamageSource eraseAttacker(EntityDamageSource source) {
        if (source instanceof ProjectileDamageSource) {
            ProjectileDamageSource res = new ProjectileDamageSource(
                    source.getName(), source.getSource(), null);
            copyParams(res, source);
            return res;
        } else {
            EntityDamageSource res =
                    new EntityDamageSource(source.getName(), null);
            copyParams(res, source);
            return res;
        }
    }

    public static void attemptAttack(PlayerEntity me, boolean successful) {
        if (!me.isCreative() && !me.isSpectator()) {
            LivingEntityStats stats = LivingEntityStats.getFor(me);
            long staminaCost = EquipmentUtils.getStaminaCostForAttacking(
                    me.getMainHandStack(), stats.getLevel(), successful);
            if (stats.getStamina() >= staminaCost) {
                stats.setStamina(stats.getStamina() - staminaCost);
            } else {
                playExhaustedSoundEffect(me);
            }
        }
    }

    public static void playExhaustedSoundEffect(PlayerEntity me) {
        if (!me.getEntityWorld().isClient()) {
            Vec3d pos = me.getPos();
            me.getEntityWorld().playSound(
                    null, pos.getX(), pos.getY(), pos.getZ(),
                    SoundEvents.ENTITY_WOLF_PANT,
                    SoundCategory.PLAYERS,
                    1.0f,
                    1.0f);
        }
    }
}
