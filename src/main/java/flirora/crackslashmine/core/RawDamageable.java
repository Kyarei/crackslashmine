package flirora.crackslashmine.core;

import net.minecraft.entity.damage.DamageSource;

public interface RawDamageable {
    /**
     * Pretend to deal damage to the entity but don't.
     *
     * @param source the {@link DamageSource}
     * @param amount the amount of vanilla health to pretend to deduct
     * @return whether it succeeded
     */
    float damageRaw(DamageSource source, float amount);
}
