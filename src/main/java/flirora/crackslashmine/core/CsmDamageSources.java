package flirora.crackslashmine.core;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.damage.EntityDamageSource;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import org.jetbrains.annotations.Nullable;

public class CsmDamageSources {
    public static DamageSource damageOverTimeFrom(DamageType type, @Nullable LivingEntity attacker) {
        DamageSource source = new EntityDamageSource(type.getName() + "Dot", null) {
            @Override
            public Text getDeathMessage(LivingEntity entity) {
                if (attacker == null)
                    return new TranslatableText("death.attack.csm." + name, entity.getDisplayName());
                return new TranslatableText("death.attack.csm." + name + ".by", entity.getDisplayName(), attacker.getDisplayName());
            }
        };
        return source.setUsesMagic();
    }

    public static DamageSource delayedDamageFrom(DamageType type, @Nullable LivingEntity attacker) {
        DamageSource source = new EntityDamageSource(type.getName() + "Delayed", null) {
            @Override
            public Text getDeathMessage(LivingEntity entity) {
                if (attacker == null)
                    return new TranslatableText("death.attack.csm." + name, entity.getDisplayName());
                return new TranslatableText("death.attack.csm." + name + ".by", entity.getDisplayName(), attacker.getDisplayName());
            }
        };
        return source.setUsesMagic();
    }
}
