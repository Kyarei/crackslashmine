package flirora.crackslashmine.core;

import java.util.Random;

import org.apache.commons.math3.distribution.GammaDistribution;
import org.apache.commons.math3.distribution.PoissonDistribution;
import org.apache.commons.math3.random.JDKRandomGenerator;

public class MathUtils {
  /**
   * Return a random number from a gamma distribution.
   *
   * @param r     the {@link Random} object to sample from
   * @param k     the shape of the gamma distribution
   * @param theta the scale of the gamma distribution
   * @return a double randomly sampled from the distribution
   */
  public static double nextGamma(Random r, double k, double theta) {
    return new GammaDistribution(new JDKRandomGenerator(r.nextInt()), k, theta)
        .sample();
  }

  public static int nextIntInclusive(Random r, int min, int max) {
    return min + r.nextInt(max - min + 1);
  }

  public static long nextLongInclusive(Random r, long min, long max) {
    return min + nextLong(r, max - min + 1);
  }

  /**
   * Returns a random long in [0, max).
   *
   * @param r   the {@link Random} object to sample from
   * @param max the (exclusive) upper bound, as an unsigned long
   * @return a random long in [0, max)
   */
  public static long nextLong(Random r, long max) {
    long nBlocks = Long.divideUnsigned(-1L, max);
    long rejectAbove = nBlocks * max;
    while (true) {
      long roll = r.nextLong();
      if (Long.compareUnsigned(roll, rejectAbove) < 0) {
        // done!
        return Long.remainderUnsigned(roll, max);
      }
    }
  }

  public static double nextTriangular(Random r) {
    return r.nextDouble() + r.nextDouble() - 1;
  }

  public static int nextPoisson(Random r, double lambda) {
    return new PoissonDistribution(new JDKRandomGenerator(r.nextInt()), lambda,
        PoissonDistribution.DEFAULT_EPSILON,
        PoissonDistribution.DEFAULT_MAX_ITERATIONS).sample();
  }

  /**
   * Splits up an integral quantity over a certain number of samples such that
   * the sum of the samples equals the original quantity and the samples are
   * approximately equal.
   *
   * @param total   The total quantity to split
   * @param period  The number of samples
   * @param current The index of the current sample
   * @return The amount given to sample number `current`
   * @throws IllegalArgumentException if period <= 0
   * @throws IllegalArgumentException if current < 0 or current >= period
   */
  public static long clock(long total, long period, long current) {
    if (current < 0 || current >= period)
      throw new IllegalArgumentException();
    return total * (current + 1) / period - total * current / period;
  }

  public static int sampleSetBitIndex(Random r, int eligible) {
    int[] bitsSet = new int[32];
    int numSet = 0;
    for (int i = 0; i < 32; ++i) {
      boolean set = (eligible & (1 << i)) != 0;
      if (set) {
        bitsSet[numSet++] = i;
      }
    }
    return bitsSet[r.nextInt(numSet)];
  }
}
