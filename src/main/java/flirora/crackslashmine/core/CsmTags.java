package flirora.crackslashmine.core;

import flirora.crackslashmine.CrackSlashMineMod;
import net.fabricmc.fabric.api.tag.TagRegistry;
import net.minecraft.entity.EntityType;
import net.minecraft.tag.Tag;
import net.minecraft.util.Identifier;

public class CsmTags {
    // A tag for extra-annoying mobs.
    public static final Tag<EntityType<?>> NASTY_MOBS =
            TagRegistry.entityType(new Identifier(CrackSlashMineMod.MOD_ID, "nasty_mobs"));

    public static final Tag<EntityType<?>> BOSSES =
            TagRegistry.entityType(new Identifier(CrackSlashMineMod.MOD_ID, "bosses"));
}
