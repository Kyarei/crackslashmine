package flirora.crackslashmine.core;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

import org.jetbrains.annotations.Nullable;

import flirora.crackslashmine.core.xse.RangedExtendedStatusEffectInstance;
import net.fabricmc.fabric.api.util.NbtType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;

public class CsmPrimaryAttackStat {
  public static class Entry {
    private final DamageType type;
    private final long min, max;

    public Entry(DamageType type, long min, long max) {
      this.type = type;
      this.min = min;
      this.max = max;
    }

    public DamageType getType() {
      return type;
    }

    public long getMin() {
      return min;
    }

    public long getMax() {
      return max;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o)
        return true;
      if (o == null || getClass() != o.getClass())
        return false;
      Entry entry = (Entry) o;
      return min == entry.min && max == entry.max && type == entry.type;
    }

    @Override
    public String toString() {
      return "Entry{" + "type=" + type + ", min=" + min + ", max=" + max + '}';
    }

    public CompoundTag toTag() {
      CompoundTag tag = new CompoundTag();
      tag.putString("type", type.getName());
      tag.putLong("min", min);
      tag.putLong("max", max);
      return tag;
    }

    public static Entry fromTag(CompoundTag tag) {
      return new Entry(DamageType.BY_NAME.get(tag.getString("type")),
          tag.getLong("min"), tag.getLong("max"));
    }
  }

  public static final CsmPrimaryAttackStat EMPTY = new CsmPrimaryAttackStat();

  private final List<Entry> entries;
  private final List<RangedExtendedStatusEffectInstance> effects;

  public CsmPrimaryAttackStat(List<Entry> entries,
      List<RangedExtendedStatusEffectInstance> effects) {
    this.entries = entries;
    this.effects = effects;
  }

  public CsmPrimaryAttackStat(List<Entry> entries) {
    this(entries, Collections.emptyList());
  }

  public CsmPrimaryAttackStat(Entry... entries) {
    this(Arrays.asList(entries));
  }

  public List<Entry> getEntries() {
    return entries;
  }

  public List<RangedExtendedStatusEffectInstance> getEffects() {
    return effects;
  }

  public CsmAttack roll(Random r, @Nullable LivingEntity attacker) {
    // TODO: effects
    return new CsmAttack(
        entries.stream()
            .map(entry -> new CsmAttack.Entry(entry.getType(),
                MathUtils.nextLongInclusive(r, entry.getMin(), entry.getMax())))
            .collect(Collectors.toList()),
        effects.stream().map(effect -> effect.roll(r, attacker))
            .collect(Collectors.toList()),
        false);
  }

  public static @Nullable CsmPrimaryAttackStat fromTag(CompoundTag tag) {
    ListTag entryList = tag.getList("entries", NbtType.COMPOUND);
    if (entryList == null)
      return null;
    ListTag effectList = tag.getList("effects", NbtType.COMPOUND);
    // backwards compatibility with existing saves
    if (effectList == null)
      effectList = new ListTag();
    return new CsmPrimaryAttackStat(
        entryList.stream().map(e -> Entry.fromTag((CompoundTag) e))
            .collect(Collectors.toList()),
        effectList.stream().map(
            e -> RangedExtendedStatusEffectInstance.fromTag((CompoundTag) e))
            .collect(Collectors.toList()));
  }

  public CompoundTag toTag() {
    CompoundTag tag = new CompoundTag();
    ListTag entryList = new ListTag();
    for (Entry entry : entries) {
      entryList.add(entry.toTag());
    }
    tag.put("entries", entryList);
    ListTag effectList = new ListTag();
    for (RangedExtendedStatusEffectInstance effect : effects) {
      effectList.add(effect.toTag());
    }
    tag.put("effects", effectList);
    return tag;
  }

  @Override
  public String toString() {
    return "CsmPrimaryAttackStat{" + "entries=" + entries + ", effects="
        + effects + '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    CsmPrimaryAttackStat that = (CsmPrimaryAttackStat) o;
    return Objects.equals(entries, that.entries)
        && Objects.equals(effects, that.effects);
  }

  @Override
  public int hashCode() {
    return Objects.hash(entries, effects);
  }
}
