package flirora.crackslashmine.core;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.stat.Stat;
import flirora.crackslashmine.core.xse.ExtendedStatusEffect;
import flirora.crackslashmine.generation.feature.condition.ConfiguredFeatureCondition;
import flirora.crackslashmine.generation.feature.condition.FeatureCondition;
import flirora.crackslashmine.item.equipment.types.EquipmentType;
import flirora.crackslashmine.item.spell.data.DataSerializer;
import flirora.crackslashmine.item.spell.data.action.SpellAction;
import flirora.crackslashmine.item.spell.data.factory.EntityFactory;
import flirora.crackslashmine.item.spell.data.target.SpellTarget;
import net.fabricmc.fabric.api.event.registry.FabricRegistryBuilder;
import net.fabricmc.fabric.api.event.registry.RegistryAttribute;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.RegistryKey;

public class CsmRegistries {
  public static final Registry<Stat> STAT = FabricRegistryBuilder
      .createSimple(Stat.class,
          new Identifier(CrackSlashMineMod.MOD_ID, "stat"))
      .attribute(RegistryAttribute.SYNCED).buildAndRegister();

  public static final Registry<EquipmentType> EQUIPMENT_TYPE =
      FabricRegistryBuilder
          .createSimple(EquipmentType.class,
              new Identifier(CrackSlashMineMod.MOD_ID, "equipment_type"))
          .attribute(RegistryAttribute.SYNCED).buildAndRegister();

  public static final Registry<ExtendedStatusEffect> EXTENDED_STATUS_EFFECT =
      FabricRegistryBuilder
          .createSimple(ExtendedStatusEffect.class,
              new Identifier(CrackSlashMineMod.MOD_ID,
                  "extended_status_effect"))
          .attribute(RegistryAttribute.SYNCED).buildAndRegister();

  public static final Registry<DataSerializer<? extends SpellTarget>> SPELL_TARGET_SERIALIZER =
      FabricRegistryBuilder.createSimple(
          (Class<DataSerializer<? extends SpellTarget>>) (Class<?>) DataSerializer.class,
          new Identifier(CrackSlashMineMod.MOD_ID, "spell_target_serializer"))
          .attribute(RegistryAttribute.SYNCED).buildAndRegister();

  public static final Registry<DataSerializer<? extends SpellAction>> SPELL_ACTION_SERIALIZER =
      FabricRegistryBuilder.createSimple(
          (Class<DataSerializer<? extends SpellAction>>) (Class<?>) DataSerializer.class,
          new Identifier(CrackSlashMineMod.MOD_ID, "spell_action_serializer"))
          .attribute(RegistryAttribute.SYNCED).buildAndRegister();

  public static final Registry<DataSerializer<? extends EntityFactory<?>>> ENTITY_FACTORY_SERIALIZER =
      FabricRegistryBuilder.createSimple(
          (Class<DataSerializer<? extends EntityFactory<?>>>) (Class<?>) DataSerializer.class,
          new Identifier(CrackSlashMineMod.MOD_ID, "entity_factory_serializer"))
          .attribute(RegistryAttribute.SYNCED).buildAndRegister();

  private static <T> RegistryKey<Registry<T>> createRegistryKey(String path) {
    return RegistryKey
        .ofRegistry(new Identifier(CrackSlashMineMod.MOD_ID, path));
  }

  public static final Registry<FeatureCondition<?>> FEATURE_CONDITION =
      FabricRegistryBuilder.createSimple(
          // ああああああああああああああああああああああああああ
          (Class<FeatureCondition<?>>) (Class<?>) FeatureCondition.class,
          new Identifier(CrackSlashMineMod.MOD_ID,
              "worldgen/feature_condition"))
          .buildAndRegister();

  public static final RegistryKey<Registry<ConfiguredFeatureCondition<?, ?>>> CONFIGURED_FEATURE_CONDITION_WORLDGEN =
      createRegistryKey("worldgen/configured_feature_condition");
}
