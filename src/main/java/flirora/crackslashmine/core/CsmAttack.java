package flirora.crackslashmine.core;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.jetbrains.annotations.Nullable;

import flirora.crackslashmine.core.stat.StatView;
import flirora.crackslashmine.core.stat.Stats;
import flirora.crackslashmine.core.xse.ExtendedStatusEffectInstance;
import flirora.crackslashmine.item.equipment.CompatibleItemDescription;
import flirora.crackslashmine.item.equipment.CompatibleItemManager;
import flirora.crackslashmine.item.equipment.types.WeaponType;
import net.fabricmc.fabric.api.util.NbtType;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.EntityGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.text.LiteralText;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.text.TextColor;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;

public class CsmAttack {
  public static class Entry {
    private final DamageType type;
    private final long amount;
    private final boolean critical;

    public Entry(DamageType type, long amount) {
      this(type, amount, false);
    }

    public Entry(DamageType type, long amount, boolean critical) {
      this.type = type;
      this.amount = amount;
      this.critical = critical;
    }

    public DamageType getType() {
      return type;
    }

    public long getAmount() {
      return amount;
    }

    public boolean isCritical() {
      return critical;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o)
        return true;
      if (o == null || getClass() != o.getClass())
        return false;
      Entry entry = (Entry) o;
      return amount == entry.amount && type == entry.type;
    }

    @Override
    public int hashCode() {
      return Objects.hash(type, amount);
    }

    @Override
    public String toString() {
      return "Entry{" + "type=" + type + ", amount=" + amount + '}';
    }

    public CompoundTag toTag() {
      CompoundTag tag = new CompoundTag();
      tag.putString("type", type.getName());
      tag.putLong("amount", amount);
      tag.putBoolean("critical", critical);
      return tag;
    }

    public static Entry fromTag(CompoundTag tag) {
      return new Entry(DamageType.BY_NAME.get(tag.getString("type")),
          tag.getLong("amount"), tag.getBoolean("critical"));
    }

    public static Entry read(PacketByteBuf buf) {
      return new Entry(DamageType.values()[buf.readByte()], buf.readVarLong(),
          buf.readBoolean());
    }

    public void write(PacketByteBuf buf) {
      buf.writeByte((byte) type.ordinal());
      buf.writeVarLong(amount);
      buf.writeBoolean(critical);
    }

    public Text asText() {
      String suffix = critical ? "!" : "";
      return new LiteralText(Beautify.beautify(amount) + suffix)
          .styled(s -> s.withColor(TextColor.fromRgb(type.getColor())));
    }
  }

  private final List<Entry> entries;
  private final List<ExtendedStatusEffectInstance> effects;
  private final boolean infallible;

  public CsmAttack(List<Entry> entries,
      List<ExtendedStatusEffectInstance> effects) {
    this(entries, effects, false);
  }

  public CsmAttack(List<Entry> entries,
      List<ExtendedStatusEffectInstance> effects, boolean infallible) {
    this.entries = entries;
    this.effects = effects;
    this.infallible = infallible;
  }

  public CsmAttack(List<Entry> entries) {
    this(entries, Collections.emptyList(), false);
  }

  public CsmAttack(List<Entry> entries, boolean infallible) {
    this(entries, Collections.emptyList());
  }

  public CsmAttack(Entry... entries) {
    this(false, entries);
  }

  public CsmAttack(boolean infallible, Entry... entries) {
    this(Arrays.asList(entries), infallible);
  }

  public List<Entry> getEntries() {
    return entries;
  }

  public List<ExtendedStatusEffectInstance> getEffects() {
    return effects;
  }

  public boolean isInfallible() {
    return infallible;
  }

  public CsmAttack withAppliedBonuses(StatView attackerStats, Random r,
      AttackContext ctx) {
    return withAppliedBonuses(attackerStats, r, ctx, (WeaponType) null);
  }

  public CsmAttack withAppliedBonuses(StatView attackerStats, Random r,
      AttackContext ctx, ItemStack weapon) {
    CompatibleItemDescription desc = CompatibleItemManager.INSTANCE
        .get(Registry.ITEM.getId(weapon.getItem()));
    WeaponType type = (desc != null && desc.getType() instanceof WeaponType)
        ? ((WeaponType) desc.getType())
        : null;
    return withAppliedBonuses(attackerStats, r, ctx, type);
  }

  public CsmAttack withAppliedBonuses(StatView attackerStats, Random r,
      AttackContext ctx, @Nullable WeaponType type) {
    long weaponBonus =
        type != null ? type.getWeaponTypeBonus(attackerStats) : 0;
    return new CsmAttack(entries.stream().map(entry -> {
      long bonus = attackerStats.get(Stats.DAMAGE_BONUS.get(entry.getType()));
      long multi = attackerStats.get(Stats.DAMAGE_MULTI.get(entry.getType()))
          + weaponBonus;
      long amount = (entry.getAmount() + bonus) * (10000 + multi) / 10000;
      long criticalMulti = 0;
      boolean jumpCrit = ctx.shouldCountJumpCritical() && r.nextInt(3) == 0;
      if (entry.getType() == DamageType.PHYSICAL) {
        if (jumpCrit
            || r.nextInt(10000) < attackerStats.get(Stats.CRITICAL_RATE))
          criticalMulti = attackerStats.get(Stats.CRITICAL_DAMAGE);
      } else {
        if (jumpCrit
            || r.nextInt(10000) < attackerStats.get(Stats.KRITIKHÄLH_RATE))
          criticalMulti = attackerStats.get(Stats.KRITIKHÄLH_DAMAGE);
      }
      return new Entry(entry.getType(),
          amount * (10000 + criticalMulti) / 10000, criticalMulti != 0);
    }).collect(Collectors.toList()),
        effects.stream().map(inst -> inst.scaleToAttackerStats(attackerStats))
            .collect(Collectors.toList()),
        infallible);
  }

  public CsmAttack withAppliedResistances(StatView defenderStats) {
    return new CsmAttack(entries.stream().map(entry -> {
      long resist =
          defenderStats.get(Stats.RESISTANCE_RATING.get(entry.getType()));
      long adjustedDamage = 10000 * entry.getAmount() / (10000 + resist);
      return new Entry(entry.getType(), adjustedDamage, entry.critical);
    }).collect(Collectors.toList()),
        effects.stream().map(inst -> inst.scaleToDefenderStats(defenderStats))
            .collect(Collectors.toList()),
        infallible);
  }

  public CsmAttack applyMeleeWeaponEnchants(ItemStack weapon,
      EntityGroup group) {
    float bonus = EnchantmentHelper.getAttackDamage(weapon, group);
    return scaled(1.0 + bonus * 0.1);
  }

  public CsmAttack scaled(double scale) {
    return new CsmAttack(
        entries.stream()
            .map(entry -> new Entry(entry.getType(),
                Math.round(entry.getAmount() * scale), entry.critical))
            .collect(Collectors.toList()),
        effects.stream()
            .map(effect -> effect.getEffect().scaleDamage(effect, scale))
            .collect(Collectors.toList()),
        infallible);
  }

  public CsmAttack scaledEntries(double scale) {
    return new CsmAttack(entries.stream()
        .map(entry -> new Entry(entry.getType(),
            Math.round(entry.getAmount() * scale), entry.critical))
        .collect(Collectors.toList()), effects, infallible);
  }

  public long totalDamage() {
    return entries.stream().mapToLong(Entry::getAmount).sum();
  }

  @Override
  public String toString() {
    return "CsmAttack{" + "entries=" + entries + ", effects=" + effects
        + ", infallible=" + infallible + '}';
  }

  public static CsmAttack read(PacketByteBuf buf, World world) {
    int entryCount = buf.readInt();
    List<Entry> entries = IntStream.range(0, entryCount)
        .mapToObj(i -> Entry.read(buf)).collect(Collectors.toList());
    int effectCount = buf.readInt();
    List<ExtendedStatusEffectInstance> effects = IntStream.range(0, effectCount)
        .mapToObj(i -> ExtendedStatusEffectInstance.readFromPacket(buf, world))
        .collect(Collectors.toList());
    return new CsmAttack(entries, effects, buf.readBoolean());
  }

  public void write(PacketByteBuf buf) {
    buf.writeInt(entries.size());
    for (Entry e : entries) {
      e.write(buf);
    }
    buf.writeInt(effects.size());
    for (ExtendedStatusEffectInstance e : effects) {
      e.writeToPacket(buf);
    }
    buf.writeBoolean(infallible);
  }

  public static CsmAttack fromTag(CompoundTag tag, World world) {
    ListTag entryList = tag.getList("entries", NbtType.COMPOUND);
    if (entryList == null)
      entryList = new ListTag();
    ListTag effectList = tag.getList("effects", NbtType.COMPOUND);
    if (effectList == null)
      effectList = new ListTag();
    return new CsmAttack(
        entryList.stream().map(e -> CsmAttack.Entry.fromTag((CompoundTag) e))
            .collect(Collectors.toList()),
        effectList.stream().map(
            e -> ExtendedStatusEffectInstance.fromTag((CompoundTag) e, world))
            .collect(Collectors.toList()),
        tag.getBoolean("infallible"));
  }

  public CompoundTag toTag() {
    CompoundTag tag = new CompoundTag();
    tag.putBoolean("infallible", infallible);
    ListTag entryList = new ListTag();
    for (Entry entry : entries) {
      entryList.add(entry.toTag());
    }
    tag.put("entries", entryList);
    ListTag effectList = new ListTag();
    for (ExtendedStatusEffectInstance effect : effects) {
      effectList.add(effect.toTag());
    }
    tag.put("effects", effectList);
    return tag;
  }

  private static final Text EMPTY = new LiteralText("");
  private static final Text PLUS = new TranslatableText("csm.hud.plus")
      .styled(s -> s.withColor(TextColor.fromRgb(0xFFFFFF)));

  public Text asText() {
    if (entries.isEmpty()) {
      return EMPTY;
    }
    Iterator<Entry> iter = entries.iterator();
    MutableText result = iter.next().asText().shallowCopy();
    while (iter.hasNext()) {
      result.append(PLUS).append(iter.next().asText());
    }
    return result;
  }
}
