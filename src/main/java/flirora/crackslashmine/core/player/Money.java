package flirora.crackslashmine.core.player;

import flirora.crackslashmine.core.Beautify;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;

public class Money {
    public enum Context {
        HELD("csm.hud.youreBroke"),
        BUY("gui.csm.free"),
        SELL("gui.csm.worthless"),
        ;

        private final String zeroKey;

        Context(String zeroKey) {
            this.zeroKey = zeroKey;
        }

        public String getZeroKey() {
            return zeroKey;
        }
    }

    public static Text format(long amount, Context context) {
        if (amount == 0) {
            return new TranslatableText(context.getZeroKey()).formatted(Formatting.GRAY);
        }
        return new TranslatableText("gui.csm.money", Beautify.beautify(amount));
    }
}
