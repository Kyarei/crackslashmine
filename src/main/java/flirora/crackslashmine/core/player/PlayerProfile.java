package flirora.crackslashmine.core.player;

import dev.onyxstudios.cca.api.v3.component.ComponentV3;
import dev.onyxstudios.cca.api.v3.entity.PlayerComponent;
import flirora.crackslashmine.core.CrackSlashMineComponents;
import nerdhub.cardinal.components.api.util.sync.EntitySyncedComponent;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundTag;

public class PlayerProfile implements EntitySyncedComponent,
        PlayerComponent<PlayerProfile>, ComponentV3 {
    private final PlayerEntity player;
    private long money;

    public PlayerProfile(PlayerEntity player) {
        this.player = player;
    }

    @Override
    public void readFromNbt(CompoundTag tag) {
        money = tag.getLong("money");
    }

    @Override
    public void writeToNbt(CompoundTag tag) {
        tag.putLong("money", money);
    }

    @Override
    public Entity getEntity() {
        return player;
    }

    public long getMoney() {
        return money;
    }

    public void setMoney(long money) {
        this.money = money;
        sync();
    }

    @Override
    public boolean shouldCopyForRespawn(boolean lossless,
                                        boolean keepInventory) {
        return true;
    }

    @Override
    public void copyForRespawn(PlayerProfile original, boolean lossless,
                               boolean keepInventory) {
        this.money = original.money;
    }

    public static PlayerProfile getFor(PlayerEntity player) {
        return CrackSlashMineComponents.PLAYER_PROFILE.get(player);
    }
}
