package flirora.crackslashmine.core;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.entity.player.PlayerEntity;

public class AttackContext {
  public static final AttackContext DEFAULT = new AttackContext(false);

  private final boolean countJumpCritical;

  private AttackContext(boolean countJumpCritical) {
    this.countJumpCritical = countJumpCritical;
  }

  public AttackContext(LivingEntity e) {
    this(
        e instanceof PlayerEntity && shouldCountJumpCritical((PlayerEntity) e));
  }

  public boolean shouldCountJumpCritical() {
    return countJumpCritical;
  }

  private static boolean shouldCountJumpCritical(PlayerEntity player) {
    float cooldownProgress = player.getAttackCooldownProgress(0.5F);
    return cooldownProgress > 0.9f && player.fallDistance > 0.0f
        && !player.isOnGround() && !player.isClimbing()
        && !player.isTouchingWater()
        && !player.hasStatusEffect(StatusEffects.BLINDNESS)
        && !player.hasVehicle() && !player.isSprinting();
  }
}
