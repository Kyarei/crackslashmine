package flirora.crackslashmine.core;

import flirora.crackslashmine.generation.AreaManager;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.world.ServerWorld;

import java.util.Arrays;
import java.util.Random;

/**
 * Stores base data about a player that is not derived from another source.
 *
 * @author #flirora
 */
public class CsmCharacter {
    public static final CsmCharacter EMPTY = new CsmCharacter();
    private final int[] fundamentalStore;
    private int level = 1;
    private long xp;

    public CsmCharacter() {
        fundamentalStore = new int[Fundamental.values().length];
    }

    public static CsmCharacter fromEntity(LivingEntity e) {
        if (e.getEntityWorld().isClient()) {
            throw new UnsupportedOperationException("fromEntity called on " +
                    "client");
        }
        AreaManager areaManager =
                AreaManager.getFor((ServerWorld) e.getEntityWorld());
        CsmCharacter result = new CsmCharacter();
        if (e instanceof PlayerEntity) {
            return result;
        }
        int level = areaManager.getOrCreateSync(e.getPos()).getLevel();
        result.level = level;
        Random random = e.getRandom();
        if (level > 0) {
            for (int i = 0; i < result.fundamentalStore.length; ++i) {
                result.fundamentalStore[i] =
                        (int) Math.round(MathUtils.nextGamma(random,
                                Math.sqrt(level), Math.sqrt(level)));
            }
        }
        return result;
    }

    public int getIntrinsicFundamental(Fundamental f) {
        return fundamentalStore[f.ordinal()];
    }

    public void setIntrinsicFundamental(Fundamental f, int val) {
        fundamentalStore[f.ordinal()] = val;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public long getXp() {
        return xp;
    }

    public void setXp(long xp) {
        this.xp = xp;
    }

    public void addXp(long xp) {
        this.xp += xp;
        while (true) {
            long req = Levels.getXpForNextLevel(level);
            if (this.xp < req) break;
            ++this.level;
            this.xp -= req;
        }
    }

    public CompoundTag writeToTag(CompoundTag tag) {
        tag.putIntArray("fundamentals", fundamentalStore);
        tag.putInt("level", level);
        tag.putLong("xp", xp);
        return tag;
    }

    public void readFromTag(CompoundTag tag) {
        level = tag.getInt("level");
        xp = tag.getLong("xp");
        int[] fundamentals = tag.getIntArray("fundamentals");
        Arrays.fill(fundamentalStore, 0);
        System.arraycopy(fundamentals, 0, fundamentalStore, 0,
                Math.min(this.fundamentalStore.length, fundamentals.length));
    }

    public void write(PacketByteBuf buf) {
        for (int f : fundamentalStore) buf.writeInt(f);
        buf.writeInt(level);
        buf.writeLong(xp);
    }

    public void read(PacketByteBuf buf) {
        for (int i = 0; i < fundamentalStore.length; ++i) {
            fundamentalStore[i] = buf.readInt();
        }
        level = buf.readInt();
        xp = buf.readLong();
    }

    public int totalPointsSpent() {
        int n = 0;
        for (int value : fundamentalStore) {
            n += value;
        }
        return n;
    }

    public int statPointsRemaining() {
        return 6 * level - totalPointsSpent();
    }

    public boolean incrementFundamental(Fundamental f) {
        if (statPointsRemaining() <= 0) return false;
        fundamentalStore[f.ordinal()] += 1;
        return true;
    }

    @Override
    public String toString() {
        return "CsmCharacter{" +
                "fundamentalStore=" + Arrays.toString(fundamentalStore) +
                ", level=" + level +
                ", xp=" + xp +
                '}';
    }
}
