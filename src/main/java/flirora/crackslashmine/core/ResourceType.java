package flirora.crackslashmine.core;

import java.util.EnumMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum ResourceType {
  HEALTH("health", 0xf7769f), MANA("mana", 0x76d2f7),
  STAMINA("stamina", 0x81f776),;

  private final String name;
  private final int color;

  ResourceType(String name, int color) {
    this.name = name;
    this.color = color;
  }

  public String getName() {
    return name;
  }

  public int getColor() {
    return color;
  }

  public static <T> EnumMap<ResourceType, T> forEachResourceType(
      Function<ResourceType, T> f) {
    EnumMap<ResourceType, T> res = new EnumMap<>(ResourceType.class);
    for (ResourceType d : ResourceType.values()) {
      res.put(d, f.apply(d));
    }
    return res;
  }

  public static final Map<String, ResourceType> BY_NAME =
      Stream.of(ResourceType.values())
          .collect(Collectors.toMap(ResourceType::getName, d -> d));
}
