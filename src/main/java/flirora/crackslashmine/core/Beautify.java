package flirora.crackslashmine.core;

import java.text.NumberFormat;
import java.util.List;
import java.util.function.UnaryOperator;

import org.apache.commons.math3.analysis.polynomials.PolynomialFunction;

import net.minecraft.text.LiteralText;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;

public class Beautify {

  public Beautify() {
    // TODO Auto-generated constructor stub
  }

  public static String beautify(long n) {
    NumberFormat nf = NumberFormat.getIntegerInstance();
    return nf.format(n);
  }

  public static String beautify0(long n) {
    return beautify(Math.max(n, 0));
  }

  public static String beautifyTime(int duration) {
    int totalSeconds = duration / 20;
    int hours = totalSeconds / 3600;
    int minutes = (totalSeconds / 60) % 60;
    int seconds = totalSeconds % 60;
    if (hours > 0) {
      return String.format("%d:%02d:%02d", hours, minutes, seconds);
    }
    return String.format("%d:%02d", minutes, seconds);
  }

  public static String beautifyTimeS(int duration) {
    NumberFormat nf = NumberFormat.getNumberInstance();
    nf.setMinimumFractionDigits(1);
    nf.setMaximumFractionDigits(1);
    return nf.format(duration / 20.0);
  }

  public static String beautifyPercentage(double d) {
    NumberFormat nf = NumberFormat.getPercentInstance();
    nf.setMinimumFractionDigits(2);
    nf.setMaximumFractionDigits(2);
    return nf.format(d);
  }

  private static final char[] SUPERSCRIPTS =
      { '⁰', '¹', '²', '³', '⁴', '⁵', '⁶', '⁷', '⁸', '⁹' };

  public static String exponentText(int ex) {
    String s = Integer.toString(ex);
    StringBuilder res = new StringBuilder();
    for (int i = 0; i < s.length(); ++i) {
      res.append(SUPERSCRIPTS[s.charAt(i) - '0']);
    }
    return res.toString();
  }

  public static String beautifyPolynomial(PolynomialFunction poly) {
    NumberFormat nf = NumberFormat.getNumberInstance();
    double[] coefficients = poly.getCoefficients();
    StringBuilder res = new StringBuilder();
    for (int i = 0; i < coefficients.length; ++i) {
      if (i != 0)
        res.append(" + ");
      res.append(nf.format(coefficients[i]));
      if (i > 0)
        res.append("x");
      if (i > 1)
        res.append(exponentText(i));
    }
    return res.toString();
  }

  public static void addLines(List<Text> out, String text,
      UnaryOperator<MutableText> callback) {
    for (String s : text.split("\n")) {
      out.add(callback.apply(new LiteralText(s)));
    }
  }
}
