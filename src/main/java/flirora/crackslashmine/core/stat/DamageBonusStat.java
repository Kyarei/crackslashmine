package flirora.crackslashmine.core.stat;

import java.util.Collections;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.CsmCharacter;
import flirora.crackslashmine.core.DamageType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.Identifier;

public class DamageBonusStat extends Stat {
  private final DamageType damageType;
  private final Stat dependency;

  private DamageBonusStat(DamageType damageType, Stat dependency) {
    super(
        new Identifier(CrackSlashMineMod.MOD_ID,
            damageType.getName() + "_bonus"),
        -1, damageType, Collections.singletonList(dependency));
    this.damageType = damageType;
    this.dependency = dependency;
  }

  public DamageBonusStat(DamageType damageType) {
    this(damageType,
        damageType == DamageType.PHYSICAL ? Stats.STRENGTH : Stats.WISDOM);
  }

  public DamageType getDamageType() {
    return damageType;
  }

  @Override
  public long getBase(CsmCharacter character, LivingEntity entity) {
    return 0;
  }

  @Override
  public long postprocess(long old, StatContainer stats) {
    return old + 3 * stats.get(dependency) / 5;
  }
}
