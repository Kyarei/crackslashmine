package flirora.crackslashmine.core.stat;

import java.util.Collections;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.CsmCharacter;
import flirora.crackslashmine.core.DamageType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.Identifier;

public class UniversalResistanceRatingStat extends Stat {
  protected UniversalResistanceRatingStat() {
    super(new Identifier(CrackSlashMineMod.MOD_ID, "resistance"), 0xb3c7c9,
        DamageType.PHYSICAL, Collections.singletonList(Stats.FORTITUDE));
  }

  @Override
  public long getBase(CsmCharacter character, LivingEntity entity) {
    return 0;
  }

  @Override
  public long postprocess(long old, StatContainer stats) {
    return old + stats.get(Stats.FORTITUDE) * 14;
  }
}
