package flirora.crackslashmine.core.stat;

import java.text.NumberFormat;

/**
 * Indicates how to format a stat boost as a string.
 */
public enum StatDisplayType {
    /**
     * Display as an integral amount.
     */
    FLAT(1.0, "") {
        @Override
        public NumberFormat getNumberFormat() {
            NumberFormat nf = NumberFormat.getIntegerInstance();
            nf.setGroupingUsed(true);
            return nf;
        }
    },
    /**
     * Display as a percentage. As a convention, amounts for the respective
     * stats are given in one-hundredths of a percent: for instance, a value
     * of 3682 would be displayed as <code>36.82%</code>.
     */
    MULTI(10000.0, "") {
        @Override
        public NumberFormat getNumberFormat() {
            NumberFormat nf = NumberFormat.getPercentInstance();
            nf.setMinimumFractionDigits(2);
            nf.setMaximumFractionDigits(2);
            nf.setGroupingUsed(true);
            return nf;
        }
    },
    /**
     * Display as a time. The amounts are given in game ticks.
     */
    TIME(20.0, "s") {
        @Override
        public NumberFormat getNumberFormat() {
            NumberFormat nf = NumberFormat.getNumberInstance();
            nf.setMinimumFractionDigits(2);
            nf.setMaximumFractionDigits(2);
            nf.setGroupingUsed(true);
            return nf;
        }
    },
    /**
     * Display in hundredths.
     */
    HUNDREDTHS(100.0, "") {
        @Override
        public NumberFormat getNumberFormat() {
            NumberFormat nf = NumberFormat.getNumberInstance();
            nf.setMinimumFractionDigits(2);
            nf.setMaximumFractionDigits(2);
            nf.setGroupingUsed(true);
            return nf;
        }
    },
    ;

    private final double divisor;
    private final String suffix;

    StatDisplayType(double divisor, String suffix) {
        this.divisor = divisor;
        this.suffix = suffix;
    }

    /**
     * Format a stat boost.
     *
     * @param amt the amount of boost
     * @return a formatted string
     */
    public String format(long amt) {
        String formatted = getNumberFormat().format(Math.abs(amt / divisor)) + suffix;
        if (amt > 0) return "+" + formatted;
        if (amt < 0) return "-" + formatted;
        return formatted;
    }

    public abstract NumberFormat getNumberFormat();
}
