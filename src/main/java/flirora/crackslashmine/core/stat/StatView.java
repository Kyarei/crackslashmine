package flirora.crackslashmine.core.stat;

import org.jetbrains.annotations.NotNull;

import java.util.Iterator;

public class StatView implements Iterable<StatContainer.Entry> {
    private final StatContainer underlying;

    public StatView(StatContainer underlying) {
        this.underlying = underlying;
    }

    public long get(Stat stat) {
        return underlying.get(stat);
    }

    public int size() {
        return underlying.size();
    }

    public StatContainer copy() {
        return new StatContainer(underlying);
    }

    @Override
    @NotNull
    public Iterator<StatContainer.Entry> iterator() {
        return underlying.iterator();
    }
}
