package flirora.crackslashmine.core.stat;

import com.google.common.collect.ImmutableList;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.CsmCharacter;
import flirora.crackslashmine.core.DamageType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.Identifier;

public class DamageMultiStat extends Stat {
  private final DamageType damageType;
  private final Stat dependency;

  private DamageMultiStat(DamageType damageType, Stat dependency) {
    super(
        new Identifier(CrackSlashMineMod.MOD_ID,
            damageType.getName() + "_multi"),
        -1, damageType, ImmutableList.of(dependency, Stats.UNIVERSAL_DAMAGE));
    this.damageType = damageType;
    this.dependency = dependency;
  }

  public DamageMultiStat(DamageType damageType) {
    this(damageType,
        damageType == DamageType.PHYSICAL ? Stats.STRENGTH : Stats.WISDOM);
  }

  public DamageType getDamageType() {
    return damageType;
  }

  @Override
  public long getBase(CsmCharacter character, LivingEntity entity) {
    return 0;
  }

  @Override
  public long postprocess(long old, StatContainer stats) {
    return old + stats.get(dependency) * 20 + stats.get(Stats.UNIVERSAL_DAMAGE);
  }

  @Override
  public StatDisplayParams getParams() {
    return StatDisplayParams.MULTI;
  }
}
