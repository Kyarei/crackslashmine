package flirora.crackslashmine.core.stat;

import com.google.common.collect.ImmutableList;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.CsmCharacter;
import flirora.crackslashmine.core.DamageType;
import flirora.crackslashmine.core.ResourceType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.Identifier;

public class ResourceRegenStat extends Stat {
  private final ResourceType resourceType;
  private final Stat fundamentalDependency;

  public ResourceRegenStat(ResourceType resourceType, Stat dependency) {
    super(
        new Identifier(CrackSlashMineMod.MOD_ID,
            resourceType.getName() + "_regen"),
        resourceType.getColor(), DamageType.PHYSICAL,
        ImmutableList.of(dependency, Stats.LEVEL));
    this.resourceType = resourceType;
    this.fundamentalDependency = dependency;
  }

  @Override
  public long getBase(CsmCharacter character, LivingEntity entity) {
    return 600;
  }

  @Override
  public long postprocess(long old, StatContainer stats) {
    long bonus = 0;
    bonus += 65 * stats.get(Stats.LEVEL);
    bonus += 20 * stats.get(fundamentalDependency);
    if (resourceType == ResourceType.HEALTH)
      bonus /= 3;
    return old + bonus;
  }

  @Override
  public StatDisplayParams getParams() {
    return StatDisplayParams.HUNDREDTHS;
  }
}
