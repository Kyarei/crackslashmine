package flirora.crackslashmine.core.stat;

import java.util.List;

import com.google.common.collect.ImmutableList;

import flirora.crackslashmine.core.CsmCharacter;
import flirora.crackslashmine.core.DamageType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;

/**
 * Describes a C/M stat. Not to be confused with vanilla statistics.
 * <p>
 * Implementors of this interface should override {@link Object#hashCode()}
 * because maps with {@link Stat} keys are used in many places.
 */
public abstract class Stat {
  /**
   * An empty list that indicates that this stat has no dependencies.
   */
  List<Stat> NO_DEPENDENCIES = ImmutableList.of();

  private final Identifier id;
  private final int color;
  private final DamageType type;
  private final List<Stat> dependencies;
  private int ordinal = -1;

  protected Stat(Identifier id, int color, DamageType type,
      List<Stat> dependencies) {
    this.id = id;
    this.color = color == -1 ? type.getColor() : color;
    this.type = type;
    this.dependencies = dependencies;
  }

  protected Stat(Identifier id, int color, DamageType type) {
    this.id = id;
    this.color = color == -1 ? type.getColor() : color;
    this.type = type;
    this.dependencies = NO_DEPENDENCIES;
  }

  /**
   * Gets the ID that this stat should be registered under.
   *
   * @return an {@link Identifier}
   */
  public final Identifier getId() {
    return id;
  }

  public int getColor() {
    return color;
  }

  public DamageType getType() {
    return type;
  }

  /**
   * Gets the base value of this stat.
   *
   * @param character the {@link CsmCharacter} of <code>entity</code>
   * @param entity    the {@link LivingEntity} to get the base value for
   * @return the base value
   */
  public abstract long getBase(CsmCharacter character, LivingEntity entity);

  /**
   * Gets all stats that this stat depends on for their values.
   *
   * @return a list of {@link Stat} objects
   */
  public final List<Stat> getPostprocessingDependencies() {
    return dependencies;
  }

  /**
   * Postprocesses the base value with all applicable stat boosts from
   * equipment. This can be used to modify the value according to other stats.
   * <p>
   * Any stats whose values are used in this method should be listed in
   * {@link Stat#getPostprocessingDependencies()}.
   *
   * @param old   the old value
   * @param stats an {@link StatContainer} of values of known stats. All stats
   *              that were returned in
   *              {@link Stat#getPostprocessingDependencies()} are guaranteed to
   *              exist as a key in this map.
   * @return the new value
   */
  public long postprocess(long old, StatContainer stats) {
    return old;
  }

  /**
   * Get the display parameters of this stat.
   *
   * @return the display parameters
   */
  public StatDisplayParams getParams() {
    return StatDisplayParams.DEFAULT;
  }

  /**
   * Format a stat boost as text.
   *
   * @param amt the amount of boost
   * @return a {@link Text} object that describes the boost
   */
  public final Text format(long amt) {
    return new TranslatableText("csm.hud.stat", getParams().format(amt),
        getName());
  }

  /**
   * Get a {@link Text} with the name of this stat.
   *
   * @return the name of this stat
   */
  public final Text getName() {
    Identifier id = getId();
    return new TranslatableText(
        "csm.stat." + id.getNamespace() + "." + id.getPath());
  }

  public final Text getDescription() {
    Identifier id = getId();
    return new TranslatableText(
        "csm.stat." + id.getNamespace() + "." + id.getPath() + ".description")
            .formatted(Formatting.AQUA, Formatting.ITALIC);
  }

  /**
   * Get the ordinal index of this stat, for use in array indexing.
   *
   * @return this stat's ordinal, or {@code -1} if not set
   */
  public int getOrdinal() {
    return ordinal;
  }

  // Yes, this is a package-private method.
  void setOrdinal(int ordinal) {
    this.ordinal = ordinal;
  }

  @Override
  public int hashCode() {
    return id.hashCode();
  }

  @Override
  public String toString() {
    return String.format("<stat %s @ %d>", id, ordinal);
  }
}
