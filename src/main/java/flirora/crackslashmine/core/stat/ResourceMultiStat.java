package flirora.crackslashmine.core.stat;

import com.google.common.collect.ImmutableList;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.CsmCharacter;
import flirora.crackslashmine.core.DamageType;
import flirora.crackslashmine.core.ResourceType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.Identifier;

public class ResourceMultiStat extends Stat {
  private final ResourceType resourceType;
  private final Stat fundamentalDependency;

  public ResourceMultiStat(ResourceType resourceType, Stat dependency) {
    super(
        new Identifier(CrackSlashMineMod.MOD_ID,
            resourceType.getName() + "_multi"),
        resourceType.getColor(), DamageType.PHYSICAL,
        ImmutableList.of(dependency, Stats.LEVEL));
    this.resourceType = resourceType;
    this.fundamentalDependency = dependency;
  }

  @Override
  public long getBase(CsmCharacter character, LivingEntity entity) {
    return 0;
  }

  @Override
  public long postprocess(long old, StatContainer stats) {
    long res = old;
    res += 4 * stats.get(fundamentalDependency);
    return res;
  }

  @Override
  public StatDisplayParams getParams() {
    return StatDisplayParams.MULTI;
  }
}
