package flirora.crackslashmine.core.stat;

import flirora.crackslashmine.core.CsmCharacter;
import flirora.crackslashmine.core.DamageType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.Identifier;

public class SimpleStat extends Stat /* This class contains no weeb jokes */ {
  private final StatDisplayParams displayParams;

  public SimpleStat(Identifier id, StatDisplayParams displayParams) {
    this(id, displayParams, -1, DamageType.PHYSICAL);
  }

  public SimpleStat(Identifier id, StatDisplayParams displayParams, int color) {
    this(id, displayParams, color, DamageType.PHYSICAL);
  }

  public SimpleStat(Identifier id, StatDisplayParams displayParams, int color,
      DamageType type) {
    super(id, color, type);
    this.displayParams = displayParams;
  }

  @Override
  public long getBase(CsmCharacter character, LivingEntity entity) {
    return 0;
  }

  @Override
  public StatDisplayParams getParams() {
    return displayParams;
  }
}
