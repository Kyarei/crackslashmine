package flirora.crackslashmine.core.stat;

import java.util.List;

import com.google.common.collect.ImmutableList;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.CsmCharacter;
import flirora.crackslashmine.core.DamageType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.Identifier;

public class AirTimeStat extends Stat {
  private static final Identifier ID =
      new Identifier(CrackSlashMineMod.MOD_ID, "air_time");
  private static final List<Stat> DEPENDENCIES =
      ImmutableList.of(Stats.ENDURANCE);

  public AirTimeStat() {
    super(ID, 0x7ff9f9, DamageType.PHYSICAL, DEPENDENCIES);
  }

  @Override
  public long getBase(CsmCharacter character, LivingEntity entity) {
    return 300;
  }

  @Override
  public long postprocess(long old, StatContainer stats) {
    return old + 4 * stats.get(Stats.ENDURANCE) / 5;
  }

  @Override
  public StatDisplayParams getParams() {
    return StatDisplayParams.TIME;
  }
}
