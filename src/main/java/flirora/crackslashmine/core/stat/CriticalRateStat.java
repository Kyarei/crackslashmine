package flirora.crackslashmine.core.stat;

import java.util.Collections;

import flirora.crackslashmine.core.CsmCharacter;
import flirora.crackslashmine.core.DamageType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.Identifier;

public class CriticalRateStat extends Stat {
  private final Stat fundamentalDependency;

  public CriticalRateStat(Identifier id, Stat fundamentalDependency) {
    super(id, 0xf9f77f, DamageType.PHYSICAL,
        Collections.singletonList(fundamentalDependency));
    this.fundamentalDependency = fundamentalDependency;
  }

  @Override
  public long getBase(CsmCharacter character, LivingEntity entity) {
    return 0;
  }

  @Override
  public long postprocess(long old, StatContainer stats) {
    return old + 10 * stats.get(fundamentalDependency);
  }

  @Override
  public StatDisplayParams getParams() {
    return StatDisplayParams.MULTI;
  }
}
