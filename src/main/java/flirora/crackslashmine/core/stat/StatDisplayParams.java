package flirora.crackslashmine.core.stat;

import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;

/**
 * Indicates how to display and color a stat boost.
 */
public class StatDisplayParams {
    public static final StatDisplayParams DEFAULT =
            new StatDisplayParams(StatDisplayType.FLAT);
    public static final StatDisplayParams MULTI =
            new StatDisplayParams(StatDisplayType.MULTI);
    public static final StatDisplayParams TIME =
            new StatDisplayParams(StatDisplayType.TIME);
    public static final StatDisplayParams HUNDREDTHS =
            new StatDisplayParams(StatDisplayType.HUNDREDTHS);
    public static final StatDisplayParams DETRIMENTAL =
            new StatDisplayParams(StatDisplayType.FLAT, true);
    public static final StatDisplayParams MULTI_DETRIMENTAL =
            new StatDisplayParams(StatDisplayType.MULTI, true);
    private final StatDisplayType type;
    private final boolean downIsGood;

    /**
     * Constructs a new object.
     *
     * @param type       the {@link StatDisplayType} used to format the stat boost
     * @param downIsGood true if negative boosts are considered beneficial
     */
    public StatDisplayParams(StatDisplayType type, boolean downIsGood) {
        this.type = type;
        this.downIsGood = downIsGood;
    }

    public StatDisplayParams(StatDisplayType type) {
        this(type, false);
    }

    public StatDisplayType getType() {
        return type;
    }

    public boolean isDownGood() {
        return downIsGood;
    }

    @Override
    public String toString() {
        return "StatDisplayParams{" +
                "type=" + type +
                ", downIsGood=" + downIsGood +
                '}';
    }

    /**
     * Formats and colors a stat boost.
     *
     * @param amt the amount of boost
     * @return a {@link Text} object
     */
    public Text format(long amt) {
        String raw = type.format(amt);
        LiteralText uncolored = new LiteralText(raw);
        if (amt > 0) {
            return uncolored.formatted(downIsGood ? Formatting.RED : Formatting.GREEN);
        }
        if (amt < 0) {
            return uncolored.formatted(downIsGood ? Formatting.GREEN : Formatting.RED);
        }
        return uncolored;
    }
}
