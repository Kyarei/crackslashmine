package flirora.crackslashmine.core.stat;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.CsmCharacter;
import flirora.crackslashmine.core.DamageType;
import flirora.crackslashmine.core.Fundamental;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.Identifier;

public class FundamentalStat extends Stat {
  private final Fundamental fundamental;

  public FundamentalStat(Fundamental fundamental) {
    super(new Identifier(CrackSlashMineMod.MOD_ID, fundamental.getName()), -1,
        DamageType.PHYSICAL);
    this.fundamental = fundamental;
  }

  @Override
  public long getBase(CsmCharacter character, LivingEntity entity) {
    return character.getIntrinsicFundamental(fundamental);
  }
}
