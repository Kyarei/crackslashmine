package flirora.crackslashmine.core.stat;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.CsmCharacter;
import flirora.crackslashmine.core.DamageType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.Identifier;

public class LevelStat extends Stat {
  private static final Identifier ID =
      new Identifier(CrackSlashMineMod.MOD_ID, "level");

  public LevelStat() {
    super(ID, -1, DamageType.PHYSICAL);
  }

  @Override
  public long getBase(CsmCharacter character, LivingEntity entity) {
    return character.getLevel() - 1;
  }
}
