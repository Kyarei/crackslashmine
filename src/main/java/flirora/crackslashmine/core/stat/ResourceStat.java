package flirora.crackslashmine.core.stat;

import com.google.common.collect.ImmutableList;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.CsmCharacter;
import flirora.crackslashmine.core.DamageType;
import flirora.crackslashmine.core.ResourceType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.Identifier;

public class ResourceStat extends Stat {
  private final ResourceType resourceType;
  private final Stat multi;
  private final Stat fundamentalDependency;

  public ResourceStat(ResourceType resourceType, Stat multi,
      Stat fundamentalDependency) {
    super(
        new Identifier(CrackSlashMineMod.MOD_ID,
            resourceType.getName() + "_bonus"),
        resourceType.getColor(), DamageType.PHYSICAL,
        ImmutableList.of(multi, fundamentalDependency));
    this.resourceType = resourceType;
    this.multi = multi;
    this.fundamentalDependency = fundamentalDependency;
  }

  @Override
  public long getBase(CsmCharacter character, LivingEntity entity) {
    long ratio = CrackSlashMineMod.config.csmToVanillaHealthRatio;
    if (resourceType == ResourceType.HEALTH) {
      return ratio * Math.round(entity.getMaxHealth());
    }
    return ratio * 20;
  }

  @Override
  public long postprocess(long old, StatContainer stats) {
    long res = old;
    res += 5 * stats.get(Stats.LEVEL);
    res += stats.get(fundamentalDependency);
    res = (10000 + stats.get(multi)) * res / 10000;
    return res;
  }
}
