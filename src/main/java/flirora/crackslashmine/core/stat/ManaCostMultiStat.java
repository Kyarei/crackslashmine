package flirora.crackslashmine.core.stat;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.CsmCharacter;
import flirora.crackslashmine.core.DamageType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.Identifier;

public class ManaCostMultiStat extends Stat {
  private final DamageType damageType;

  public ManaCostMultiStat(DamageType damageType) {
    super(new Identifier(CrackSlashMineMod.MOD_ID,
        damageType.getName() + "_mana_cost_multi"), -1, damageType);
    this.damageType = damageType;
  }

  public DamageType getDamageType() {
    return damageType;
  }

  @Override
  public long getBase(CsmCharacter character, LivingEntity entity) {
    return 0;
  }

  @Override
  public StatDisplayParams getParams() {
    return StatDisplayParams.MULTI_DETRIMENTAL;
  }
}
