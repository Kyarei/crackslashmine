package flirora.crackslashmine.core.stat;

import java.util.Collections;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.CsmCharacter;
import flirora.crackslashmine.core.DamageType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.Identifier;

public class ResistanceRatingStat extends Stat {
  private final DamageType damageType;

  public ResistanceRatingStat(DamageType damageType) {
    super(
        new Identifier(CrackSlashMineMod.MOD_ID,
            damageType.getName() + "_resistance"),
        -1, damageType, Collections.singletonList(Stats.UNIVERSAL_RESIST));
    this.damageType = damageType;
  }

  @Override
  public long getBase(CsmCharacter character, LivingEntity entity) {
    return 0;
  }

  @Override
  public long postprocess(long old, StatContainer stats) {
    return old + stats.get(Stats.UNIVERSAL_RESIST);
  }
}
