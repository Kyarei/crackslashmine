package flirora.crackslashmine.core.stat;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import org.jetbrains.annotations.ApiStatus;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.CsmRegistries;
import flirora.crackslashmine.core.DamageType;
import flirora.crackslashmine.core.Fundamental;
import flirora.crackslashmine.core.ResourceType;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class Stats {
  public static final Stat LEVEL = register(new LevelStat());

  public static final Stat STRENGTH =
      register(new FundamentalStat(Fundamental.STRENGTH));
  public static final Stat DEXTERITY =
      register(new FundamentalStat(Fundamental.DEXTERITY));
  public static final Stat INTELLECT =
      register(new FundamentalStat(Fundamental.INTELLECT));
  public static final Stat WISDOM =
      register(new FundamentalStat(Fundamental.WISDOM));
  public static final Stat ENDURANCE =
      register(new FundamentalStat(Fundamental.ENDURANCE));
  public static final Stat FORTITUDE =
      register(new FundamentalStat(Fundamental.FORTITUDE));

  public static final Stat HEALTH_REGEN =
      register(new ResourceRegenStat(ResourceType.HEALTH, FORTITUDE));
  public static final Stat HEALTH_MULTI =
      register(new ResourceMultiStat(ResourceType.HEALTH, FORTITUDE));
  public static final Stat HEALTH_BONUS =
      register(new ResourceStat(ResourceType.HEALTH, HEALTH_MULTI, FORTITUDE));
  public static final Stat MANA_REGEN =
      register(new ResourceRegenStat(ResourceType.MANA, INTELLECT));
  public static final Stat MANA_MULTI =
      register(new ResourceMultiStat(ResourceType.MANA, INTELLECT));
  public static final Stat MANA_BONUS =
      register(new ResourceStat(ResourceType.MANA, MANA_MULTI, INTELLECT));
  public static final Stat STAMINA_REGEN =
      register(new ResourceRegenStat(ResourceType.STAMINA, ENDURANCE));
  public static final Stat STAMINA_MULTI =
      register(new ResourceMultiStat(ResourceType.STAMINA, ENDURANCE));
  public static final Stat STAMINA_BONUS = register(
      new ResourceStat(ResourceType.STAMINA, STAMINA_MULTI, ENDURANCE));

  public static final Stat CRITICAL_DAMAGE = register(new CriticalDamageStat(
      new Identifier(CrackSlashMineMod.MOD_ID, "critical_damage"), STRENGTH));
  public static final Stat CRITICAL_RATE = register(new CriticalRateStat(
      new Identifier(CrackSlashMineMod.MOD_ID, "critical_rate"), STRENGTH));
  // Thanks to Valoeghese for suggesting "kritikhälh"
  public static final Stat KRITIKHÄLH_DAMAGE = register(new CriticalDamageStat(
      new Identifier(CrackSlashMineMod.MOD_ID, "kritikhaelh_damage"), WISDOM));
  public static final Stat KRITIKHÄLH_RATE = register(new CriticalRateStat(
      new Identifier(CrackSlashMineMod.MOD_ID, "kritikhaelh_rate"), WISDOM));

  public static final Stat DODGE_RATING = register(new DodgeRatingStat());
  public static final Stat ACCURACY = register(new AccuracyStat());
  public static final Stat UNIVERSAL_DAMAGE = register(new SimpleStat(
      new Identifier(CrackSlashMineMod.MOD_ID, "universal_damage"),
      StatDisplayParams.MULTI));
  public static final Stat UNIVERSAL_RESIST =
      register(new UniversalResistanceRatingStat());

  public static final Stat LOOT_FIND = register(
      new SimpleStat(new Identifier(CrackSlashMineMod.MOD_ID, "loot_find"),
          StatDisplayParams.MULTI));
  public static final Stat GOLD_FIND = register(
      new SimpleStat(new Identifier(CrackSlashMineMod.MOD_ID, "gold_find"),
          StatDisplayParams.MULTI));
  public static final Stat BONUS_FORTUNE_CHANCE = register(new SimpleStat(
      new Identifier(CrackSlashMineMod.MOD_ID, "bonus_fortune_chance"),
      StatDisplayParams.MULTI));

  public static final Stat OUTGOING_HEAL_MULTI = register(new HealMultiStat(
      new Identifier(CrackSlashMineMod.MOD_ID, "outgoing_heal_multi"), WISDOM));
  public static final Stat INCOMING_HEAL_MULTI = register(new HealMultiStat(
      new Identifier(CrackSlashMineMod.MOD_ID, "incoming_heal_multi"),
      DEXTERITY));

  public static final Stat AIR_TIME = register(new AirTimeStat());

  public static final Stat MELEE_WEAPON_BONUS = register(new SimpleStat(
      new Identifier(CrackSlashMineMod.MOD_ID, "melee_weapon_bonus"),
      StatDisplayParams.MULTI));
  public static final Stat RANGED_WEAPON_BONUS = register(new SimpleStat(
      new Identifier(CrackSlashMineMod.MOD_ID, "ranged_weapon_bonus"),
      StatDisplayParams.MULTI));
  public static final Stat SWORD_BONUS = register(
      new SimpleStat(new Identifier(CrackSlashMineMod.MOD_ID, "sword_bonus"),
          StatDisplayParams.MULTI));
  public static final Stat AXE_BONUS = register(
      new SimpleStat(new Identifier(CrackSlashMineMod.MOD_ID, "axe_bonus"),
          StatDisplayParams.MULTI));
  public static final Stat BOW_BONUS = register(
      new SimpleStat(new Identifier(CrackSlashMineMod.MOD_ID, "bow_bonus"),
          StatDisplayParams.MULTI));
  public static final Stat CROSSBOW_BONUS = register(
      new SimpleStat(new Identifier(CrackSlashMineMod.MOD_ID, "crossbow_bonus"),
          StatDisplayParams.MULTI));

  public static final EnumMap<DamageType, Stat> DAMAGE_BONUS =
      registerForEachDamageType(DamageBonusStat::new);
  public static final EnumMap<DamageType, Stat> DAMAGE_MULTI =
      registerForEachDamageType(DamageMultiStat::new);
  public static final EnumMap<DamageType, Stat> RESISTANCE_RATING =
      registerForEachDamageType(ResistanceRatingStat::new);
  public static final EnumMap<DamageType, Stat> MANA_COST_MULTI =
      registerForEachDamageType(ManaCostMultiStat::new);

  /**
   * A topological order of stats, such that a stat is guaranteed to occur after
   * all of its dependencies. This field is computed when the {@link Stat} class
   * is loaded; any stats registered afterwards will not be reflected here.
   */
  public static final List<Stat> TOPOLOGICAL_ORDER =
      new TopologicalSorter().compute();

  /**
   * Only used to force loading of this class.
   */
  @ApiStatus.Internal
  public static void load() {
  }

  private static Stat register(Stat s) {
    Registry.register(CsmRegistries.STAT, s.getId(), s);
    return s;
  }

  private static EnumMap<DamageType, Stat> registerForEachDamageType(
      Function<DamageType, Stat> f) {
    EnumMap<DamageType, Stat> res = DamageType.forEachDamageType(f);
    for (Stat stat : res.values()) {
      register(stat);
    }
    return res;
  }

  private static EnumMap<ResourceType, Stat> registerForEachResourceType(
      Function<ResourceType, Stat> f) {
    EnumMap<ResourceType, Stat> res = ResourceType.forEachResourceType(f);
    for (Stat stat : res.values()) {
      register(stat);
    }
    return res;
  }

  private static class TopologicalSorter {
    private final List<Stat> res;
    private final Set<Stat> visited;

    public TopologicalSorter() {
      res = new ArrayList<>();
      visited = new HashSet<>();
    }

    private void add(Stat stat) {
      if (visited.contains(stat))
        return; // No need to add it again
      List<Stat> dependencies = stat.getPostprocessingDependencies();
      for (Stat dep : dependencies) {
        add(dep);
      }
      stat.setOrdinal(res.size());
      res.add(stat);
      visited.add(stat);
    }

    public List<Stat> compute() {
      for (Stat stat : CsmRegistries.STAT) {
        add(stat);
      }
      return res;
    }
  }
}
