package flirora.crackslashmine.core.stat;

import java.util.Collections;

import flirora.crackslashmine.core.CsmCharacter;
import flirora.crackslashmine.core.DamageType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.Identifier;

public class HealMultiStat extends Stat {
  private final Stat dependency;

  public HealMultiStat(Identifier id, Stat dependency) {
    super(id, 0xc4fca9, DamageType.PHYSICAL,
        Collections.singletonList(dependency));
    this.dependency = dependency;
  }

  @Override
  public long getBase(CsmCharacter character, LivingEntity entity) {
    return 0;
  }

  @Override
  public long postprocess(long old, StatContainer stats) {
    return old + 5 * stats.get(dependency);
  }

  @Override
  public StatDisplayParams getParams() {
    return StatDisplayParams.MULTI;
  }
}
