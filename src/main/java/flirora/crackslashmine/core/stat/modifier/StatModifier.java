package flirora.crackslashmine.core.stat.modifier;

import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonSyntaxException;
import flirora.crackslashmine.core.CsmRegistries;
import flirora.crackslashmine.core.stat.Stat;
import flirora.crackslashmine.item.equipment.naming.AffixType;
import flirora.crackslashmine.item.equipment.types.EquipmentType;
import net.minecraft.util.Identifier;
import net.minecraft.util.Pair;
import org.apache.commons.math3.analysis.polynomials.PolynomialFunction;

import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A prepackaged stat modifier, which adds one or more stat boosts.
 */
public class StatModifier {
    private final Identifier id;

    /**
     * Describes a stat boost associated with a modifier.
     */
    public static class Entry {
        private final Stat stat;
        // This is evaluated at the level requirement.
        private final PolynomialFunction curveByLevel;
        // The actual amount is scaled by
        // 1 + adjustmentFactor * (equipmentAnima - intrinsicAnima).
        private final double adjustmentFactor;
        // The actual amount is scaled by a factor sampled uniformly in
        // [1 - spread, 1 + spread),
        // where spread is adjusted according to discordia.
        private final double baseSpread;

        /**
         * Constructs a new stat boost entry.
         *
         * @param stat             the {@link Stat} to boost
         * @param curveByLevel     a {@link PolynomialFunction} describing the
         *                         growth in the amount by level
         * @param adjustmentFactor the adjustment factor; the actual boost is
         *                         scaled by
         *                         <code>1 + adjustmentFactor * (equipmentAnima - intrinsicAnima)</code>
         * @param baseSpread       how much to randomly increase or decrease
         *                         the actual boost by; that is, the actual
         *                         boost is scaled by
         *                         [<code>1 - spread</code>, <code>1 + spread</code>).
         */
        public Entry(Stat stat, PolynomialFunction curveByLevel, double adjustmentFactor, double baseSpread) {
            this.stat = stat;
            this.curveByLevel = curveByLevel;
            this.adjustmentFactor = adjustmentFactor;
            this.baseSpread = baseSpread;
        }

        public Stat getStat() {
            return stat;
        }

        public PolynomialFunction getCurveByLevel() {
            return curveByLevel;
        }

        public double getAdjustmentFactor() {
            return adjustmentFactor;
        }

        public double getBaseSpread() {
            return baseSpread;
        }

        @Override
        public String toString() {
            return "Entry{" +
                    "stat=" + stat +
                    ", curveByLevel=" + curveByLevel +
                    ", adjustmentFactor=" + adjustmentFactor +
                    ", baseSpread=" + baseSpread +
                    '}';
        }

        /**
         * Gets the total boost to the stat.
         *
         * @param r              a {@link Random} object to sample from
         * @param level          the level of the equipment to place the boost on
         * @param anima          the anima of the equipment
         * @param discordia      the discordia of the equipment
         * @param intrinsicAnima the intrinsic anima of the stat modifier
         * @return the total boost; this should be rounded
         */
        public double sampleAmount(Random r, int level, double anima, double discordia, double intrinsicAnima) {
            double base = curveByLevel.value(level);
            double spread = baseSpread * Math.exp(discordia);
            return base *
                    (1 + adjustmentFactor * (anima - intrinsicAnima)) *
                    (1 + spread * (2 * r.nextDouble() - 1));
        }

        private static class JsonFormat {
            public String stat;
            public double[] curveByLevel;
            public double adjustmentFactor;
            public double baseSpread;
        }

        public static Entry fromJson(JsonElement json) {
            flirora.crackslashmine.core.stat.modifier.StatModifier.Entry.JsonFormat deserialized = GSON.fromJson(json, flirora.crackslashmine.core.stat.modifier.StatModifier.Entry.JsonFormat.class);
            return new Entry(
                    CsmRegistries.STAT.get(Identifier.tryParse(deserialized.stat)),
                    new PolynomialFunction(deserialized.curveByLevel),
                    deserialized.adjustmentFactor,
                    deserialized.baseSpread
            ); // Phew.
        }
    }

    /**
     * Data related to naming.
     */
    public static class Naming {
        private final int affixFlags;

        /**
         * Construct a new naming object.
         *
         * @param affixFlags a bitfield, such that bit number
         *                   <code>affixType.ordinal()</code> is set if
         *                   an affix at <code>affixType</code> should be
         *                   considered when naming
         */
        public Naming(int affixFlags) {
            this.affixFlags = affixFlags;
        }

        public Naming() {
            this(0);
        }

        public int getAffixFlags() {
            return affixFlags;
        }

        private static class JsonFormat {
            public List<String> affixFlags = ImmutableList.of();
        }

        public static Naming fromJson(Identifier id, JsonElement json) {
            flirora.crackslashmine.core.stat.modifier.StatModifier.Naming.JsonFormat deserialized = GSON.fromJson(json, flirora.crackslashmine.core.stat.modifier.StatModifier.Naming.JsonFormat.class);
            int affixFlags = 0;
            for (String s : deserialized.affixFlags) {
                AffixType t = AffixType.fromName(s);
                if (t == null)
                    throw new JsonSyntaxException(s + " is not a valid affix type");
                affixFlags |= t.flag();
            }
            return new Naming(affixFlags);
        }
    }

    private final List<Entry> modifiers;
    /*
        Makes the weight of this modifier
        1 / (1 + exp(-intrinsicAnima * (equipmentAnima - intrinsicAnima)))
        That is, stat mods with intrinsicAnima < 0 are considered
        unfavorable mods, and those with intrinsicAnima > 0 are favorable.
     */
    private final double baseWeight;
    private final double intrinsicAnima;
    private final Set<EquipmentType> eligibleTypes;
    private final Naming naming;

    /**
     * Constructs a new stat modifier.
     *
     * @param id             the ID that this modifier should be registered at
     * @param modifiers      a list of {@link Entry} objects describing each
     *                       stat boost
     * @param baseWeight     the base weight of this modifier
     * @param intrinsicAnima the intrinsic anima of this modifier; the weight
     *                       of each eligible modifier is set to
     *                       <code>baseWeight / (1 + Math.exp(-intrinsicAnima * (equipmentAnima - intrinsicAnima)))</code>.
     * @param eligibleTypes  the set of {@link EquipmentType}s that can obtain
     *                       this modifier
     * @param naming         a {@link Naming} object for this modifier
     */
    public StatModifier(Identifier id,
                        List<Entry> modifiers, double baseWeight, double intrinsicAnima,
                        Set<EquipmentType> eligibleTypes, Naming naming) {
        if (baseWeight <= 0.0)
            throw new IllegalArgumentException("base weight must be positive");
        this.id = id;
        this.modifiers = modifiers;
        this.baseWeight = baseWeight;
        this.intrinsicAnima = intrinsicAnima;
        this.eligibleTypes = eligibleTypes;
        this.naming = naming;
    }

    public Identifier getId() {
        return id;
    }

    public List<Entry> getModifiers() {
        return modifiers;
    }

    public double getIntrinsicAnima() {
        return intrinsicAnima;
    }

    public Set<EquipmentType> getEligibleTypes() {
        return eligibleTypes;
    }

    public Naming getNaming() {
        return naming;
    }

    /**
     * Gets the weight of this modifier at a given equipment anima.
     *
     * @param anima the anima of the equipment in question
     * @return the weight
     */
    public double getWeightAtAnima(double anima) {
        return baseWeight / (1 + Math.exp(-intrinsicAnima * (anima - intrinsicAnima)));
    }

    /**
     * Samples the amount of stat boost from each {@link Entry}.
     *
     * @param r         the {@link Random} object to sample from
     * @param level     the level of the equipment
     * @param anima     the anima of the equipment
     * @param discordia the discordia of the equipment
     * @return a {@link Stream} of pairs of a stat and its associated boost.
     * The boost can be rounded into a <code>long</code>.
     */
    public Stream<Pair<Stat, Double>>
    sampleAmount(Random r, int level, double anima, double discordia) {
        return modifiers.stream()
                .map(e -> new Pair<>(
                        e.getStat(),
                        e.sampleAmount(r, level, anima, discordia, intrinsicAnima)));
    }

    @Override
    public String toString() {
        return "StatModifier{" +
                "id=" + id +
                ", modifiers=" + modifiers +
                ", intrinsicAnima=" + intrinsicAnima +
                ", eligibleTypes=" + eligibleTypes +
                '}';
    }

    private static class JsonFormat {
        public List<JsonElement> modifiers;
        public double intrinsicAnima = 0.0;
        public double baseWeight = 1.0;
        public List<String> eligibleTypes;
        public JsonElement naming;
    }

    private static final Gson GSON = (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();

    public static StatModifier fromJson(Identifier id, JsonElement json) {
        JsonFormat deserialized = GSON.fromJson(json, JsonFormat.class);
        return new StatModifier(
                id,
                deserialized.modifiers.stream()
                        .map(Entry::fromJson)
                        .collect(Collectors.toList()),
                deserialized.baseWeight,
                deserialized.intrinsicAnima,
                deserialized.eligibleTypes.stream().map(name ->
                        EquipmentType.getByName(Identifier.tryParse(name)))
                        .collect(Collectors.toSet()),
                deserialized.naming == null ?
                        new Naming() :
                        Naming.fromJson(id, deserialized.naming)); // Phew.
    }

}
