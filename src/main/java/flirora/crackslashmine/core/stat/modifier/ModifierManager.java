package flirora.crackslashmine.core.stat.modifier;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.item.equipment.types.EquipmentType;
import net.fabricmc.fabric.api.resource.IdentifiableResourceReloadListener;
import net.minecraft.resource.JsonDataLoader;
import net.minecraft.resource.ResourceManager;
import net.minecraft.util.Identifier;
import net.minecraft.util.profiler.Profiler;
import org.apache.logging.log4j.Level;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

public class ModifierManager extends JsonDataLoader implements IdentifiableResourceReloadListener {
    private static final Gson GSON = (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();
    private static final Identifier ID = new Identifier(CrackSlashMineMod.MOD_ID, "stat_modifiers");

    private ImmutableMap<Identifier, StatModifier> modifiersById;

    public static final ModifierManager INSTANCE = new ModifierManager();

    private ModifierManager() {
        super(GSON, "csm/stat_modifiers");
    }

    // In our case, we have no data that needs to be sent to the client,
    // so we can just deserialize the jsons.

    @Override
    protected void apply(Map<Identifier, JsonElement> loader,
                         ResourceManager resourceManager,
                         Profiler profiler) {
        int count = 0;
        ImmutableMap.Builder<Identifier, StatModifier> builder =
                new ImmutableMap.Builder<>();
        for (Map.Entry<Identifier, JsonElement> entry : loader.entrySet()) {
            Identifier id = entry.getKey();
            JsonElement json = entry.getValue();
            try {
                StatModifier modifier = StatModifier.fromJson(id, json);
                builder.put(id, modifier);
            } catch (Exception e) {
                CrackSlashMineMod.logException("Failed to load modifier " + id, e);
            }
            ++count;
        }
        modifiersById = builder.build();
        CrackSlashMineMod.log(Level.INFO, "Loaded " + count + " stat modifiers");
    }

    public @Nullable
    StatModifier get(Identifier id) {
        return modifiersById.get(id);
    }

    @Override
    public Identifier getFabricId() {
        return ID;
    }

    @Override
    public Collection<Identifier> getFabricDependencies() {
        return ImmutableList.of();
    }

    public ArrayList<StatModifier> getAllEligibleModifiers(EquipmentType equipmentType) {
        return modifiersById.values().stream()
                .filter(mod -> mod.getEligibleTypes().contains(equipmentType))
                .collect(Collectors.toCollection(ArrayList::new));
    }
}
