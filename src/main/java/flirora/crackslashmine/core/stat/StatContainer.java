package flirora.crackslashmine.core.stat;

import java.util.Arrays;
import java.util.Iterator;

import org.jetbrains.annotations.NotNull;

public class StatContainer implements Iterable<StatContainer.Entry> {
  private final long[] values;

  public StatContainer() {
    this.values = new long[Stats.TOPOLOGICAL_ORDER.size()];
  }

  public StatContainer(StatContainer from) {
    this.values = Arrays.copyOf(from.values, Stats.TOPOLOGICAL_ORDER.size());
  }

  public long get(Stat stat) {
    return values[stat.getOrdinal()];
  }

  public void set(Stat stat, long value) {
    values[stat.getOrdinal()] = value;
  }

  public void add(Stat stat, long value) {
    values[stat.getOrdinal()] += value;
  }

  public int size() {
    return values.length;
  }

  public void clear() {
    Arrays.fill(values, 0);
  }

  public StatView frozen() {
    return new StatView(this);
  }

  @NotNull
  @Override
  public Iterator<Entry> iterator() {
    return new StatIterator();
  }

  public static class Entry {
    private final Stat key;
    private final long value;

    public Entry(Stat key, long value) {
      this.key = key;
      this.value = value;
    }

    public Stat getKey() {
      return key;
    }

    public long getValue() {
      return value;
    }
  }

  private class StatIterator implements Iterator<Entry> {
    private int i = 0;

    @Override
    public boolean hasNext() {
      return i < StatContainer.this.values.length;
    }

    @Override
    public Entry next() {
      Stat key = Stats.TOPOLOGICAL_ORDER.get(i);
      long value = StatContainer.this.values[i];
      ++i;
      return new Entry(key, value);
    }
  }
}
