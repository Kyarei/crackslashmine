package flirora.crackslashmine.core.stat;

import com.google.common.collect.ImmutableList;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.CsmCharacter;
import flirora.crackslashmine.core.DamageType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.Identifier;

public class DodgeRatingStat extends Stat {
  public DodgeRatingStat() {
    super(new Identifier(CrackSlashMineMod.MOD_ID, "dodge_rating"), 0x43e050,
        DamageType.PHYSICAL, ImmutableList.of(Stats.DEXTERITY));
  }

  @Override
  public long getBase(CsmCharacter character, LivingEntity entity) {
    return 0;
  }

  @Override
  public long postprocess(long old, StatContainer stats) {
    return old + stats.get(Stats.DEXTERITY) * 25;
  }
}
