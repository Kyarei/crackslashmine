package flirora.crackslashmine.core;

public interface ResourceLimitComponent {
    long getMaxHealth();

    long getMaxMana();

    long getMaxStamina();

    int getLevel();
}
