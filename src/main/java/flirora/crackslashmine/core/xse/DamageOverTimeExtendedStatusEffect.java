package flirora.crackslashmine.core.xse;

import org.jetbrains.annotations.Nullable;

import flirora.crackslashmine.core.Beautify;
import flirora.crackslashmine.core.CsmAttack;
import flirora.crackslashmine.core.CsmDamageSources;
import flirora.crackslashmine.core.DamageType;
import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.item.TooltipUtils;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffectType;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;

public class DamageOverTimeExtendedStatusEffect extends ExtendedStatusEffect {
  private final DamageType damageType;

  /**
   * Construct an instance of this class.
   *
   * @param id         the {@link Identifier} that this effect should be
   *                   registered under
   * @param color      the color of the effect particles
   * @param damageType the {@link DamageType} dealt by this effect
   */
  protected DamageOverTimeExtendedStatusEffect(Identifier id, int color,
      DamageType damageType) {
    super(id, StatusEffectType.HARMFUL, color, damageType);
    this.damageType = damageType;
  }

  @Override
  public long applyTickEffect(LivingEntity target, LivingEntityStats stats,
      @Nullable LivingEntity attacker, long quantity, int timeRemaining) {
    if (timeRemaining % 20 == 0) {
      int seconds = timeRemaining / 20;
      long rounded = (quantity + seconds / 2) / seconds;
      if (rounded > 0) {
        LivingEntityStats.dealDamage(target, attacker,
            new CsmAttack(true, new CsmAttack.Entry(damageType, rounded)),
            CsmDamageSources.damageOverTimeFrom(damageType, attacker));
        return quantity - rounded;
      }
    }
    return quantity;
  }

  @Override
  public ExtendedStatusEffectInstance scaleDamage(
      ExtendedStatusEffectInstance effect, double scale) {
    return new ExtendedStatusEffectInstance(effect.getEffect(),
        effect.getDuration(), Math.round(effect.getIntensity() * scale),
        effect.getAttacker());
  }

  @Override
  protected Text formatDetails(Text quantityText) {
    return new TranslatableText("csm.hud.xseDamage", quantityText);
  }

  @Override
  public MutableText formatInTooltip(ExtendedStatusEffectInstance instance) {
    return new TranslatableText("csm.xse.dotDisplay", getStatusEffectName(),
        Beautify.beautify(instance.getIntensity()),
        Beautify.beautifyTime(instance.getDuration()));
  }

  @Override
  public MutableText formatInTooltip(
      RangedExtendedStatusEffectInstance instance) {
    Text intensity = TooltipUtils.intensityRangeText(instance.getMinIntensity(),
        instance.getMaxIntensity());
    Text duration = TooltipUtils.durationRangeText(instance.getMinDuration(),
        instance.getMaxDuration());
    return new TranslatableText("csm.xse.dotDisplay", getStatusEffectName(),
        intensity, duration);
  }

}
