package flirora.crackslashmine.core.xse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import flirora.crackslashmine.core.CsmRegistries;
import flirora.crackslashmine.core.MathUtils;
import net.minecraft.entity.LivingEntity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.util.Random;

public class RangedExtendedStatusEffectInstance {
    private final ExtendedStatusEffect effect;
    private final int minDuration, maxDuration;
    private final long minIntensity, maxIntensity;

    public RangedExtendedStatusEffectInstance(ExtendedStatusEffect effect, int minDuration, int maxDuration, long minIntensity, long maxIntensity) {
        this.effect = effect;
        this.minDuration = minDuration;
        this.maxDuration = maxDuration;
        this.minIntensity = minIntensity;
        this.maxIntensity = maxIntensity;
    }

    public ExtendedStatusEffect getEffect() {
        return effect;
    }

    public int getMinDuration() {
        return minDuration;
    }

    public int getMaxDuration() {
        return maxDuration;
    }

    public long getMinIntensity() {
        return minIntensity;
    }

    public long getMaxIntensity() {
        return maxIntensity;
    }

    public ExtendedStatusEffectInstance roll(Random r, @Nullable LivingEntity attacker) {
        return new ExtendedStatusEffectInstance(
                effect,
                MathUtils.nextIntInclusive(r, minDuration, maxDuration),
                MathUtils.nextLongInclusive(r, minIntensity, maxIntensity),
                attacker
        );
    }

    public Text formatInTooltip() {
        return effect.formatInTooltip(this).formatted(effect.getType().getFormatting());
    }

    public CompoundTag toTag() {
        CompoundTag tag = new CompoundTag();
        tag.putString("id", effect.getId().toString());
        tag.putInt("minDuration", minDuration);
        tag.putInt("maxDuration", maxDuration);
        tag.putLong("minIntensity", minIntensity);
        tag.putLong("maxIntensity", maxIntensity);
        return tag;
    }

    public static RangedExtendedStatusEffectInstance fromTag(CompoundTag tag) {
        return new RangedExtendedStatusEffectInstance(
                CsmRegistries.EXTENDED_STATUS_EFFECT.get(Identifier.tryParse(tag.getString("id"))),
                tag.getInt("minDuration"),
                tag.getInt("maxDuration"),
                tag.getLong("minIntensity"),
                tag.getLong("maxIntensity")
        );
    }

    private static class JsonFormat {
        public String effect;
        public int minDuration, maxDuration;
        public long minIntensity, maxIntensity;
    }

    private static final Gson GSON = (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();

    public static RangedExtendedStatusEffectInstance fromJson(JsonElement element) {
        JsonFormat deserialized = GSON.fromJson(element, JsonFormat.class);
        return new RangedExtendedStatusEffectInstance(
                CsmRegistries.EXTENDED_STATUS_EFFECT.get(Identifier.tryParse(deserialized.effect)),
                deserialized.minDuration, deserialized.maxDuration,
                deserialized.minIntensity, deserialized.maxIntensity
        );
    }

    public void writeToPacket(PacketByteBuf buf) {
        buf.writeInt(CsmRegistries.EXTENDED_STATUS_EFFECT.getRawId(effect));
        buf.writeInt(minDuration);
        buf.writeInt(maxDuration);
        buf.writeLong(minIntensity);
        buf.writeLong(maxIntensity);
    }

    public static RangedExtendedStatusEffectInstance readFromPacket(PacketByteBuf buf) {
        return new RangedExtendedStatusEffectInstance(
                CsmRegistries.EXTENDED_STATUS_EFFECT.get(buf.readInt()),
                buf.readInt(),
                buf.readInt(),
                buf.readLong(),
                buf.readLong()
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RangedExtendedStatusEffectInstance that = (RangedExtendedStatusEffectInstance) o;
        return minDuration == that.minDuration &&
                maxDuration == that.maxDuration &&
                minIntensity == that.minIntensity &&
                maxIntensity == that.maxIntensity &&
                effect == that.effect;
    }

    @Override
    public int hashCode() {
        return Objects.hash(effect, minDuration, maxDuration, minIntensity, maxIntensity);
    }
}
