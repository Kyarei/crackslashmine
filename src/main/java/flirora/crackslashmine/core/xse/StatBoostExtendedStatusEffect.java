package flirora.crackslashmine.core.xse;

import org.jetbrains.annotations.Nullable;

import flirora.crackslashmine.core.Beautify;
import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.core.stat.Stat;
import flirora.crackslashmine.item.TooltipUtils;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffectType;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;

public class StatBoostExtendedStatusEffect extends ExtendedStatusEffect {
  private final Stat stat;

  public StatBoostExtendedStatusEffect(Stat stat) {
    this(stat.getId(), stat);
  }

  private StatBoostExtendedStatusEffect(Identifier statId, Stat stat) {
    super(new Identifier(statId.getNamespace(), statId.getPath() + "_boost"),
        stat.getParams().isDownGood() ? StatusEffectType.HARMFUL
            : StatusEffectType.BENEFICIAL,
        stat.getColor(), stat.getType());
    this.stat = stat;
  }

  @Override
  public long applyTickEffect(LivingEntity target, LivingEntityStats stats,
      @Nullable LivingEntity attacker, long quantity, int timeRemaining) {
    return quantity;
  }

  @Override
  protected Text formatDetails(Text quantityText) {
    throw new UnsupportedOperationException("Don't call this method directly!");
  }

  @Override
  public Text formatDetails(long quantity) {
    return stat.format(quantity);
  }

  @Override
  public Text formatDetails(long min, long max) {
    return new TranslatableText("tooltip.csm.range",
        stat.getParams().format(min), stat.format(max));
  }

  @Override
  public MutableText formatInTooltip(ExtendedStatusEffectInstance instance) {
    return new TranslatableText("csm.xse.statDisplay", getStatusEffectName(),
        stat.getParams().getType().format(instance.getIntensity()),
        stat.getName(), Beautify.beautifyTime(instance.getDuration()));
  }

  @Override
  public MutableText formatInTooltip(
      RangedExtendedStatusEffectInstance instance) {
    Text intensity = TooltipUtils.intensityRangeText(stat.getParams().getType(),
        instance.getMinIntensity(), instance.getMaxIntensity());
    Text duration = TooltipUtils.durationRangeText(instance.getMinDuration(),
        instance.getMaxDuration());
    return new TranslatableText("csm.xse.statDisplay", getStatusEffectName(),
        intensity, stat.getName(), duration);
  }

  public Stat getStat() {
    return stat;
  }

}
