package flirora.crackslashmine.core.xse;

import org.jetbrains.annotations.Nullable;

import flirora.crackslashmine.core.Beautify;
import flirora.crackslashmine.core.CsmRegistries;
import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.core.stat.StatView;
import flirora.crackslashmine.core.stat.Stats;
import net.fabricmc.fabric.api.util.NbtType;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;
import net.minecraft.world.World;

public class ExtendedStatusEffectInstance {
  private final ExtendedStatusEffect effect;
  private int duration;
  private long intensity;
  // always null on the client
  private final @Nullable LivingEntity attacker;

  public ExtendedStatusEffectInstance(ExtendedStatusEffect effect, int duration,
      long intensity, @Nullable LivingEntity attacker) {
    this.effect = effect;
    this.duration = duration;
    this.intensity = intensity;
    this.attacker = attacker;
  }

  public ExtendedStatusEffect getEffect() {
    return effect;
  }

  public int getDuration() {
    return duration;
  }

  public void setDuration(int duration) {
    this.duration = duration;
  }

  public long getIntensity() {
    return intensity;
  }

  public void setIntensity(long intensity) {
    this.intensity = intensity;
  }

  public @Nullable LivingEntity getAttacker() {
    return attacker;
  }

  public boolean tick(LivingEntity target, LivingEntityStats stats) {
    intensity =
        effect.applyTickEffect(target, stats, attacker, intensity, duration);
    return --duration == 0;
  }

  public Text formatInTooltip() {
    return effect.formatInTooltip(this)
        .formatted(effect.getType().getFormatting());
  }

  public Text formatInHud() {
    return new TranslatableText("gui.csm.xse.display",
        effect.formatDetails(intensity), Beautify.beautifyTime(duration));
  }

  public ExtendedStatusEffectInstance scaleToAttackerStats(StatView stats) {
    long bonus = stats.get(Stats.DAMAGE_MULTI.get(effect.getElement()));
    return effect.scaleDamage(this, 1 + 1e-4 * bonus);
  }

  public ExtendedStatusEffectInstance scaleToDefenderStats(StatView stats) {
    long bonus = stats.get(Stats.RESISTANCE_RATING.get(effect.getElement()));
    return effect.scaleDamage(this, 1 / (1 + 1e-4 * bonus));
  }

  public CompoundTag toTag() {
    CompoundTag tag = new CompoundTag();
    tag.putString("id", effect.getId().toString());
    tag.putInt("duration", duration);
    tag.putLong("intensity", intensity);
    if (attacker != null)
      tag.putUuid("attacker", attacker.getUuid());
    return tag;
  }

  public static ExtendedStatusEffectInstance fromTag(CompoundTag tag,
      World world) {
    LivingEntity attacker = null;
    if (world instanceof ServerWorld
        && tag.contains("attacker", NbtType.INT_ARRAY)) {
      attacker = (LivingEntity) ((ServerWorld) world)
          .getEntity(tag.getUuid("attacker"));
    }
    return new ExtendedStatusEffectInstance(
        CsmRegistries.EXTENDED_STATUS_EFFECT
            .get(Identifier.tryParse(tag.getString("id"))),
        tag.getInt("duration"), tag.getLong("intensity"), attacker);
  }

  public void writeToPacket(PacketByteBuf data) {
    data.writeInt(CsmRegistries.EXTENDED_STATUS_EFFECT.getRawId(effect));
    data.writeInt(duration);
    data.writeLong(intensity);
    data.writeInt(attacker != null ? attacker.getEntityId() : -1);
  }

  public static ExtendedStatusEffectInstance readFromPacket(PacketByteBuf data,
      World world) {
    ExtendedStatusEffect effect =
        CsmRegistries.EXTENDED_STATUS_EFFECT.get(data.readInt());
    int duration = data.readInt();
    long intensity = data.readLong();
    int attackerId = data.readInt();
    Entity attacker = attackerId == -1 ? null : world.getEntityById(attackerId);
    LivingEntity livingAttacker =
        attacker instanceof LivingEntity ? (LivingEntity) attacker : null;
    return new ExtendedStatusEffectInstance(effect, duration, intensity,
        livingAttacker);
  }

  public void tickClient() {
    --duration;
  }
}
