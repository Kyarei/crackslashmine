package flirora.crackslashmine.core.xse;

import java.util.EnumMap;
import java.util.Map;
import java.util.function.Function;

import com.google.common.collect.ImmutableMap;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.CsmRegistries;
import flirora.crackslashmine.core.DamageType;
import flirora.crackslashmine.core.ResourceType;
import flirora.crackslashmine.core.stat.Stat;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class ExtendedStatusEffects {
  public static void load() {
    //
  }

  private static ExtendedStatusEffect register(ExtendedStatusEffect stat) {
    Registry.register(CsmRegistries.EXTENDED_STATUS_EFFECT, stat.getId(), stat);
    return stat;
  }

  static EnumMap<DamageType, ExtendedStatusEffect> registerForEachDamageType(
      Function<DamageType, ExtendedStatusEffect> f) {
    EnumMap<DamageType, ExtendedStatusEffect> res =
        DamageType.forEachDamageType(f);
    for (ExtendedStatusEffect stat : res.values()) {
      register(stat);
    }
    return res;
  }

  public static final EnumMap<DamageType, ExtendedStatusEffect> DAMAGE_OVER_TIME =
      registerForEachDamageType(
          damageType -> new DamageOverTimeExtendedStatusEffect(
              new Identifier(CrackSlashMineMod.MOD_ID,
                  "dot_" + damageType.getName()),
              damageType.getColor(), damageType));

  public static final EnumMap<DamageType, ExtendedStatusEffect> DELAYED_DAMAGE =
      registerForEachDamageType(
          damageType -> new DelayedDamageExtendedStatusEffect(
              new Identifier(CrackSlashMineMod.MOD_ID,
                  "delay_" + damageType.getName()),
              damageType.getColor(), damageType));

  public static final ExtendedStatusEffect HEALTH_REGEN =
      register(new ResourceRegenExtendedStatusEffect(
          new Identifier(CrackSlashMineMod.MOD_ID, "health_regen"), 0xFFCCCC,
          ResourceType.HEALTH));

  public static final ExtendedStatusEffect STAMINA_REGEN =
      register(new ResourceRegenExtendedStatusEffect(
          new Identifier(CrackSlashMineMod.MOD_ID, "stamina_regen"), 0xCCFFCC,
          ResourceType.STAMINA));

  public static final Map<Stat, ExtendedStatusEffect> STAT_BOOSTS;

  static {
    ImmutableMap.Builder<Stat, ExtendedStatusEffect> builder =
        ImmutableMap.builder();
    for (Stat s : CsmRegistries.STAT) {
      builder.put(s, register(new StatBoostExtendedStatusEffect(s)));
    }
    STAT_BOOSTS = builder.build();
  }
}
