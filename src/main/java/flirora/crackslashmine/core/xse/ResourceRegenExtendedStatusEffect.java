package flirora.crackslashmine.core.xse;

import org.jetbrains.annotations.Nullable;

import flirora.crackslashmine.core.Beautify;
import flirora.crackslashmine.core.DamageType;
import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.core.ResourceType;
import flirora.crackslashmine.item.TooltipUtils;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffectType;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;

public class ResourceRegenExtendedStatusEffect extends ExtendedStatusEffect {
  private final ResourceType resourceType;
  private final TranslatableText resourceText;

  /**
   * Construct an instance of this class.
   *
   * @param id           the {@link Identifier} that this effect should be
   *                     registered under
   * @param color        the color of the effect particles
   * @param resourceType
   */
  protected ResourceRegenExtendedStatusEffect(Identifier id, int color,
      ResourceType resourceType) {
    super(id, StatusEffectType.BENEFICIAL, color, DamageType.PHYSICAL);
    this.resourceType = resourceType;
    this.resourceText =
        new TranslatableText("tooltip.csm.resource." + resourceType.getName());
  }

  @Override
  public long applyTickEffect(LivingEntity target, LivingEntityStats stats,
      @Nullable LivingEntity attacker, long quantity, int timeRemaining) {
    if (timeRemaining % 20 == 0) {
      int seconds = timeRemaining / 20;
      long rounded = (quantity + seconds / 2) / seconds;
      if (rounded > 0) {
        switch (resourceType) {
        case HEALTH:
          stats.heal(rounded, false);
          break;
        case MANA:
          stats.setMana(stats.getMana() + rounded);
          break;
        case STAMINA:
          stats.setStamina(stats.getStamina() + rounded);
        }
        return quantity - rounded;
      }
    }
    return quantity;
  }

  @Override
  protected Text formatDetails(Text quantityText) {
    return new TranslatableText("csm.hud.xseRegen", quantityText, resourceText);
  }

  @Override
  public MutableText formatInTooltip(ExtendedStatusEffectInstance instance) {
    return new TranslatableText("csm.xse.regenDisplay", getStatusEffectName(),
        Beautify.beautify(instance.getIntensity()), resourceText,
        Beautify.beautifyTime(instance.getDuration()));
  }

  @Override
  public MutableText formatInTooltip(
      RangedExtendedStatusEffectInstance instance) {
    Text intensity = TooltipUtils.intensityRangeText(instance.getMinIntensity(),
        instance.getMaxIntensity());
    Text duration = TooltipUtils.durationRangeText(instance.getMinDuration(),
        instance.getMaxDuration());
    return new TranslatableText("csm.xse.regenDisplay", getStatusEffectName(),
        intensity, resourceText, duration);
  }
}
