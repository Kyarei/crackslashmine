package flirora.crackslashmine.core.xse;

import org.jetbrains.annotations.Nullable;

import flirora.crackslashmine.core.Beautify;
import flirora.crackslashmine.core.DamageType;
import flirora.crackslashmine.core.LivingEntityStats;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffectType;
import net.minecraft.text.LiteralText;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;

/**
 * C/M's own spin on status effects because the vanilla ones are too limited.
 * <p>
 * The differences from vanilla status effects are:
 *
 * <ul>
 * <li>extended status effects have a <code>long</code> intensity, and modders
 * are encouraged to use as much of the available range as they need</li>
 * <li>extended status effects can be stacked, but</li>
 * <li>vanilla status effects have some properties that extended effects don't,
 * such as <code>ambient</code></li>
 * </ul>
 */
public abstract class ExtendedStatusEffect {
  private final Identifier id;
  private final StatusEffectType type;
  private final int color;
  private final DamageType element;

  /**
   * Construct an instance of this class.
   *
   * @param id      the {@link Identifier} that this effect should be registered
   *                under
   * @param type    the {@link StatusEffectType} of this effect
   * @param color   the color of the effect particles
   * @param element the {@link DamageType} associated with this effect
   */
  protected ExtendedStatusEffect(Identifier id, StatusEffectType type,
      int color, DamageType element) {
    this.id = id;
    this.type = type;
    this.color = color;
    this.element = element;
  }

  public Identifier getId() {
    return id;
  }

  public StatusEffectType getType() {
    return type;
  }

  public int getColor() {
    return color;
  }

  public DamageType getElement() {
    return element;
  }

  /**
   * Apply an effect on an entity every tick. The intensity of the effect is
   * modified to the return value of this method.
   *
   * @param target        the {@link LivingEntity} that should be affected
   * @param stats         <code>entity</code>'s {@link LivingEntityStats}
   * @param attacker      the {@link LivingEntity} that caused this effect, or
   *                      <code>null</code> if none
   * @param quantity      the intensity of the effect
   * @param timeRemaining the number of ticks remaining until the effect
   *                      expires; will always be positive
   * @return the new intensity of the effect
   */
  public abstract long applyTickEffect(LivingEntity target,
      LivingEntityStats stats, @Nullable LivingEntity attacker, long quantity,
      int timeRemaining);

  public Text formatDetails(long quantity) {
    return formatDetails(new LiteralText(Beautify.beautify(quantity)));
  }

  public Text formatDetails(long min, long max) {
    return formatDetails(new TranslatableText("tooltip.csm.range",
        Beautify.beautify(min), Beautify.beautify(max)));
  }

  protected abstract Text formatDetails(Text quantityText);

  public abstract MutableText formatInTooltip(
      ExtendedStatusEffectInstance instance);

  public abstract MutableText formatInTooltip(
      RangedExtendedStatusEffectInstance instance);

  public ExtendedStatusEffectInstance scaleDamage(
      ExtendedStatusEffectInstance effect, double scale) {
    return effect;
  }

  public Text getStatusEffectName() {
    return new TranslatableText(
        "csm.xse." + id.getNamespace() + "." + id.getPath());
  }

  public Identifier getTexture() {
    Identifier id = getId();
    return new Identifier(id.getNamespace(),
        "textures/csm/ese/" + id.getPath() + ".png");
  }

  public Identifier getTextureWithoutExtension() {
    Identifier id = getId();
    return new Identifier(id.getNamespace(), "csm/ese/" + id.getPath());
  }

  @Override
  public int hashCode() {
    return id.hashCode();
  }
}
