package flirora.crackslashmine.advancement;

import com.google.gson.JsonObject;
import flirora.crackslashmine.CrackSlashMineMod;
import net.minecraft.advancement.criterion.AbstractCriterion;
import net.minecraft.advancement.criterion.AbstractCriterionConditions;
import net.minecraft.item.ItemStack;
import net.minecraft.predicate.entity.AdvancementEntityPredicateDeserializer;
import net.minecraft.predicate.entity.AdvancementEntityPredicateSerializer;
import net.minecraft.predicate.entity.EntityPredicate;
import net.minecraft.predicate.item.ItemPredicate;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;

public class RubEquipmentCriterion extends AbstractCriterion<RubEquipmentCriterion.Conditions> {
    public static final Identifier ID = new Identifier(CrackSlashMineMod.MOD_ID, "rub_equipment");

    @Override
    protected Conditions conditionsFromJson(JsonObject obj, EntityPredicate.Extended playerPredicate, AdvancementEntityPredicateDeserializer predicateDeserializer) {
        ItemPredicate predicate = ItemPredicate.fromJson(obj.get("item"));
        return new Conditions(playerPredicate, predicate);
    }

    @Override
    public Identifier getId() {
        return ID;
    }

    public void trigger(ServerPlayerEntity player, ItemStack stack) {
        this.test(player, (conditions) -> conditions.matches(stack));
    }

    public static class Conditions extends AbstractCriterionConditions {
        private final ItemPredicate predicate;

        public Conditions(EntityPredicate.Extended playerPredicate, ItemPredicate predicate) {
            super(ID, playerPredicate);
            this.predicate = predicate;
        }

        public boolean matches(ItemStack stack) {
            return predicate.test(stack);
        }

        @Override
        public JsonObject toJson(AdvancementEntityPredicateSerializer predicateSerializer) {
            JsonObject jsonObject = super.toJson(predicateSerializer);
            jsonObject.add("item", predicate.toJson());
            return jsonObject;
        }
    }
}
