package flirora.crackslashmine.advancement;

import com.google.gson.JsonObject;
import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.LivingEntityStats;
import net.minecraft.advancement.criterion.AbstractCriterion;
import net.minecraft.advancement.criterion.AbstractCriterionConditions;
import net.minecraft.predicate.entity.AdvancementEntityPredicateDeserializer;
import net.minecraft.predicate.entity.AdvancementEntityPredicateSerializer;
import net.minecraft.predicate.entity.EntityPredicate;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;

public class LevelUpCriterion extends AbstractCriterion<LevelUpCriterion.Conditions> {
    public static final Identifier ID = new Identifier(CrackSlashMineMod.MOD_ID, "combat_level");

    @Override
    protected Conditions conditionsFromJson(JsonObject obj, EntityPredicate.Extended playerPredicate, AdvancementEntityPredicateDeserializer predicateDeserializer) {
        int level = obj.get("level").getAsInt();
        return new Conditions(playerPredicate, level);
    }

    @Override
    public Identifier getId() {
        return ID;
    }

    public void trigger(ServerPlayerEntity player) {
        this.test(player, (conditions) -> conditions.matches(LivingEntityStats.getFor(player)));
    }

    public static class Conditions extends AbstractCriterionConditions {
        private final int level;

        public Conditions(EntityPredicate.Extended playerPredicate, int level) {
            super(ID, playerPredicate);
            this.level = level;
        }

        public boolean matches(LivingEntityStats stats) {
            return stats.getLevel() >= level;
        }

        @Override
        public JsonObject toJson(AdvancementEntityPredicateSerializer predicateSerializer) {
            JsonObject jsonObject = super.toJson(predicateSerializer);
            jsonObject.addProperty("level", level);
            return jsonObject;
        }
    }
}
