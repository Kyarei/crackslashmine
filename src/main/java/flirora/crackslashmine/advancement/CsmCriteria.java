package flirora.crackslashmine.advancement;

import net.fabricmc.fabric.api.object.builder.v1.advancement.CriterionRegistry;

public class CsmCriteria {
  public static final RubEquipmentCriterion RUB_EQUIPMENT =
      CriterionRegistry.register(new RubEquipmentCriterion());
  public static final LevelUpCriterion COMBAT_LEVEL =
      CriterionRegistry.register(new LevelUpCriterion());

  public static void load() {
    //
  }
}
