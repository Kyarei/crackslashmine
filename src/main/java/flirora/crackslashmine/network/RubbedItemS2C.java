package flirora.crackslashmine.network;

import flirora.crackslashmine.CrackSlashMineMod;
import io.netty.buffer.Unpooled;
import net.fabricmc.fabric.api.network.ServerSidePacketRegistry;
import net.fabricmc.fabric.api.server.PlayerStream;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class RubbedItemS2C {
    public static final Identifier ID = new Identifier(CrackSlashMineMod.MOD_ID, "rubbed_item");
    private final BlockPos pos;
    private final double hue;

    public RubbedItemS2C(BlockPos pos, double hue) {
        this.pos = pos;
        this.hue = hue;
    }

    public BlockPos getPos() {
        return pos;
    }

    public double getHue() {
        return hue;
    }

    @Override
    public String toString() {
        return "RubbedItemS2C{" +
                "pos=" + pos +
                ", hue=" + hue +
                '}';
    }

    public static RubbedItemS2C read(PacketByteBuf buf) {
        return new RubbedItemS2C(buf.readBlockPos(), buf.readDouble());
    }

    public void write(PacketByteBuf buf) {
        buf.writeBlockPos(pos);
        buf.writeDouble(hue);
    }

    public void send(World world) {
        PacketByteBuf data = new PacketByteBuf(Unpooled.buffer());
        write(data);
        PlayerStream.around(world, pos, 64).forEach(recipient ->
                ServerSidePacketRegistry.INSTANCE.sendToPlayer(
                        recipient, ID, data));
    }

    public static void create(World world, BlockPos pos, double hue) {
        if (world.isClient()) return;
        new RubbedItemS2C(pos, hue).send(world);
    }
}
