package flirora.crackslashmine.network;

import flirora.crackslashmine.CrackSlashMineMod;
import io.netty.buffer.Unpooled;
import net.fabricmc.fabric.api.network.ClientSidePacketRegistry;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.Identifier;

public class UserKeyC2S {
    public static final Identifier ID = new Identifier(CrackSlashMineMod.MOD_ID, "user_key");

    public static void send(int num) {
        PacketByteBuf data = new PacketByteBuf(Unpooled.buffer());
        data.writeByte(num);
        ClientSidePacketRegistry.INSTANCE.sendToServer(ID, data);
    }
}
