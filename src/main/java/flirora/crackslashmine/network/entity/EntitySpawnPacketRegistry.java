package flirora.crackslashmine.network.entity;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.network.packet.s2c.play.EntitySpawnS2CPacket;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;

@Environment(EnvType.CLIENT)
public class EntitySpawnPacketRegistry {
    public static final EntitySpawnPacketRegistry INSTANCE = new EntitySpawnPacketRegistry();

    private final Map<EntityType<?>, Handler> handlers = new HashMap<>();

    public void register(EntityType<?> type, Handler handler) {
        if (handlers.containsKey(type)) {
            throw new UnsupportedOperationException("Entity type " + type + " is already registered");
        }
        handlers.put(type, handler);
    }

    public @Nullable Handler get(EntityType<?> type) {
        return handlers.get(type);
    }

    public @Nullable Entity spawn(EntitySpawnS2CPacket packet, ClientWorld w) {
        Handler h = get(packet.getEntityTypeId());
        if (h == null) return null;
        return h.spawn(packet, w);
    }

    @FunctionalInterface
    public interface Handler {
        Entity spawn(EntitySpawnS2CPacket packet, ClientWorld w);
    }
}
