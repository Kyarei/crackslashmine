package flirora.crackslashmine.network.entity;

import flirora.crackslashmine.CrackSlashMineMod;
import io.netty.buffer.Unpooled;
import net.fabricmc.fabric.api.network.PacketContext;
import net.fabricmc.fabric.api.network.ServerSidePacketRegistry;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.Entity;
import net.minecraft.network.Packet;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.packet.s2c.play.EntitySpawnS2CPacket;
import net.minecraft.util.Identifier;
import net.minecraft.world.World;
import org.apache.logging.log4j.Level;

import java.io.IOException;

public class CsmEntitySpawnS2CPacket {
    public static final Identifier ID = new Identifier(CrackSlashMineMod.MOD_ID, "entity_spawn");

    public static Packet<?> fromVanilla(EntitySpawnS2CPacket packet) {
        try {
            PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
            packet.write(buf);
            return ServerSidePacketRegistry.INSTANCE.toPacket(ID, buf);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void receive(PacketContext context, PacketByteBuf data) {
        try {
            EntitySpawnS2CPacket packet = new EntitySpawnS2CPacket();
            packet.read(data);
            context.getTaskQueue().execute(() -> {
                World world = context.getPlayer().getEntityWorld();
                if (!(world instanceof ClientWorld)) {
                    CrackSlashMineMod.log(Level.WARN, "Received packet " + ID + " in non-client world");
                    return;
                }
                handle((ClientWorld) world, packet);
            });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void handle(ClientWorld world, EntitySpawnS2CPacket packet) {
        Entity e = EntitySpawnPacketRegistry.INSTANCE.spawn(packet, world);

        if (e != null) {
            double x = packet.getX();
            double y = packet.getY();
            double z = packet.getZ();
            int id = packet.getId();
            e.updateTrackedPosition(x, y, z);
            e.refreshPositionAfterTeleport(x, y, z);
            e.pitch = (float) (packet.getPitch() * 360) / 256.0F;
            e.yaw = (float) (packet.getYaw() * 360) / 256.0F;
            e.setEntityId(id);
            e.setUuid(packet.getUuid());
            world.addEntity(id, e);
        }
    }
}
