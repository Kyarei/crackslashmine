package flirora.crackslashmine.network;

import flirora.crackslashmine.CrackSlashMineMod;
import io.netty.buffer.Unpooled;
import net.fabricmc.fabric.api.network.ServerSidePacketRegistry;
import net.fabricmc.fabric.api.server.PlayerStream;
import net.minecraft.entity.Entity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.Identifier;
import net.minecraft.world.World;

public class HealSplatS2C {
    public static final Identifier ID = new Identifier(CrackSlashMineMod.MOD_ID, "heal_splat");
    private final int entityId;
    private final long healAmt;

    public HealSplatS2C(int entityId, long healAmt) {
        this.entityId = entityId;
        this.healAmt = healAmt;
    }

    public HealSplatS2C(Entity e, long healAmt) {
        this(e.getEntityId(), healAmt);
    }

    public int getEntityId() {
        return entityId;
    }

    public long getHealAmt() {
        return healAmt;
    }

    @Override
    public String toString() {
        return "HealSplatS2C{" +
                "healAmt=" + healAmt +
                '}';
    }

    public static HealSplatS2C read(PacketByteBuf buf) {
        return new HealSplatS2C(buf.readInt(), buf.readVarLong());
    }

    public void write(PacketByteBuf buf) {
        buf.writeInt(entityId);
        buf.writeVarLong(healAmt);
    }

    public void send(World world, Entity e) {
        PacketByteBuf data = new PacketByteBuf(Unpooled.buffer());
        write(data);
        PlayerStream.around(world, e.getPos(), 64).forEach(player -> {
            if (e != player) {
                ServerSidePacketRegistry.INSTANCE.sendToPlayer(
                        player, ID, data);
            }
        });
    }

    public static void create(World world, Entity e, long xp) {
        if (world.isClient()) return;
        new HealSplatS2C(e, xp).send(world, e);
    }
}
