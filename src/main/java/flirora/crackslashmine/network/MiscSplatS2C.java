package flirora.crackslashmine.network;

import flirora.crackslashmine.CrackSlashMineMod;
import io.netty.buffer.Unpooled;
import net.fabricmc.fabric.api.network.ServerSidePacketRegistry;
import net.fabricmc.fabric.api.server.PlayerStream;
import net.minecraft.entity.Entity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;
import net.minecraft.world.World;

public class MiscSplatS2C {
    public enum Type {
        DODGED("dodged"),
        ;

        private final String name;

        Type(String name) {
            this.name = name;
        }

        public Text asText() {
            return new TranslatableText("csm.hud.splat." + name);
        }
    }

    public static final Identifier ID = new Identifier(CrackSlashMineMod.MOD_ID, "misc_splat");
    private final int entityId;
    private final Type type;

    public MiscSplatS2C(int entityId, Type type) {
        this.entityId = entityId;
        this.type = type;
    }

    public MiscSplatS2C(Entity e, Type type) {
        this(e.getEntityId(), type);
    }

    public int getEntityId() {
        return entityId;
    }

    public Type getType() {
        return type;
    }

    @Override
    public String toString() {
        return "MiscSplatS2C{" +
                "entityId=" + entityId +
                ", type=" + type +
                '}';
    }

    public static MiscSplatS2C read(PacketByteBuf buf) {
        return new MiscSplatS2C(
                buf.readInt(),
                Type.values()[buf.readByte()]);
    }

    public void write(PacketByteBuf buf) {
        buf.writeInt(entityId);
        buf.writeByte((byte) type.ordinal());
    }

    public void send(World world, Entity e) {
        PacketByteBuf data = new PacketByteBuf(Unpooled.buffer());
        write(data);
        PlayerStream.around(world, e.getPos(), 64).forEach(player -> {
            if (e != player) {
                ServerSidePacketRegistry.INSTANCE.sendToPlayer(
                        player, ID, data);
            }
        });
    }

    public static void create(World world, Entity e, Type type) {
        if (world.isClient()) return;
        new MiscSplatS2C(e, type).send(world, e);
    }
}
