package flirora.crackslashmine.network;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.CsmAttack;
import io.netty.buffer.Unpooled;
import net.fabricmc.fabric.api.network.ServerSidePacketRegistry;
import net.fabricmc.fabric.api.server.PlayerStream;
import net.minecraft.entity.Entity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.Identifier;
import net.minecraft.world.World;

public class DamageSplatS2C {
    public static final Identifier ID = new Identifier(CrackSlashMineMod.MOD_ID, "damage_splat");
    private final int entityId;
    private final CsmAttack damage;

    public DamageSplatS2C(int entityId, CsmAttack damage) {
        this.entityId = entityId;
        this.damage = damage;
    }

    public DamageSplatS2C(Entity e, CsmAttack damage) {
        this(e.getEntityId(), damage);
    }

    public int getEntityId() {
        return entityId;
    }

    public CsmAttack getDamage() {
        return damage;
    }

    @Override
    public String toString() {
        return "DamageSplatS2C{" +
                "entityId=" + entityId +
                ", damage=" + damage +
                '}';
    }

    public static DamageSplatS2C read(PacketByteBuf buf, World world) {
        return new DamageSplatS2C(
                buf.readInt(),
                CsmAttack.read(buf, world));
    }

    public void write(PacketByteBuf buf) {
        buf.writeInt(entityId);
        damage.write(buf);
    }

    public void send(World world, Entity e) {
        PacketByteBuf data = new PacketByteBuf(Unpooled.buffer());
        write(data);
        PlayerStream.around(world, e.getPos(), 64).forEach(player -> {
            if (e != player) {
                ServerSidePacketRegistry.INSTANCE.sendToPlayer(
                        player, ID, data);
            }
        });
    }

    public static void create(World world, Entity e, CsmAttack attack) {
        if (world.isClient()) return;
        new DamageSplatS2C(e, attack).send(world, e);
    }
}
