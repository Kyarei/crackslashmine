package flirora.crackslashmine.network;

import flirora.crackslashmine.CrackSlashMineMod;
import io.netty.buffer.Unpooled;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.network.ClientSidePacketRegistry;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.Identifier;

public class AttackAttemptedC2S {
    public static final Identifier ID = new Identifier(CrackSlashMineMod.MOD_ID, "attack_attempted");

    private final boolean succeeded;

    public AttackAttemptedC2S(boolean succeeded) {
        this.succeeded = succeeded;
    }

    public boolean isSuccessful() {
        return succeeded;
    }

    public static AttackAttemptedC2S read(PacketByteBuf buf) {
        return new AttackAttemptedC2S(buf.readBoolean());
    }

    public void write(PacketByteBuf buf) {
        buf.writeBoolean(succeeded);
    }

    @Environment(EnvType.CLIENT)
    public void send() {
        PacketByteBuf data = new PacketByteBuf(Unpooled.buffer());
        write(data);
        ClientSidePacketRegistry.INSTANCE.sendToServer(ID, data);
    }

    @Environment(EnvType.CLIENT)
    public static void create(boolean succeeded) {
        new AttackAttemptedC2S(succeeded).send();
    }
}
