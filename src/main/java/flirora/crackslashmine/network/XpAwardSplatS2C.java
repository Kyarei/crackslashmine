package flirora.crackslashmine.network;

import flirora.crackslashmine.CrackSlashMineMod;
import io.netty.buffer.Unpooled;
import net.fabricmc.fabric.api.network.ServerSidePacketRegistry;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.Identifier;
import net.minecraft.world.World;

public class XpAwardSplatS2C {
    public static final Identifier ID = new Identifier(CrackSlashMineMod.MOD_ID, "xp_award_splat");
    private final int entityId;
    private final long xp;

    public XpAwardSplatS2C(int entityId, long xp) {
        this.entityId = entityId;
        this.xp = xp;
    }

    public XpAwardSplatS2C(Entity e, long xp) {
        this(e.getEntityId(), xp);
    }

    public int getEntityId() {
        return entityId;
    }

    public long getXp() {
        return xp;
    }

    @Override
    public String toString() {
        return "XpAwardSplatS2C{" +
                "xp=" + xp +
                '}';
    }

    public static XpAwardSplatS2C read(PacketByteBuf buf) {
        return new XpAwardSplatS2C(buf.readInt(), buf.readVarLong());
    }

    public void write(PacketByteBuf buf) {
        buf.writeInt(entityId);
        buf.writeVarLong(xp);
    }

    public void send(PlayerEntity recipient, World world, Entity e) {
        PacketByteBuf data = new PacketByteBuf(Unpooled.buffer());
        write(data);
        ServerSidePacketRegistry.INSTANCE.sendToPlayer(
                recipient, ID, data);
    }

    public static void create(PlayerEntity recipient, World world, Entity e, long xp) {
        if (world.isClient()) return;
        new XpAwardSplatS2C(e, xp).send(recipient, world, e);
    }
}
