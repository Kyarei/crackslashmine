package flirora.crackslashmine.item;

import java.util.stream.IntStream;

import com.google.common.collect.ImmutableList;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.item.baggim.SpellLootBagItem;
import flirora.crackslashmine.item.baggim.TonatLootBagItem;
import flirora.crackslashmine.item.debug.CommandWandItem;
import flirora.crackslashmine.item.equipment.CsmArmorMaterial;
import flirora.crackslashmine.item.equipment.CsmToolMaterial;
import flirora.crackslashmine.item.equipment.base.GlovesItem;
import flirora.crackslashmine.item.equipment.base.NecklaceItem;
import flirora.crackslashmine.item.equipment.base.RingItem;
import flirora.crackslashmine.item.essence.CrystallizedChromaItem;
import flirora.crackslashmine.item.essence.tonat.TonatItem;
import flirora.crackslashmine.item.spell.SpellItem;
import flirora.crackslashmine.item.spell.WandItem;
import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.SwordItem;
import net.minecraft.item.ToolMaterials;
import net.minecraft.util.Identifier;
import net.minecraft.util.Rarity;
import net.minecraft.util.registry.Registry;

public class CsmItems {
  public static final ItemGroup UNIQUES = FabricItemGroupBuilder
      .create(new Identifier(CrackSlashMineMod.MOD_ID, "uniques"))
      .icon(CsmItems::getUniquesIcon).build();

  public static final Item COMMAND_WAND = new CommandWandItem();

  public static final Item MONEY = new MoneyItem(new Item.Settings());

  public static final Item GOLDEN_CHAIN =
      new Item(new Item.Settings().group(ItemGroup.MISC));
  public static final Item CITRINE =
      new Item(new Item.Settings().group(ItemGroup.MISC));

  public static final Item SAPLING_RING = new RingItem(new Item.Settings());
  public static final Item CITRINE_RING = new RingItem(new Item.Settings());

  public static final Item CITRINE_NECKLACE =
      new NecklaceItem(new Item.Settings());

  public static final Item WOOL_GLOVES = new GlovesItem(new Item.Settings());

  public static final Item CITRINE_WAND =
      new WandItem(CsmToolMaterial.WAND_CITRINE);

  public static final Item WOOL_HELMET = new ArmorItem(CsmArmorMaterial.WOOL,
      EquipmentSlot.HEAD, new Item.Settings().group(ItemGroup.COMBAT));
  public static final Item WOOL_CHESTPLATE =
      new ArmorItem(CsmArmorMaterial.WOOL, EquipmentSlot.CHEST,
          new Item.Settings().group(ItemGroup.COMBAT));
  public static final Item WOOL_LEGGINGS = new ArmorItem(CsmArmorMaterial.WOOL,
      EquipmentSlot.LEGS, new Item.Settings().group(ItemGroup.COMBAT));
  public static final Item WOOL_BOOTIM = new ArmorItem(CsmArmorMaterial.WOOL,
      EquipmentSlot.FEET, new Item.Settings().group(ItemGroup.COMBAT));

  public static final Item ENCHANTED_CLOTH_HELMET =
      new ArmorItem(CsmArmorMaterial.ENCHANTED_CLOTH, EquipmentSlot.HEAD,
          new Item.Settings().group(ItemGroup.COMBAT));
  public static final Item ENCHANTED_CLOTH_CHESTPLATE =
      new ArmorItem(CsmArmorMaterial.ENCHANTED_CLOTH, EquipmentSlot.CHEST,
          new Item.Settings().group(ItemGroup.COMBAT));
  public static final Item ENCHANTED_CLOTH_LEGGINGS =
      new ArmorItem(CsmArmorMaterial.ENCHANTED_CLOTH, EquipmentSlot.LEGS,
          new Item.Settings().group(ItemGroup.COMBAT));
  public static final Item ENCHANTED_CLOTH_BOOTIM =
      new ArmorItem(CsmArmorMaterial.ENCHANTED_CLOTH, EquipmentSlot.FEET,
          new Item.Settings().group(ItemGroup.COMBAT));

  public static final Item CHROMA_COLORLESS = new CrystallizedChromaItem(-1);
  public static final ImmutableList<Item> COLORED_CHROMAS =
      IntStream.range(0, 24).mapToObj(CrystallizedChromaItem::new)
          .collect(ImmutableList.toImmutableList());

  public static final Item ERYTHRUM_INGOT =
      new Item(new Item.Settings().group(ItemGroup.MISC));
  public static final Item UAINE_INGOT =
      new Item(new Item.Settings().group(ItemGroup.MISC));
  public static final Item AZURITE_INGOT =
      new Item(new Item.Settings().group(ItemGroup.MISC));

  public static final Item ENCHANTED_CLOTH =
      new Item(new Item.Settings().group(ItemGroup.MISC));

  public static final Item SPELL_RUNE = new SpellItem();

  public static final Item SWORD_OF_THE_BLUE_SKY =
      new SwordItem(ToolMaterials.DIAMOND, 3, -2.4F,
          new Item.Settings().rarity(Rarity.RARE).group(UNIQUES));

  public static final ImmutableList<TonatItem> ASCII_TONATS =
      IntStream.range(0, 26)
          .mapToObj(i -> new TonatItem((char) ('A' + i), 163 * i % 360))
          .collect(ImmutableList.toImmutableList());

  public static final Item SPELL_LOOT_BAG = new SpellLootBagItem();
  public static final Item TONAT_LOOT_BAG = new TonatLootBagItem();

  public static void registerItems() {
    Registry.register(Registry.ITEM, CommandWandItem.ID, COMMAND_WAND);
    Registry.register(Registry.ITEM, MoneyItem.ID, MONEY);
    Registry.register(Registry.ITEM,
        new Identifier(CrackSlashMineMod.MOD_ID, "golden_chain"), GOLDEN_CHAIN);
    Registry.register(Registry.ITEM,
        new Identifier(CrackSlashMineMod.MOD_ID, "citrine"), CITRINE);

    Registry.register(Registry.ITEM,
        new Identifier(CrackSlashMineMod.MOD_ID, "sapling_ring"), SAPLING_RING);
    Registry.register(Registry.ITEM,
        new Identifier(CrackSlashMineMod.MOD_ID, "citrine_ring"), CITRINE_RING);

    Registry.register(Registry.ITEM,
        new Identifier(CrackSlashMineMod.MOD_ID, "citrine_necklace"),
        CITRINE_NECKLACE);

    Registry.register(Registry.ITEM,
        new Identifier(CrackSlashMineMod.MOD_ID, "wool_gloves"), WOOL_GLOVES);

    Registry.register(Registry.ITEM,
        new Identifier(CrackSlashMineMod.MOD_ID, "citrine_wand"), CITRINE_WAND);

    Registry.register(Registry.ITEM,
        new Identifier(CrackSlashMineMod.MOD_ID, "wool_helmet"), WOOL_HELMET);
    Registry.register(Registry.ITEM,
        new Identifier(CrackSlashMineMod.MOD_ID, "wool_chestplate"),
        WOOL_CHESTPLATE);
    Registry.register(Registry.ITEM,
        new Identifier(CrackSlashMineMod.MOD_ID, "wool_leggings"),
        WOOL_LEGGINGS);
    Registry.register(Registry.ITEM,
        new Identifier(CrackSlashMineMod.MOD_ID, "wool_boots"), WOOL_BOOTIM);

    Registry.register(Registry.ITEM,
        new Identifier(CrackSlashMineMod.MOD_ID, "enchanted_cloth_helmet"),
        ENCHANTED_CLOTH_HELMET);
    Registry.register(Registry.ITEM,
        new Identifier(CrackSlashMineMod.MOD_ID, "enchanted_cloth_chestplate"),
        ENCHANTED_CLOTH_CHESTPLATE);
    Registry.register(Registry.ITEM,
        new Identifier(CrackSlashMineMod.MOD_ID, "enchanted_cloth_leggings"),
        ENCHANTED_CLOTH_LEGGINGS);
    Registry.register(Registry.ITEM,
        new Identifier(CrackSlashMineMod.MOD_ID, "enchanted_cloth_boots"),
        ENCHANTED_CLOTH_BOOTIM);

    Registry.register(Registry.ITEM, new Identifier(CrackSlashMineMod.MOD_ID,
        "crystallized_chroma/colorless"), CHROMA_COLORLESS);
    for (int i = 0; i < 24; ++i) {
      Registry.register(Registry.ITEM,
          new Identifier(CrackSlashMineMod.MOD_ID, "crystallized_chroma/" + i),
          COLORED_CHROMAS.get(i));
    }

    Registry.register(Registry.ITEM,
        new Identifier(CrackSlashMineMod.MOD_ID, "erythrum_ingot"),
        ERYTHRUM_INGOT);
    Registry.register(Registry.ITEM,
        new Identifier(CrackSlashMineMod.MOD_ID, "uaine_ingot"), UAINE_INGOT);
    Registry.register(Registry.ITEM,
        new Identifier(CrackSlashMineMod.MOD_ID, "azurite_ingot"),
        AZURITE_INGOT);

    Registry.register(Registry.ITEM,
        new Identifier(CrackSlashMineMod.MOD_ID, "enchanted_cloth"),
        ENCHANTED_CLOTH);

    Registry.register(Registry.ITEM, SpellItem.ID, SPELL_RUNE);

    for (int i = 0; i < 26; ++i) {
      Registry.register(Registry.ITEM,
          new Identifier(CrackSlashMineMod.MOD_ID, "tonat/" + (char) ('a' + i)),
          ASCII_TONATS.get(i));
    }

    Registry.register(Registry.ITEM, SpellLootBagItem.ID, SPELL_LOOT_BAG);
    Registry.register(Registry.ITEM, TonatLootBagItem.ID, TONAT_LOOT_BAG);

    Registry.register(Registry.ITEM,
        new Identifier(CrackSlashMineMod.MOD_ID, "sword_of_the_blue_sky"),
        SWORD_OF_THE_BLUE_SKY);
  }

  private static ItemStack getUniquesIcon() {
    return new ItemStack(SWORD_OF_THE_BLUE_SKY);
  }
}
