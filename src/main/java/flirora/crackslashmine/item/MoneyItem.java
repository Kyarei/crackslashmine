package flirora.crackslashmine.item;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.player.PlayerProfile;
import flirora.crackslashmine.item.traits.CustomPickup;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.Identifier;

public class MoneyItem extends Item implements CustomPickup {
    public static final Identifier ID =
            new Identifier(CrackSlashMineMod.MOD_ID, "money");

    public MoneyItem(Settings settings) {
        super(settings.maxCount(1));
    }

    @Override
    public boolean canPickUp(PlayerEntity player, ItemStack item) {
        PlayerProfile profile = PlayerProfile.getFor(player);
        return profile.getMoney() < Long.MAX_VALUE;
    }

    @Override
    public ItemStack doPickUp(PlayerEntity player, ItemStack item) {
        PlayerProfile profile = PlayerProfile.getFor(player);
        long amount = getAmount(item);
        long maxAwardable = Long.MAX_VALUE - profile.getMoney();
        long awarded = Math.min(amount, maxAwardable);
        profile.setMoney(profile.getMoney() + awarded);
        setAmount(item, amount - awarded);
        if (amount - awarded <= 0) return ItemStack.EMPTY;
        return item;
    }

    public static long getAmount(ItemStack stack) {
        CompoundTag tag = stack.getTag();
        if (tag == null) return 0;
        return tag.getLong("csmMoneyAmount");
    }

    public static void setAmount(ItemStack stack, long amount) {
        CompoundTag tag = stack.getOrCreateTag();
        tag.putLong("csmMoneyAmount", amount);
        tag.putFloat("CustomModelData", (float) Math.log10(amount));
    }

    public static ItemStack of(long amount) {
        ItemStack stack = new ItemStack(CsmItems.MONEY);
        setAmount(stack, amount);
        return stack;
    }
}
