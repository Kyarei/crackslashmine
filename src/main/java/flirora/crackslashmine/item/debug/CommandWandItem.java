package flirora.crackslashmine.item.debug;

import flirora.crackslashmine.CrackSlashMineMod;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Formatting;
import net.minecraft.util.Hand;
import net.minecraft.util.Identifier;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Objects;

public class CommandWandItem extends Item {
    public static final Identifier ID = new Identifier(CrackSlashMineMod.MOD_ID, "command_wand");

    public CommandWandItem() {
        super(new Item.Settings().group(ItemGroup.MISC).maxCount(1));
    }

    @Override
    public ActionResult useOnEntity(ItemStack itemStack, PlayerEntity player, LivingEntity target, Hand hand) {
        if (target.getEntityWorld().isClient) return ActionResult.PASS;
        String name = itemStack.getName().asString();
        ServerCommandSource source = player.getCommandSource().withEntity(target);
        Objects.requireNonNull(target.getServer())
                .getCommandManager().execute(source, name);
        return ActionResult.CONSUME;
    }

    @Override
    public boolean hasGlint(ItemStack itemStack) {
        return true;
    }

    @Override
    public void appendTooltip(
            ItemStack itemStack,
            @Nullable World world,
            List<Text> list,
            TooltipContext tooltipContext) {
        super.appendTooltip(itemStack, world, list, tooltipContext);
        list.add(new TranslatableText("item.crackslashmine.command_wand.tooltip")
                .formatted(Formatting.AQUA));
    }
}
