package flirora.crackslashmine.item;

import java.util.List;

import org.jetbrains.annotations.NotNull;

import flirora.crackslashmine.core.Beautify;
import flirora.crackslashmine.core.stat.StatDisplayType;
import net.minecraft.text.LiteralText;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;

public class TooltipUtils {
  public static final Text EMPTY_LINE = LiteralText.EMPTY;

  public static void addEmptyLine(List<Text> list) {
    if (list.get(list.size() - 1) != EMPTY_LINE) {
      list.add(EMPTY_LINE);
    }
  }

  public static void removeLastEmptyLine(List<Text> list) {
    // Remove the last blank line
    // Yes, we mean ==.
    while (list.get(list.size() - 1) == EMPTY_LINE) {
      list.remove(list.size() - 1);
    }
  }

  @NotNull
  public static MutableText intensityRangeText(long minIntensity,
      long maxIntensity) {
    if (minIntensity == maxIntensity)
      return new LiteralText(Beautify.beautify(minIntensity));
    return new TranslatableText("tooltip.csm.range",
        Beautify.beautify(minIntensity), Beautify.beautify(maxIntensity));
  }

  @NotNull
  public static MutableText intensityRangeText(StatDisplayType dt,
      long minIntensity, long maxIntensity) {
    if (minIntensity == maxIntensity)
      return new LiteralText(Beautify.beautify(minIntensity));
    return new TranslatableText("tooltip.csm.range", dt.format(minIntensity),
        dt.format(maxIntensity));
  }

  @NotNull
  public static MutableText durationRangeText(int minDuration,
      int maxDuration) {
    if (minDuration == maxDuration)
      return new LiteralText(Beautify.beautifyTime(minDuration));
    return new TranslatableText("tooltip.csm.range",
        Beautify.beautifyTime(minDuration), Beautify.beautifyTime(maxDuration));
  }
}
