package flirora.crackslashmine.item.equipment;

/**
 * Indicates the origin of drops.
 */
public enum Origin {
    /**
     * Applies to items crafted, usually by the player.
     */
    CRAFTED,
    /**
     * Applied to items dropped from mobs or found in chests.
     */
    LOOTED,
    /**
     * Applied to items bought from NPCs.
     */
    BOUGHT,
    ;
}
