package flirora.crackslashmine.item.equipment;

import java.util.Locale;

import org.apache.commons.lang3.Validate;
import org.jetbrains.annotations.NotNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import flirora.crackslashmine.core.DamageType;

public class WeaponDamageProperties {
  private static final @NotNull WeaponDamageProperties DEFAULT =
      new WeaponDamageProperties(BaseDamageType.PHYSICAL, DamageType.FIRE);
  private final BaseDamageType baseType;
  private final DamageType damageType;

  public WeaponDamageProperties(BaseDamageType baseType,
      DamageType damageType) {
    this.baseType = baseType;
    this.damageType = damageType;
  }

  public BaseDamageType getBaseType() {
    return baseType;
  }

  public DamageType getDamageType() {
    return damageType;
  }

  private static final Gson GSON =
      (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();

  private static class JsonFormat {
    public String baseType;
    public String damageType;
  }

  public static WeaponDamageProperties fromJson(JsonObject json) {
    WeaponDamageProperties.JsonFormat deserialized =
        GSON.fromJson(json, WeaponDamageProperties.JsonFormat.class);
    return new WeaponDamageProperties(
        Validate.notNull(BaseDamageType
            .valueOf(deserialized.baseType.toUpperCase(Locale.US)), "baseType"),
        Validate.notNull(DamageType.BY_NAME.get(deserialized.damageType),
            "damageType"));
  }
}
