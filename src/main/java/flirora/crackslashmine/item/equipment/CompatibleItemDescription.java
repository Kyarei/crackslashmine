package flirora.crackslashmine.item.equipment;

import java.util.List;
import java.util.Random;

import com.google.gson.JsonElement;
import com.google.gson.JsonSyntaxException;

import flirora.crackslashmine.core.stat.Stat;
import flirora.crackslashmine.item.equipment.naming.EquipmentName;
import flirora.crackslashmine.item.equipment.types.EquipmentType;
import flirora.crackslashmine.item.equipment.types.WeaponType;
import it.unimi.dsi.fastutil.objects.Object2LongMap;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

/**
 * Describes traits of a C/M-compatible item, which is eligible to gain C/M
 * attributes.
 */
public abstract class CompatibleItemDescription {

  protected final EquipmentType type;
  protected final int minLevel;
  protected final int maxLevel;
  protected final double baseValue;

  public CompatibleItemDescription(EquipmentType type, int minLevel,
      int maxLevel, double baseValue) {
    super();
    this.type = type;
    this.minLevel = minLevel;
    this.maxLevel = maxLevel;
    this.baseValue = baseValue;
  }

  public EquipmentType getType() {
    return type;
  }

  public int getMinLevel() {
    return minLevel;
  }

  public int getMaxLevel() {
    return maxLevel;
  }

  public double getBaseValue() {
    return baseValue;
  }

  public long appraise(long seed, int level, double potentia, double anima,
      double discordia) {
    Random r = new Random(seed);
    return Math.round(baseValue * (1 + 0.05 * level + 0.005 * level * level)
        * (1 + 0.1 * potentia) * (1 + 0.1 * anima)
        * (1 + 0.1 * Math.exp(discordia) * (2 * r.nextDouble() - 1)));
  }

  public abstract EquipmentName rollName(Random r);

  public abstract void addFlavor(List<Text> lines);

  public abstract void generateStatBonuses(Random r, int level, double anima,
      double discordia, Object2LongMap<Stat> result, EquipmentName name);

  public abstract EquipmentParameters rollEquipmentParameters(Random r,
      Origin origin);

  public abstract WeaponDamageProperties rollWeaponDamageProperties(Random r,
      WeaponType type);

  /**
   * Deserializes a {@link CompatibleItemDescription} from JSON.
   *
   * @param id      the ID of the compatible item
   * @param element a {@link JsonElement} that holds the data
   * @return a {@link CompatibleItemDescription}
   */
  public static CompatibleItemDescription fromJson(Identifier id,
      JsonElement element) {
    JsonElement e = element.getAsJsonObject().get("ciType");
    String ciType = e != null ? e.getAsString() : "regular";
    switch (ciType) {
    case "regular":
      return RegularCompatibleItemDescription.fromJson(id, element);
    case "unique":
      return UniqueCompatibleItemDescription.fromJson(id, element);
    default:
      throw new JsonSyntaxException("Unknown compatible item type: " + ciType);
    }
  }

}
