package flirora.crackslashmine.item.equipment.types;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.DamageType;
import flirora.crackslashmine.core.stat.Stat;
import flirora.crackslashmine.core.stat.Stats;
import flirora.crackslashmine.item.equipment.CompatibleItemDescription;
import flirora.crackslashmine.item.equipment.EquipmentComponent;
import it.unimi.dsi.fastutil.objects.Object2LongMap;
import it.unimi.dsi.fastutil.objects.Object2LongOpenHashMap;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

public class ArmorType extends EquipmentType {
  public enum Material {
    PLATE, LEATHER, CLOTH,;
  }

  private final Material material;
  private final EquipmentSlot slot;

  private static final int[] STAT_PROPORTIONS = { 3, 8, 6, 3 };

  protected ArmorType(Identifier id, EquipmentSlot slot, Material material) {
    super(id, Category.WEARABLE);
    this.material = material;
    this.slot = slot;
  }

  private static double getHealthScaling(Random r, double potentia, int level) {
    // TODO: replace base scaling with configurable polynomial
    return CrackSlashMineMod.config.armorHealthScaling.value(level)
        * Math.pow(1.12, potentia) * (1 + 0.05 * r.nextGaussian());
  }

  private static double getDamageScaling(Random r, double potentia, int level) {
    // TODO: replace base scaling with configurable polynomial
    return CrackSlashMineMod.config.armorDamageScaling.value(level)
        * Math.pow(1.12, potentia) * (1 + 0.05 * r.nextGaussian());
  }

  @Override
  protected Object2LongMap<Stat> generateInternal(Random r, int level,
      double potentia, double anima, double discordia,
      CompatibleItemDescription desc, EquipmentComponent component) {
    Object2LongMap<Stat> bonuses = new Object2LongOpenHashMap<>();
    int statProportion = STAT_PROPORTIONS[slot.getEntitySlotId()];
    switch (material) {
    case PLATE: {
      long healthBonus = Math.round(
          120 * getHealthScaling(r, potentia, level) * statProportion / 20);
      long armorBonus = Math.round(
          800 * getDamageScaling(r, potentia, level) * statProportion / 20);
      bonuses.put(Stats.HEALTH_BONUS, healthBonus);
      bonuses.put(Stats.RESISTANCE_RATING.get(DamageType.PHYSICAL), armorBonus);
      break;
    }
    case LEATHER: {
      long healthBonus = Math.round(
          60 * getHealthScaling(r, potentia, level) * statProportion / 20);
      long dodgeBonus = Math.round(
          800 * getDamageScaling(r, potentia, level) * statProportion / 20);
      bonuses.put(Stats.HEALTH_BONUS, healthBonus);
      bonuses.put(Stats.DODGE_RATING, dodgeBonus);
      break;
    }
    case CLOTH: {
      long healthBonus = Math.round(
          40 * getHealthScaling(r, potentia, level) * statProportion / 20);
      long manaBonus = Math.round(
          80 * getHealthScaling(r, potentia, level) * statProportion / 20);
      bonuses.put(Stats.HEALTH_BONUS, healthBonus);
      bonuses.put(Stats.MANA_BONUS, manaBonus);
      // Get a number of magic damage types to resist against
      DamageType[] types = Arrays.copyOfRange(DamageType.values(), 1,
          DamageType.values().length);
      List<DamageType> typeList = Arrays.asList(types);
      Collections.shuffle(typeList);
      int n = 1;
      if (r.nextGaussian() < potentia + 1)
        ++n;
      if (r.nextGaussian() < potentia + 1)
        ++n;
      if (r.nextGaussian() < potentia - 1)
        ++n;
      for (int i = 0; i < n; ++i) {
        long resistBonus = Math.round(
            300 * getDamageScaling(r, potentia, level) * statProportion / 20);
        bonuses.put(Stats.RESISTANCE_RATING.get(typeList.get(i)), resistBonus);
      }
      if (r.nextGaussian() < potentia) {
        long bonus = Math.round(
            15 * getDamageScaling(r, potentia, level) * statProportion / 20);
        bonuses.put(Stats.DAMAGE_BONUS.get(DamageType.randomElemental(r)),
            bonus);
      }
      break;
    }
    }
    return bonuses;
  }

  @Override
  public void addInfoToTooltip(EquipmentComponent component, ItemStack stack,
      List<Text> tooltip) {
    //
  }

  @Override
  public boolean isValidInSlot(EquipmentSlot slot) {
    return slot == this.slot;
  }

  @Override
  public boolean isValidInTrinketSlot(String group, String slot) {
    return false;
  }
}
