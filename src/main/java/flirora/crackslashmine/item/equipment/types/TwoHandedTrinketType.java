package flirora.crackslashmine.item.equipment.types;

import java.util.List;

import dev.emi.trinkets.api.SlotGroups;
import dev.emi.trinkets.api.Slots;
import net.minecraft.util.Identifier;

public class TwoHandedTrinketType extends TrinketType {
  public TwoHandedTrinketType(Identifier id, String slot,
      List<PrimaryStat> primaryStatPool) {
    super(id, SlotGroups.HAND, slot, primaryStatPool);
  }

  public TwoHandedTrinketType(Identifier id, String slot, PrimaryStat... primaryStatPool) {
    super(id, SlotGroups.HAND, slot, primaryStatPool);
  }

  @Override
  public boolean isValidInTrinketSlot(String group, String slot) {
    return super.isValidInTrinketSlot(group, slot)
        || (group.equals(SlotGroups.OFFHAND) && slot.equals(Slots.RING));
  }
}
