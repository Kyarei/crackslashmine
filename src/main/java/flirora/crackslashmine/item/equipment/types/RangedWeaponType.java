package flirora.crackslashmine.item.equipment.types;

import flirora.crackslashmine.core.stat.StatView;
import flirora.crackslashmine.core.stat.Stats;
import net.minecraft.util.Identifier;

public class RangedWeaponType extends WeaponType {
  protected RangedWeaponType(Identifier id, double energyCostMultiplier,
      double baseDamageMultiplier, double pureMagicalChance,
      double splitDamageChance) {
    super(id, Category.RANGED, energyCostMultiplier, baseDamageMultiplier,
        pureMagicalChance, splitDamageChance);
  }

  @Override
  public long getWeaponTypeBonus(StatView stats) {
    return stats.get(Stats.RANGED_WEAPON_BONUS);
  }
}
