package flirora.crackslashmine.item.equipment.types;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.DamageType;
import flirora.crackslashmine.core.stat.Stat;
import flirora.crackslashmine.core.stat.Stats;
import flirora.crackslashmine.item.equipment.CompatibleItemDescription;
import flirora.crackslashmine.item.equipment.EquipmentComponent;
import it.unimi.dsi.fastutil.objects.Object2LongMap;
import it.unimi.dsi.fastutil.objects.Object2LongOpenHashMap;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.text.KeybindText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;

import java.util.List;
import java.util.Random;

public class WandType extends EquipmentType {
    public WandType() {
        super(new Identifier(CrackSlashMineMod.MOD_ID, "wand"), Category.MAGIC);
    }

    @Override
    protected Object2LongMap<Stat> generateInternal(Random r, int level, double potentia, double anima, double discordia, CompatibleItemDescription desc, EquipmentComponent component) {
        Object2LongMap<Stat> base = new Object2LongOpenHashMap<>();
        double baseMulti = 500.0 * Math.pow(1.2, potentia);
        DamageType favored = DamageType.randomElemental(r);
        DamageType disfavored;
        do {
            disfavored = DamageType.randomElemental(r);
        } while (disfavored == favored);
        double sd = 0.1 + 0.02 * discordia;
        base.put(Stats.MANA_COST_MULTI.get(favored), -Math.round(baseMulti * (1 + sd * r.nextGaussian())));
        base.put(Stats.MANA_COST_MULTI.get(disfavored), Math.round(baseMulti * (1 + sd * r.nextGaussian())));
        return base;
    }

    @Override
    public void addInfoToTooltip(EquipmentComponent component, ItemStack stack, List<Text> tooltip) {
        tooltip.add(new TranslatableText("tooltip.csm.wand.usage0",
                new KeybindText("key.crackslashmine.user.0"),
                new KeybindText("key.crackslashmine.user.1"),
                new KeybindText("key.crackslashmine.user.2"),
                new KeybindText("key.crackslashmine.user.3"))
                .formatted(Formatting.RED));
        tooltip.add(new TranslatableText("tooltip.csm.wand.usage1")
                .formatted(Formatting.RED));
    }

    @Override
    public boolean isValidInSlot(EquipmentSlot slot) {
        return slot == EquipmentSlot.MAINHAND;
    }

    @Override
    public boolean isValidInTrinketSlot(String group, String slot) {
        return false;
    }
}
