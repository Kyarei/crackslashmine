package flirora.crackslashmine.item.equipment.types;

import java.util.List;
import java.util.Random;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.CsmPrimaryAttackStat;
import flirora.crackslashmine.core.DamageType;
import flirora.crackslashmine.core.EntityDamageUtils;
import flirora.crackslashmine.core.stat.Stat;
import flirora.crackslashmine.core.stat.StatView;
import flirora.crackslashmine.item.equipment.BaseDamageType;
import flirora.crackslashmine.item.equipment.CompatibleItemDescription;
import flirora.crackslashmine.item.equipment.EquipmentComponent;
import flirora.crackslashmine.item.equipment.LootUtils;
import flirora.crackslashmine.item.equipment.WeaponDamageProperties;
import it.unimi.dsi.fastutil.objects.Object2LongMap;
import it.unimi.dsi.fastutil.objects.Object2LongOpenHashMap;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.MathHelper;

public abstract class WeaponType extends EquipmentType {
  // How many times more tiring is using this weapon than punching?
  private final double energyCostMultiplier;
  // How much damage does this weapon deal compared to a sword?
  private final double baseDamageMultiplier;
  private final double pureMagicalChance, splitDamageChance;

  protected WeaponType(Identifier id, Category category,
      double energyCostMultiplier, double baseDamageMultiplier,
      double pureMagicalChance, double splitDamageChance) {
    super(id, category);
    this.energyCostMultiplier = energyCostMultiplier;
    this.baseDamageMultiplier = baseDamageMultiplier;
    this.pureMagicalChance = pureMagicalChance;
    this.splitDamageChance = splitDamageChance;
  }

  public double getEnergyCostMultiplier() {
    return energyCostMultiplier;
  }

  public long netStaminaCost(int level) {
    return Math.round(
        EntityDamageUtils.getUnarmedEnergyCost(level) * energyCostMultiplier);
  }

  private double sampleMeanDamage(Random r, double potentia, int level) {
    return CrackSlashMineMod.config.weaponDamageScaling.value(level)
        * baseDamageMultiplier * Math.pow(1.12, potentia)
        * (1 + 0.03 * r.nextGaussian());
  }

  public BaseDamageType sampleDamageType(Random r) {
    double roll = r.nextDouble();
    if (roll < pureMagicalChance)
      return BaseDamageType.MAGIC;
    if (roll < pureMagicalChance + splitDamageChance)
      return BaseDamageType.SPLIT;
    return BaseDamageType.PHYSICAL;
  }

  private CsmPrimaryAttackStat samplePrimaryAttackStat(Random r, int level,
      double potentia, double discordia, CompatibleItemDescription desc) {
    WeaponDamageProperties wp = desc.rollWeaponDamageProperties(r, this);
    BaseDamageType wd = wp.getBaseType();
    DamageType de = wp.getDamageType();
    double mean = sampleMeanDamage(r, potentia, level);
    double expDiscordia = Math.exp(discordia);
    double minFactor = 1 - Math.min(0.99, 0.1 * expDiscordia);
    double maxFactor = 1 + Math.min(0.99, 0.1 * expDiscordia);
    switch (wd) {
    case PHYSICAL:
      return new CsmPrimaryAttackStat(
          new CsmPrimaryAttackStat.Entry(DamageType.PHYSICAL,
              Math.round(minFactor * mean), Math.round(maxFactor * mean)));
    case MAGIC:
      return new CsmPrimaryAttackStat(new CsmPrimaryAttackStat.Entry(de,
          Math.round(minFactor * mean), Math.round(maxFactor * mean)));
    case SPLIT:
      double proportionMagical =
          MathHelper.clamp(0.5 + 0.1 * expDiscordia * r.nextGaussian(), 0, 1);
      double proportionPhysical = 1 - proportionMagical;
      return new CsmPrimaryAttackStat(
          new CsmPrimaryAttackStat.Entry(DamageType.PHYSICAL,
              Math.round(proportionPhysical * minFactor * mean),
              Math.round(proportionPhysical * maxFactor * mean)),
          new CsmPrimaryAttackStat.Entry(de,
              Math.round(proportionMagical * minFactor * mean),
              Math.round(proportionMagical * maxFactor * mean)));
    }
    throw new IllegalStateException("unreachable");
  }

  @Override
  public Object2LongMap<Stat> generateInternal(Random r, int level,
      double potentia, double anima, double discordia,
      CompatibleItemDescription desc, EquipmentComponent component) {
    CsmPrimaryAttackStat rawAttackStat =
        samplePrimaryAttackStat(r, level, potentia, discordia, desc);
    CsmPrimaryAttackStat attackStat =
        LootUtils.transformPrimaryStat(rawAttackStat, r);
    component.setWeaponBaseStat(attackStat);
    return new Object2LongOpenHashMap<>();
  }

  @Override
  public void addInfoToTooltip(EquipmentComponent component, ItemStack stack,
      List<Text> tooltip) {
    long netEnergyCost = netStaminaCost(component.getMeta().getLevel());
    tooltip.add(new TranslatableText("tooltip.csm.energyCost", netEnergyCost)
        .formatted(Formatting.RED));
  }

  @Override
  public boolean isValidInSlot(EquipmentSlot slot) {
    return slot == EquipmentSlot.MAINHAND;
  }

  @Override
  public boolean isValidInTrinketSlot(String group, String slot) {
    return false;
  }

  public abstract long getWeaponTypeBonus(StatView stats);
}
