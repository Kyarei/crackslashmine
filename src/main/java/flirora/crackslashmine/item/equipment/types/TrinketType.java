package flirora.crackslashmine.item.equipment.types;

import flirora.crackslashmine.core.DamageType;
import flirora.crackslashmine.core.stat.Stat;
import flirora.crackslashmine.item.equipment.CompatibleItemDescription;
import flirora.crackslashmine.item.equipment.EquipmentComponent;
import it.unimi.dsi.fastutil.objects.Object2LongMap;
import it.unimi.dsi.fastutil.objects.Object2LongOpenHashMap;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import org.apache.commons.math3.analysis.polynomials.PolynomialFunction;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Random;
import java.util.function.Function;

public class TrinketType extends EquipmentType {
    public static class PrimaryStat {
        private final Function<Random, Stat> stat;
        private final PolynomialFunction curveByLevel;

        public PrimaryStat(Stat stat, PolynomialFunction curveByLevel) {
            this.stat = r -> stat;
            this.curveByLevel = curveByLevel;
        }

        public PrimaryStat(Stat stat, double[] coefficients) {
            this(stat, new PolynomialFunction(coefficients));
        }

        public PrimaryStat(Function<Random, Stat> statGen, PolynomialFunction curveByLevel) {
            this.stat = statGen;
            this.curveByLevel = curveByLevel;
        }

        public PrimaryStat(Function<Random, Stat> statGen, double[] coefficients) {
            this(statGen, new PolynomialFunction(coefficients));
        }

        public Stat getStat(Random r) {
            return stat.apply(r);
        }

        public PolynomialFunction getCurveByLevel() {
            return curveByLevel;
        }
    }

    private final String group;
    private final String slot;
    private final List<PrimaryStat> primaryStatPool;

    public TrinketType(Identifier id, String group, String slot, List<PrimaryStat> primaryStatPool) {
        super(id, Category.WEARABLE);
        this.group = group;
        this.slot = slot;
        this.primaryStatPool = primaryStatPool;
    }

    public TrinketType(Identifier id, String group, String slot, PrimaryStat... primaryStatPool) {
        this(id, group, slot, Arrays.asList(primaryStatPool));
    }

    @Override
    protected Object2LongMap<Stat> generateInternal(
            Random r,
            int level,
            double potentia,
            double anima,
            double discordia,
            CompatibleItemDescription desc, EquipmentComponent component) {
        PrimaryStat primaryStat = primaryStatPool.get(r.nextInt(primaryStatPool.size()));
        Object2LongMap<Stat> primaryStatMap = new Object2LongOpenHashMap<>();
        double expDiscordia = Math.exp(discordia);
        double potentiaTerm = Math.pow(1.15, potentia);
        double discordiaTerm = 1 + 0.1 * expDiscordia * r.nextGaussian();
        long amount = Math.round(
                primaryStat.getCurveByLevel().value(level) *
                        potentiaTerm *
                        discordiaTerm);
        primaryStatMap.put(primaryStat.getStat(r), amount);
        return primaryStatMap;
    }

    @Override
    public void addInfoToTooltip(EquipmentComponent component, ItemStack stack, List<Text> tooltip) {
        //
    }

    @Override
    public boolean isValidInSlot(EquipmentSlot slot) {
        return false;
    }

    @Override
    public boolean isValidInTrinketSlot(String group, String slot) {
        return this.group.equals(group) && this.slot.equals(slot);
    }

    public static Function<Random, Stat> elemental(EnumMap<DamageType, Stat> stats) {
        return r -> stats.get(DamageType.randomElemental(r));
    }
}
