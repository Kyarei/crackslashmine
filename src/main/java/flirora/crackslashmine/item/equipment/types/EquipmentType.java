package flirora.crackslashmine.item.equipment.types;

import java.util.List;
import java.util.Random;

import org.jetbrains.annotations.ApiStatus;

import com.google.gson.JsonSyntaxException;

import flirora.crackslashmine.core.CsmRegistries;
import flirora.crackslashmine.core.stat.Stat;
import flirora.crackslashmine.item.ChromaticUtils;
import flirora.crackslashmine.item.equipment.CompatibleItemDescription;
import flirora.crackslashmine.item.equipment.CompatibleItemManager;
import flirora.crackslashmine.item.equipment.EquipmentComponent;
import flirora.crackslashmine.item.equipment.EquipmentParameters;
import flirora.crackslashmine.item.equipment.Origin;
import flirora.crackslashmine.item.equipment.naming.EquipmentName;
import it.unimi.dsi.fastutil.objects.Object2LongMap;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

/**
 * Represents a type of equipment, such as "swords", "helmets", or "rings".
 */
public abstract class EquipmentType {
  /**
   * The general category of equipment.
   */
  public enum Category {
    /**
     * A melee weapon.
     */
    MELEE,
    /**
     * A ranged weapon.
     */
    RANGED,
    /**
     * A wearable item, such as armor or jewelry.
     */
    WEARABLE,
    /**
     * A spell-casting implement.
     */
    MAGIC,;
  }

  private final Identifier id;
  private final Category category;

  /**
   * Constructs an object.
   *
   * @param id       the ID to register this equipment type under
   * @param category the {@link Category} of this type
   */
  protected EquipmentType(Identifier id, Category category) {
    this.id = id;
    this.category = category;
  }

  public Identifier getId() {
    return id;
  }

  public Category getCategory() {
    return category;
  }

  @Override
  public int hashCode() {
    return id.hashCode();
  }

  @Override
  public String toString() {
    return "EquipmentType@" + id.toString();
  }

  /**
   * Custom entry point for generating stats for equipment.
   * <p>
   * This method should be overridden, not called. Call
   * {@link EquipmentType#generate(Random, int, double, double, double, double, Origin, ItemStack)}
   * instead.
   *
   * @param r         a {@link Random} object to sample from
   * @param level     the level that this equipment should be generated at
   * @param potentia  the potentia of this item
   * @param anima     the anima of this item
   * @param discordia the discordia of this item
   * @param desc      TODO
   * @param component the {@link EquipmentComponent} to populate
   * @return an {@link Object2LongMap} that describes any base stat boosts to
   *         generate
   */
  @ApiStatus.OverrideOnly
  protected abstract Object2LongMap<Stat> generateInternal(Random r, int level,
      double potentia, double anima, double discordia,
      CompatibleItemDescription desc, EquipmentComponent component);

  /**
   * Add any equipment type-specific entries to a tooltip.
   *
   * @param component the {@link EquipmentComponent} of the item
   * @param stack     the {@link ItemStack} of the item
   * @param tooltip   a list of {@link Text} objects to append to
   */
  public abstract void addInfoToTooltip(EquipmentComponent component,
      ItemStack stack, List<Text> tooltip);

  public abstract boolean isValidInSlot(EquipmentSlot slot);

  public abstract boolean isValidInTrinketSlot(String group, String slot);

  private int maxVödLength(Random r, double ambitio) {
    return 3 + (int) Math.round(0.4 * Math.exp(ambitio))
        + (r.nextInt(2) == 0 ? 1 : 0) + (r.nextInt(6) == 0 ? 1 : 0);
  }

  /**
   * Populate the stats of a piece of equipment according to its type.
   *
   * @param r         a {@link Random} object to sample from
   * @param level     the level that this equipment should be generated at
   * @param potentia  the potentia of this item
   * @param anima     the anima of this item
   * @param discordia the discordia of this item
   * @param ambitio   the ambitio of this item
   * @param origin    the {@link Origin} of the equipment
   * @param stack     the {@link ItemStack} to populate
   */
  public void generate(Random r, int level, EquipmentParameters ep,
      Origin origin, ItemStack stack) {
    Identifier itemId = Registry.ITEM.getId(stack.getItem());
    CompatibleItemDescription desc = CompatibleItemManager.INSTANCE.get(itemId);
    EquipmentName name = desc.rollName(r);
    EquipmentComponent.initialize(stack, component -> {
      Object2LongMap<Stat> statBonuses = generateInternal(r, level,
          ep.getPotentia(), ep.getAnima(), ep.getDiscordia(), desc, component);
      desc.generateStatBonuses(r, level, ep.getAnima(), ep.getDiscordia(),
          statBonuses, name);
      EquipmentComponent.Meta meta = new EquipmentComponent.Meta(level, ep,
          maxVödLength(r, ep.getAmbitio()), ChromaticUtils.padToHue(r,
              ep.getPotentia(), ep.getAnima(), ep.getDiscordia()),
          r.nextLong(), origin);
      component.setMeta(meta);
      component.setName(name);
      component.setStatBonuses(statBonuses);
    });
  }

  public static EquipmentType getByName(Identifier id) {
    EquipmentType type = CsmRegistries.EQUIPMENT_TYPE.get(id);
    if (type == null)
      throw new JsonSyntaxException(id + " is not a valid equipment type");
    return type;
  }
}
