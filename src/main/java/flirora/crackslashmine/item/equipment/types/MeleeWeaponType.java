package flirora.crackslashmine.item.equipment.types;

import flirora.crackslashmine.core.stat.StatView;
import flirora.crackslashmine.core.stat.Stats;
import net.minecraft.util.Identifier;

public class MeleeWeaponType extends WeaponType {
  public MeleeWeaponType(Identifier id, double energyCostMultiplier,
      double baseDamageMultiplier, double pureMagicalChance,
      double splitDamageChance) {
    super(id, Category.MELEE, energyCostMultiplier, baseDamageMultiplier,
        pureMagicalChance, splitDamageChance);
  }

  @Override
  public long getWeaponTypeBonus(StatView stats) {
    return stats.get(Stats.MELEE_WEAPON_BONUS);
  }
}
