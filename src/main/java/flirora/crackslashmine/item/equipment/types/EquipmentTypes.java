package flirora.crackslashmine.item.equipment.types;

import dev.emi.trinkets.api.SlotGroups;
import dev.emi.trinkets.api.Slots;
import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.CsmRegistries;
import flirora.crackslashmine.core.DamageType;
import flirora.crackslashmine.core.stat.StatView;
import flirora.crackslashmine.core.stat.Stats;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class EquipmentTypes {
  private static EquipmentType register(EquipmentType type) {
    Registry.register(CsmRegistries.EQUIPMENT_TYPE, type.getId(), type);
    return type;
  }

  public static final EquipmentType SWORD = register(new MeleeWeaponType(
      new Identifier(CrackSlashMineMod.MOD_ID, "sword"), 4.5, 1.0, 0.0, 0.2) {
    @Override
    public long getWeaponTypeBonus(StatView stats) {
      return super.getWeaponTypeBonus(stats) + stats.get(Stats.SWORD_BONUS);
    }
  });

  public static final EquipmentType AXE = register(new MeleeWeaponType(
      new Identifier(CrackSlashMineMod.MOD_ID, "axe"), 9.0, 1.8, 0.0, 0.1) {
    @Override
    public long getWeaponTypeBonus(StatView stats) {
      return super.getWeaponTypeBonus(stats) + stats.get(Stats.AXE_BONUS);
    }
  });

  public static final EquipmentType BOW = register(new RangedWeaponType(
      new Identifier(CrackSlashMineMod.MOD_ID, "bow"), 15.0, 1.8, 0.0, 0.25) {
    @Override
    public long getWeaponTypeBonus(StatView stats) {
      return super.getWeaponTypeBonus(stats) + stats.get(Stats.BOW_BONUS);
    }
  });

  public static final EquipmentType CROSSBOW = register(
      new RangedWeaponType(new Identifier(CrackSlashMineMod.MOD_ID, "crossbow"),
          11.0, 1.5, 0.0, 0.15) {
        @Override
        public long getWeaponTypeBonus(StatView stats) {
          return super.getWeaponTypeBonus(stats)
              + stats.get(Stats.CROSSBOW_BONUS);
        }
      });

  public static final EquipmentType TRIDENT = register(
      new RangedWeaponType(new Identifier(CrackSlashMineMod.MOD_ID, "trident"),
          10.5, 2.0, 0.1, 0.2));

  public static final EquipmentType WAND = register(new WandType());

  public static final EquipmentType PLATE_HELMET = register(
      new ArmorType(new Identifier(CrackSlashMineMod.MOD_ID, "plate_helmet"),
          EquipmentSlot.HEAD, ArmorType.Material.PLATE));
  public static final EquipmentType PLATE_CHESTPLATE = register(new ArmorType(
      new Identifier(CrackSlashMineMod.MOD_ID, "plate_chestplate"),
      EquipmentSlot.CHEST, ArmorType.Material.PLATE));
  public static final EquipmentType PLATE_LEGGINGS = register(
      new ArmorType(new Identifier(CrackSlashMineMod.MOD_ID, "plate_leggings"),
          EquipmentSlot.LEGS, ArmorType.Material.PLATE));
  public static final EquipmentType PLATE_BOOTIM = register(
      new ArmorType(new Identifier(CrackSlashMineMod.MOD_ID, "plate_boots"),
          EquipmentSlot.FEET, ArmorType.Material.PLATE));

  public static final EquipmentType LEATHER_HELMET = register(
      new ArmorType(new Identifier(CrackSlashMineMod.MOD_ID, "leather_helmet"),
          EquipmentSlot.HEAD, ArmorType.Material.LEATHER));
  public static final EquipmentType LEATHER_CHESTPLATE = register(new ArmorType(
      new Identifier(CrackSlashMineMod.MOD_ID, "leather_chestplate"),
      EquipmentSlot.CHEST, ArmorType.Material.LEATHER));
  public static final EquipmentType LEATHER_LEGGINGS = register(new ArmorType(
      new Identifier(CrackSlashMineMod.MOD_ID, "leather_leggings"),
      EquipmentSlot.LEGS, ArmorType.Material.LEATHER));
  public static final EquipmentType LEATHER_BOOTIM = register(
      new ArmorType(new Identifier(CrackSlashMineMod.MOD_ID, "leather_boots"),
          EquipmentSlot.FEET, ArmorType.Material.LEATHER));

  public static final EquipmentType CLOTH_HELMET = register(
      new ArmorType(new Identifier(CrackSlashMineMod.MOD_ID, "cloth_helmet"),
          EquipmentSlot.HEAD, ArmorType.Material.CLOTH));
  public static final EquipmentType CLOTH_CHESTPLATE = register(new ArmorType(
      new Identifier(CrackSlashMineMod.MOD_ID, "cloth_chestplate"),
      EquipmentSlot.CHEST, ArmorType.Material.CLOTH));
  public static final EquipmentType CLOTH_LEGGINGS = register(
      new ArmorType(new Identifier(CrackSlashMineMod.MOD_ID, "cloth_leggings"),
          EquipmentSlot.LEGS, ArmorType.Material.CLOTH));
  public static final EquipmentType CLOTH_BOOTIM = register(
      new ArmorType(new Identifier(CrackSlashMineMod.MOD_ID, "cloth_boots"),
          EquipmentSlot.FEET, ArmorType.Material.CLOTH));

  public static final EquipmentType RING = register(new TwoHandedTrinketType(
      new Identifier(CrackSlashMineMod.MOD_ID, "ring"), Slots.RING,
      new TrinketType.PrimaryStat(Stats.HEALTH_REGEN,
          new double[] { 60.0, 12.0, 0.9 }),
      new TrinketType.PrimaryStat(Stats.MANA_REGEN,
          new double[] { 100.0, 20.0, 1.5 }),
      new TrinketType.PrimaryStat(Stats.STAMINA_REGEN,
          new double[] { 100.0, 20.0, 1.5 })));

  public static final EquipmentType NECKLACE = register(new TrinketType(
      new Identifier(CrackSlashMineMod.MOD_ID, "necklace"), SlotGroups.CHEST,
      Slots.NECKLACE,
      new TrinketType.PrimaryStat(Stats.LOOT_FIND, new double[] { 300.0 }),
      new TrinketType.PrimaryStat(Stats.GOLD_FIND, new double[] { 300.0 }),
      new TrinketType.PrimaryStat(Stats.DAMAGE_BONUS.get(DamageType.PHYSICAL),
          new double[] { 10.0, 2.0, 0.03 }),
      new TrinketType.PrimaryStat(TrinketType.elemental(Stats.DAMAGE_BONUS),
          new double[] { 10.0, 2.0, 0.03 }),
      new TrinketType.PrimaryStat(Stats.OUTGOING_HEAL_MULTI,
          new double[] { 400.0, 0.5, 0.0001 })));

  public static final EquipmentType GLOVES = register(new TrinketType(
      new Identifier(CrackSlashMineMod.MOD_ID, "gloves"), SlotGroups.CHEST,
      Slots.GLOVES,
      new TrinketType.PrimaryStat(Stats.DAMAGE_BONUS.get(DamageType.PHYSICAL),
          new double[] { 7, 0.7, 0.01 }),
      new TrinketType.PrimaryStat(TrinketType.elemental(Stats.DAMAGE_BONUS),
          new double[] { 7, 0.7, 0.01 }),
      new TrinketType.PrimaryStat(Stats.CRITICAL_DAMAGE,
          new double[] { 600.0, 1.5, 0.0003 }),
      new TrinketType.PrimaryStat(Stats.KRITIKHÄLH_DAMAGE,
          new double[] { 600.0, 1.5, 0.0003 })));

  public static void load() {
    // This is just to load the class
  }
}
