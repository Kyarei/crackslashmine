package flirora.crackslashmine.item.equipment;

import java.util.List;
import java.util.Random;

import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import net.minecraft.util.Identifier;

/**
 * Describes the naming data for a compatible item.
 */
public class CompatibleEquipmentNaming {
  private final List<String> alternateNames;

  /**
   * Constructs a new object.
   *
   * @param alternateNames a list of translation keys that describe alternate
   *                       item names
   */
  public CompatibleEquipmentNaming(List<String> alternateNames) {
    this.alternateNames = alternateNames;
  }

  public CompatibleEquipmentNaming(Identifier id) {
    this(ImmutableList.of("item." + id.getNamespace() + "." + id.getPath()));
  }

  /**
   * Gets a list of alternate item names.
   *
   * @return a list of translation keys that describe alternate item names
   */
  public List<String> getAlternateNames() {
    return alternateNames;
  }

  /**
   * Chooses an alternate item name at random
   *
   * @param r a {@link Random} object to sample from
   * @return the translation key of the chosen name
   */
  public String rollName(Random r) {
    return alternateNames.get(r.nextInt(alternateNames.size()));
  }

  private static class JsonFormat {
    List<String> alternateNames;
  }

  private static final Gson GSON =
      (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();

  public static CompatibleEquipmentNaming fromJson(Identifier id, JsonElement element) {
    flirora.crackslashmine.item.equipment.CompatibleEquipmentNaming.JsonFormat deserialized =
        GSON.fromJson(element,
            flirora.crackslashmine.item.equipment.CompatibleEquipmentNaming.JsonFormat.class);
    ImmutableList.Builder<String> alternateNames = ImmutableList.builder();
    alternateNames.add("item." + id.getNamespace() + "." + id.getPath());
    alternateNames.addAll(deserialized.alternateNames);
    return new CompatibleEquipmentNaming(alternateNames.build());
  }
}