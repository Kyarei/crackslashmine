package flirora.crackslashmine.item.equipment;

import java.util.Objects;
import java.util.Random;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import flirora.crackslashmine.core.MathUtils;

public final class EquipmentParameters {
  private final double potentia, anima, discordia, ambitio;

  public EquipmentParameters(double potentia, double anima, double discordia,
      double ambitio) {
    this.potentia = potentia;
    this.anima = anima;
    this.discordia = discordia;
    this.ambitio = ambitio;
  }

  public double getPotentia() {
    return potentia;
  }

  public double getAnima() {
    return anima;
  }

  public double getDiscordia() {
    return discordia;
  }

  public double getAmbitio() {
    return ambitio;
  }

  public static EquipmentParameters roll(Random r, Origin origin) {
    return origin == Origin.CRAFTED
        ? new EquipmentParameters(0.35 * MathUtils.nextTriangular(r),
            0.35 * MathUtils.nextTriangular(r),
            0.35 * MathUtils.nextTriangular(r), -3)
        : new EquipmentParameters(r.nextGaussian(), r.nextGaussian(),
            r.nextGaussian(), r.nextGaussian());
  }

  @Override
  public String toString() {
    return "EquipmentParameters [potentia=" + potentia + ", anima=" + anima
        + ", discordia=" + discordia + ", ambitio=" + ambitio + "]";
  }

  @Override
  public int hashCode() {
    return Objects.hash(ambitio, anima, discordia, potentia);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    EquipmentParameters other = (EquipmentParameters) obj;
    return Double.doubleToLongBits(ambitio) == Double
        .doubleToLongBits(other.ambitio)
        && Double.doubleToLongBits(anima) == Double
            .doubleToLongBits(other.anima)
        && Double.doubleToLongBits(discordia) == Double
            .doubleToLongBits(other.discordia)
        && Double.doubleToLongBits(potentia) == Double
            .doubleToLongBits(other.potentia);
  }

  private static final Gson GSON =
      (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();

  private static class JsonFormat {
    public double potentia, anima, discordia, ambitio;
  }

  public static EquipmentParameters fromJson(JsonObject json) {
    JsonFormat deserialized = GSON.fromJson(json, JsonFormat.class);
    return new EquipmentParameters(deserialized.potentia, deserialized.anima,
        deserialized.discordia, deserialized.ambitio);
  }
}
