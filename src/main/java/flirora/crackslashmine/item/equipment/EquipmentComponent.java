package flirora.crackslashmine.item.equipment;

import java.util.Objects;
import java.util.function.Consumer;

import org.apache.logging.log4j.Level;
import org.jetbrains.annotations.Nullable;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.CrackSlashMineComponents;
import flirora.crackslashmine.core.CsmPrimaryAttackStat;
import flirora.crackslashmine.core.stat.Stat;
import flirora.crackslashmine.item.equipment.naming.EquipmentName;
import flirora.crackslashmine.item.essence.tonat.VödManager;
import it.unimi.dsi.fastutil.objects.Object2LongMap;
import it.unimi.dsi.fastutil.objects.Object2LongOpenHashMap;
import nerdhub.cardinal.components.api.component.Component;
import nerdhub.cardinal.components.api.util.ItemComponent;
import nerdhub.cardinal.components.api.util.MethodsReturnNonnullByDefault;
import net.fabricmc.fabric.api.util.NbtType;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;

@MethodsReturnNonnullByDefault
public class EquipmentComponent implements ItemComponent<EquipmentComponent> {
  public static class Meta {
    private final int level;
    private final double potentia, anima, discordia, ambitio;
    private final int maxVödLength;
    private final double hue;
    private final long seed;
    private final Origin origin;

    public Meta(int level, double potentia, double anima, double discordia,
        double ambitio, int maxVödLength, double hue, long seed,
        Origin origin) {
      this.level = level;
      this.potentia = potentia;
      this.anima = anima;
      this.discordia = discordia;
      this.ambitio = ambitio;
      this.maxVödLength = maxVödLength;
      this.hue = hue;
      this.seed = seed;
      this.origin = origin;
    }

    public Meta(int level, EquipmentParameters ep, int maxVödLength, double hue,
        long seed, Origin origin) {
      this(level, ep.getPotentia(), ep.getAnima(), ep.getDiscordia(),
          ep.getAmbitio(), maxVödLength, hue, seed, origin);
    }

    public int getLevel() {
      return level;
    }

    public double getPotentia() {
      return potentia;
    }

    public double getAnima() {
      return anima;
    }

    public double getDiscordia() {
      return discordia;
    }

    public double getAmbitio() {
      return ambitio;
    }

    public int getMaxVödLength() {
      return maxVödLength;
    }

    public double getHue() {
      return hue;
    }

    public long getSeed() {
      return seed;
    }

    public Origin getOrigin() {
      return origin;
    }

    public CompoundTag toTag() {
      CompoundTag tag = new CompoundTag();
      tag.putInt("level", level);
      tag.putDouble("potentia", potentia);
      tag.putDouble("anima", anima);
      tag.putDouble("discordia", discordia);
      tag.putDouble("ambitio", ambitio);
      tag.putInt("max_voed_length", maxVödLength);
      tag.putDouble("hue", hue);
      tag.putLong("seed", seed);
      tag.putByte("origin", (byte) origin.ordinal());
      return tag;
    }

    public static Meta fromTag(CompoundTag tag) {
      return new Meta(tag.getInt("level"), tag.getDouble("potentia"),
          tag.getDouble("anima"), tag.getDouble("discordia"),
          tag.getDouble("ambitio"), tag.getInt("max_voed_length"),
          tag.getDouble("hue"), tag.getLong("seed"),
          Origin.values()[tag.getByte("origin")]);
    }

    @Override
    public boolean equals(Object o) {
      if (this == o)
        return true;
      if (o == null || getClass() != o.getClass())
        return false;
      Meta meta = (Meta) o;
      return level == meta.level && Double.compare(meta.potentia, potentia) == 0
          && Double.compare(meta.anima, anima) == 0
          && Double.compare(meta.discordia, discordia) == 0
          && Double.compare(meta.ambitio, ambitio) == 0
          && maxVödLength == meta.maxVödLength
          && Double.compare(meta.hue, hue) == 0 && seed == meta.seed
          && origin == meta.origin;
    }

    @Override
    public int hashCode() {
      return Objects.hash(level, potentia, anima, discordia, ambitio,
          maxVödLength, hue, seed, origin);
    }
  }

  private final ItemStack stack;
  private Meta meta;
  private @Nullable CsmPrimaryAttackStat weaponBaseStat;
  private Object2LongMap<Stat> statBonuses;
  private EquipmentUtils.@Nullable VödData vödData;
  private EquipmentName name;
  private boolean initialized;

  public EquipmentComponent(ItemStack stack) {
    this.stack = stack;
  }

  @Override
  public void fromTag(CompoundTag tag) {
    initialized = tag.getBoolean("initialized");
    if (!initialized)
      return;
    meta = Meta.fromTag(tag.getCompound("CSMMeta"));
    if (tag.contains("CSMBaseAttack", NbtType.COMPOUND))
      weaponBaseStat =
          CsmPrimaryAttackStat.fromTag(tag.getCompound("CSMBaseAttack"));
    statBonuses =
        EquipmentUtils.statsFromTag(tag.getCompound("CSMStatBonuses"));
    if (tag.contains("voed", NbtType.COMPOUND))
      vödData = EquipmentUtils.VödData.fromTag(tag.getCompound("voed"),
          VödManager.SERVER);
    name = EquipmentName.fromTag(tag.getCompound("CSMName"));
  }

  @Override
  public CompoundTag toTag(CompoundTag tag) {
    tag.putBoolean("initialized", initialized);
    if (!initialized)
      return tag;
    tag.put("CSMMeta", meta.toTag());
    if (weaponBaseStat != null)
      tag.put("CSMBaseAttack", weaponBaseStat.toTag());
    CompoundTag statTag = new CompoundTag();
    EquipmentUtils.saveStatsToTag(statBonuses, statTag);
    tag.put("CSMStatBonuses", statTag);
    if (vödData != null)
      tag.put("voed", vödData.toTag());
    tag.put("CSMName", name.toTag());
    return tag;
  }

  @Override
  public boolean isComponentEqual(Component other) {
    return this.equals(other);
  }

  public Meta getMeta() {
    return meta;
  }

  public void setMeta(Meta meta) {
    this.meta = meta;
  }

  public @Nullable CsmPrimaryAttackStat getWeaponBaseStat() {
    return weaponBaseStat;
  }

  public void setWeaponBaseStat(@Nullable CsmPrimaryAttackStat weaponBaseStat) {
    this.weaponBaseStat = weaponBaseStat;
  }

  public Object2LongMap<Stat> getStatBonuses() {
    return statBonuses;
  }

  public Object2LongMap<Stat> getNetStatBonuses() {
    if (vödData == null)
      return statBonuses;
    Object2LongMap<Stat> stats = new Object2LongOpenHashMap<>(statBonuses);
    for (Object2LongMap.Entry<Stat> e : vödData.getStats()
        .object2LongEntrySet()) {
      stats.mergeLong(e.getKey(), e.getLongValue(), Long::sum);
    }
    return stats;
  }

  public void setStatBonuses(Object2LongMap<Stat> statBonuses) {
    this.statBonuses = statBonuses;
  }

  public EquipmentUtils.@Nullable VödData getVödData() {
    return vödData;
  }

  public void setVödData(@Nullable EquipmentUtils.VödData vödData) {
    this.vödData = vödData;
  }

  public EquipmentName getName() {
    return name;
  }

  public void setName(EquipmentName name) {
    this.name = name;
  }

  public boolean isInitialized() {
    return initialized;
  }

  public void initialize() {
    initialized = true;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    EquipmentComponent that = (EquipmentComponent) o;
    return Objects.equals(meta, that.meta)
        && Objects.equals(weaponBaseStat, that.weaponBaseStat)
        && Objects.equals(statBonuses, that.statBonuses)
        && Objects.equals(vödData, that.vödData)
        && Objects.equals(name, that.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(meta, weaponBaseStat, statBonuses, vödData, name);
  }

  public static void initialize(ItemStack stack,
      Consumer<EquipmentComponent> callback) {
    EquipmentComponent component =
        CrackSlashMineComponents.EQUIPMENT_COMPONENT.get(stack);
    if (component.isInitialized()) {
      CrackSlashMineMod.log(Level.WARN,
          "Re-initializing component for item stack " + stack);
    }
    callback.accept(component);
    component.initialize();
  }

  public static @Nullable EquipmentComponent getFor(ItemStack stack) {
    EquipmentComponent component =
        CrackSlashMineComponents.EQUIPMENT_COMPONENT.getNullable(stack);
    return component != null && component.isInitialized() ? component : null;
  }
}
