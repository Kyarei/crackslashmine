package flirora.crackslashmine.item.equipment;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.jetbrains.annotations.NotNull;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.config.CsmConfig;
import flirora.crackslashmine.core.CsmPrimaryAttackStat;
import flirora.crackslashmine.core.CsmTags;
import flirora.crackslashmine.core.Levels;
import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.core.MathUtils;
import flirora.crackslashmine.core.stat.Stats;
import flirora.crackslashmine.core.xse.ExtendedStatusEffect;
import flirora.crackslashmine.core.xse.ExtendedStatusEffects;
import flirora.crackslashmine.core.xse.RangedExtendedStatusEffectInstance;
import flirora.crackslashmine.item.MoneyItem;
import flirora.crackslashmine.item.equipment.types.EquipmentType;
import flirora.crackslashmine.mixin.LivingEntityInvoker;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.mob.Angerable;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.mob.Monster;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.context.LootContextTypes;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;

/**
 * Helper functions for generating equipment.
 * <p>
 * Equipment in this mod has three base properties:
 *
 * <ul>
 * <li>Potentia: affects the strength of the primary stat</li>
 * <li>Anima: affects the strength and quantity of secondary stats</li>
 * <li>Discordia: affects the range of the primary stat, and something else for
 * armor and trinkets?</li>
 * </ul>
 * <p>
 * For dropped, gear, all of these variables are sampled from a standard normal
 * distribution.
 */
public class LootUtils {
  /**
   * Populates a compatible item with statistics.
   *
   * @param item      the item type of `itemStack`
   * @param itemStack the item stack to populate
   * @param world     the world in which the item was generated
   * @param from      the player that caused this item to appear; may be null
   * @param level     the level to generate the item at
   * @param origin    the origin of the item
   * @return whether the item was compatible and therefore the population
   *         succeeded
   */
  public static boolean generateItem(Item item, ItemStack itemStack,
      World world, LivingEntity from, int level, Origin origin) {
    Identifier itemId = Registry.ITEM.getId(item);
    CompatibleItemDescription desc = CompatibleItemManager.INSTANCE.get(itemId);
    if (desc != null) {
      int minLevel = desc.getMinLevel();
      int maxLevel = desc.getMaxLevel();
      LivingEntityStats stats =
          from != null ? LivingEntityStats.getFor(from) : null;
      int myLevel = stats != null ? stats.getLevel() : 0;
      Random random = world.getRandom();
      int chosenLevel = Math.min(maxLevel, Math.max(minLevel, level));
      if (level < 0) {
        chosenLevel = (myLevel >= minLevel && myLevel <= maxLevel)
            ? MathUtils.nextIntInclusive(random, minLevel, myLevel)
            : MathUtils.nextIntInclusive(random, minLevel, maxLevel);
      }
      EquipmentType type = desc.getType();
      type.generate(random, chosenLevel,
          desc.rollEquipmentParameters(random, origin), origin, itemStack);
      return true;
    }
    return false;
  }

  /**
   * Populates all equipment on a mob.
   * <p>
   * Should be called once the mob has gained all of its equipment.
   *
   * @param mob The mob to populate equipment on.
   */
  public static void populateEquipmentOnMob(MobEntity mob) {
    // populate CSM-compatible equipment
    LivingEntityStats stats = LivingEntityStats.getFor(mob);
    for (EquipmentSlot slot : EquipmentSlot.values()) {
      ItemStack stack = mob.getEquippedStack(slot);
      CompatibleItemDescription description = CompatibleItemManager.INSTANCE
          .get(Registry.ITEM.getId(stack.getItem()));
      if (description == null)
        return;
      int level = stats.getLevel();
      if (level < description.getMinLevel()
          || level > description.getMaxLevel()) {
        // This equipment is level-inappropriate; replace it with a
        // level-appropriate alternative if there is any
        List<Map.Entry<Identifier, RegularCompatibleItemDescription>> alternatives =
            CompatibleItemManager.INSTANCE
                .regularAtLevel(level, description.getType())
                .collect(Collectors.toList());
        if (!alternatives.isEmpty()) {
          Identifier id = alternatives
              .get(mob.getRandom().nextInt(alternatives.size())).getKey();
          stack = new ItemStack(Registry.ITEM.get(id));
          mob.equipStack(slot, stack);
        }
      }
      generateItem(stack.getItem(), stack, mob.getEntityWorld(), mob,
          stats.getLevel(), Origin.LOOTED);
    }
  }

  private static double dropMultiplierByLevelDifference(int diff) {
    if (diff < -5) {
      // Killed a much weaker mob
      return Math.pow(0.8, -5 - diff);
    }
    if (diff > 5) {
      // Killed a much stronger mob
      return Math.pow(0.9, diff - 5);
    }
    return 1;
  }

  private static final Identifier SIDE_LOOT_ID =
      new Identifier(CrackSlashMineMod.MOD_ID, "csm/misc_csm_drops");

  /**
   * Drop extra loot from a mob that was killed by a player.
   *
   * @param r            an instance of {@link Random}
   * @param victim       the mob that was killed
   * @param lastAttacker the player who last killed `victim`
   * @param dropCallback a callback to apply to each item dropped
   * @return the amount of experience that should be awarded
   */
  public static long dropExtras(Random r, MobEntity victim,
      PlayerEntity lastAttacker, Consumer<ItemStack> dropCallback) {
    assert victim.getEntityWorld().getServer() != null;
    if (lastAttacker == null)
      return 0;
    if (!(victim instanceof Monster || (victim instanceof Angerable
        && ((Angerable) victim).hasAngerTime())))
      return 0;
    LivingEntityStats victimStats = LivingEntityStats.getFor(victim);
    if (victimStats
        .getProportionOfDamageFromPlayer() < CrackSlashMineMod.config.minimumPlayerDamageProportion) {
      return 0;
    }
    LivingEntityStats attackerStats = LivingEntityStats.getFor(lastAttacker);
    long lootFind = attackerStats.getStat(Stats.LOOT_FIND);
    long goldFind = attackerStats.getStat(Stats.GOLD_FIND);
    int victimLevel = victimStats.getLevel();
    int levelDiff = victimLevel - attackerStats.getLevel();
    // We use vanilla health here
    double resourceVolume = victim.getMaxHealth();
    if (CsmTags.NASTY_MOBS.contains(victim.getType()))
      resourceVolume *= 3;
    if (CsmTags.BOSSES.contains(victim.getType()))
      resourceVolume *= 4;
    // TODO: implement mob 'rarity'?
    ArrayList<Entry<Identifier, RegularCompatibleItemDescription>> eligibleEntries =
        CompatibleItemManager.INSTANCE.regularAtLevel(victimLevel)
            .collect(Collectors.toCollection(ArrayList::new));
    ArrayList<Entry<Identifier, UniqueCompatibleItemDescription>> eligibleUniqueEntries =
        CompatibleItemManager.INSTANCE.uniqueAtLevel(victimLevel)
            .collect(Collectors.toCollection(ArrayList::new));
    LootTable sideTable = victim.getEntityWorld().getServer().getLootManager()
        .getTable(SIDE_LOOT_ID);
    LootContext ctx = ((LivingEntityInvoker) victim)
        .callGetLootContextBuilder(true, DamageSource.GENERIC)
        .build(LootContextTypes.ENTITY);
    resourceVolume *= dropMultiplierByLevelDifference(levelDiff);
    double expectedDrops = 0.015 * resourceVolume * (1 + 1e-4 * lootFind);
    int actualDrops = MathUtils.nextPoisson(r, expectedDrops);
    for (int i = 0; i < actualDrops; ++i) {
      boolean sideloot =
          r.nextDouble() < CrackSlashMineMod.config.miscDropChance;
      if (sideloot) {
        sideTable.generateLoot(ctx, dropCallback);
      } else if (!eligibleEntries.isEmpty()) {
        Identifier id;
        if (r.nextInt(100) == 0 && !eligibleUniqueEntries.isEmpty()) {
          Entry<Identifier, UniqueCompatibleItemDescription> e =
              eligibleUniqueEntries
                  .get(r.nextInt(eligibleUniqueEntries.size()));
          id = e.getKey();
        } else {
          Entry<Identifier, RegularCompatibleItemDescription> e =
              eligibleEntries.get(r.nextInt(eligibleEntries.size()));
          id = e.getKey();
        }
        Item item = Registry.ITEM.get(id);
        ItemStack itemStack = new ItemStack(item);
        generateItem(item, itemStack, victim.getEntityWorld(), lastAttacker,
            victimLevel, Origin.LOOTED);
        if (itemStack.getMaxDamage() > 0)
          itemStack.setDamage(r.nextInt(itemStack.getMaxDamage()));
        dropCallback.accept(itemStack);
      }
    }
    double expectedGold =
        resourceVolume * CrackSlashMineMod.config.goldCurve.value(victimLevel)
            * (1 + 1e-4 * goldFind);
    int actualGold = MathUtils.nextPoisson(r, expectedGold);
    if (actualGold > 0)
      dropCallback.accept(MoneyItem.of(actualGold));
    double expectedXpPercent = resourceVolume
        / (20 * CrackSlashMineMod.config.zombiesToNextLevel.value(victimLevel));
    double gammaShape = Math.max(1.0, r.nextGaussian() + 3);
    double xpPercent =
        MathUtils.nextGamma(r, gammaShape, expectedXpPercent / gammaShape);
    return Math.round(xpPercent * Levels.getXpForNextLevel(victimLevel));
  }

  /**
   * Makes some changes to a weapon's primary attack stat. Sometimes, weapons
   * will generate with some of the damage offloaded to an extended status
   * effect; this method is responsible for doing so.
   *
   * @param attack a {@link CsmPrimaryAttackStat} to transform
   * @param r      a {@link Random} object to sample from
   * @return the changed primary attack stat
   */
  public static CsmPrimaryAttackStat transformPrimaryStat(
      CsmPrimaryAttackStat attack, Random r) {
    List<CsmPrimaryAttackStat.Entry> entries = attack.getEntries();
    // copy this because we want to mutate
    List<RangedExtendedStatusEffectInstance> effects =
        new ArrayList<>(attack.getEffects());
    List<CsmPrimaryAttackStat.Entry> newEntries = new ArrayList<>();
    for (CsmPrimaryAttackStat.Entry e : entries) {
      switch (r.nextInt(12)) {
      case 0: {
        // Replace some of the damage with a DoT effect
        CsmConfig.EffectConfig dot = CrackSlashMineMod.config.dotEffectConfig;
        double proportion = dot.minProportionDeducted + r.nextDouble()
            * (dot.maxProportionDeducted - dot.minProportionDeducted);
        long minDeducted = Math.round(e.getMin() * proportion);
        long maxDeducted = Math.round(e.getMax() * proportion);
        newEntries.add(new CsmPrimaryAttackStat.Entry(e.getType(),
            e.getMin() - minDeducted, e.getMax() - maxDeducted));
        effects.add(createRangedFromConfig(
            ExtendedStatusEffects.DAMAGE_OVER_TIME.get(e.getType()),
            minDeducted, maxDeducted, dot));
        break;
      }
      case 1: {
        CsmConfig.EffectConfig delay =
            CrackSlashMineMod.config.delayEffectConfig;
        double proportion = delay.minProportionDeducted + r.nextDouble()
            * (delay.maxProportionDeducted - delay.minProportionDeducted);
        long minDeducted = Math.round(e.getMin() * proportion);
        long maxDeducted = Math.round(e.getMax() * proportion);
        newEntries.add(new CsmPrimaryAttackStat.Entry(e.getType(),
            e.getMin() - minDeducted, e.getMax() - maxDeducted));
        effects.add(createRangedFromConfig(
            ExtendedStatusEffects.DELAYED_DAMAGE.get(e.getType()), minDeducted,
            maxDeducted, delay));
        break;
      }
      default:
        newEntries.add(e);
      }
    }
    return new CsmPrimaryAttackStat(newEntries, effects);
  }

  @NotNull
  private static RangedExtendedStatusEffectInstance createRangedFromConfig(
      ExtendedStatusEffect effect, long minDeducted, long maxDeducted,
      CsmConfig.EffectConfig config) {
    return new RangedExtendedStatusEffectInstance(effect, config.minDuration,
        config.maxDuration,
        (minDeducted * config.scaleNumerator + config.scaleDenominator / 2)
            / config.scaleDenominator,
        (maxDeducted * config.scaleNumerator + config.scaleDenominator / 2)
            / config.scaleDenominator);
  }
}
