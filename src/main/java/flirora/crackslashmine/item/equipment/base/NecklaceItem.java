package flirora.crackslashmine.item.equipment.base;

import dev.emi.trinkets.api.SlotGroups;
import dev.emi.trinkets.api.Slots;
import dev.emi.trinkets.api.TrinketItem;
import net.minecraft.item.ItemGroup;

public class NecklaceItem extends TrinketItem {
    public NecklaceItem(Settings settings) {
        super(settings.group(ItemGroup.COMBAT).maxCount(1));
    }

    @Override
    public boolean canWearInSlot(String group, String slot) {
        return group.equals(SlotGroups.CHEST) && slot.equals(Slots.NECKLACE);
    }
}
