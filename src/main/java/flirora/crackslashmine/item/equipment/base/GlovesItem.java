package flirora.crackslashmine.item.equipment.base;

import dev.emi.trinkets.api.SlotGroups;
import dev.emi.trinkets.api.Slots;
import dev.emi.trinkets.api.TrinketItem;
import net.minecraft.item.ItemGroup;

public class GlovesItem extends TrinketItem {

  public GlovesItem(Settings settings) {
    super(settings.group(ItemGroup.COMBAT).maxCount(1));
  }

  @Override
  public boolean canWearInSlot(String group, String slot) {
    return group.equals(SlotGroups.HAND) && slot.equals(Slots.GLOVES);
  }
}
