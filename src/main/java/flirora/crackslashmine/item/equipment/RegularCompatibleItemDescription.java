package flirora.crackslashmine.item.equipment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import flirora.crackslashmine.core.DamageType;
import flirora.crackslashmine.core.MathUtils;
import flirora.crackslashmine.core.stat.Stat;
import flirora.crackslashmine.core.stat.modifier.ModifierManager;
import flirora.crackslashmine.core.stat.modifier.StatModifier;
import flirora.crackslashmine.item.equipment.naming.AffixType;
import flirora.crackslashmine.item.equipment.naming.EquipmentName;
import flirora.crackslashmine.item.equipment.types.EquipmentType;
import flirora.crackslashmine.item.equipment.types.WeaponType;
import it.unimi.dsi.fastutil.objects.Object2LongMap;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

/**
 * Traits for a regular C/M equipment item.
 */
public class RegularCompatibleItemDescription
    extends CompatibleItemDescription {
  private final CompatibleEquipmentNaming naming;
  private final boolean natural;

  /**
   * Constructs a new {@link RegularCompatibleItemDescription}.
   *
   * @param type      The {@link EquipmentType} of the item.
   * @param minLevel  The minimum level at which this item will generate.
   * @param maxLevel  The maximum level at which this item will generate.
   * @param baseValue The base monetary value of this item.
   * @param naming    A {@link CompatibleEquipmentNaming} object for this item.
   */
  public RegularCompatibleItemDescription(EquipmentType type, int minLevel,
      int maxLevel, double baseValue, CompatibleEquipmentNaming naming,
      boolean natural) {
    super(type, minLevel, maxLevel, baseValue);
    this.naming = naming;
    this.natural = natural;
  }

  public CompatibleEquipmentNaming getNaming() {
    return naming;
  }

  public boolean isNatural() {
    return natural;
  }

  @Override
  public EquipmentName rollName(Random r) {
    return new EquipmentName(naming.rollName(r));
  }

  @Override
  public void addFlavor(List<Text> lines) {
  }

  private static double[] constructCumulativeWeights(List<StatModifier> mods,
      double anima) {
    double[] res = new double[mods.size() + 1];
    int i = 0;
    for (StatModifier mod : mods) {
      double weight = mod.getWeightAtAnima(anima);
      res[i + 1] = res[i] + weight;
      ++i;
    }
    return res;
  }

  @Override
  public void generateStatBonuses(Random r, int level, double anima,
      double discordia, Object2LongMap<Stat> result, EquipmentName name) {
    ArrayList<StatModifier> eligibleStatMods =
        ModifierManager.INSTANCE.getAllEligibleModifiers(this.type);
    int totalEligible = eligibleStatMods.size();
    while (true) {
      if (eligibleStatMods.isEmpty())
        break; // no more
      double[] cumulativeWeights =
          constructCumulativeWeights(eligibleStatMods, anima);
      double maxRoll = cumulativeWeights[cumulativeWeights.length - 1]
          + totalEligible * (1.0 + Math.exp(-anima * anima)) / 3.0;
      int index =
          Arrays.binarySearch(cumulativeWeights, maxRoll * r.nextDouble());
      if (index < 0) // If not found, then the index is one less than the
                     // 'insertion point'.
        index = -(index + 1) - 1;
      if (index == eligibleStatMods.size())
        break;
      StatModifier selected = eligibleStatMods.get(index);
      // Apply the stat mod
      selected.sampleAmount(r, level, anima, discordia).forEach(p -> {
        result.mergeLong(p.getLeft(), Math.round(p.getRight()), Long::sum);
      });
      int affixFlags = selected.getNaming().getAffixFlags();
      int freeFlags = name.getFreeAffixSlotFlags();
      int eligible = affixFlags & freeFlags;
      if (eligible != 0) {
        AffixType which =
            AffixType.values()[MathUtils.sampleSetBitIndex(r, eligible)];
        Identifier id = selected.getId();
        name.setAffix(which, "csm.affix." + id.getNamespace() + "."
            + id.getPath() + "." + which.getName());
      }
      // Remove the selected modifier
      StatModifier tail = eligibleStatMods.get(eligibleStatMods.size() - 1);
      eligibleStatMods.set(index, tail);
      eligibleStatMods.remove(eligibleStatMods.size() - 1);
    }
  }

  @Override
  public EquipmentParameters rollEquipmentParameters(Random r, Origin origin) {
    return EquipmentParameters.roll(r, origin);
  }

  @Override
  public WeaponDamageProperties rollWeaponDamageProperties(Random r,
      WeaponType type) {
    BaseDamageType wd = type.sampleDamageType(r);
    DamageType de = DamageType.randomElemental(r);
    return new WeaponDamageProperties(wd, de);
  }

  private static class JsonFormat {
    public String type;
    public int minLevel, maxLevel;
    public double baseValue;
    public JsonElement naming;
    public boolean natural;
  }

  private static final Gson GSON =
      (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();

  /**
   * Deserializes a {@link RegularCompatibleItemDescription} from JSON.
   *
   * @param id      the ID of the compatible item
   * @param element a {@link JsonElement} that holds the data
   * @return a {@link RegularCompatibleItemDescription}
   */
  public static RegularCompatibleItemDescription fromJson(Identifier id,
      JsonElement element) {
    JsonFormat deserialized = GSON.fromJson(element, JsonFormat.class);
    return new RegularCompatibleItemDescription(
        EquipmentType.getByName(Identifier.tryParse(deserialized.type)),
        deserialized.minLevel, deserialized.maxLevel, deserialized.baseValue,
        deserialized.naming != null
            ? CompatibleEquipmentNaming.fromJson(id, deserialized.naming)
            : new CompatibleEquipmentNaming(id),
        deserialized.natural);
  }
}
