package flirora.crackslashmine.item.equipment.naming;

import java.util.EnumMap;
import java.util.Objects;

import org.jetbrains.annotations.Nullable;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.util.NbtType;
import net.minecraft.client.resource.language.I18n;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;

/**
 * An object that holds data about the default name on a piece of equipment.
 * This class is used instead of storing the raw name itself to allow
 * localization.
 * <p>
 * A name contains a <i>base name</i> and one <i>affix</i> per <i>affix
 * slot</i>. To display the name, affixes are applied to the base name in some
 * order.
 */
public class EquipmentName {
  private final String baseNameKey;
  private final EnumMap<AffixType, @Nullable String> affixSlots;

  /**
   * Constructs a new equipment name with no affixes filled.
   *
   * @param baseNameKey the translation key for the base name.
   */
  public EquipmentName(String baseNameKey) {
    this.baseNameKey = baseNameKey;
    this.affixSlots = new EnumMap<>(AffixType.class);
  }

  /**
   * Get a bitfield of the free affix slots of this name.
   *
   * @return a bitfield where bit number <code>affixType.ordinal()</code> is set
   *         if the affix slot for <code>affixType</code> does not have any
   *         affix
   */
  public int getFreeAffixSlotFlags() {
    int flag = 0;
    for (AffixType type : AffixType.values()) {
      if (!affixSlots.containsKey(type))
        flag |= type.flag();
    }
    return flag;
  }

  /**
   * Set an affix in a slot.
   *
   * @param type    the affix type to insert to
   * @param i18nKey the translation key for the affix
   */
  public void setAffix(AffixType type, String i18nKey) {
    affixSlots.put(type, i18nKey);
  }

  /**
   * Return a text representation of the name.
   * <p>
   * This method can be called only on the client because it relies on the value
   * of a translation key to determine the order in which the affixes are
   * applied. In particular, the string with the key
   * <code>tooltip.csm.names.affixApplicationOrder</code> is decoded, such that
   * each affix type in the order is represented as
   * <code>'a' + affixType.ordinal()</code> in the string.
   *
   * @return a {@link Text} object for this name.
   */
  @Environment(EnvType.CLIENT)
  public Text asText() {
    String stringifiedOrder =
        I18n.translate("tooltip.csm.names.affixApplicationOrder");
    Text result = new TranslatableText(baseNameKey);
    for (char c : stringifiedOrder.toCharArray()) {
      AffixType type = AffixType.values()[c - 'a'];
      String affixKey = affixSlots.get(type);
      if (affixKey != null) {
        result = new TranslatableText(
            "tooltip.csm.names.postprocessor." + type.getName(),
            new TranslatableText(affixKey), result);
      }
    }
    return result;
  }

  public CompoundTag toTag() {
    CompoundTag tag = new CompoundTag();
    tag.putString("base", baseNameKey);
    for (EnumMap.Entry<AffixType, @Nullable String> e : affixSlots.entrySet()) {
      if (e.getValue() != null)
        tag.putString(e.getKey().getName(), e.getValue());
    }
    return tag;
  }

  public static EquipmentName fromTag(CompoundTag tag) {
    String baseName = tag.getString("base");
    if (baseName == null)
      throw new IllegalArgumentException("tag has no base name");
    EquipmentName result = new EquipmentName(baseName);
    for (AffixType affixType : AffixType.values()) {
      String name = affixType.getName();
      if (tag.contains(name, NbtType.STRING))
        result.setAffix(affixType, tag.getString(name));
    }
    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hash(affixSlots, baseNameKey);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    EquipmentName other = (EquipmentName) obj;
    return Objects.equals(affixSlots, other.affixSlots)
        && Objects.equals(baseNameKey, other.baseNameKey);
  }
}
