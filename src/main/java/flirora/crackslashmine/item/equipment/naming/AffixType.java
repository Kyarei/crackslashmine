package flirora.crackslashmine.item.equipment.naming;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * An affix type. An equipment name may have at most one affix of each type.
 */
public enum AffixType {
    /**
     * Represents the genitive of a proper noun.
     */
    PROPER_GENITIVE("proper_genitive"),
    /**
     * Represents an adjective or the closest equivalent in the respective
     * language.
     */
    ADJECTIVE("adjective"),
    /**
     * Represents an adpositional phrase using the equivalent of "of",
     * or the closest equivalent in the respective language.
     */
    OF_GENITIVE("of_genitive"),
    ;

    private final String name;

    AffixType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    private static final Map<String, AffixType> TYPES_BY_NAME =
            Arrays.stream(AffixType.values())
                    .collect(Collectors.toMap(AffixType::getName, t -> t));

    public static AffixType fromName(String name) {
        return TYPES_BY_NAME.get(name);
    }

    public int flag() {
        return 1 << ordinal();
    }

    public boolean in(int i) {
        return (i & flag()) != 0;
    }
}
