package flirora.crackslashmine.item.equipment;

import java.util.function.Supplier;

import flirora.crackslashmine.item.CsmItems;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ArmorMaterial;
import net.minecraft.item.Items;
import net.minecraft.recipe.Ingredient;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.Lazy;

public enum CsmArmorMaterial implements ArmorMaterial {
  WOOL("csm_wool", 5, new int[] { 1, 3, 2, 1 }, 15,
      SoundEvents.BLOCK_WOOL_PLACE, 0.0F, 0.0F,
      () -> Ingredient.ofItems(Items.WHITE_WOOL)),
  ENCHANTED_CLOTH("csm_enchanted_cloth", 14, new int[] { 1, 3, 5, 2 }, 19,
      SoundEvents.BLOCK_WOOL_PLACE, 0.0F, 0.0F,
      () -> Ingredient.ofItems(CsmItems.ENCHANTED_CLOTH)),;

  private static final int[] BASE_DURABILITY = { 13, 15, 16, 11 };
  private final String name;
  private final int durabilityMultiplier;
  private final int[] armorValues;
  private final int enchantability;
  private final SoundEvent equipSound;
  private final float toughness;
  private final float knockbackResistance;
  private final Lazy<Ingredient> repairIngredient;

  CsmArmorMaterial(String name, int durabilityMultiplier, int[] armorValues,
      int enchantability, SoundEvent soundEvent, float toughness,
      float knockbackResistance, Supplier<Ingredient> repairIngredient) {
    this.name = name;
    this.durabilityMultiplier = durabilityMultiplier;
    this.armorValues = armorValues;
    this.enchantability = enchantability;
    this.equipSound = soundEvent;
    this.toughness = toughness;
    this.knockbackResistance = knockbackResistance;
    this.repairIngredient = new Lazy<>(repairIngredient);
  }

  @Override
  public int getDurability(EquipmentSlot equipmentSlot_1) {
    return BASE_DURABILITY[equipmentSlot_1.getEntitySlotId()]
        * this.durabilityMultiplier;
  }

  @Override
  public int getProtectionAmount(EquipmentSlot equipmentSlot_1) {
    return this.armorValues[equipmentSlot_1.getEntitySlotId()];
  }

  @Override
  public int getEnchantability() {
    return this.enchantability;
  }

  @Override
  public SoundEvent getEquipSound() {
    return this.equipSound;
  }

  @Override
  public Ingredient getRepairIngredient() {
    // We needed to make it a Lazy type so we can actually get the
    // Ingredient from the Supplier.
    return this.repairIngredient.get();
  }

  @Override
  @Environment(EnvType.CLIENT)
  public String getName() {
    return this.name;
  }

  @Override
  public float getToughness() {
    return this.toughness;
  }

  @Override
  public float getKnockbackResistance() {
    return this.knockbackResistance;
  }
}
