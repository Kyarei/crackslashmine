package flirora.crackslashmine.item.equipment;

import java.text.NumberFormat;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.function.Consumer;

import org.apache.logging.log4j.Level;
import org.jetbrains.annotations.Nullable;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.Beautify;
import flirora.crackslashmine.core.CsmPrimaryAttackStat;
import flirora.crackslashmine.core.CsmRegistries;
import flirora.crackslashmine.core.EntityDamageUtils;
import flirora.crackslashmine.core.MathUtils;
import flirora.crackslashmine.core.stat.Stat;
import flirora.crackslashmine.core.xse.RangedExtendedStatusEffectInstance;
import flirora.crackslashmine.item.ChromaticUtils;
import flirora.crackslashmine.item.CsmItems;
import flirora.crackslashmine.item.TooltipUtils;
import flirora.crackslashmine.item.equipment.types.EquipmentType;
import flirora.crackslashmine.item.equipment.types.WeaponType;
import flirora.crackslashmine.item.essence.tonat.VödDescription;
import flirora.crackslashmine.item.essence.tonat.VödManager;
import flirora.crackslashmine.item.traits.Salvageable;
import it.unimi.dsi.fastutil.objects.Object2LongMap;
import it.unimi.dsi.fastutil.objects.Object2LongMaps;
import it.unimi.dsi.fastutil.objects.Object2LongOpenHashMap;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.text.TextColor;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class EquipmentUtils {
  public static final Object2LongMap<Stat> NO_STATS =
      Object2LongMaps.emptyMap();

  public static Object2LongMap<Stat> statsFromTag(CompoundTag tag) {
    Object2LongMap<Stat> result = new Object2LongOpenHashMap<>();
    for (String key : tag.getKeys()) {
      Stat stat = CsmRegistries.STAT.get(Identifier.tryParse(key));
      if (stat == null) {
        CrackSlashMineMod.log(Level.WARN, "Unknown stat " + key);
        continue;
      }
      long value = tag.getLong(key);
      result.put(stat, value);
    }
    return result;
  }

  public static void saveStatsToTag(Object2LongMap<Stat> bonuses,
      CompoundTag tag) {
    for (Map.Entry<Stat, Long> entry : bonuses.object2LongEntrySet()) {
      Stat stat = entry.getKey();
      long amt = entry.getValue();
      tag.putLong(stat.getId().toString(), amt);
    }
  }

  private static void addCompatibleItemInfo(CompatibleItemDescription desc,
      List<Text> list) {
    list.add(new TranslatableText("tooltip.csm.compatibleItem")
        .formatted(Formatting.GOLD));
    list.add(new TranslatableText("tooltip.csm.compatibleItem.levelRange",
        desc.getMinLevel(),
        Math.min(desc.getMaxLevel(), CrackSlashMineMod.config.maxLevel))
            .formatted(Formatting.GOLD));
  }

  /**
   * Adds a custom tooltip to compatible items.
   *
   * @param stack  The {@link ItemStack} whose tooltip needs to be generated
   * @param list   A list of {@link Text} objects to be appended to
   * @param player The {@link PlayerEntity} who should view the tooltip, or null
   *               if none
   */
  @Environment(EnvType.CLIENT)
  public static void addCsmTooltip(ItemStack stack, List<Text> list,
      @Nullable PlayerEntity player) {
    boolean isShifting = Screen.hasShiftDown();
    EquipmentComponent component = EquipmentComponent.getFor(stack);
    CompatibleItemDescription desc = CompatibleItemManager.INSTANCE
        .get(Registry.ITEM.getId(stack.getItem()));
    if (component == null || !component.isInitialized()) {
      if (desc != null) {
        TooltipUtils.addEmptyLine(list);
        addCompatibleItemInfo(desc, list); // The best we can do
      }
      return;
    }
    EquipmentComponent.Meta meta = component.getMeta();
    TooltipUtils.addEmptyLine(list);
    list.add(new TranslatableText("tooltip.csm.levelRequired",
        Beautify.beautify(meta.getLevel())).formatted(Formatting.BLUE));
    TooltipUtils.addEmptyLine(list);
    CsmPrimaryAttackStat attackStat = component.getWeaponBaseStat();
    if (attackStat != null) {
      for (CsmPrimaryAttackStat.Entry e : attackStat.getEntries()) {
        TranslatableText damageTypeText = new TranslatableText(
            "csm.stat.crackslashmine." + e.getType().getName() + "_bonus");
        list.add(new TranslatableText("tooltip.csm.attackStat",
            Beautify.beautify(e.getMin()), Beautify.beautify(e.getMax()),
            damageTypeText).styled(
                s -> s.withColor(TextColor.fromRgb(e.getType().getColor()))));
      }
      for (RangedExtendedStatusEffectInstance e : attackStat.getEffects()) {
        list.add(e.formatInTooltip());
      }
      TooltipUtils.addEmptyLine(list);
    }
    Object2LongMap<Stat> statBonuses = component.getStatBonuses();
    for (Object2LongMap.Entry<Stat> e : statBonuses.object2LongEntrySet()) {
      Stat stat = e.getKey();
      long amt = e.getLongValue();
      list.add(stat.format(amt));
    }
    TooltipUtils.addEmptyLine(list);
    if (desc != null) {
      desc.getType().addInfoToTooltip(component, stack, list);
      TooltipUtils.addEmptyLine(list);
    }
    VödData vödData = component.getVödData();
    if (vödData == null) {
      list.add(
          new TranslatableText("tooltip.csm.voed.empty", meta.getMaxVödLength())
              .formatted(Formatting.GOLD));
    } else {
      list.add(new TranslatableText("tooltip.csm.voed.set",
          new LiteralText(vödData.getWord().getWord())
              .formatted(Formatting.AQUA)).formatted(Formatting.GOLD));
      vödData.getWord().addFlavorText(list);
      if (isShifting) {
        for (Object2LongMap.Entry<Stat> e : vödData.getStats()
            .object2LongEntrySet()) {
          Stat stat = e.getKey();
          long amt = e.getLongValue();
          list.add(stat.format(amt));
        }
      } else {
        list.add(new TranslatableText("tooltip.csm.voed.statsHidden")
            .formatted(Formatting.LIGHT_PURPLE, Formatting.ITALIC));
      }
    }
    TooltipUtils.addEmptyLine(list);
    if (player != null && player.isCreative()) {
      NumberFormat nf = NumberFormat.getNumberInstance();
      nf.setMaximumFractionDigits(2);
      list.add(new TranslatableText("tooltip.csm.creative.potentia",
          nf.format(meta.getPotentia())).formatted(Formatting.AQUA,
              Formatting.ITALIC));
      list.add(new TranslatableText("tooltip.csm.creative.anima",
          nf.format(meta.getAnima())).formatted(Formatting.GREEN,
              Formatting.ITALIC));
      list.add(new TranslatableText("tooltip.csm.creative.discordia",
          nf.format(meta.getDiscordia())).formatted(Formatting.RED,
              Formatting.ITALIC));
      list.add(new TranslatableText("tooltip.csm.creative.ambitio",
          nf.format(meta.getAmbitio())).formatted(Formatting.YELLOW,
              Formatting.ITALIC));
      double hue = meta.getHue();
      list.add(new TranslatableText("tooltip.csm.creative.hue",
          ChromaticUtils.forHue(hue), nf.format(hue))
              .formatted(Formatting.ITALIC));
    }
    TooltipUtils.addEmptyLine(list);
    if (desc != null)
      desc.addFlavor(list);
    TooltipUtils.removeLastEmptyLine(list);
  }

  /**
   * Returns the amount of stamina required to attack.
   *
   * @param weapon     the weapon to attack with
   * @param level      the level of the attacker
   * @param successful whether or not the attack hit an entity
   * @return the amount of stamina required
   */
  public static long getStaminaCostForAttacking(ItemStack weapon, int level,
      boolean successful) {
    double unarmedCost = EntityDamageUtils.getUnarmedEnergyCost(level);
    Identifier itemId = Registry.ITEM.getId(weapon.getItem());
    CompatibleItemDescription desc = CompatibleItemManager.INSTANCE.get(itemId);
    if (desc == null)
      return Math.round(unarmedCost);
    EquipmentType type = desc.getType();
    EquipmentComponent component = EquipmentComponent.getFor(weapon);
    if (type instanceof WeaponType && component != null) {
      long cost =
          ((WeaponType) type).netStaminaCost(component.getMeta().getLevel());
      // Reduce cost for unsuccessful armed attacks
      if (!successful)
        cost = 2 * cost / 3;
      return cost;
    }
    return Math.round(unarmedCost);
  }

  /**
   * Returns whether an item can be salvaged in an adventurer's crucible -- that
   * is, if it is C/M-compatible equipment that was not crafted.
   *
   * @param stack An {@link ItemStack} describing the item
   * @return true if it is salvageable
   */
  public static boolean canSalvage(ItemStack stack) {
    EquipmentComponent component = EquipmentComponent.getFor(stack);
    if (component != null && component.getMeta().getOrigin() == Origin.CRAFTED)
      return false;
    CompatibleItemDescription desc = CompatibleItemManager.INSTANCE
        .get(Registry.ITEM.getId(stack.getItem()));
    if (desc != null && component != null)
      return true;
    return (stack.getItem() instanceof Salvageable)
        && ((Salvageable) stack.getItem()).canSalvage(stack);
  }

  /**
   * Tries to salvage a piece of equipment in an adventurer's crucible.
   *
   * @param stack               the {@link ItemStack} that should be salvaged
   * @param avgExpectedProducts the number of products expected when P+A+D is
   *                            zero
   * @param callback            a callback to call on any output items
   * @param r                   a {@link Random} to sample from
   * @return true if the salvage succeeded
   */
  public static boolean salvage(ItemStack stack, double avgExpectedProducts,
      Consumer<ItemStack> callback, Random r) {
    if (!canSalvage(stack)) {
      CrackSlashMineMod.log(Level.WARN,
          "Tried to salvage unsalvageable item " + stack);
      return false;
    }
    if (stack.getItem() instanceof Salvageable) {
      return ((Salvageable) stack.getItem()).salvage(stack, avgExpectedProducts,
          callback, r);
    }
    EquipmentComponent component = EquipmentComponent.getFor(stack);
    if (component == null)
      return false;
    EquipmentComponent.Meta meta = component.getMeta();
    double hue = meta.getHue();
    double potentia = meta.getPotentia();
    double anima = meta.getAnima();
    double discordia = meta.getDiscordia();
    double padTotal = potentia + anima + discordia;
    double expectedProducts = avgExpectedProducts * Math.exp(0.5 * padTotal);
    generateChromas(callback, r, hue, expectedProducts);
    return true;
  }

  public static void generateChromas(Consumer<ItemStack> callback, Random r,
      double hue, double expectedProducts) {
    int[] actualProducts = new int[24];
    if (Double.isNaN(hue)) {
      double expectedColored = 0.05 * expectedProducts;
      double expectedColorless = 0.95 * expectedProducts;
      int nColorless = (int) Math
          .round(MathUtils.nextGamma(r, expectedColorless / 2.0, 2.0));
      callback.accept(new ItemStack(CsmItems.CHROMA_COLORLESS, nColorless));
      int nColored =
          (int) Math.round(MathUtils.nextGamma(r, expectedColored / 2.0, 2.0));
      for (int i = 0; i < nColored; ++i) {
        ++actualProducts[r.nextInt(24)];
      }
    } else {
      int index = (int) Math.round(hue / 15.0) % 24;
      double expectedColored = 0.95 * expectedProducts;
      double expectedColorless = 0.05 * expectedProducts;
      int nColorless = (int) Math
          .round(MathUtils.nextGamma(r, expectedColorless / 2.0, 2.0));
      callback.accept(new ItemStack(CsmItems.CHROMA_COLORLESS, nColorless));
      int nColored =
          (int) Math.round(MathUtils.nextGamma(r, expectedColored / 2.0, 2.0));
      for (int i = 0; i < nColored; ++i) {
        int offset = 0;
        for (int j = 0; j < 6; ++j) {
          offset += r.nextInt(3) - 1;
        }
        offset /= 2;
        ++actualProducts[Math.floorMod(index + offset, 24)];
      }
    }
    for (int i = 0; i < 24; ++i) {
      int count = actualProducts[i];
      if (count != 0)
        callback.accept(new ItemStack(CsmItems.COLORED_CHROMAS.get(i), count));
    }
  }

  public static class VödData {
    private final VödDescription word;
    private final Object2LongMap<Stat> stats;

    public VödData(VödDescription word, Object2LongMap<Stat> stats) {
      if (word == null)
        throw new NullPointerException();
      this.word = word;
      this.stats = stats;
    }

    public VödDescription getWord() {
      return word;
    }

    public Object2LongMap<Stat> getStats() {
      return stats;
    }

    public static @Nullable VödData fromTag(CompoundTag tag,
        VödManager manager) {
      VödDescription desc =
          manager.get(Identifier.tryParse(tag.getString("word")));
      if (desc == null)
        return null;
      return new VödData(desc,
          EquipmentUtils.statsFromTag(tag.getCompound("bonus")));
    }

    public CompoundTag toTag() {
      CompoundTag tag = new CompoundTag();
      tag.putString("word", word.getId().toString());
      CompoundTag bonusTag = new CompoundTag();
      EquipmentUtils.saveStatsToTag(stats, bonusTag);
      tag.put("bonus", bonusTag);
      return tag;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o)
        return true;
      if (o == null || getClass() != o.getClass())
        return false;
      VödData vödData = (VödData) o;
      return word == vödData.word && Objects.equals(stats, vödData.stats);
    }

    @Override
    public int hashCode() {
      return Objects.hash(word, stats);
    }
  }
}
