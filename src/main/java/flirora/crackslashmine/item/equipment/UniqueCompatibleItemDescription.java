package flirora.crackslashmine.item.equipment;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.jetbrains.annotations.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import flirora.crackslashmine.core.DamageType;
import flirora.crackslashmine.core.stat.Stat;
import flirora.crackslashmine.core.stat.modifier.StatModifier;
import flirora.crackslashmine.item.equipment.naming.EquipmentName;
import flirora.crackslashmine.item.equipment.types.EquipmentType;
import flirora.crackslashmine.item.equipment.types.WeaponType;
import it.unimi.dsi.fastutil.objects.Object2LongMap;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;

public class UniqueCompatibleItemDescription extends CompatibleItemDescription {
  private final String nameKey;
  private final List<StatModifier.Entry> entries;
  private final EquipmentParameters parameters;
  private final @Nullable WeaponDamageProperties weaponProperties;

  public UniqueCompatibleItemDescription(EquipmentType type, int minLevel,
      int maxLevel, double baseValue, String nameKey,
      List<StatModifier.Entry> entries, EquipmentParameters parameters,
      @Nullable WeaponDamageProperties weaponProperties) {
    super(type, minLevel, maxLevel, baseValue);
    this.nameKey = nameKey;
    this.entries = entries;
    this.parameters = parameters;
    this.weaponProperties = weaponProperties;
  }

  @Override
  public EquipmentName rollName(Random r) {
    return new EquipmentName(nameKey);
  }

  @Override
  public void generateStatBonuses(Random r, int level, double anima,
      double discordia, Object2LongMap<Stat> result, EquipmentName name) {
    for (StatModifier.Entry e : entries) {
      double amt = e.sampleAmount(r, level, anima, discordia, 0.0);
      result.mergeLong(e.getStat(), Math.round(amt), Long::sum);
    }
  }

  @Override
  public void addFlavor(List<Text> lines) {
    lines.add(new TranslatableText(nameKey + ".flavor")
        .formatted(Formatting.ITALIC, Formatting.RED));
  }

  @Override
  public EquipmentParameters rollEquipmentParameters(Random r, Origin origin) {
    return parameters;
  }

  @Override
  public WeaponDamageProperties rollWeaponDamageProperties(Random r,
      WeaponType type) {
    if (weaponProperties == null) {
      BaseDamageType wd = type.sampleDamageType(r);
      DamageType de = DamageType.randomElemental(r);
      return new WeaponDamageProperties(wd, de);
    }
    return weaponProperties;
  }

  private static final Gson GSON =
      (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();

  private static class JsonFormat {
    public String type;
    public int minLevel, maxLevel;
    public double baseValue;
    public String nameKey;
    public List<JsonObject> entries;
    public JsonObject parameters;
    public JsonObject weaponProperties;
  }

  public static UniqueCompatibleItemDescription fromJson(Identifier id,
      JsonElement json) {
    JsonFormat deserialized = GSON.fromJson(json, JsonFormat.class);
    return new UniqueCompatibleItemDescription(
        EquipmentType.getByName(Identifier.tryParse(deserialized.type)),
        deserialized.minLevel, deserialized.maxLevel, deserialized.baseValue,
        deserialized.nameKey,
        deserialized.entries.stream().map(StatModifier.Entry::fromJson)
            .collect(Collectors.toList()),
        EquipmentParameters.fromJson(deserialized.parameters),
        deserialized.weaponProperties != null
            ? WeaponDamageProperties.fromJson(deserialized.weaponProperties)
            : null);
  }
}
