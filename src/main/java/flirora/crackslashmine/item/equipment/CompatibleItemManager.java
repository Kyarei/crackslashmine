package flirora.crackslashmine.item.equipment;

import java.util.Map;
import java.util.stream.Stream;

import org.apache.logging.log4j.Level;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.item.equipment.types.EquipmentType;
import net.fabricmc.fabric.api.resource.IdentifiableResourceReloadListener;
import net.minecraft.resource.JsonDataLoader;
import net.minecraft.resource.ResourceManager;
import net.minecraft.util.Identifier;
import net.minecraft.util.profiler.Profiler;

public class CompatibleItemManager extends JsonDataLoader
    implements IdentifiableResourceReloadListener {
  private static final Gson GSON =
      (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();
  private static final Identifier ID =
      new Identifier(CrackSlashMineMod.MOD_ID, "compatible_items");

  public static final CompatibleItemManager INSTANCE =
      new CompatibleItemManager();

  private ImmutableMap<Identifier, CompatibleItemDescription> descriptionsById =
      ImmutableMap.of();
  private ImmutableMap<Identifier, RegularCompatibleItemDescription> regularDescriptionsById =
      ImmutableMap.of();
  private ImmutableMap<Identifier, UniqueCompatibleItemDescription> uniqueDescriptionsById =
      ImmutableMap.of();

  private CompatibleItemManager() {
    super(GSON, "csm/compatible_items");
  }

  @Override
  public Identifier getFabricId() {
    return ID;
  }

  @Override
  protected void apply(Map<Identifier, JsonElement> loader,
      ResourceManager resourceManager, Profiler profiler) {
    int count = 0;
    ImmutableMap.Builder<Identifier, CompatibleItemDescription> builder =
        new ImmutableMap.Builder<>();
    ImmutableMap.Builder<Identifier, RegularCompatibleItemDescription> rbuilder =
        new ImmutableMap.Builder<>();
    ImmutableMap.Builder<Identifier, UniqueCompatibleItemDescription> ubuilder =
        new ImmutableMap.Builder<>();
    for (Map.Entry<Identifier, JsonElement> entry : loader.entrySet()) {
      Identifier id = entry.getKey();
      JsonElement json = entry.getValue();
      try {
        CompatibleItemDescription description =
            CompatibleItemDescription.fromJson(id, json);
        builder.put(id, description);
        if (description instanceof RegularCompatibleItemDescription)
          rbuilder.put(id, (RegularCompatibleItemDescription) description);
        else if (description instanceof UniqueCompatibleItemDescription)
          ubuilder.put(id, (UniqueCompatibleItemDescription) description);
      } catch (Exception e) {
        CrackSlashMineMod.logException(
            "Failed to load compatible item description " + id, e);
      }
      ++count;
    }
    descriptionsById = builder.build();
    regularDescriptionsById = rbuilder.build();
    uniqueDescriptionsById = ubuilder.build();
    CrackSlashMineMod.log(Level.INFO,
        "Loaded " + count + " compatible item descriptions");
  }

  public CompatibleItemDescription get(Identifier id) {
    return descriptionsById.get(id);
  }

  // TODO: make this faster if it's too slow

  public Stream<ImmutableMap.Entry<Identifier, RegularCompatibleItemDescription>> regularAtLevel(
      int level) {
    return regularDescriptionsById.entrySet().stream().filter((e) -> {
      RegularCompatibleItemDescription desc = e.getValue();
      return desc.isNatural() && level >= desc.getMinLevel()
          && level <= desc.getMaxLevel();
    });
  }

  public Stream<ImmutableMap.Entry<Identifier, RegularCompatibleItemDescription>> regularAtLevel(
      int level, EquipmentType type) {
    return regularDescriptionsById.entrySet().stream().filter((e) -> {
      RegularCompatibleItemDescription desc = e.getValue();
      return desc.isNatural() && level >= desc.getMinLevel()
          && level <= desc.getMaxLevel() && desc.getType() == type;
    });
  }

  public Stream<ImmutableMap.Entry<Identifier, UniqueCompatibleItemDescription>> uniqueAtLevel(
      int level) {
    return uniqueDescriptionsById.entrySet().stream().filter((e) -> {
      UniqueCompatibleItemDescription desc = e.getValue();
      return level >= desc.getMinLevel() && level <= desc.getMaxLevel();
    });
  }

  public Stream<ImmutableMap.Entry<Identifier, UniqueCompatibleItemDescription>> uniqueAtLevel(
      int level, EquipmentType type) {
    return uniqueDescriptionsById.entrySet().stream().filter((e) -> {
      UniqueCompatibleItemDescription desc = e.getValue();
      return level >= desc.getMinLevel() && level <= desc.getMaxLevel()
          && desc.getType() == type;
    });
  }
}
