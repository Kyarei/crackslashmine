package flirora.crackslashmine.item.baggim;

import java.util.List;
import java.util.Random;
import java.util.function.Consumer;

import flirora.crackslashmine.network.RubbedItemS2C;
import it.unimi.dsi.fastutil.objects.Object2DoubleMap;
import it.unimi.dsi.fastutil.objects.Object2DoubleMaps;
import it.unimi.dsi.fastutil.objects.Object2DoubleOpenHashMap;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.Hand;
import net.minecraft.util.ItemScatterer;
import net.minecraft.util.Rarity;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;

public abstract class LootBagItem extends Item {
  private final List<LootParameter> lootParameters;

  public LootBagItem(Settings settings, List<LootParameter> lootParameters) {
    super(settings.rarity(Rarity.RARE));
    this.lootParameters = lootParameters;
  }

  @Override
  public TypedActionResult<ItemStack> use(World world, PlayerEntity user,
      Hand hand) {
    ItemStack itemStack = user.getStackInHand(hand);
    if (world.isClient())
      return TypedActionResult.pass(itemStack);
    world.playSound(user.getX(), user.getY(), user.getZ(),
        SoundEvents.ENTITY_FIREWORK_ROCKET_BLAST, SoundCategory.AMBIENT, 1.0f,
        1.0f, false);
    // TODO: more customizable particle effects?
    RubbedItemS2C.create(world, user.getBlockPos(),
        360 * world.getRandom().nextDouble());
    if (!user.abilities.creativeMode) {
      itemStack.decrement(1);
    }
    int level = itemStack.getOrCreateTag().getInt("level");
    Object2DoubleMap<LootParameter> parameters = getLootParameters(itemStack);
    open(level, parameters, itemStack, user, world.getRandom(),
        output -> ItemScatterer.spawn(world, user.getX(), user.getEyeY(),
            user.getZ(), output));
    return TypedActionResult.consume(itemStack);
  }

  private static final Object2DoubleMap<LootParameter> EMPTY =
      Object2DoubleMaps.emptyMap();

  private Object2DoubleMap<LootParameter> getLootParameters(ItemStack stack) {
    CompoundTag subTag = stack.getSubTag("params");
    if (subTag == null)
      return EMPTY;
    Object2DoubleMap<LootParameter> parameters =
        new Object2DoubleOpenHashMap<>();
    for (LootParameter p : lootParameters) {
      parameters.put(p, subTag.getDouble(p.getName()));
    }
    return parameters;
  }

  protected abstract void open(int level,
      Object2DoubleMap<LootParameter> sampled, ItemStack stack,
      PlayerEntity opener, Random r, Consumer<ItemStack> dropCallback);

  public ItemStack create(Random r, int level) {
    ItemStack result = new ItemStack(this);
    CompoundTag tag = result.getOrCreateTag();
    tag.putInt("level", level);
    CompoundTag subTag = result.getOrCreateSubTag("params");
    for (LootParameter p : lootParameters) {
      subTag.putDouble(p.getName(), p.sample(r));
    }
    return result;
  }
}
