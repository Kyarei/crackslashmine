package flirora.crackslashmine.item.baggim;

import flirora.crackslashmine.core.distribution.NormalDistribution;

public class LootParameters {
  public static final LootParameter COUNT =
      new LootParameter("count", new NormalDistribution(3.5, 0.7));
}
