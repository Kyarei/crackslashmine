package flirora.crackslashmine.item.baggim;

import java.util.List;
import java.util.Random;
import java.util.function.Consumer;

import com.google.common.collect.ImmutableList;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.item.CsmItems;
import flirora.crackslashmine.item.essence.tonat.TonatItem;
import it.unimi.dsi.fastutil.objects.Object2DoubleMap;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;

public class TonatLootBagItem extends LootBagItem {
  public static final Identifier ID =
      new Identifier(CrackSlashMineMod.MOD_ID, "tonat_loot_bag");
  private static final List<LootParameter> PARAMS =
      ImmutableList.of(LootParameters.COUNT);

  public TonatLootBagItem() {
    super(new Settings().maxCount(1), PARAMS);
  }

  @Override
  protected void open(int level, Object2DoubleMap<LootParameter> sampled,
      ItemStack stack, PlayerEntity opener, Random r,
      Consumer<ItemStack> dropCallback) {
    int count = (int) sampled.getDouble(LootParameters.COUNT);
    for (int i = 0; i < count; ++i) {
      int tonatIndex = r.nextInt(26);
      TonatItem tonat = CsmItems.ASCII_TONATS.get(tonatIndex);
      dropCallback.accept(tonat.createWithRandomStats(r, level));
    }
  }

}
