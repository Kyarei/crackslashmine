package flirora.crackslashmine.item.baggim;

import java.util.Random;

import flirora.crackslashmine.core.distribution.ContinuousDistribution;

public class LootParameter {
  private final String name;
  private final ContinuousDistribution distribution;

  public LootParameter(String name, ContinuousDistribution distribution) {
    super();
    this.name = name;
    this.distribution = distribution;
  }

  public String getName() {
    return name;
  }

  public ContinuousDistribution getDistribution() {
    return distribution;
  }

  public double sample(Random r) {
    return distribution.sample(r);
  }
}
