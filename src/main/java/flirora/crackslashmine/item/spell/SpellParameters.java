package flirora.crackslashmine.item.spell;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.core.stat.Stats;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;

import java.util.Objects;
import java.util.Random;

/**
 * Parameters associated with a spell item.
 * <p>
 * Spell items have some parameters that affect how a spell is cast:
 *
 * <ul>
 *     <li>Acritas increases the power of a spell (e.g. radius, damage)</li>
 *     <li>Lumen decreases the mana cost of a spell</li>
 *     <li>Discordia increases power and decreases mana, but also increases
 *       the chance of backfiring</li>
 * </ul>
 */
public class SpellParameters {
    private final int level;
    private final double acritas;
    private final double lumen;
    private final double discordia;

    public SpellParameters(int level, double acritas, double lumen, double discordia) {
        this.level = level;
        this.acritas = acritas;
        this.lumen = lumen;
        this.discordia = discordia;
    }

    public SpellParameters(int level, Random r) {
        this(level, r.nextGaussian(), r.nextGaussian(), r.nextGaussian());
    }

    public int getLevel() {
        return level;
    }

    public double getAcritas() {
        return acritas;
    }

    public double getLumen() {
        return lumen;
    }

    public double getDiscordia() {
        return discordia;
    }

    private double getMedianDamage(double factor) {
        double baseDamage = CrackSlashMineMod.config.weaponDamageScaling.value(level) * factor;
        return baseDamage * Math.pow(1.2, acritas + 0.25 * discordia);
    }

    private double getSpread() {
        return 0.1 + 0.01 * discordia;
    }

    public long[] getDamageRangeGivenMultiplier(double factor) {
        double median = getMedianDamage(factor);
        double spread = getSpread();
        return new long[]{
                Math.round(median * (1 - spread)),
                Math.round(median * (1 + spread))
        };
    }

    public long getDamageGivenMultiplier(double factor, Random r) {
        double median = getMedianDamage(factor);
        double spread = getSpread();
        return Math.round(median * (1 + spread * (2 * r.nextDouble() - 1)));
    }

    public Text getDamageRangeTextGivenMultiplier(double factor) {
        double median = getMedianDamage(factor);
        double spread = getSpread();
        return new TranslatableText("tooltip.csm.range",
                Math.round(median * (1 - spread)),
                Math.round(median * (1 + spread)));
    }

    public double getManaScaling() {
        return CrackSlashMineMod.config.weaponStaminaScaling.value(level) *
                Math.pow(5.0 / 6, lumen + 0.25 * discordia);
    }

    public double backfireChance(LivingEntityStats casterStats) {
        if (casterStats.getLevel() < this.getLevel()) return 1.0;
        long advantage =
                casterStats.getStat(Stats.INTELLECT)
                        + casterStats.getStat(Stats.WISDOM)
                        - 2 * casterStats.getLevel();
        double relativeAdvantage = ((double) advantage) / (2 * casterStats.getLevel());
        return Math.pow(3.0, -relativeAdvantage + 0.5 * discordia) * 0.05;
    }

    public static SpellParameters fromTag(CompoundTag tag) {
        return new SpellParameters(
                tag.getInt("level"),
                tag.getDouble("acritas"),
                tag.getDouble("lumen"),
                tag.getDouble("discordia")
        );
    }

    public CompoundTag toTag(CompoundTag tag) {
        tag.putInt("level", level);
        tag.putDouble("acritas", acritas);
        tag.putDouble("lumen", lumen);
        tag.putDouble("discordia", discordia);
        return tag;
    }

    @Override
    public String toString() {
        return "SpellParameters{" +
                "level=" + level +
                ", acritas=" + acritas +
                ", lumen=" + lumen +
                ", discordia=" + discordia +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SpellParameters that = (SpellParameters) o;
        return level == that.level &&
                Double.compare(that.acritas, acritas) == 0 &&
                Double.compare(that.lumen, lumen) == 0 &&
                Double.compare(that.discordia, discordia) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(level, acritas, lumen, discordia);
    }
}
