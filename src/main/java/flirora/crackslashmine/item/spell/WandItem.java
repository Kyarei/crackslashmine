package flirora.crackslashmine.item.spell;

import org.jetbrains.annotations.Nullable;

import flirora.crackslashmine.block.ImplementedInventory;
import flirora.crackslashmine.core.Beautify;
import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.item.traits.Configurable;
import flirora.crackslashmine.item.traits.ItemHudRenderer;
import flirora.crackslashmine.item.traits.UserKeyListener;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.util.Window;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ToolItem;
import net.minecraft.item.ToolMaterial;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.KeybindText;
import net.minecraft.text.LiteralText;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Hand;
import net.minecraft.util.Identifier;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;

public class WandItem extends ToolItem
    implements Configurable, UserKeyListener {
  public static final int MAX_SPELLS = 4;
  public static final ItemHudRenderer HUD_RENDERER = new HudRenderer();

  public WandItem(ToolMaterial material) {
    super(material, new Item.Settings().maxCount(1));
  }

  @Override
  public TypedActionResult<ItemStack> use(World world, PlayerEntity user,
      Hand hand) {
    ItemStack stack = user.getStackInHand(hand);
    if (world.isClient())
      return TypedActionResult.pass(stack);
    if (stack.getDamage() >= stack.getMaxDamage() - 1)
      return TypedActionResult.fail(stack);

    WandComponent component = WandComponent.getFor(stack);
    int[] cooldowns = component.getCooldowns();
    int slot = component.getActive();
    if (cooldowns[slot] > 0) {
      return TypedActionResult.fail(stack);
    }

    ItemStack spellItem = component.getStack(slot);

    if (!(spellItem.getItem() instanceof SpellItem))
      return TypedActionResult.fail(stack);
    SpellComponent spellComponent = SpellComponent.getFor(spellItem);

    long cost = spellComponent.getManaCost(user);
    LivingEntityStats stats = LivingEntityStats.getFor(user);
    if (stats.getMana() < cost) {
      world.playSound(null, user.getX(), user.getY(), user.getZ(),
          SoundEvents.BLOCK_FIRE_EXTINGUISH, SoundCategory.BLOCKS, 1.0f, 1.0f);
      return TypedActionResult.fail(stack);
    }

    Spell spell = spellComponent.getSpell();
    SpellParameters parameters = spellComponent.getParameters();
    if (spell == null || parameters == null
        || !(world instanceof ServerWorld)) {
      return TypedActionResult.pass(stack);
    }

    spell.tryCast(user, parameters, world.getRandom(), (ServerWorld) world);
    if (!user.isCreative()) {
      stats.setMana(stats.getMana() - cost);
      stack.damage(1, user, p -> p.sendToolBreakStatus(hand));
    }

    cooldowns[slot] = spell.getCooldown();
    return TypedActionResult.success(stack);
  }

  @Override
  public void inventoryTick(ItemStack stack, World world, Entity entity,
      int slot, boolean selected) {
    super.inventoryTick(stack, world, entity, slot, selected);
    if (entity instanceof PlayerEntity)
      WandComponent.getFor(stack).updateCooldowns();
  }

  @Override
  public void onKey(ItemStack stack, ServerPlayerEntity player,
      ServerWorld world, int num) {
    WandComponent.getFor(stack).setActive(num);
  }

  private class MyNamedScreenHandlerFactory
      implements NamedScreenHandlerFactory {
    private final ItemStack stack;
    private final ImplementedInventory inventory;
    private final int slot;

    public MyNamedScreenHandlerFactory(ItemStack stack,
        ImplementedInventory inventory, int slot) {
      this.stack = stack;
      this.inventory = inventory;
      this.slot = slot;
    }

    @Override
    public Text getDisplayName() {
      return WandItem.this.getName(stack);
    }

    @Override
    public @Nullable ScreenHandler createMenu(int syncId, PlayerInventory inv,
        PlayerEntity player) {
      return new WandGui(syncId, inv, inventory, stack, slot);
    }
  }

  @Override
  public void configure(ItemStack stack, PlayerEntity player, ServerWorld world,
      int slot) {
    WandComponent component = WandComponent.getFor(stack);
    player.openHandledScreen(
        new MyNamedScreenHandlerFactory(stack, component, slot));
  }

  @Environment(EnvType.CLIENT)
  public static class HudRenderer implements ItemHudRenderer {
    private static final Text EMPTY =
        new TranslatableText("csm.hud.spell.empty").formatted(Formatting.GRAY);

    private static Text getTextFor(ImplementedInventory spells, int i,
        boolean selected, int cooldown) {
      ItemStack spell = spells.getStack(i);
      Text spellName = EMPTY;
      if (spell.getItem() instanceof SpellItem) {
        SpellComponent component = SpellComponent.getFor(spell);
        spellName = component.getSpellName();
      }
      MutableText result = new TranslatableText("csm.hud.spell.keybindAndSpell",
          new KeybindText("key.crackslashmine.user." + i), spellName);
      if (cooldown > 0) {
        result = new TranslatableText("gui.csm.xse.display", result,
            new LiteralText(Beautify.beautifyTimeS(cooldown))
                .formatted(Formatting.GRAY));
      }
      if (selected)
        result = result.formatted(Formatting.GREEN);
      return result;
    }

    @Override
    public void onHudRender(ItemStack stack, ClientPlayerEntity player,
        MatrixStack matrixStack, float delta) {
      MinecraftClient client = MinecraftClient.getInstance();
      TextRenderer textRenderer = client.textRenderer;
      Window window = client.getWindow();
      int height = window.getScaledHeight();
      int bottomEdge = height - 50;
      int spriteLeft = 10;
      int textLeft = spriteLeft + 34;
      int topEdge = bottomEdge - MAX_SPELLS * 12;
      WandComponent component = WandComponent.getFor(stack);
      int selected = component.getActive();
      int[] cooldowns = component.getCooldowns();
      for (int i = 0; i < MAX_SPELLS; ++i) {
        Text text = getTextFor(component, i, i == selected, cooldowns[i]);
        textRenderer.drawWithShadow(matrixStack, text, textLeft,
            topEdge + 12 * i, -1);
      }
      ItemStack spell = component.getStack(selected);
      if (spell.getItem() instanceof SpellItem) {
        Identifier selectedSpellId = SpellComponent.getFor(spell).getSpellId();
        Identifier spriteId = new Identifier(selectedSpellId.getNamespace(),
            "textures/csm/spell/" + selectedSpellId.getPath() + ".png");
        client.getTextureManager().bindTexture(spriteId);
        DrawableHelper.drawTexture(matrixStack, spriteLeft, bottomEdge - 36, 32,
            32, 0, 0, 32, 32, 32, 32);
      }
    }
  }
}
