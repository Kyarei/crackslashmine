package flirora.crackslashmine.item.spell;

import java.util.Objects;

import org.jetbrains.annotations.Nullable;

import flirora.crackslashmine.core.CrackSlashMineComponents;
import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.core.stat.Stats;
import flirora.crackslashmine.item.spell.data.SpellDataManager;
import nerdhub.cardinal.components.api.component.Component;
import nerdhub.cardinal.components.api.util.ItemComponent;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.text.Text;
import net.minecraft.text.TextColor;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;

public class SpellComponent implements ItemComponent<SpellComponent> {
  private Spell spell;
  private SpellParameters parameters;

  public Spell getSpell() {
    return spell;
  }

  public void setSpell(Spell spell) {
    this.spell = spell;
  }

  public SpellParameters getParameters() {
    return parameters;
  }

  public void setParameters(SpellParameters parameters) {
    this.parameters = parameters;
  }

  public Identifier getSpellId() {
    return spell.getId();
  }

  public long getManaCost() {
    return spell.getManaCost(parameters);
  }

  public long getManaCost(@Nullable LivingEntity caster) {
    if (spell == null || parameters == null)
      return Long.MAX_VALUE;
    long cost = getManaCost();
    if (caster == null)
      return cost;
    long manaCostMulti = LivingEntityStats.getFor(caster)
        .getStat(Stats.MANA_COST_MULTI.get(spell.getCastType()));
    return cost * (10000 + manaCostMulti) / 10000;
  }

  public Text getSpellName() {
    Identifier id = getSpellId();
    if (id == null)
      return null;
    return new TranslatableText(
        "csm.spell." + id.getNamespace() + "." + id.getPath())
            .styled(st -> st
                .withColor(TextColor.fromRgb(spell.getCastType().getColor())));
  }

  @Override
  public void fromTag(CompoundTag tag) {
    spell = SpellDataManager.SERVER
        .getSpell(Identifier.tryParse(tag.getString("spell")));
    if (spell == null)
      spell = SpellDataManager.SERVER.getDefault();
    parameters = SpellParameters.fromTag(tag.getCompound("parameters"));
  }

  @Override
  public CompoundTag toTag(CompoundTag tag) {
    tag.putString("spell", Objects.requireNonNull(spell.getId()).toString());
    tag.put("parameters", parameters.toTag(new CompoundTag()));
    return tag;
  }

  @Override
  public boolean isComponentEqual(Component other) {
    return this.equals(other);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    SpellComponent that = (SpellComponent) o;
    return spell == that.spell && Objects.equals(parameters, that.parameters);
  }

  @Override
  public int hashCode() {
    return Objects.hash(spell, parameters);
  }

  public static SpellComponent getFor(ItemStack stack) {
    return CrackSlashMineComponents.SPELL_COMPONENT.get(stack);
  }
}
