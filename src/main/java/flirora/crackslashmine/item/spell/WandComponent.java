package flirora.crackslashmine.item.spell;

import java.util.Arrays;
import java.util.Objects;

import flirora.crackslashmine.block.ImplementedInventory;
import flirora.crackslashmine.core.CrackSlashMineComponents;
import nerdhub.cardinal.components.api.component.Component;
import nerdhub.cardinal.components.api.util.ItemComponent;
import net.minecraft.inventory.Inventories;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.collection.DefaultedList;

public class WandComponent
    implements ItemComponent<WandComponent>, ImplementedInventory {
  private DefaultedList<ItemStack> items =
      DefaultedList.ofSize(4, ItemStack.EMPTY);
  private int[] cooldowns = new int[4];
  private int active = 0;

  public WandComponent() {
    // TODO Auto-generated constructor stub
  }

  @Override
  public DefaultedList<ItemStack> getItems() {
    return items;
  }

  public int getActive() {
    return active;
  }

  public void setActive(int active) {
    this.active = active;
  }

  public int cooldown(int index) {
    return cooldowns[index];
  }

  public int[] getCooldowns() {
    return cooldowns;
  }

  public void updateCooldowns() {
    for (int i = 0; i < cooldowns.length; ++i) {
      if (cooldowns[i] > 0)
        --cooldowns[i];
    }
  }

  @Override
  public void fromTag(CompoundTag tag) {
    int[] dcs = tag.getIntArray("cooldowns");
    cooldowns = Arrays.copyOf(dcs, 4);
    active = tag.getInt("active");
    Inventories.fromTag(tag, items);
  }

  @Override
  public CompoundTag toTag(CompoundTag tag) {
    tag.putIntArray("cooldowns", cooldowns);
    tag.putInt("active", active);
    Inventories.toTag(tag, items);
    return tag;
  }

  @Override
  public boolean isComponentEqual(Component other) {
    return this.equals(other);
  }

  @Override
  public int hashCode() {
    int res = 0;
    for (int i = 0; i < items.size(); ++i) {
      res ^= items.get(i).hashCode();
      res *= 31;
    }
    return (res * 89) ^ Objects.hash(active, cooldowns);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    WandComponent other = (WandComponent) obj;
    // Dear Mojang,
    // Please define a proper equals() method for DefaultedList or
    // N******List or whatever crap you call it.
    if (items.size() != other.items.size())
      return false;
    for (int i = 0; i < items.size(); ++i) {
      if (!ItemStack.areEqual(items.get(i), other.items.get(i)))
        return false;
    }
    return active == other.active && Arrays.equals(cooldowns, other.cooldowns);
  }

  @Override
  public boolean isValid(int slot, ItemStack stack) {
    return stack.getItem() instanceof SpellItem;
  }

  public static WandComponent getFor(ItemStack stack) {
    return CrackSlashMineComponents.WAND_COMPONENT.get(stack);
  }
}
