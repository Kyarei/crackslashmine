package flirora.crackslashmine.item.spell;

import io.github.cottonmc.cotton.gui.client.CottonInventoryScreen;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.text.Text;

public class WandScreen extends CottonInventoryScreen<WandGui> {
    public WandScreen(WandGui description, PlayerEntity player, Text title) {
        super(description, player, title);
    }
}
