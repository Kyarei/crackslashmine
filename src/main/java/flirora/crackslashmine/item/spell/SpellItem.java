package flirora.crackslashmine.item.spell;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.jetbrains.annotations.Nullable;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.Beautify;
import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.item.CsmItems;
import flirora.crackslashmine.item.equipment.EquipmentUtils;
import flirora.crackslashmine.item.spell.data.SpellDataManager;
import flirora.crackslashmine.item.traits.Salvageable;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import net.minecraft.world.World;

public class SpellItem extends Item implements Salvageable {
  public static final Identifier ID =
      new Identifier(CrackSlashMineMod.MOD_ID, "spell_rune");

  public SpellItem() {
    super(new Item.Settings().maxCount(1));
  }

  @Override
  public void appendTooltip(ItemStack stack, @Nullable World world,
      List<Text> tooltip, TooltipContext context) {
    super.appendTooltip(stack, world, tooltip, context);
    boolean detailed = Screen.hasShiftDown();
    SpellComponent component = SpellComponent.getFor(stack);
    if (component.getSpell() == null) {
      tooltip.add(new TranslatableText("tooltip.csm.spell.empty")
          .formatted(Formatting.RED));
      tooltip.add(new TranslatableText("tooltip.csm.spell.empty.2")
          .formatted(Formatting.RED));
      return;
    }
    Identifier id = component.getSpellId();
    tooltip.add(new TranslatableText(
        "csm.spell." + id.getNamespace() + "." + id.getPath() + ".flavor")
            .formatted(Formatting.GOLD, Formatting.ITALIC));
    tooltip.add(new TranslatableText("tooltip.csm.spell.level",
        Beautify.beautify(component.getParameters().getLevel())));
    tooltip.add(new TranslatableText("tooltip.csm.spell.cost",
        Beautify.beautify(component.getManaCost())));
    MinecraftClient client = MinecraftClient.getInstance();
    if (client != null && client.player != null) {
      LivingEntityStats stats = LivingEntityStats.getFor(client.player);
      tooltip.add(new TranslatableText("tooltip.csm.spell.successRate",
          Beautify.beautifyPercentage(
              1 - component.getParameters().backfireChance(stats))));
    }
    tooltip.addAll(
        component.getSpell().describe(component.getParameters(), detailed));
  }

  @Override
  public Text getName(ItemStack stack) {
    SpellComponent component = SpellComponent.getFor(stack);
    if (component.getSpell() == null) {
      return getName();
    }
    return new TranslatableText("item.crackslashmine.spell_rune.of",
        component.getSpellName());
  }

  public static ItemStack createStack(Spell spell, int level, Random r) {
    ItemStack res = new ItemStack(CsmItems.SPELL_RUNE);
    SpellComponent component = SpellComponent.getFor(res);
    component.setSpell(spell);
    SpellParameters parameters = new SpellParameters(level, r);
    component.setParameters(parameters);
    return res;
  }

  public static ItemStack createStack(int level, Random r) {
    ArrayList<Spell> eligible = SpellDataManager.SERVER.getAllSpells()
        .filter(s -> s.canGenerateForLevel(level))
        .collect(Collectors.toCollection(ArrayList::new));
    Spell s = eligible.get(r.nextInt(eligible.size()));
    return createStack(s, level, r);
  }

  @Override
  public boolean canSalvage(ItemStack stack) {
    return true;
  }

  @Override
  public boolean salvage(ItemStack stack, double avgExpectedProducts,
      Consumer<ItemStack> callback, Random r) {
    SpellComponent component = SpellComponent.getFor(stack);
    Spell spell = component.getSpell();
    SpellParameters parameters = component.getParameters();
    if (spell == null || parameters == null)
      return false;
    double hue = spell.getCastType().getHue();
    double aldTotal = parameters.getAcritas() + parameters.getLumen()
        + parameters.getDiscordia();
    double expectedProducts = avgExpectedProducts * Math.exp(0.5 * aldTotal);
    EquipmentUtils.generateChromas(callback, r, hue, expectedProducts);
    return false;
  }
}
