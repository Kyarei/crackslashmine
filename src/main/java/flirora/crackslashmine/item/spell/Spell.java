package flirora.crackslashmine.item.spell;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.jetbrains.annotations.NotNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import flirora.crackslashmine.core.DamageType;
import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.item.spell.data.IndentedTooltipBuilder;
import flirora.crackslashmine.item.spell.data.action.SpellAction;
import flirora.crackslashmine.item.spell.data.target.SpellTargetContext;
import net.minecraft.entity.LivingEntity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

public class Spell {
  private final Identifier id;
  private final DamageType castType;
  private final double manaCostMultiplier;
  private final int minLevel, maxLevel;
  private final int cooldown;
  private final List<SpellAction> successActions;
  private final List<SpellAction> backfireActions;

  public Spell(Identifier id, DamageType castType, double manaCostMultiplier,
      int minLevel, int maxLevel, int cooldown,
      List<SpellAction> successActions, List<SpellAction> backfireActions) {
    this.id = id;
    this.castType = castType;
    this.manaCostMultiplier = manaCostMultiplier;
    this.minLevel = minLevel;
    this.maxLevel = maxLevel;
    this.cooldown = cooldown;
    this.successActions = successActions;
    this.backfireActions = backfireActions;
  }

  public DamageType getCastType() {
    return castType;
  }

  public double getManaCostMultiplier() {
    return manaCostMultiplier;
  }

  public int getMinLevel() {
    return minLevel;
  }

  public int getMaxLevel() {
    return maxLevel;
  }

  public int getCooldown() {
    return cooldown;
  }

  public Identifier getId() {
    return id;
  }

  public boolean canGenerateForLevel(int level) {
    return level >= minLevel && level <= maxLevel;
  }

  public void onCastSuccess(@NotNull LivingEntity caster,
      @NotNull SpellParameters parameters, Random r, @NotNull ServerWorld w) {
    SpellTargetContext context = new SpellTargetContext(caster);
    for (SpellAction action : successActions) {
      action.perform(context, w, parameters);
    }
  }

  public void onBackfire(@NotNull LivingEntity caster,
      @NotNull SpellParameters parameters, Random r, @NotNull ServerWorld w) {
    SpellTargetContext context = new SpellTargetContext(caster);
    for (SpellAction action : backfireActions) {
      action.perform(context, w, parameters);
    }
  }

  public List<Text> describe(SpellParameters parameters, boolean detailed) {
    IndentedTooltipBuilder tooltip = new IndentedTooltipBuilder();
    for (SpellAction s : successActions) {
      s.describe(detailed, parameters, tooltip);
    }
    return tooltip.getTooltip();
  }

  public long getManaCost(SpellParameters parameters) {
    return Math.round(getManaCostMultiplier() * parameters.getManaScaling());
  }

  public void tryCast(@NotNull LivingEntity caster,
      @NotNull SpellParameters parameters, Random r, @NotNull ServerWorld w) {
    LivingEntityStats casterStats = LivingEntityStats.getFor(caster);
    double backfireChance = parameters.backfireChance(casterStats);
    if (r.nextDouble() >= backfireChance) {
      onCastSuccess(caster, parameters, r, w);
    } else {
      onBackfire(caster, parameters, r, w);
    }
  }

  private static class JsonFormat {
    public String castType;
    public double manaCostMultiplier;
    public int minLevel, maxLevel;
    public int cooldown;
    public List<JsonObject> successActions;
    public List<JsonObject> backfireActions;
  }

  private static final Gson GSON =
      (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();

  public static Spell deserialize(Identifier id, JsonObject json) {
    JsonFormat deserialized = GSON.fromJson(json, JsonFormat.class);
    return new Spell(id, DamageType.BY_NAME.get(deserialized.castType),
        deserialized.manaCostMultiplier, deserialized.minLevel,
        deserialized.maxLevel, deserialized.cooldown,
        deserialized.successActions.stream().map(SpellAction::deserialize)
            .collect(Collectors.toList()),
        deserialized.backfireActions.stream().map(SpellAction::deserialize)
            .collect(Collectors.toList()));
  }

  public static Spell deserialize(Identifier id, PacketByteBuf data) {
    DamageType castType = DamageType.values()[data.readByte()];
    double manaCostMultiplier = data.readDouble();
    int minLevel = data.readInt();
    int maxLevel = data.readInt();
    int cooldown = data.readInt();
    int nsa = data.readVarInt();
    List<SpellAction> successActions = new ArrayList<>();
    for (int i = 0; i < nsa; ++i) {
      successActions.add(SpellAction.deserialize(data));
    }
    int nba = data.readVarInt();
    List<SpellAction> backfireActions = new ArrayList<>();
    for (int i = 0; i < nba; ++i) {
      backfireActions.add(SpellAction.deserialize(data));
    }
    return new Spell(id, castType, manaCostMultiplier, minLevel, maxLevel,
        cooldown, successActions, backfireActions);
  }

  public void serialize(PacketByteBuf data) {
    data.writeByte(castType.ordinal());
    data.writeDouble(manaCostMultiplier);
    data.writeInt(minLevel);
    data.writeInt(maxLevel);
    data.writeInt(cooldown);
    data.writeVarInt(successActions.size());
    int i = 0;
    for (SpellAction a : successActions) {
      a.serialize(data);
    }
    data.writeVarInt(backfireActions.size());
    for (SpellAction a : backfireActions) {
      a.serialize(data);
    }
  }
}
