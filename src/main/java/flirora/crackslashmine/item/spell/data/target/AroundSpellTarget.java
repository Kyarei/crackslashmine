package flirora.crackslashmine.item.spell.data.target;

import java.util.stream.Stream;

import org.jetbrains.annotations.NotNull;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import flirora.crackslashmine.entity.EntityUtils;
import flirora.crackslashmine.item.spell.data.DataSerializer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;

public class AroundSpellTarget implements SpellTarget {
  private final double yRange;
  private final double distance, maxAngleDiff;
  private final boolean aroundCaster;

  public AroundSpellTarget(double yRange, double distance, double maxAngleDiff,
      boolean aroundCaster) {
    super();
    this.yRange = yRange;
    this.distance = distance;
    this.maxAngleDiff = maxAngleDiff;
    this.aroundCaster = aroundCaster;
  }

  @Override
  public @NotNull Stream<LivingEntity> getTargets(SpellTargetContext ctx) {
    Entity which = aroundCaster ? ctx.getCaster() : ctx.getTarget();
    return EntityUtils
        .getAround(which, distance, yRange, distance,
            e -> EntityUtils.isInRange(which, e, distance, maxAngleDiff))
        .stream();
  }

  @Override
  public @NotNull Text describe(boolean detailed) {
    // TODO Auto-generated method stub
    return detailed
        ? new TranslatableText("tooltip.csm.spell.target.around.more", distance,
            maxAngleDiff, yRange)
        : new TranslatableText("tooltip.csm.spell.target.around");
  }

  public static class Serializer extends DataSerializer<AroundSpellTarget> {
    public Serializer() {
      super(CODEC);
    }

    @Override
    public AroundSpellTarget deserialize(PacketByteBuf data) {
      double yRange = data.readDouble();
      double distance = data.readDouble();
      double maxAngleDiff = data.readDouble();
      boolean aroundCaster = data.readBoolean();
      return new AroundSpellTarget(yRange, distance, maxAngleDiff,
          aroundCaster);
    }

    @Override
    public void serialize(PacketByteBuf data, AroundSpellTarget object) {
      data.writeDouble(object.yRange);
      data.writeDouble(object.distance);
      data.writeDouble(object.maxAngleDiff);
      data.writeBoolean(object.aroundCaster);
    }
  }

  public static final Codec<AroundSpellTarget> CODEC =
      RecordCodecBuilder.create(inst -> {
        return inst
            .group(Codec.DOUBLE.fieldOf("yRange").forGetter(a -> a.yRange),
                Codec.DOUBLE.fieldOf("distance").forGetter(a -> a.distance),
                Codec.DOUBLE.fieldOf("maxAngleDiff")
                    .forGetter(a -> a.maxAngleDiff),
                Codec.BOOL.optionalFieldOf("aroundCaster", false)
                    .forGetter(a -> a.aroundCaster))
            .apply(inst, AroundSpellTarget::new);
      });
  public static final Serializer SERIALIZER = new Serializer();

  @Override
  public DataSerializer<AroundSpellTarget> getSerializer() {
    return SERIALIZER;
  }

}
