package flirora.crackslashmine.item.spell.data.factory;

import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.Codec;
import com.mojang.serialization.Dynamic;

import it.unimi.dsi.fastutil.objects.Object2DoubleMap;
import it.unimi.dsi.fastutil.objects.Object2DoubleMap.Entry;
import it.unimi.dsi.fastutil.objects.Object2DoubleMaps;
import it.unimi.dsi.fastutil.objects.Object2DoubleOpenHashMap;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.math.MathHelper;

public class ReplicaParams {
  public static final ReplicaParams EMPTY =
      new ReplicaParams(Object2DoubleMaps.emptyMap());

  private final Object2DoubleMap<String> params;

  public ReplicaParams(Object2DoubleMap<String> params) {
    this.params = params;
  }

  public double getDouble(double old, String name, int replicaIndex) {
    return old + params.getDouble(name) * replicaIndex;
  }

  public long getLong(long old, String name, int replicaIndex) {
    return old + Math.round(getDouble(0.0, name, replicaIndex));
  }

  public float getFloat(float old, String name, int replicaIndex) {
    return (float) getDouble(old, name, replicaIndex);
  }

  public int getInt(int old, String name, int replicaIndex) {
    long amt = getLong(old, name, replicaIndex);
    return (int) MathHelper.clamp(amt, Integer.MIN_VALUE, Integer.MAX_VALUE);
  }

  public Number getNumber(Number old, String name, int replicaIndex) {
    return getDouble(old.doubleValue(), name, replicaIndex);
  }

  public static final Codec<ReplicaParams> CODEC =
      Codec.unboundedMap(Codec.STRING, Codec.DOUBLE).xmap(
          map -> new ReplicaParams(new Object2DoubleOpenHashMap<>(map)),
          params -> params.params);

  public <T> Dynamic<T> replicate(Dynamic<T> obj, int replicaIndex) {
    return replicate(obj, replicaIndex, "");
  }

  private static <T> Dynamic<T> visitNumber(Dynamic<T> old, Number newNum) {
    return new Dynamic<T>(old.getOps(), old.getOps().createNumeric(newNum));
  }

  private <T> Dynamic<T> replicate(Dynamic<T> obj, int replicaIndex,
      String prefix) {
    return obj.updateMapValues(e -> {
      Dynamic<?> key = e.getFirst();
      Dynamic<?> value = e.getSecond();
      String name = prefix + key.asString().getOrThrow(false, s -> {
        throw new RuntimeException("unexpected non-string key: " + s);
      });
      Dynamic<?> value2 = value.asNumber().map(num -> {
        Number transformed = getNumber(num, name, replicaIndex);
        return visitNumber(value, transformed);
      }).get().map(complete -> complete,
          partial -> replicate(value, replicaIndex, prefix + name + "."));
      return Pair.of(key, value2);
    });
  }

  public static ReplicaParams deserialize(PacketByteBuf data) {
    Object2DoubleMap<String> entries = new Object2DoubleOpenHashMap<>();
    int n = data.readVarInt();
    for (int i = 0; i < n; ++i) {
      String key = data.readString();
      double value = data.readDouble();
      entries.put(key, value);
    }
    return new ReplicaParams(entries);
  }

  public void serialize(PacketByteBuf data) {
    data.writeVarInt(params.size());
    for (Entry<String> e : params.object2DoubleEntrySet()) {
      data.writeString(e.getKey());
      data.writeDouble(e.getDoubleValue());
    }
  }
}
