package flirora.crackslashmine.item.spell.data.factory;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import flirora.crackslashmine.core.CsmAttack;
import flirora.crackslashmine.core.CsmPrimaryAttackStat;
import flirora.crackslashmine.entity.EntityUtils;
import flirora.crackslashmine.entity.projectile.ProjectileComponent;
import flirora.crackslashmine.item.spell.SpellParameters;
import flirora.crackslashmine.item.spell.data.DataSerializer;
import flirora.crackslashmine.item.spell.data.IndentedTooltipBuilder;
import flirora.crackslashmine.item.spell.data.target.SpellTargetContext;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.ArrowEntity;
import net.minecraft.entity.projectile.PersistentProjectileEntity.PickupPermission;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.text.TextColor;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.EntityHitResult;

public class ArrowEntityFactory extends ProjectileEntityFactory<ArrowEntity> {
  public ArrowEntityFactory(BaseEntityFactory.Context base,
      Context projectileContext) {
    super(base, projectileContext);
  }

  public ArrowEntityFactory(PacketByteBuf data) {
    super(data);
  }

  @Override
  public void describe(boolean detailed, SpellParameters params,
      IndentedTooltipBuilder tooltip) {
    Text locationText = locationText();
    long[] range =
        params.getDamageRangeGivenMultiplier(projectileContext.multi);
    tooltip.add(new TranslatableText(
        getTranslationKey("tooltip.csm.spell.factory.arrow"), locationText,
        range[0], range[1],
        new TranslatableText("tooltip.csm.spell.action.damage."
            + projectileContext.type.getName())
                .styled(s -> s.withColor(
                    TextColor.fromRgb(projectileContext.type.getColor())))));
    this.describeCollisionHandlers(detailed, params, tooltip);
  }

  @Override
  protected ArrowEntity create(SpellTargetContext stContext,
      SpellParameters params, CreationContext projectileContext) {
    LivingEntity caster = stContext.getCaster();
    long[] range =
        params.getDamageRangeGivenMultiplier(projectileContext.proj.multi);
    CsmAttack attack = new CsmPrimaryAttackStat(new CsmPrimaryAttackStat.Entry(
        projectileContext.proj.type, range[0], range[1]))
            .roll(caster.getRandom(), caster);
    ArrowEntity arrow =
        new ArrowEntity(EntityType.ARROW, caster.getEntityWorld()) {
          @Override
          protected void onEntityHit(EntityHitResult entityHitResult) {
            super.onEntityHit(entityHitResult);
            Entity entity = entityHitResult.getEntity();
            if (world instanceof ServerWorld) {
              SpellTargetContext context =
                  new SpellTargetContext(caster, entity, this);
              projectileContext.proj.onCollideWithEntity.execute(context,
                  (ServerWorld) world, params);
            }
          }

          @Override
          protected void onBlockHit(BlockHitResult blockHitResult) {
            super.onBlockHit(blockHitResult);
            if (world instanceof ServerWorld) {
              SpellTargetContext context =
                  new SpellTargetContext(caster, this, this);
              projectileContext.proj.onCollideWithBlock.execute(context,
                  (ServerWorld) world, params);
            }
          }
        };
    EntityUtils.initFromContext(arrow, stContext.getTarget(), context);
    arrow.setOwner(caster);
    arrow.pickupType = PickupPermission.DISALLOWED;
    ProjectileComponent.getFor(arrow).setAttack(attack);
    return arrow;
  }

  public static final Codec<ArrowEntityFactory> CODEC =
      RecordCodecBuilder.create(builder -> {
        return builder
            .group(
                BaseEntityFactory.Context.MAP_CODEC.forGetter(f -> f.context),
                ProjectileEntityFactory.Context.MAP_CODEC
                    .forGetter(f -> f.projectileContext))
            .apply(builder, ArrowEntityFactory::new);
      });

  public static class Serializer extends DataSerializer<ArrowEntityFactory> {
    public Serializer() {
      super(CODEC);
    }

    @Override
    public ArrowEntityFactory deserialize(PacketByteBuf data) {
      return new ArrowEntityFactory(data);
    }

    @Override
    public void serialize(PacketByteBuf data, ArrowEntityFactory object) {
      object.serializeRaw(data);
    }
  }

  public static final Serializer SERIALIZER = new Serializer();

  @Override
  public DataSerializer<? extends ArrowEntityFactory> getSerializer() {
    // TODO Auto-generated method stub
    return SERIALIZER;
  }

}
