package flirora.crackslashmine.item.spell.data.action;

import org.jetbrains.annotations.Nullable;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import flirora.crackslashmine.item.spell.SpellParameters;
import flirora.crackslashmine.item.spell.data.DataSerializer;
import flirora.crackslashmine.item.spell.data.IndentedTooltipBuilder;
import flirora.crackslashmine.item.spell.data.SpellDataManager;
import flirora.crackslashmine.item.spell.data.factory.EntityFactory;
import flirora.crackslashmine.item.spell.data.target.SpellTargetContext;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.world.ServerWorld;

public class SpawnEntitySpellAction implements SpellAction {
  private final EntityFactory<?> factory;

  public SpawnEntitySpellAction(EntityFactory<?> factory) {
    this.factory = factory;
  }

  @Override
  public void perform(SpellTargetContext context, ServerWorld world,
      SpellParameters params) {
    world.spawnEntity(factory.create(context, params));
  }

  @Override
  public @Nullable void describe(boolean detailed, SpellParameters params,
      IndentedTooltipBuilder tooltip) {
    factory.describe(detailed, params, tooltip);
  }

  public static final Codec<SpawnEntitySpellAction> CODEC =
      RecordCodecBuilder.create(inst -> {
        return inst
            .group(SpellDataManager.FACTORIES.getCodec().fieldOf("entity")
                .forGetter(a -> a.factory))
            .apply(inst, SpawnEntitySpellAction::new);
      });

  public static class Serializer
      extends DataSerializer<SpawnEntitySpellAction> {
    public Serializer() {
      super(CODEC);
    }

    @Override
    public SpawnEntitySpellAction deserialize(PacketByteBuf data) {
      return new SpawnEntitySpellAction(EntityFactory.deserialize(data));
    }

    @Override
    public void serialize(PacketByteBuf data, SpawnEntitySpellAction object) {
      object.factory.serialize(data);
    }
  }

  public static final Serializer SERIALIZER = new Serializer();

  @Override
  public DataSerializer<? extends SpellAction> getSerializer() {
    return SERIALIZER;
  }
}
