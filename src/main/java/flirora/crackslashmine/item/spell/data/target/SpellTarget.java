package flirora.crackslashmine.item.spell.data.target;

import java.util.stream.Stream;

import org.jetbrains.annotations.NotNull;

import com.google.gson.JsonObject;

import flirora.crackslashmine.item.spell.data.SpellDataManager;
import flirora.crackslashmine.item.spell.data.WithSerializer;
import net.minecraft.entity.LivingEntity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.text.Text;

public interface SpellTarget extends WithSerializer<SpellTarget> {
  @NotNull
  Stream<LivingEntity> getTargets(SpellTargetContext ctx);

  @NotNull
  Text describe(boolean detailed);

  static SpellTarget deserialize(JsonObject json) {
    return SpellDataManager.TARGETS.deserialize(json);
  }

  static SpellTarget deserialize(PacketByteBuf data) {
    return SpellDataManager.TARGETS.deserialize(data);
  }

  default void serialize(PacketByteBuf data) {
    SpellDataManager.TARGETS.serialize(data, this);
  }

  default JsonObject serialize(JsonObject json) {
    return SpellDataManager.TARGETS.serialize(json, this);
  }
}
