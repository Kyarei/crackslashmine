package flirora.crackslashmine.item.spell.data;

import java.util.Collections;
import java.util.Map;
import java.util.stream.Stream;

import org.apache.logging.log4j.Level;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.CsmRegistries;
import flirora.crackslashmine.core.DamageType;
import flirora.crackslashmine.item.spell.Spell;
import flirora.crackslashmine.item.spell.data.action.SpellAction;
import flirora.crackslashmine.item.spell.data.factory.EntityFactory;
import flirora.crackslashmine.item.spell.data.target.SpellTarget;
import io.netty.buffer.Unpooled;
import net.fabricmc.fabric.api.network.PacketContext;
import net.fabricmc.fabric.api.network.ServerSidePacketRegistry;
import net.fabricmc.fabric.api.resource.IdentifiableResourceReloadListener;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.resource.JsonDataLoader;
import net.minecraft.resource.ResourceManager;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.profiler.Profiler;

public class SpellDataManager extends JsonDataLoader
    implements IdentifiableResourceReloadListener {
  private static final Gson GSON =
      (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();

  public static final Identifier PACKET_ID =
      new Identifier(CrackSlashMineMod.MOD_ID, "packet_data");
  public static final SpellDataManager SERVER = new SpellDataManager(false);
  public static final SpellDataManager CLIENT = new SpellDataManager(true);

  public static final SerializationDispatcher<SpellTarget> TARGETS =
      new SerializationDispatcher<>(CsmRegistries.SPELL_TARGET_SERIALIZER,
          SpellTarget.class);
  public static final SerializationDispatcher<SpellAction> ACTIONS =
      new SerializationDispatcher<>(CsmRegistries.SPELL_ACTION_SERIALIZER,
          SpellAction.class);
  public static final SerializationDispatcher<EntityFactory<?>> FACTORIES =
      new SerializationDispatcher<>(CsmRegistries.ENTITY_FACTORY_SERIALIZER,
          (Class<EntityFactory<?>>) (Class<?>) EntityFactory.class);

  private static final Identifier ID =
      new Identifier(CrackSlashMineMod.MOD_ID, "spell_data");
  private static final Identifier DEFAULT_SPELL_ID =
      new Identifier(CrackSlashMineMod.MOD_ID, "default");
  private static final Spell DEFAULT_SPELL =
      new Spell(DEFAULT_SPELL_ID, DamageType.PHYSICAL, 0, -5, -5, 0,
          Collections.emptyList(), Collections.emptyList());

  private final boolean client;
  private PacketByteBuf data = null;
  private ImmutableMap<Identifier, Spell> entries;

  private SpellDataManager(boolean client) {
    super(GSON, "csm/spell_data");
    this.client = client;
    BuiltinSpellSerializers.load();
  }

  @Override
  public Identifier getFabricId() {
    return ID;
  }

  @Override
  protected void apply(Map<Identifier, JsonElement> loader,
      ResourceManager manager, Profiler profiler) {
    if (client) {
      throw new IllegalStateException("Tried to load JSON data on the client!");
    }
    int count = 0;
    ImmutableMap.Builder<Identifier, Spell> builder =
        new ImmutableMap.Builder<>();
    builder.put(DEFAULT_SPELL_ID, DEFAULT_SPELL);
    for (Map.Entry<Identifier, JsonElement> entry : loader.entrySet()) {
      Identifier id = entry.getKey();
      JsonElement json = entry.getValue();
      try {
        Spell description = Spell.deserialize(id, (JsonObject) json);
        builder.put(id, description);
      } catch (Exception e) {
        CrackSlashMineMod.logException("Failed to load spell description " + id,
            e);
      }
      ++count;
    }
    entries = builder.build();
    buildPacketData();
    CrackSlashMineMod.log(Level.INFO,
        "Loaded " + count + " spell descriptions");
  }

  private void buildPacketData() {
    data = new PacketByteBuf(Unpooled.buffer());
    data.writeInt(entries.size() - 1);
    for (Map.Entry<Identifier, Spell> e : entries.entrySet()) {
      if (e.getKey().equals(DEFAULT_SPELL_ID))
        continue;
      data.writeIdentifier(e.getKey());
      e.getValue().serialize(data);
    }
  }

  public void syncToClients(Stream<ServerPlayerEntity> players) {
    if (data == null) {
      CrackSlashMineMod.log(Level.WARN, "syncToClients called w/o data loaded");
      return;
    }
    players.forEach(player -> {
      ServerSidePacketRegistry.INSTANCE.sendToPlayer(player, PACKET_ID, data);
    });
  }

  public void fillDataFrom(PacketContext context, PacketByteBuf data) {
    if (!client) {
      throw new IllegalStateException(
          "Tried to load network data on the server!");
    }
    this.data = data;
    int count = data.readInt();
    ImmutableMap.Builder<Identifier, Spell> builder =
        new ImmutableMap.Builder<>();
    builder.put(DEFAULT_SPELL_ID, DEFAULT_SPELL);
    for (int i = 0; i < count; ++i) {
      Identifier id = new Identifier("fluffy", "carrots");
      try {
        id = data.readIdentifier();
        Spell description = Spell.deserialize(id, data);
        builder.put(id, description);
      } catch (Exception e) {
        CrackSlashMineMod.logException("Failed to load spell description " + id,
            e);
      }
    }
    final int finalCount = count;
    context.getTaskQueue().execute(() -> {
      entries = builder.build();
      CrackSlashMineMod.log(Level.INFO,
          "Loaded " + finalCount + " spell descriptions from network");
      SERVER.entries = entries;
    });
  }

  public Spell getSpell(Identifier id) {
    return entries.get(id);
  }

  public Stream<Spell> getAllSpells() {
    return entries.values().stream();
  }

  public Spell getDefault() {
    return DEFAULT_SPELL;
  }

  public Iterable<Identifier> getAllSpellIds() {
    return entries.keySet();
  }

}
