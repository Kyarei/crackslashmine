package flirora.crackslashmine.item.spell.data.factory;

import com.google.gson.JsonObject;

import flirora.crackslashmine.item.spell.SpellParameters;
import flirora.crackslashmine.item.spell.data.IndentedTooltipBuilder;
import flirora.crackslashmine.item.spell.data.SpellDataManager;
import flirora.crackslashmine.item.spell.data.WithSerializer;
import flirora.crackslashmine.item.spell.data.target.SpellTargetContext;
import net.minecraft.entity.Entity;
import net.minecraft.network.PacketByteBuf;

public interface EntityFactory<E extends Entity>
    /*
     * Note that due to quirks of javac, this type bound has to use a '?' and
     * not 'E'. ECJ, for some reason, was fine with the latter case.
     */
    extends WithSerializer<EntityFactory<?>> {
  E create(SpellTargetContext context, SpellParameters params);

  static EntityFactory<?> deserialize(JsonObject json) {
    return SpellDataManager.FACTORIES.deserialize(json);
  }

  static EntityFactory<?> deserialize(PacketByteBuf data) {
    return SpellDataManager.FACTORIES.deserialize(data);
  }

  default void serialize(PacketByteBuf data) {
    SpellDataManager.FACTORIES.serialize(data, this);
  }

  default JsonObject serialize(JsonObject json) {
    return SpellDataManager.FACTORIES.serialize(json, this);
  }

  void describe(boolean detailed, SpellParameters params,
      IndentedTooltipBuilder tooltip);
}
