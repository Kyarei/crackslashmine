package flirora.crackslashmine.item.spell.data;

import com.google.gson.JsonObject;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.math.MathHelper;

public class DecayParameters {
  private final double start;
  private final double multi;
  private final double max;

  public static final DecayParameters DEFAULT =
      new DecayParameters(0.0, 0.0, 1.0);
  public static final Codec<DecayParameters> CODEC =
      RecordCodecBuilder.create(inst -> {
        return inst.group(
            Codec.DOUBLE.optionalFieldOf("start", 0.0).forGetter(p -> p.start),
            Codec.DOUBLE.optionalFieldOf("multi", 0.0).forGetter(p -> p.multi),
            Codec.DOUBLE.optionalFieldOf("max", 1.0).forGetter(p -> p.max))
            .apply(inst, DecayParameters::new);
      });

  public DecayParameters(double start, double multi, double max) {
    this.start = start;
    this.multi = multi;
    this.max = max;
  }

  public double getScaling(double distance) {
    return MathHelper.clamp(multi * (distance - start), 0, max);
  }

  public static DecayParameters deserialize(JsonObject json) {
    return new DecayParameters(SpellHelper.getDouble(json, "start", 0.0),
        SpellHelper.getDouble(json, "multi", 0.0),
        SpellHelper.getDouble(json, "max", 1.0));
  }

  public static DecayParameters deserialize(PacketByteBuf data) {
    return new DecayParameters(data.readDouble(), data.readDouble(),
        data.readDouble());
  }

  public void serialize(PacketByteBuf data) {
    data.writeDouble(start);
    data.writeDouble(multi);
    data.writeDouble(max);
  }
}
