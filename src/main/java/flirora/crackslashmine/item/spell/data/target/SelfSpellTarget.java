package flirora.crackslashmine.item.spell.data.target;

import java.util.stream.Stream;

import org.jetbrains.annotations.NotNull;

import com.mojang.serialization.Codec;

import flirora.crackslashmine.item.spell.data.DataSerializer;
import net.minecraft.entity.LivingEntity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;

public class SelfSpellTarget implements SpellTarget {
  private SelfSpellTarget() {
    // TODO Auto-generated constructor stub
  }

  public static SelfSpellTarget INSTANCE = new SelfSpellTarget();

  @Override
  public @NotNull Stream<LivingEntity> getTargets(SpellTargetContext ctx) {
    return ctx.getTarget() instanceof LivingEntity ? Stream.of((LivingEntity) ctx.getTarget())
        : Stream.empty();
  }

  @Override
  public @NotNull Text describe(boolean detailed) {
    return new TranslatableText("tooltip.csm.spell.target.self");
  }

  public static class Serializer extends DataSerializer<SelfSpellTarget> {
    public Serializer() {
      super(CODEC);
    }

    @Override
    public SelfSpellTarget deserialize(PacketByteBuf data) {
      return INSTANCE;
    }

    @Override
    public void serialize(PacketByteBuf data, SelfSpellTarget object) {
      // Nothing!
    }
  }

  public static final Codec<SelfSpellTarget> CODEC = Codec.unit(INSTANCE);
  public static final Serializer SERIALIZER = new Serializer();

  @Override
  public DataSerializer<SelfSpellTarget> getSerializer() {
    return SERIALIZER;
  }
}
