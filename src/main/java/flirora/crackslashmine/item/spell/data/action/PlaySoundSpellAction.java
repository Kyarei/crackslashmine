package flirora.crackslashmine.item.spell.data.action;

import org.apache.commons.lang3.Validate;
import org.jetbrains.annotations.NotNull;

import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import flirora.crackslashmine.item.spell.SpellParameters;
import flirora.crackslashmine.item.spell.data.DataSerializer;
import flirora.crackslashmine.item.spell.data.target.SpellTargetContext;
import flirora.crackslashmine.mixin.SoundCategoryAccessor;
import net.minecraft.entity.Entity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvent;
import net.minecraft.util.registry.Registry;

public class PlaySoundSpellAction implements SpellAction {
  private final SoundEvent event;
  private final SoundCategory category;
  private float volume, pitch;

  private PlaySoundSpellAction(SoundEvent event, SoundCategory category,
      float volume, float pitch) {
    Validate.notNull(event, "event", new Object[0]);
    this.event = event;
    this.category = category;
    this.volume = volume;
    this.pitch = pitch;
  }

  @Override
  public void perform(SpellTargetContext context, @NotNull ServerWorld world,
      @NotNull SpellParameters params) {
    Entity target = context.getTarget();
    world.playSound(null, target.getX(), target.getY(), target.getZ(), event,
        category, volume, pitch);
  }

  private static final Codec<SoundCategory> SOUND_CATEGORY_CODEC =
      Codec.STRING.comapFlatMap(name -> {
        SoundCategory cat = SoundCategoryAccessor.getByName().get(name);
        return cat == null ? DataResult.error("Unknown sound category " + name)
            : DataResult.success(cat);
      }, SoundCategory::getName);

  public static final Codec<PlaySoundSpellAction> CODEC =
      RecordCodecBuilder.create(inst -> {
        return inst.group(
            SoundEvent.CODEC.fieldOf("event").forGetter(a -> a.event),
            SOUND_CATEGORY_CODEC.fieldOf("category").forGetter(a -> a.category),
            Codec.FLOAT.optionalFieldOf("volume", 1.0f)
                .forGetter(a -> a.volume),
            Codec.FLOAT.optionalFieldOf("pitch", 1.0f).forGetter(a -> a.pitch))
            .apply(inst, PlaySoundSpellAction::new);
      });

  public static class Serializer extends DataSerializer<PlaySoundSpellAction> {
    public Serializer() {
      super(CODEC);
    }

    @Override
    public PlaySoundSpellAction deserialize(PacketByteBuf data) {
      SoundEvent event = Registry.SOUND_EVENT.get(data.readIdentifier());
      SoundCategory category = SoundCategory.values()[data.readByte()];
      float volume = data.readFloat();
      float pitch = data.readFloat();
      return new PlaySoundSpellAction(event, category, volume, pitch);
    }

    @Override
    public void serialize(PacketByteBuf data, PlaySoundSpellAction object) {
      data.writeIdentifier(object.event.getId());
      data.writeByte(object.category.ordinal());
      data.writeFloat(object.volume);
      data.writeFloat(object.pitch);
    }
  }

  public static final Serializer SERIALIZER = new Serializer();

  @Override
  public DataSerializer<? extends SpellAction> getSerializer() {
    return SERIALIZER;
  }
}
