package flirora.crackslashmine.item.spell.data.factory;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import flirora.crackslashmine.core.CsmAttack;
import flirora.crackslashmine.core.CsmPrimaryAttackStat;
import flirora.crackslashmine.core.DamageType;
import flirora.crackslashmine.entity.CsmEntities;
import flirora.crackslashmine.entity.spell.CsmLightningEntity;
import flirora.crackslashmine.item.spell.SpellParameters;
import flirora.crackslashmine.item.spell.data.DataSerializer;
import flirora.crackslashmine.item.spell.data.IndentedTooltipBuilder;
import flirora.crackslashmine.item.spell.data.target.SpellTargetContext;
import net.minecraft.entity.LivingEntity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.text.Text;
import net.minecraft.text.TextColor;
import net.minecraft.text.TranslatableText;

public class LightningEntityFactory
    extends BaseEntityFactory<CsmLightningEntity> {
  private final double multi;
  private final DamageType damageType;

  public LightningEntityFactory(Context context, double multi,
      DamageType damageType) {
    super(context);
    this.multi = multi;
    this.damageType = damageType;
  }

  public LightningEntityFactory(PacketByteBuf data) {
    super(data);
    multi = data.readDouble();
    damageType = DamageType.values()[data.readByte()];
  }

  @Override
  public void describe(boolean detailed, SpellParameters params,
      IndentedTooltipBuilder tooltip) {
    Text locationText = locationText();
    long[] range = params.getDamageRangeGivenMultiplier(multi);
    tooltip.add(new TranslatableText(
        getTranslationKey("tooltip.csm.spell.factory.lightning"), locationText,
        range[0], range[1],
        new TranslatableText(
            "tooltip.csm.spell.action.damage." + damageType.getName()).styled(
                s -> s.withColor(TextColor.fromRgb(damageType.getColor())))));
  }

  @Override
  protected CsmLightningEntity create(SpellTargetContext stContext,
      SpellParameters params, Context context) {
    LivingEntity caster = stContext.getCaster();
    long[] range = params.getDamageRangeGivenMultiplier(multi);
    CsmAttack attack = new CsmPrimaryAttackStat(
        new CsmPrimaryAttackStat.Entry(damageType, range[0], range[1]))
            .roll(caster.getRandom(), caster);
    CsmLightningEntity lightning = new CsmLightningEntity(CsmEntities.LIGHTNING,
        caster.getEntityWorld(), caster, attack);
    lightning.refreshPositionAfterTeleport(stContext.getTarget().getPos());
    return lightning;
  }

  @Override
  public void serializeRaw(PacketByteBuf data) {
    super.serializeRaw(data);
    data.writeDouble(multi);
    data.writeByte(damageType.ordinal());
  }

  public static final Codec<LightningEntityFactory> CODEC =
      RecordCodecBuilder.create(inst -> {
        return inst
            .group(
                BaseEntityFactory.Context.MAP_CODEC.forGetter(f -> f.context),
                Codec.DOUBLE.fieldOf("multi").forGetter(f -> f.multi),
                DamageType.CODEC.fieldOf("damageType")
                    .forGetter(f -> f.damageType))
            .apply(inst, LightningEntityFactory::new);
      });

  public static class Serializer
      extends DataSerializer<LightningEntityFactory> {
    public Serializer() {
      super(CODEC);
    }

    @Override
    public LightningEntityFactory deserialize(PacketByteBuf data) {
      return new LightningEntityFactory(data);
    }

    @Override
    public void serialize(PacketByteBuf data, LightningEntityFactory object) {
      object.serializeRaw(data);
    }
  }

  public static final Serializer SERIALIZER = new Serializer();

  @Override
  public DataSerializer<? extends EntityFactory<CsmLightningEntity>> getSerializer() {
    return SERIALIZER;
  }

}
