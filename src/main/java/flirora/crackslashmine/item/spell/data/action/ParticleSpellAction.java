package flirora.crackslashmine.item.spell.data.action;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import flirora.crackslashmine.item.spell.SpellParameters;
import flirora.crackslashmine.item.spell.data.DataSerializer;
import flirora.crackslashmine.item.spell.data.target.SpellTargetContext;
import net.minecraft.entity.Entity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.particle.ParticleEffect;
import net.minecraft.particle.ParticleType;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.registry.Registry;

public class ParticleSpellAction implements SpellAction {
  private final ParticleEffect type;
  private final double frontOff, yOff, sideOff;
  private final int count;
  private final double deltaX, deltaY, deltaZ;
  private final double speed;

  private ParticleSpellAction(ParticleEffect type, double frontOff, double yOff,
      double sideOff, int count, double deltaX, double deltaY, double deltaZ,
      double speed) {
    super();
    this.type = type;
    this.frontOff = frontOff;
    this.yOff = yOff;
    this.sideOff = sideOff;
    this.count = count;
    this.deltaX = deltaX;
    this.deltaY = deltaY;
    this.deltaZ = deltaZ;
    this.speed = speed;
  }

  @Override
  public void perform(SpellTargetContext context, ServerWorld world,
      SpellParameters params) {
    Entity target = context.getTarget();
    double yaw = Math.toRadians(target.getHeadYaw());
    double cy = Math.cos(yaw);
    double sy = Math.sin(yaw);
    double x = target.getX() + frontOff * cy + sideOff * sy;
    double y = target.getY() + yOff;
    double z = target.getZ() + frontOff * sy - sideOff * cy;
    world.spawnParticles(type, x, y, z, count, deltaX, deltaY, deltaZ, speed);
  }

  public static final Codec<ParticleSpellAction> CODEC =
      RecordCodecBuilder.create(inst -> {
        return inst.group(
            ParticleTypes.TYPE_CODEC.fieldOf("particle").forGetter(a -> a.type),
            Codec.DOUBLE.optionalFieldOf("frontOff", 0.0)
                .forGetter(a -> a.frontOff),
            Codec.DOUBLE.optionalFieldOf("yOff", 1.0).forGetter(a -> a.yOff),
            Codec.DOUBLE.optionalFieldOf("sideOff", 0.0)
                .forGetter(a -> a.sideOff),
            Codec.INT.optionalFieldOf("count", 1).forGetter(a -> a.count),
            Codec.DOUBLE.optionalFieldOf("deltaX", 0.5)
                .forGetter(a -> a.deltaX),
            Codec.DOUBLE.optionalFieldOf("deltaY", 1.0)
                .forGetter(a -> a.deltaY),
            Codec.DOUBLE.optionalFieldOf("deltaZ", 0.5)
                .forGetter(a -> a.deltaZ),
            Codec.DOUBLE.optionalFieldOf("speed", 0.0).forGetter(a -> a.speed))
            .apply(inst, ParticleSpellAction::new);
      });

  public static class Serializer extends DataSerializer<ParticleSpellAction> {
    public Serializer() {
      super(CODEC);
    }

    private static <T extends ParticleEffect> T readParticleParameters(
        PacketByteBuf buf, ParticleType<T> type) {
      return type.getParametersFactory().read(type, buf);
    }

    @Override
    public ParticleSpellAction deserialize(PacketByteBuf data) {
      ParticleType<?> pt = Registry.PARTICLE_TYPE.get(data.readInt());
      ParticleEffect type = readParticleParameters(data, pt);
      double frontOff = data.readDouble();
      double yOff = data.readDouble();
      double sideOff = data.readDouble();
      int count = data.readInt();
      double deltaX = data.readDouble();
      double deltaY = data.readDouble();
      double deltaZ = data.readDouble();
      double speed = data.readDouble();
      return new ParticleSpellAction(type, frontOff, yOff, sideOff, count,
          deltaX, deltaY, deltaZ, speed);
    }

    @Override
    public void serialize(PacketByteBuf data, ParticleSpellAction object) {
      data.writeInt(Registry.PARTICLE_TYPE.getRawId(object.type.getType()));
      object.type.write(data);
      data.writeDouble(object.frontOff);
      data.writeDouble(object.yOff);
      data.writeDouble(object.sideOff);
      data.writeInt(object.count);
      data.writeDouble(object.deltaX);
      data.writeDouble(object.deltaY);
      data.writeDouble(object.deltaZ);
      data.writeDouble(object.speed);
    }
  }

  public static final Serializer SERIALIZER = new Serializer();

  @Override
  public DataSerializer<? extends SpellAction> getSerializer() {
    return SERIALIZER;
  }
}
