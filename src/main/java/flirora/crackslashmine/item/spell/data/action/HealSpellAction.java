package flirora.crackslashmine.item.spell.data.action;

import org.jetbrains.annotations.Nullable;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.item.spell.SpellParameters;
import flirora.crackslashmine.item.spell.data.DataSerializer;
import flirora.crackslashmine.item.spell.data.DecayParameters;
import flirora.crackslashmine.item.spell.data.IndentedTooltipBuilder;
import flirora.crackslashmine.item.spell.data.SpellDataManager;
import flirora.crackslashmine.item.spell.data.target.SpellTarget;
import flirora.crackslashmine.item.spell.data.target.SpellTargetContext;
import net.minecraft.entity.LivingEntity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.TranslatableText;

public class HealSpellAction implements SpellAction {
  private final SpellTarget targetSelector;
  private final double multi;
  private final DecayParameters decay;

  private HealSpellAction(SpellTarget target, double multi,
      DecayParameters decay) {
    super();
    this.targetSelector = target;
    this.multi = multi;
    this.decay = decay;
  }

  @Override
  public void perform(SpellTargetContext context, ServerWorld world,
      SpellParameters params) {
    LivingEntity caster = context.getCaster();
    LivingEntityStats casterStats = LivingEntityStats.getFor(caster);
    long amt = params.getDamageGivenMultiplier(multi, world.getRandom());
    targetSelector.getTargets(context).forEach(e -> {
      double decay = this.decay.getScaling(e.distanceTo(caster));
      long adjusted = Math.round(amt * (1 - decay));
      LivingEntityStats.getFor(e).healFrom(casterStats, adjusted);
    });
  }

  @Override
  public @Nullable void describe(boolean detailed, SpellParameters params,
      IndentedTooltipBuilder tooltip) {
    long[] range = params.getDamageRangeGivenMultiplier(multi);
    tooltip.add(new TranslatableText("tooltip.csm.spell.action.heal", range[0],
        range[1], targetSelector.describe(detailed)));
  }

  public static final Codec<HealSpellAction> CODEC =
      RecordCodecBuilder.create(inst -> {
        return inst.group(
            SpellDataManager.TARGETS.getCodec().fieldOf("target")
                .forGetter(a -> a.targetSelector),
            Codec.DOUBLE.fieldOf("multi").forGetter(a -> a.multi),
            DecayParameters.CODEC
                .optionalFieldOf("decay", DecayParameters.DEFAULT)
                .forGetter(a -> a.decay))
            .apply(inst, HealSpellAction::new);
      });

  public static class Serializer extends DataSerializer<HealSpellAction> {
    public Serializer() {
      super(CODEC);
    }

    @Override
    public HealSpellAction deserialize(PacketByteBuf data) {
      SpellTarget target = SpellTarget.deserialize(data);
      double multi = data.readDouble();
      DecayParameters decay = DecayParameters.deserialize(data);
      return new HealSpellAction(target, multi, decay);
    }

    @Override
    public void serialize(PacketByteBuf data, HealSpellAction object) {
      object.targetSelector.serialize(data);
      data.writeDouble(object.multi);
      object.decay.serialize(data);
    }
  }

  public static final Serializer SERIALIZER = new Serializer();

  @Override
  public DataSerializer<? extends SpellAction> getSerializer() {
    return SERIALIZER;
  }
}
