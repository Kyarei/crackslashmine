package flirora.crackslashmine.item.spell.data.action;

import org.jetbrains.annotations.Nullable;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import flirora.crackslashmine.core.Beautify;
import flirora.crackslashmine.item.spell.SpellParameters;
import flirora.crackslashmine.item.spell.data.DataSerializer;
import flirora.crackslashmine.item.spell.data.DecayParameters;
import flirora.crackslashmine.item.spell.data.IndentedTooltipBuilder;
import flirora.crackslashmine.item.spell.data.SpellDataManager;
import flirora.crackslashmine.item.spell.data.target.SpellTarget;
import flirora.crackslashmine.item.spell.data.target.SpellTargetContext;
import net.minecraft.client.resource.language.I18n;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.registry.Registry;

public class ApplyEffectSpellAction implements SpellAction {
  private final SpellTarget targetSelector;
  private final StatusEffect effect;
  private final int intensity;
  private final int minTime;
  private final int maxTime;
  private final DecayParameters durationDecay;

  public ApplyEffectSpellAction(SpellTarget target, StatusEffect effect,
      int intensity, int minTime, int maxTime, DecayParameters durationDecay) {
    this.targetSelector = target;
    this.effect = effect;
    this.intensity = intensity;
    this.minTime = minTime;
    this.maxTime = maxTime;
    this.durationDecay = durationDecay;
  }

  @Override
  public void perform(SpellTargetContext context, ServerWorld world,
      SpellParameters params) {
    targetSelector.getTargets(context).forEach(e -> {
      double distance = e.distanceTo(context.getCaster());
      double durationDecay = this.durationDecay.getScaling(distance);
      int duration = MathHelper.nextInt(world.getRandom(), minTime, maxTime);
      duration = (int) Math.round(duration * (1 - durationDecay));
      e.applyStatusEffect(
          new StatusEffectInstance(effect, duration, intensity));
    });
  }

  @Override
  public @Nullable void describe(boolean detailed, SpellParameters params,
      IndentedTooltipBuilder tooltip) {
    tooltip.add(new TranslatableText("tooltip.csm.spell.action.effect.simple",
        effect.getName(), targetSelector.describe(detailed),
        I18n.translate("enchantment.level." + (intensity + 1)),
        Beautify.beautifyTime(minTime), Beautify.beautifyTime(maxTime)));
  }

  public static final Codec<ApplyEffectSpellAction> CODEC =
      RecordCodecBuilder.create(inst -> {
        return inst.group(
            SpellDataManager.TARGETS.getCodec().fieldOf("target")
                .forGetter(a -> a.targetSelector),
            Registry.STATUS_EFFECT.fieldOf("effect").forGetter(a -> a.effect),
            Codec.INT.optionalFieldOf("intensity", 0)
                .forGetter(a -> a.intensity),
            Codec.INT.fieldOf("minTime").forGetter(a -> a.minTime),
            Codec.INT.fieldOf("maxTime").forGetter(a -> a.maxTime),
            DecayParameters.CODEC
                .optionalFieldOf("durationDecay", DecayParameters.DEFAULT)
                .forGetter(a -> a.durationDecay))
            .apply(inst, ApplyEffectSpellAction::new);
      });

  public static class Serializer
      extends DataSerializer<ApplyEffectSpellAction> {
    public Serializer() {
      super(CODEC);
    }

    @Override
    public ApplyEffectSpellAction deserialize(PacketByteBuf data) {
      SpellTarget target = SpellTarget.deserialize(data);
      StatusEffect effect = Registry.STATUS_EFFECT.get(data.readInt());
      int intensity = data.readInt();
      int minTime = data.readInt();
      int maxTime = data.readInt();
      DecayParameters durationDecay = DecayParameters.deserialize(data);
      return new ApplyEffectSpellAction(target, effect, intensity, minTime,
          maxTime, durationDecay);
    }

    @Override
    public void serialize(PacketByteBuf data, ApplyEffectSpellAction object) {
      object.targetSelector.serialize(data);
      data.writeInt(Registry.STATUS_EFFECT.getRawId(object.effect));
      data.writeInt(object.intensity);
      data.writeInt(object.minTime);
      data.writeInt(object.maxTime);
      object.durationDecay.serialize(data);
    }
  }

  public static final Serializer SERIALIZER = new Serializer();

  @Override
  public DataSerializer<? extends SpellAction> getSerializer() {
    return SERIALIZER;
  }
}
