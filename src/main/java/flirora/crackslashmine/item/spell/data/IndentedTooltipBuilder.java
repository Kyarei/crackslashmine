package flirora.crackslashmine.item.spell.data;

import java.util.ArrayList;
import java.util.List;

import joptsimple.internal.Strings;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;

public class IndentedTooltipBuilder {
  private final List<Text> tooltip;
  private final int indent;
  private final String prefix;

  public IndentedTooltipBuilder(List<Text> tooltip, int indent) {
    this.tooltip = tooltip;
    this.indent = indent;
    this.prefix = Strings.repeat(' ', indent);
  }

  public IndentedTooltipBuilder() {
    this(new ArrayList<>(), 0);
  }

  public IndentedTooltipBuilder indented() {
    return new IndentedTooltipBuilder(tooltip, indent + 1);
  }

  public void add(Text t) {
    Text indented = new LiteralText(prefix).append(t);
    tooltip.add(indented);
  }

  public List<Text> getTooltip() {
    return tooltip;
  }
}
