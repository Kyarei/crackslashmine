package flirora.crackslashmine.item.spell.data;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.mojang.serialization.Codec;
import com.mojang.serialization.JsonOps;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtOps;
import net.minecraft.nbt.Tag;
import net.minecraft.network.PacketByteBuf;

public abstract class DataSerializer<T> {
  private final Codec<T> codec;

  public DataSerializer(Codec<T> codec) {
    this.codec = codec;
  }

  public T deserialize(JsonObject json) {
    return codec.decode(JsonOps.INSTANCE, json).getOrThrow(false, s -> {
      throw new JsonSyntaxException(s);
    }).getFirst();
  }

  public JsonObject serialize(JsonObject json, T object) {
    JsonElement elem =
        codec.encode(object, JsonOps.INSTANCE, json).getOrThrow(false, s -> {
          throw new JsonSyntaxException(s);
        });
    if (!(elem instanceof JsonObject)) {
      throw new JsonSyntaxException("Didn't get a JsonObject?!!");
    }
    return (JsonObject) elem;
  }

  public T deserialize(CompoundTag json) {
    return codec.decode(NbtOps.INSTANCE, json).getOrThrow(false, s -> {
      throw new JsonSyntaxException(s);
    }).getFirst();
  }

  public CompoundTag serialize(CompoundTag json, T object) {
    Tag elem =
        codec.encode(object, NbtOps.INSTANCE, json).getOrThrow(false, s -> {
          throw new JsonSyntaxException(s);
        });
    if (!(elem instanceof CompoundTag)) {
      throw new JsonSyntaxException("Didn't get a CompoundTag?!!");
    }
    return (CompoundTag) elem;
  }

  public abstract T deserialize(PacketByteBuf data);

  public abstract void serialize(PacketByteBuf data, T object);

  public Codec<T> getCodec() {
    return codec;
  }
}
