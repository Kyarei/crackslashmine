package flirora.crackslashmine.item.spell.data.target;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.ProjectileEntity;

public class SpellTargetContext {
  private final @NotNull LivingEntity caster;
  private final @NotNull Entity target;
  private final @Nullable ProjectileEntity projectile;

  public SpellTargetContext(@NotNull LivingEntity caster) {
    this(caster, caster, null);
  }

  public SpellTargetContext(@NotNull LivingEntity caster,
      @NotNull Entity target, @Nullable ProjectileEntity projectile) {
    this.caster = caster;
    this.target = target;
    this.projectile = projectile;
  }

  public @NotNull LivingEntity getCaster() {
    return caster;
  }

  public Entity getTarget() {
    return target;
  }

  public ProjectileEntity getProjectile() {
    return projectile;
  }

  public SpellTargetContext asCaster() {
    return new SpellTargetContext(caster, caster, projectile);
  }

  public SpellTargetContext asProjectile() {
    if (projectile == null) {
      throw new NullPointerException("There is no projectile!");
    }
    return new SpellTargetContext(caster, projectile, projectile);
  }

  public SpellTargetContext withTarget(@NotNull Entity target) {
    return new SpellTargetContext(caster, target, projectile);
  }

  public SpellTargetContext withProjectile(
      @NotNull ProjectileEntity projectile) {
    return new SpellTargetContext(caster, target, projectile);
  }
}
