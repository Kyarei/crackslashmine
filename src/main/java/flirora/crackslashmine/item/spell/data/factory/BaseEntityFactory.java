package flirora.crackslashmine.item.spell.data.factory;

import java.util.Optional;

import org.jetbrains.annotations.Nullable;

import com.mojang.serialization.Codec;
import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import flirora.crackslashmine.item.spell.SpellParameters;
import flirora.crackslashmine.item.spell.data.DataSerializer;
import flirora.crackslashmine.item.spell.data.target.SpellTargetContext;
import net.minecraft.entity.Entity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;

public abstract class BaseEntityFactory<E extends Entity>
    implements EntityFactory<E> {
  public static class Context {
    // Fuck it. I have other shit to do.
    public final double frontOff, yOff, sideOff;
    public final float pitchOffset, yawOffset;
    public final float speed;
    public final float divergence;
    public final @Nullable String customDisplay;

    private Context(double frontOff, double yOff, double sideOff,
        float pitchOffset, float yawOffset, float speed, float divergence,
        @Nullable String customDisplay) {
      this.frontOff = frontOff;
      this.yOff = yOff;
      this.sideOff = sideOff;
      this.pitchOffset = pitchOffset;
      this.yawOffset = yawOffset;
      this.speed = speed;
      this.divergence = divergence;
      this.customDisplay = customDisplay;
    }

    public static final MapCodec<Context> MAP_CODEC =
        RecordCodecBuilder.mapCodec(inst -> {
          return inst.group(
              Codec.DOUBLE.optionalFieldOf("frontOff", 0.0)
                  .forGetter(c -> c.frontOff),
              Codec.DOUBLE.optionalFieldOf("yOff", 0.0).forGetter(c -> c.yOff),
              Codec.DOUBLE.optionalFieldOf("sideOff", 0.0)
                  .forGetter(c -> c.sideOff),
              Codec.FLOAT.optionalFieldOf("pitchOffset", 0.0f)
                  .forGetter(c -> c.pitchOffset),
              Codec.FLOAT.optionalFieldOf("yawOffset", 0.0f)
                  .forGetter(c -> c.yawOffset),
              Codec.FLOAT.optionalFieldOf("speed", 0.0f)
                  .forGetter(c -> c.speed),
              Codec.FLOAT.optionalFieldOf("divergence", 0.25f)
                  .forGetter(c -> c.divergence),
              Codec.STRING.optionalFieldOf("customDisplay")
                  .forGetter(c -> Optional.ofNullable(c.customDisplay)))
              .apply(inst,
                  (frontOff, yOff, sideOff, pitchOffset, yawOffset, speed,
                      divergence, customDisplay) -> new Context(frontOff, yOff,
                          sideOff, pitchOffset, yawOffset, speed, divergence,
                          customDisplay.orElse(null)));
        });

    public static final Codec<Context> CODEC = MAP_CODEC.codec();

    public static class Serializer extends DataSerializer<Context> {
      public Serializer() {
        super(CODEC);
      }

      @Override
      public Context deserialize(PacketByteBuf data) {
        double frontOff = data.readDouble();
        double yOff = data.readDouble();
        double sideOff = data.readDouble();
        float pitchOffset = data.readFloat();
        float yawOffset = data.readFloat();
        float speed = data.readFloat();
        float divergence = data.readFloat();
        @Nullable
        String customDisplay = data.readBoolean() ? data.readString() : null;
        return new Context(frontOff, yOff, sideOff, pitchOffset, yawOffset,
            speed, divergence, customDisplay);
      }

      @Override
      public void serialize(PacketByteBuf data, Context object) {
        data.writeDouble(object.frontOff);
        data.writeDouble(object.yOff);
        data.writeDouble(object.sideOff);
        data.writeFloat(object.pitchOffset);
        data.writeFloat(object.yawOffset);
        data.writeFloat(object.speed);
        data.writeFloat(object.divergence);
        data.writeBoolean(object.customDisplay != null);
        if (object.customDisplay != null)
          data.writeString(object.customDisplay);
      }
    }

    public static final Serializer SERIALIZER = new Serializer();
  }

  protected final Context context;

  public BaseEntityFactory(Context context) {
    this.context = context;
  }

  public BaseEntityFactory(PacketByteBuf data) {
    this.context = Context.SERIALIZER.deserialize(data);
  }

  @Override
  public E create(SpellTargetContext stContext, SpellParameters params) {
    return create(stContext, params, context);
  }

  protected abstract E create(SpellTargetContext stContext,
      SpellParameters params, Context context);

  public String getTranslationKey(String defo) {
    return context.customDisplay != null ? context.customDisplay : defo;
  }

  public void serializeRaw(PacketByteBuf data) {
    Context.SERIALIZER.serialize(data, context);
  }

  private static final Text AT = new LiteralText("");
  private static final Text ABOVE =
      new TranslatableText("tooltip.csm.spell.factory.above");
  private static final Text BELOW =
      new TranslatableText("tooltip.csm.spell.factory.below");

  public Text locationText() {
    Text locationText = AT;
    if (context.yOff > 0)
      locationText = ABOVE;
    else if (context.yOff < 0)
      locationText = BELOW;
    return locationText;
  }
}
