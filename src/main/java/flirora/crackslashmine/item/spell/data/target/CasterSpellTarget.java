package flirora.crackslashmine.item.spell.data.target;

import java.util.stream.Stream;

import org.jetbrains.annotations.NotNull;

import com.mojang.serialization.Codec;

import flirora.crackslashmine.item.spell.data.DataSerializer;
import net.minecraft.entity.LivingEntity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;

public class CasterSpellTarget implements SpellTarget {
  private CasterSpellTarget() {
    // TODO Auto-generated constructor stub
  }

  public static CasterSpellTarget INSTANCE = new CasterSpellTarget();

  @Override
  public @NotNull Stream<LivingEntity> getTargets(SpellTargetContext ctx) {
    return Stream.of(ctx.getCaster());
  }

  @Override
  public @NotNull Text describe(boolean detailed) {
    return new TranslatableText("tooltip.csm.spell.target.caster");
  }

  public static class Serializer extends DataSerializer<CasterSpellTarget> {
    public Serializer() {
      super(CODEC);
    }

    @Override
    public CasterSpellTarget deserialize(PacketByteBuf data) {
      return INSTANCE;
    }

    @Override
    public void serialize(PacketByteBuf data, CasterSpellTarget object) {
      // Nothing!
    }
  }

  public static final Codec<CasterSpellTarget> CODEC = Codec.unit(INSTANCE);
  public static final Serializer SERIALIZER = new Serializer();

  @Override
  public DataSerializer<CasterSpellTarget> getSerializer() {
    return SERIALIZER;
  }
}
