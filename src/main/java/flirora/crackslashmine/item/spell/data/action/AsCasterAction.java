package flirora.crackslashmine.item.spell.data.action;

import org.jetbrains.annotations.NotNull;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import flirora.crackslashmine.item.spell.SpellParameters;
import flirora.crackslashmine.item.spell.data.DataSerializer;
import flirora.crackslashmine.item.spell.data.IndentedTooltipBuilder;
import flirora.crackslashmine.item.spell.data.SpellDataManager;
import flirora.crackslashmine.item.spell.data.target.SpellTargetContext;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.TranslatableText;

public class AsCasterAction implements SpellAction {
  private final SpellAction underlying;

  public AsCasterAction(SpellAction underlying) {
    this.underlying = underlying;
  }

  @Override
  public void perform(SpellTargetContext context, @NotNull ServerWorld world,
      @NotNull SpellParameters params) {
    underlying.perform(context.asCaster(), world, params);
  }

  @Override
  public void describe(boolean detailed, SpellParameters params,
      IndentedTooltipBuilder tooltip) {
    tooltip.add(new TranslatableText("tooltip.csm.spell.action.asCaster"));
    underlying.describe(detailed, params, tooltip.indented());
  }

  public static final Codec<AsCasterAction> CODEC =
      RecordCodecBuilder.create(inst -> {
        return inst.group(SpellDataManager.ACTIONS.getCodec().fieldOf("with")
            .forGetter(a -> a.underlying)).apply(inst, AsCasterAction::new);
      });

  public static class Serializer extends DataSerializer<AsCasterAction> {
    public Serializer() {
      super(CODEC);
    }

    @Override
    public AsCasterAction deserialize(PacketByteBuf data) {
      return new AsCasterAction(SpellAction.deserialize(data));
    }

    @Override
    public void serialize(PacketByteBuf data, AsCasterAction object) {
      object.underlying.serialize(data);
    }
  }

  public static final Serializer SERIALIZER = new Serializer();

  @Override
  public DataSerializer<AsCasterAction> getSerializer() {
    return SERIALIZER;
  }
}
