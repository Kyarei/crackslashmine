package flirora.crackslashmine.item.spell.data.action;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.google.gson.JsonSyntaxException;
import com.mojang.serialization.Codec;
import com.mojang.serialization.Dynamic;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import flirora.crackslashmine.item.spell.SpellParameters;
import flirora.crackslashmine.item.spell.data.DataSerializer;
import flirora.crackslashmine.item.spell.data.IndentedTooltipBuilder;
import flirora.crackslashmine.item.spell.data.SpellDataManager;
import flirora.crackslashmine.item.spell.data.factory.EntityFactory;
import flirora.crackslashmine.item.spell.data.factory.ReplicaParams;
import flirora.crackslashmine.item.spell.data.target.SpellTargetContext;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtOps;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;

public class SpawnReplicatedEntitySpellAction implements SpellAction {
  private final Dynamic<?> baseFactory;
  private final ReplicaParams rp;
  private final int min, max;
  private final int displayLike;
  private final List<EntityFactory<?>> cachedReplicates;
  private final EntityFactory<?> displayLikeFactory;

  private SpawnReplicatedEntitySpellAction(Dynamic<?> baseFactory,
      ReplicaParams rp, int min, int max, int displayLike) {
    this.baseFactory = baseFactory;
    this.rp = rp;
    this.min = min;
    this.max = max;
    this.displayLike = displayLike;

    this.cachedReplicates =
        IntStream.rangeClosed(min, max).mapToObj(replicaIndex -> {
          Dynamic<?> serializedReplicate =
              rp.replicate(baseFactory, replicaIndex);
          return SpellDataManager.FACTORIES.getCodec()
              .decode(serializedReplicate).getOrThrow(false, s -> {
                throw new JsonSyntaxException("Replica " + replicaIndex
                    + " didn't deserialize properly! " + s);
              }).getFirst();
        }).collect(Collectors.toList());
    Dynamic<?> serializedDisplayLikeFactory =
        rp.replicate(baseFactory, displayLike);
    this.displayLikeFactory = SpellDataManager.FACTORIES.getCodec()
        .decode(serializedDisplayLikeFactory).getOrThrow(false, s -> {
          throw new JsonSyntaxException(
              "Replica " + displayLike + " didn't deserialize properly! " + s);
        }).getFirst();
  }

  @Override
  public @Nullable void describe(boolean detailed, SpellParameters params,
      IndentedTooltipBuilder tooltip) {
    tooltip.add(new TranslatableText("tooltip.csm.spell.action.spawnReplicate",
        max - min + 1).formatted(Formatting.GREEN));
    displayLikeFactory.describe(detailed, params, tooltip.indented());
  }

  @Override
  public void perform(SpellTargetContext context, @NotNull ServerWorld world,
      @NotNull SpellParameters params) {
    for (EntityFactory<?> replicate : cachedReplicates) {
      world.spawnEntity(replicate.create(context, params));
    }
  }

  public static final Codec<SpawnReplicatedEntitySpellAction> CODEC =
      RecordCodecBuilder.create(inst -> {
        return inst
            .group(
                Codec.PASSTHROUGH.fieldOf("base").forGetter(a -> a.baseFactory),
                ReplicaParams.CODEC.fieldOf("replicaParams").forGetter(
                    a -> a.rp),
                Codec.INT.fieldOf("min").forGetter(a -> a.min),
                Codec.INT.fieldOf("max").forGetter(a -> a.max),
                Codec.INT.optionalFieldOf("displayLike", 0)
                    .forGetter(a -> a.displayLike))
            .apply(inst, SpawnReplicatedEntitySpellAction::new);
      });

  public static class Serializer
      extends DataSerializer<SpawnReplicatedEntitySpellAction> {
    public Serializer() {
      super(CODEC);
    }

    @Override
    public SpawnReplicatedEntitySpellAction deserialize(PacketByteBuf data) {
      Dynamic<?> baseFactory =
          Codec.PASSTHROUGH.decode(NbtOps.INSTANCE, data.readCompoundTag())
              .getOrThrow(false, s -> {
                throw new JsonSyntaxException(s);
              }).getFirst();
      ReplicaParams rp = ReplicaParams.deserialize(data);
      int min = data.readInt();
      int max = data.readInt();
      int displayLike = data.readInt();
      return new SpawnReplicatedEntitySpellAction(baseFactory, rp, min, max,
          displayLike);
    }

    @Override
    public void serialize(PacketByteBuf data,
        SpawnReplicatedEntitySpellAction object) {
      data.writeCompoundTag((CompoundTag) Codec.PASSTHROUGH
          .encode(object.baseFactory, NbtOps.INSTANCE, new CompoundTag())
          .getOrThrow(false, s -> {
            throw new JsonSyntaxException(s);
          }));
      object.rp.serialize(data);
      data.writeInt(object.min);
      data.writeInt(object.max);
      data.writeInt(object.displayLike);
    }
  }

  public static final Serializer SERIALIZER = new Serializer();

  @Override
  public DataSerializer<? extends SpellAction> getSerializer() {
    return SERIALIZER;
  }

}
