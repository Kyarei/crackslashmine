package flirora.crackslashmine.item.spell.data;

import org.jetbrains.annotations.NotNull;

import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class SerializationDispatcher<T extends WithSerializer<T>>
    extends DataSerializer<T> {
  private final Registry<DataSerializer<? extends T>> serializers;
  private final String name;

  public SerializationDispatcher(
      Registry<DataSerializer<? extends T>> serializers, Class<T> cls) {
    super(serializers.dispatch(WithSerializer::getSerializer,
        DataSerializer::getCodec));
    this.serializers = serializers;
    this.name = cls.getTypeName();
  }

  public T deserialize(JsonObject json) {
    String s = json.get("type").getAsString();
    Identifier id = new Identifier(s);
    DataSerializer<? extends T> serializer = serializers.get(id);
    if (serializer == null) {
      throw new JsonSyntaxException("Unknown type for " + name + ": " + id);
    }
    return serializer.deserialize(json);
  }

  public T deserialize(PacketByteBuf buf) {
    int id = buf.readInt();
    DataSerializer<? extends T> serializer = serializers.get(id);
    if (serializer == null) {
      throw new JsonSyntaxException("Unknown type for " + name + ": " + id);
    }
    return serializer.deserialize(buf);
  }

  @SuppressWarnings("unchecked")
  public void serialize(PacketByteBuf buf, @NotNull T object) {
    DataSerializer<? extends T> serializer = object.getSerializer();
    int id = serializers.getRawId(serializer);
    if (id == -1) {
      throw new JsonSyntaxException(serializer + " is not registered?!");
    }
    buf.writeInt(id);
    ((DataSerializer<T>) serializer).serialize(buf, object);
  }

  @SuppressWarnings("unchecked")
  public JsonObject serialize(JsonObject json, @NotNull T object) {
    DataSerializer<? extends T> serializer = object.getSerializer();
    Identifier id = serializers.getId(serializer);
    if (id == null) {
      throw new JsonSyntaxException(serializer + " is not registered?!");
    }
    json.addProperty("type", id.toString());
    return ((DataSerializer<T>) serializer).serialize(json, object);
  }
}
