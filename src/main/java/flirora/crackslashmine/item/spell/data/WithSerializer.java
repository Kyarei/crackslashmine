package flirora.crackslashmine.item.spell.data;

public interface WithSerializer<T extends WithSerializer> {
  DataSerializer<? extends T> getSerializer();
}
