package flirora.crackslashmine.item.spell.data;

import flirora.crackslashmine.CrackSlashMineMod;
import flirora.crackslashmine.core.CsmRegistries;
import flirora.crackslashmine.item.spell.data.action.ApplyEffectSpellAction;
import flirora.crackslashmine.item.spell.data.action.ApplyExtendedEffectSpellAction;
import flirora.crackslashmine.item.spell.data.action.AsCasterAction;
import flirora.crackslashmine.item.spell.data.action.AsProjectileAction;
import flirora.crackslashmine.item.spell.data.action.DamageSpellAction;
import flirora.crackslashmine.item.spell.data.action.HealSpellAction;
import flirora.crackslashmine.item.spell.data.action.ParticleSpellAction;
import flirora.crackslashmine.item.spell.data.action.PlaySoundSpellAction;
import flirora.crackslashmine.item.spell.data.action.SpawnEntitySpellAction;
import flirora.crackslashmine.item.spell.data.action.SpawnReplicatedEntitySpellAction;
import flirora.crackslashmine.item.spell.data.action.SpellAction;
import flirora.crackslashmine.item.spell.data.factory.ArrowEntityFactory;
import flirora.crackslashmine.item.spell.data.factory.BallEntityFactory;
import flirora.crackslashmine.item.spell.data.factory.EntityFactory;
import flirora.crackslashmine.item.spell.data.factory.LightningEntityFactory;
import flirora.crackslashmine.item.spell.data.target.AroundSpellTarget;
import flirora.crackslashmine.item.spell.data.target.CasterSpellTarget;
import flirora.crackslashmine.item.spell.data.target.SelfSpellTarget;
import flirora.crackslashmine.item.spell.data.target.SpellTarget;
import net.minecraft.entity.Entity;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class BuiltinSpellSerializers {
  private static <T extends SpellTarget> DataSerializer<T> registerSpellTarget(
      String id, DataSerializer<T> entry) {
    Registry.register(CsmRegistries.SPELL_TARGET_SERIALIZER,
        new Identifier(CrackSlashMineMod.MOD_ID, id), entry);
    return entry;
  }

  private static <T extends SpellAction> DataSerializer<T> registerSpellAction(
      String id, DataSerializer<T> entry) {
    Registry.register(CsmRegistries.SPELL_ACTION_SERIALIZER,
        new Identifier(CrackSlashMineMod.MOD_ID, id), entry);
    return entry;
  }

  private static <E extends Entity, T extends EntityFactory<E>> DataSerializer<T> registerEntityFactory(
      String id, DataSerializer<T> entry) {
    Registry.register(CsmRegistries.ENTITY_FACTORY_SERIALIZER,
        new Identifier(CrackSlashMineMod.MOD_ID, id), entry);
    return entry;
  }

  public static final DataSerializer<SelfSpellTarget> SELF_TARGET =
      registerSpellTarget("self", SelfSpellTarget.SERIALIZER);

  public static final DataSerializer<CasterSpellTarget> CASTER_TARGET =
      registerSpellTarget("caster", CasterSpellTarget.SERIALIZER);

  public static final DataSerializer<AroundSpellTarget> AROUND_TARGET =
      registerSpellTarget("around", AroundSpellTarget.SERIALIZER);

  public static final DataSerializer<ParticleSpellAction> PARTICLE_ACTION =
      registerSpellAction("particle", ParticleSpellAction.SERIALIZER);

  public static final DataSerializer<PlaySoundSpellAction> PLAY_SOUND_ACTION =
      registerSpellAction("play_sound", PlaySoundSpellAction.SERIALIZER);

  public static final DataSerializer<SpawnEntitySpellAction> SPAWN_ENTITY_ACTION =
      registerSpellAction("spawn_entity", SpawnEntitySpellAction.SERIALIZER);

  public static final DataSerializer<SpawnReplicatedEntitySpellAction> SPAWN_REPLICATED_ENTITY_ACTION =
      registerSpellAction("spawn_replicated_entity",
          SpawnReplicatedEntitySpellAction.SERIALIZER);

  public static final DataSerializer<DamageSpellAction> DAMAGE_ACTION =
      registerSpellAction("damage", DamageSpellAction.SERIALIZER);

  public static final DataSerializer<HealSpellAction> HEAL_ACTION =
      registerSpellAction("heal", HealSpellAction.SERIALIZER);

  public static final DataSerializer<ApplyExtendedEffectSpellAction> APPLY_EXTENDED_EFFECT_ACTION =
      registerSpellAction("apply_extended_effect",
          ApplyExtendedEffectSpellAction.SERIALIZER);

  public static final DataSerializer<ApplyEffectSpellAction> APPLY_EFFECT_ACTION =
      registerSpellAction("apply_effect", ApplyEffectSpellAction.SERIALIZER);

  public static final DataSerializer<AsCasterAction> AS_CASTER_ACTION =
      registerSpellAction("as_caster", AsCasterAction.SERIALIZER);

  public static final DataSerializer<AsProjectileAction> AS_PROJECTILE_ACTION =
      registerSpellAction("as_projectile", AsProjectileAction.SERIALIZER);

  public static final DataSerializer<BallEntityFactory> BALL_FACTORY =
      registerEntityFactory("ball", BallEntityFactory.SERIALIZER);

  public static final DataSerializer<ArrowEntityFactory> ARROW_FACTORY =
      registerEntityFactory("arrow", ArrowEntityFactory.SERIALIZER);

  public static final DataSerializer<LightningEntityFactory> LIGHTNING_FACTORY =
      registerEntityFactory("lightning", LightningEntityFactory.SERIALIZER);

  public static void load() {
  }
}
