package flirora.crackslashmine.item.spell.data.factory;

import com.google.gson.JsonObject;
import com.mojang.serialization.Codec;
import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import flirora.crackslashmine.core.DamageType;
import flirora.crackslashmine.item.spell.SpellParameters;
import flirora.crackslashmine.item.spell.data.DataSerializer;
import flirora.crackslashmine.item.spell.data.IndentedTooltipBuilder;
import flirora.crackslashmine.item.spell.data.Scrolllet;
import flirora.crackslashmine.item.spell.data.target.SpellTargetContext;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;

public abstract class ProjectileEntityFactory<E extends Entity>
    extends BaseEntityFactory<E> {
  protected final Context projectileContext;

  private static final JsonObject EMPTY = new JsonObject();

  public ProjectileEntityFactory(BaseEntityFactory.Context base,
      Context projectileContext) {
    super(base);
    this.projectileContext = projectileContext;
  }

  public ProjectileEntityFactory(PacketByteBuf data) {
    super(data);
    this.projectileContext = Context.SERIALIZER.deserialize(data);
  }

  @Override
  public void serializeRaw(PacketByteBuf data) {
    super.serializeRaw(data);
    Context.SERIALIZER.serialize(data, projectileContext);
  }

  public static class Context {
    public final double multi;
    public final DamageType type;
    public final Scrolllet onCollideWithEntity;
    public final Scrolllet onCollideWithBlock;

    public Context(double multi, DamageType type, Scrolllet onCollideWithEntity,
        Scrolllet onCollideWithBlock) {
      super();
      this.multi = multi;
      this.type = type;
      this.onCollideWithEntity = onCollideWithEntity;
      this.onCollideWithBlock = onCollideWithBlock;
    }

    public static final MapCodec<Context> MAP_CODEC =
        RecordCodecBuilder.mapCodec(inst -> {
          return inst
              .group(Codec.DOUBLE.fieldOf("multi").forGetter(c -> c.multi),
                  DamageType.CODEC.fieldOf("damageType").forGetter(c -> c.type),
                  Scrolllet.CODEC
                      .optionalFieldOf("onCollideWithEntity", Scrolllet.EMPTY)
                      .forGetter(c -> c.onCollideWithEntity),
                  Scrolllet.CODEC
                      .optionalFieldOf("onCollideWithBlock", Scrolllet.EMPTY)
                      .forGetter(c -> c.onCollideWithBlock))
              .apply(inst, Context::new);
        });
    public static final Codec<Context> CODEC = MAP_CODEC.codec();

    public static class Serializer extends DataSerializer<Context> {
      public Serializer() {
        super(CODEC);
      }

      @Override
      public Context deserialize(PacketByteBuf data) {
        double multi = data.readDouble();
        DamageType type = DamageType.values()[data.readByte()];
        Scrolllet onCollideWithEntity = Scrolllet.SERIALIZER.deserialize(data);
        Scrolllet onCollideWithBlock = Scrolllet.SERIALIZER.deserialize(data);
        return new Context(multi, type, onCollideWithEntity,
            onCollideWithBlock);
      }

      @Override
      public void serialize(PacketByteBuf data, Context object) {
        data.writeDouble(object.multi);
        data.writeByte(object.type.ordinal());
        Scrolllet.SERIALIZER.serialize(data, object.onCollideWithEntity);
        Scrolllet.SERIALIZER.serialize(data, object.onCollideWithBlock);
      }
    }

    public static final Serializer SERIALIZER = new Serializer();
  }

  public static class CreationContext {
    public final BaseEntityFactory.Context base;
    public final Context proj;
    public final LivingEntity caster;

    public CreationContext(BaseEntityFactory.Context base, Context proj,
        LivingEntity caster) {
      this.base = base;
      this.proj = proj;
      this.caster = caster;
    }
  }

  protected void describeCollisionHandlers(boolean detailed,
      SpellParameters params, IndentedTooltipBuilder tooltip) {
    if (!projectileContext.onCollideWithEntity.isEmpty()) {
      tooltip.add(
          new TranslatableText("tooltip.csm.spell.action.onCollisionWithEntity")
              .formatted(Formatting.GREEN));
      projectileContext.onCollideWithEntity.describe(detailed, params,
          tooltip.indented());
    }
    if (!projectileContext.onCollideWithBlock.isEmpty()) {
      tooltip.add(
          new TranslatableText("tooltip.csm.spell.action.onCollisionWithBlock")
              .formatted(Formatting.GREEN));
      projectileContext.onCollideWithBlock.describe(detailed, params,
          tooltip.indented());
    }
  }

  private CreationContext getProjectileContext(LivingEntity caster) {
    return new CreationContext(context, projectileContext, caster);
  }

  @Override
  public E create(SpellTargetContext stContext, SpellParameters params) {
    return create(stContext, params,
        getProjectileContext(stContext.getCaster()));
  }

  protected abstract E create(SpellTargetContext stContext,
      SpellParameters params, CreationContext projectileContext);

  @Override
  protected final E create(SpellTargetContext stContext, SpellParameters params,
      BaseEntityFactory.Context context) {
    throw new UnsupportedOperationException(
        "Use create(LivingEntity, SpellParameters, ProjectileEntityFactory.Context)");
  }
}
