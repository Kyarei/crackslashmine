package flirora.crackslashmine.item.spell.data;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Function;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.JsonHelper;

public class SpellHelper {
  public static double getDouble(JsonObject json, String key) {
    JsonElement e = json.get(key);
    if (e == null) {
      throw new JsonSyntaxException("Expected double for " + key);
    }
    try {
      return e.getAsDouble();
    } catch (Exception ex) {
      throw new JsonSyntaxException("Expected double for " + key);
    }
  }

  public static double getDouble(JsonObject json, String key, double defo) {
    JsonElement e = json.get(key);
    if (e == null) {
      return defo;
    }
    try {
      return e.getAsDouble();
    } catch (Exception ex) {
      throw new JsonSyntaxException("Expected double for " + key);
    }
  }

  public static <T> List<T> deserializeList(PacketByteBuf buf,
      Function<PacketByteBuf, T> deserializer) {
    int n = buf.readVarInt();
    List<T> list = new ArrayList<>(n);
    for (int i = 0; i < n; ++i) {
      list.add(deserializer.apply(buf));
    }
    return list;
  }

  public static <T> void serializeList(PacketByteBuf buf, List<T> list,
      BiConsumer<PacketByteBuf, T> serializer) {
    buf.writeVarInt(list.size());
    for (T t : list) {
      serializer.accept(buf, t);
    }
  }

  public static JsonObject getOrCreate(JsonObject parent, String key) {
    JsonElement elem = parent.get(key);
    if (elem == null) {
      JsonObject newChild = new JsonObject();
      parent.add(key, newChild);
      return newChild;
    }
    return JsonHelper.asObject(elem, key);
  }
}
