package flirora.crackslashmine.item.spell.data.action;

import org.jetbrains.annotations.Nullable;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import flirora.crackslashmine.core.AttackContext;
import flirora.crackslashmine.core.CsmAttack;
import flirora.crackslashmine.core.DamageType;
import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.item.spell.SpellParameters;
import flirora.crackslashmine.item.spell.data.DataSerializer;
import flirora.crackslashmine.item.spell.data.DecayParameters;
import flirora.crackslashmine.item.spell.data.IndentedTooltipBuilder;
import flirora.crackslashmine.item.spell.data.SpellDataManager;
import flirora.crackslashmine.item.spell.data.target.SpellTarget;
import flirora.crackslashmine.item.spell.data.target.SpellTargetContext;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.TranslatableText;

public class DamageSpellAction implements SpellAction {
  private final SpellTarget targetSelector;
  private final DamageType type;
  private final double multi;
  private final DecayParameters decay;

  private DamageSpellAction(SpellTarget target, DamageType type, double multi,
      DecayParameters decay) {
    super();
    this.targetSelector = target;
    this.type = type;
    this.multi = multi;
    this.decay = decay;
  }

  @Override
  public void perform(SpellTargetContext context, ServerWorld world,
      SpellParameters params) {
    LivingEntity caster = context.getCaster();
    LivingEntityStats casterStats = LivingEntityStats.getFor(caster);
    long amt = params.getDamageGivenMultiplier(multi, world.getRandom());
    targetSelector.getTargets(context).forEach(e -> {
      double decay = this.decay.getScaling(e.distanceTo(caster));
      long adjusted = Math.round(amt * (1 - decay));
      LivingEntityStats.dealDamage(e, caster,
          new CsmAttack(new CsmAttack.Entry(type, adjusted)).withAppliedBonuses(
              casterStats.getStats(), world.getRandom(),
              new AttackContext(caster), caster.getMainHandStack()),
          DamageSource.magic(caster, caster));
    });
  }

  @Override
  public @Nullable void describe(boolean detailed, SpellParameters params,
      IndentedTooltipBuilder tooltip) {
    long[] range = params.getDamageRangeGivenMultiplier(multi);
    tooltip.add(new TranslatableText("tooltip.csm.spell.action.damage",
        range[0], range[1],
        new TranslatableText(
            "tooltip.csm.spell.action.damage." + type.getName()),
        targetSelector.describe(detailed)));
  }

  public static final Codec<DamageSpellAction> CODEC =
      RecordCodecBuilder.create(inst -> {
        return inst
            .group(
                SpellDataManager.TARGETS.getCodec().fieldOf("target")
                    .forGetter(a -> a.targetSelector),
                DamageType.CODEC.fieldOf("damageType").forGetter(a -> a.type),
                Codec.DOUBLE.fieldOf("multi").forGetter(a -> a.multi),
                DecayParameters.CODEC
                    .optionalFieldOf("decay", DecayParameters.DEFAULT)
                    .forGetter(a -> a.decay))
            .apply(inst, DamageSpellAction::new);
      });

  public static class Serializer extends DataSerializer<DamageSpellAction> {
    public Serializer() {
      super(CODEC);
    }

    @Override
    public DamageSpellAction deserialize(PacketByteBuf data) {
      SpellTarget target = SpellTarget.deserialize(data);
      DamageType type = DamageType.values()[data.readByte()];
      double multi = data.readDouble();
      DecayParameters decay = DecayParameters.deserialize(data);
      return new DamageSpellAction(target, type, multi, decay);
    }

    @Override
    public void serialize(PacketByteBuf data, DamageSpellAction object) {
      object.targetSelector.serialize(data);
      data.writeByte(object.type.ordinal());
      data.writeDouble(object.multi);
      object.decay.serialize(data);
    }
  }

  public static final Serializer SERIALIZER = new Serializer();

  @Override
  public DataSerializer<? extends SpellAction> getSerializer() {
    return SERIALIZER;
  }
}
