package flirora.crackslashmine.item.spell.data.action;

import org.jetbrains.annotations.NotNull;

import com.google.gson.JsonObject;

import flirora.crackslashmine.item.spell.SpellParameters;
import flirora.crackslashmine.item.spell.data.IndentedTooltipBuilder;
import flirora.crackslashmine.item.spell.data.SpellDataManager;
import flirora.crackslashmine.item.spell.data.WithSerializer;
import flirora.crackslashmine.item.spell.data.target.SpellTargetContext;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.world.ServerWorld;

public interface SpellAction extends WithSerializer<SpellAction> {
  void perform(SpellTargetContext context, @NotNull ServerWorld world,
      @NotNull SpellParameters params);

  default void describe(boolean detailed, SpellParameters params,
      IndentedTooltipBuilder tooltip) {
  }

  static SpellAction deserialize(JsonObject json) {
    return SpellDataManager.ACTIONS.deserialize(json);
  }

  static SpellAction deserialize(PacketByteBuf data) {
    return SpellDataManager.ACTIONS.deserialize(data);
  }

  default void serialize(PacketByteBuf data) {
    SpellDataManager.ACTIONS.serialize(data, this);
  }

  default JsonObject serialize(JsonObject json) {
    return SpellDataManager.ACTIONS.serialize(json, this);
  }
}
