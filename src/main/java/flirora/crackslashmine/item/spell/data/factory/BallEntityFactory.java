package flirora.crackslashmine.item.spell.data.factory;

import org.jetbrains.annotations.Nullable;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import flirora.crackslashmine.core.DamageType;
import flirora.crackslashmine.entity.spell.BallEntity;
import flirora.crackslashmine.item.spell.SpellParameters;
import flirora.crackslashmine.item.spell.data.DataSerializer;
import flirora.crackslashmine.item.spell.data.IndentedTooltipBuilder;
import flirora.crackslashmine.item.spell.data.target.SpellTargetContext;
import net.minecraft.entity.LivingEntity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.text.Text;
import net.minecraft.text.TextColor;
import net.minecraft.text.TranslatableText;

public class BallEntityFactory extends ProjectileEntityFactory<BallEntity> {
  public BallEntityFactory(
      flirora.crackslashmine.item.spell.data.factory.BaseEntityFactory.Context base,
      Context projectileContext) {
    super(base, projectileContext);
  }

  private BallEntityFactory(PacketByteBuf data) {
    super(data);
  }

  @Override
  protected BallEntity create(SpellTargetContext stContext,
      SpellParameters params, CreationContext context) {
    long[] range = params.getDamageRangeGivenMultiplier(context.proj.multi);
    LivingEntity caster = stContext.getCaster();
    return new BallEntity(caster.getEntityWorld(), caster,
        stContext.getTarget(), context.proj.type, range[0], range[1], context,
        params);
  }

  @Override
  public @Nullable void describe(boolean detailed, SpellParameters params,
      IndentedTooltipBuilder tooltip) {
    Text locationText = locationText();
    long[] range =
        params.getDamageRangeGivenMultiplier(projectileContext.multi);
    DamageType type = projectileContext.type;
    tooltip.add(new TranslatableText(
        getTranslationKey("tooltip.csm.spell.factory.ball"),
        new TranslatableText("tooltip.csm.spell.factory.ball." + type.getName())
            .styled(s -> s.withColor(TextColor.fromRgb(type.getColor()))),
        locationText, range[0], range[1]));
    this.describeCollisionHandlers(detailed, params, tooltip);
  }

  @Override
  public void serializeRaw(PacketByteBuf data) {
    super.serializeRaw(data);
  }

  public static final Codec<BallEntityFactory> CODEC =
      RecordCodecBuilder.create(builder -> {
        return builder
            .group(
                BaseEntityFactory.Context.MAP_CODEC.forGetter(f -> f.context),
                ProjectileEntityFactory.Context.MAP_CODEC
                    .forGetter(f -> f.projectileContext))
            .apply(builder, BallEntityFactory::new);
      });

  public static class Serializer extends DataSerializer<BallEntityFactory> {
    public Serializer() {
      super(CODEC);
    }

    @Override
    public BallEntityFactory deserialize(PacketByteBuf data) {
      return new BallEntityFactory(data);
    }

    @Override
    public void serialize(PacketByteBuf data, BallEntityFactory object) {
      object.serializeRaw(data);
    }
  }

  public static final Serializer SERIALIZER = new Serializer();

  @Override
  public DataSerializer<? extends EntityFactory<BallEntity>> getSerializer() {
    return SERIALIZER;
  }
}
