package flirora.crackslashmine.item.spell.data.action;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import flirora.crackslashmine.core.Beautify;
import flirora.crackslashmine.core.CsmRegistries;
import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.core.xse.ExtendedStatusEffect;
import flirora.crackslashmine.core.xse.ExtendedStatusEffectInstance;
import flirora.crackslashmine.item.spell.SpellParameters;
import flirora.crackslashmine.item.spell.data.DataSerializer;
import flirora.crackslashmine.item.spell.data.DecayParameters;
import flirora.crackslashmine.item.spell.data.IndentedTooltipBuilder;
import flirora.crackslashmine.item.spell.data.SpellDataManager;
import flirora.crackslashmine.item.spell.data.target.SpellTarget;
import flirora.crackslashmine.item.spell.data.target.SpellTargetContext;
import net.minecraft.entity.LivingEntity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.math.MathHelper;

public class ApplyExtendedEffectSpellAction implements SpellAction {
  private final SpellTarget targetSelector;
  private final ExtendedStatusEffect effect;
  private final long fixed;
  private final double multi;
  private final DecayParameters intensityDecay;
  private final int minTime;
  private final int maxTime;
  private final DecayParameters durationDecay;

  public ApplyExtendedEffectSpellAction(SpellTarget target,
      ExtendedStatusEffect effect, long fixed, double multi,
      DecayParameters intensityDecay, int minTime, int maxTime,
      DecayParameters durationDecay) {
    this.targetSelector = target;
    this.effect = effect;
    this.fixed = fixed;
    this.multi = multi;
    this.intensityDecay = intensityDecay;
    this.minTime = minTime;
    this.maxTime = maxTime;
    this.durationDecay = durationDecay;
  }

  @Override
  public void perform(SpellTargetContext context, ServerWorld world,
      SpellParameters params) {
    LivingEntity caster = context.getCaster();
    LivingEntityStats casterStats = LivingEntityStats.getFor(caster);
    long amt =
        fixed + params.getDamageGivenMultiplier(multi, world.getRandom());
    targetSelector.getTargets(context).forEach(e -> {
      double distance = e.distanceTo(caster);
      double intensityDecay = this.intensityDecay.getScaling(distance);
      double durationDecay = this.durationDecay.getScaling(distance);
      int duration = MathHelper.nextInt(world.getRandom(), minTime, maxTime);
      duration = (int) Math.round(duration * (1 - durationDecay));
      long intensity = Math.round(amt * (1 - intensityDecay));
      ExtendedStatusEffectInstance instance =
          new ExtendedStatusEffectInstance(effect, duration, intensity, caster);
      LivingEntityStats.getFor(e)
          .applyEffect(instance.scaleToAttackerStats(casterStats.getStats()));
    });
  }

  @Override
  public void describe(boolean detailed, SpellParameters params,
      IndentedTooltipBuilder tooltip) {
    long[] range = params.getDamageRangeGivenMultiplier(multi);
    tooltip.add(new TranslatableText("tooltip.csm.spell.action.effect",
        effect.getStatusEffectName(), targetSelector.describe(detailed),
        Beautify.beautifyTime(minTime), Beautify.beautifyTime(maxTime),
        effect.formatDetails(range[0] + fixed, range[1] + fixed)));
  }

  public static final Codec<ApplyExtendedEffectSpellAction> CODEC =
      RecordCodecBuilder.create(inst -> {
        return inst.group(
            SpellDataManager.TARGETS.getCodec().fieldOf("target")
                .forGetter(a -> a.targetSelector),
            CsmRegistries.EXTENDED_STATUS_EFFECT.fieldOf("effect")
                .forGetter(a -> a.effect),
            Codec.LONG.optionalFieldOf("fixed", 0L).forGetter(a -> a.fixed),
            Codec.DOUBLE.optionalFieldOf("multi", 0.0).forGetter(a -> a.multi),
            DecayParameters.CODEC
                .optionalFieldOf("intensityDecay", DecayParameters.DEFAULT)
                .forGetter(a -> a.intensityDecay),
            Codec.INT.fieldOf("minTime").forGetter(a -> a.minTime),
            Codec.INT.fieldOf("maxTime").forGetter(a -> a.maxTime),
            DecayParameters.CODEC
                .optionalFieldOf("durationDecay", DecayParameters.DEFAULT)
                .forGetter(a -> a.durationDecay))
            .apply(inst, ApplyExtendedEffectSpellAction::new);
      });

  public static class Serializer
      extends DataSerializer<ApplyExtendedEffectSpellAction> {
    public Serializer() {
      super(CODEC);
    }

    @Override
    public ApplyExtendedEffectSpellAction deserialize(PacketByteBuf data) {
      SpellTarget target = SpellTarget.deserialize(data);
      ExtendedStatusEffect effect =
          CsmRegistries.EXTENDED_STATUS_EFFECT.get(data.readInt());
      long fixed = data.readLong();
      double multi = data.readDouble();
      DecayParameters intensityDecay = DecayParameters.deserialize(data);
      int minTime = data.readInt();
      int maxTime = data.readInt();
      DecayParameters durationDecay = DecayParameters.deserialize(data);
      return new ApplyExtendedEffectSpellAction(target, effect, fixed, multi,
          intensityDecay, minTime, maxTime, durationDecay);
    }

    @Override
    public void serialize(PacketByteBuf data,
        ApplyExtendedEffectSpellAction object) {
      object.targetSelector.serialize(data);
      data.writeInt(
          CsmRegistries.EXTENDED_STATUS_EFFECT.getRawId(object.effect));
      data.writeLong(object.fixed);
      data.writeDouble(object.multi);
      object.intensityDecay.serialize(data);
      data.writeInt(object.minTime);
      data.writeInt(object.maxTime);
      object.durationDecay.serialize(data);
    }
  }

  public static final Serializer SERIALIZER = new Serializer();

  @Override
  public DataSerializer<? extends SpellAction> getSerializer() {
    return SERIALIZER;
  }

}
