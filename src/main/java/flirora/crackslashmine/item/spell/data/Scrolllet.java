package flirora.crackslashmine.item.spell.data;

import java.util.List;

import org.jetbrains.annotations.NotNull;

import com.google.common.collect.ImmutableList;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import flirora.crackslashmine.item.spell.SpellParameters;
import flirora.crackslashmine.item.spell.data.action.SpellAction;
import flirora.crackslashmine.item.spell.data.target.SpellTargetContext;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.world.ServerWorld;

public class Scrolllet {
  public static final List<SpellAction> NO_ACTIONS = ImmutableList.of();
  public static final Scrolllet EMPTY = new Scrolllet(NO_ACTIONS, false);

  private final List<SpellAction> actions;
  private final boolean cancel;

  public Scrolllet(List<SpellAction> actions, boolean cancel) {
    this.actions = actions;
    this.cancel = cancel;
  }

  public boolean shouldCancel() {
    return cancel;
  }

  public void execute(SpellTargetContext context, @NotNull ServerWorld world,
      @NotNull SpellParameters params) {
    for (SpellAction s : actions) {
      s.perform(context, world, params);
    }
  }

  public void describe(boolean detailed, SpellParameters params,
      IndentedTooltipBuilder tooltip) {
    for (SpellAction s : actions) {
      s.describe(detailed, params, tooltip);
    }
  }

  public boolean isEmpty() {
    return actions.isEmpty();
  }

  public static final Codec<Scrolllet> CODEC =
      RecordCodecBuilder.create(inst -> {
        return inst.group(Codec.list(SpellDataManager.ACTIONS.getCodec())
            .optionalFieldOf("actions", NO_ACTIONS).forGetter(s -> s.actions),
            Codec.BOOL.optionalFieldOf("cancel", false)
                .forGetter(s -> s.cancel))
            .apply(inst, Scrolllet::new);
      });

  public static class Serializer extends DataSerializer<Scrolllet> {
    public Serializer() {
      super(CODEC);
    }

    @Override
    public Scrolllet deserialize(PacketByteBuf data) {
      boolean cancel = data.readBoolean();
      List<SpellAction> actions =
          SpellHelper.deserializeList(data, SpellAction::deserialize);
      return new Scrolllet(actions, cancel);
    }

    @Override
    public void serialize(PacketByteBuf data, Scrolllet object) {
      data.writeBoolean(object.cancel);
      SpellHelper.serializeList(data, object.actions,
          (buf, elem) -> elem.serialize(buf));

    }
  }

  public static final Serializer SERIALIZER = new Serializer();
}
