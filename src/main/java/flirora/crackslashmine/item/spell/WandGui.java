package flirora.crackslashmine.item.spell;

import static flirora.crackslashmine.item.spell.WandItem.MAX_SPELLS;

import flirora.crackslashmine.block.ImplementedInventory;
import flirora.crackslashmine.gui.CsmScreenHandlers;
import io.github.cottonmc.cotton.gui.SyncedGuiDescription;
import io.github.cottonmc.cotton.gui.widget.WGridPanel;
import io.github.cottonmc.cotton.gui.widget.WItemSlot;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.slot.SlotActionType;
import net.minecraft.util.collection.DefaultedList;

public class WandGui extends SyncedGuiDescription {
  private final ImplementedInventory wandInventory;
  private final int slot;
  private final WItemSlot[] spells;

  public WandGui(int syncId, PlayerInventory playerInventory,
      ImplementedInventory wandInventory, ItemStack wand, int slot) {
    super(CsmScreenHandlers.WAND, syncId, playerInventory, wandInventory, null);
    if (wandInventory == null) {
      wandInventory = createEmptyWandInventory();
      blockInventory = wandInventory;
    }
    this.wandInventory = wandInventory;
    this.slot = slot;

    WGridPanel root = new WGridPanel();
    setRootPanel(root);

    spells = new WItemSlot[] { WItemSlot.of(blockInventory, 0),
        WItemSlot.of(blockInventory, 1), WItemSlot.of(blockInventory, 2),
        WItemSlot.of(blockInventory, 3), };

    root.add(spells[0], 4, 1);
    root.add(spells[1], 6, 3);
    root.add(spells[2], 4, 5);
    root.add(spells[3], 2, 3);

    root.add(this.createPlayerInventoryPanel(), 0, 6);

    root.validate(this);
  }

  @Override
  public ItemStack onSlotClick(int slotNumber, int button,
      SlotActionType action, PlayerEntity player) {
    if (slotNumber == MAX_SPELLS + 27 + slot)
      return ItemStack.EMPTY;
    return super.onSlotClick(slotNumber, button, action, player);
  }

  private static ImplementedInventory createEmptyWandInventory() {
    return new ImplementedInventory() {
      private final DefaultedList<ItemStack> items =
          DefaultedList.ofSize(MAX_SPELLS, ItemStack.EMPTY);

      @Override
      public DefaultedList<ItemStack> getItems() {
        return items;
      }
    };
  }
}
