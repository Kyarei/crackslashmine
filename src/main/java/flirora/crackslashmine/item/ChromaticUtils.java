package flirora.crackslashmine.item;

import net.minecraft.text.MutableText;
import net.minecraft.text.TextColor;
import net.minecraft.text.TranslatableText;

import java.util.Random;

public class ChromaticUtils {
    /**
     * Get the hue associated with an RGB value with normalized components.
     *
     * @param r the red component, within [0, 1]
     * @param g the green component, within [0, 1]
     * @param b the blue component, within [0, 1]
     * @return the hue in degrees, or {@code Double.NaN} if undefined
     */
    public static double getHueOf(double r, double g, double b) {
        double max = Math.max(r, Math.max(g, b));
        double min = Math.min(r, Math.min(g, b));
        double chroma = max - min;
        if (Math.abs(chroma) < 1e-4) return Double.NaN;
        double rawHue;
        if (max == r) {
            rawHue = ((g - b) / chroma) % 6.0;
        } else if (max == g) {
            rawHue = (b - r) / chroma + 2.0;
        } else {
            rawHue = (r - g) / chroma + 4.0;
        }
        return 60 * rawHue;
    }

    /**
     * Get a packed color from a given hue, with maximum saturation and value.
     *
     * @param hue the hue in degrees, or {@code Math.NaN} if absent
     * @return a color in RRGGBB format
     */
    public static int fromHue(double hue) {
        if (Double.isNaN(hue)) return 0x808080;
        double rawHue = hue / 60.0;
        double x = (1 - Math.abs(rawHue % 2.0 - 1));
        double r = 0, g = 0, b = 0;
        switch ((int) Math.floor(rawHue)) {
            case 0: {
                r = 1;
                g = x;
                break;
            }
            case 1: {
                r = x;
                g = 1;
                break;
            }
            case 2: {
                g = 1;
                b = x;
                break;
            }
            case 3: {
                g = x;
                b = 1;
                break;
            }
            case 4: {
                b = 1;
                r = x;
                break;
            }
            case 5: {
                b = x;
                r = 1;
                break;
            }
            default:
                throw new AssertionError("rawHue not in [0, 6)");
        }
        int rr = (int) (255 * r);
        int gg = (int) (255 * g);
        int bb = (int) (255 * b);
        return (rr << 16) | (gg << 8) | bb;
    }

    private static MutableText forHueInternal(double hue) {
        if (Double.isNaN(hue)) return new TranslatableText("csm.color.none");
        int category = Math.floorMod((int) Math.round(hue / 15.0), 24);
        return new TranslatableText("csm.color." + category);
    }

    public static MutableText forHue(double hue) {
        int color = fromHue(hue);
        return forHueInternal(hue).styled(s -> s.withColor(TextColor.fromRgb(color)));
    }

    public static double padToHue(Random r, double potentia, double anima, double discordia) {
        double min = Math.min(potentia, Math.min(anima, discordia));
        double max = Math.max(potentia, Math.max(anima, discordia));
        double range = max - min;
        double hue = getHueOf(
                (discordia - min) / range,
                (anima - min) / range,
                (potentia - min) / range
        );
        hue += r.nextGaussian() * 15.0;
        hue %= 360.0;
        if (hue < 0) hue += 360.0;
        return hue;
    }
}
