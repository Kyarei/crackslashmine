package flirora.crackslashmine.item.recipe;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import flirora.crackslashmine.CrackSlashMineMod;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.RecipeSerializer;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class MixingRecipeSerializer implements RecipeSerializer<MixingRecipe> {
    public static MixingRecipeSerializer INSTANCE = new MixingRecipeSerializer();
    public static Identifier ID = new Identifier(CrackSlashMineMod.MOD_ID, "mixing_recipe");

    private MixingRecipeSerializer() {
        //
    }

    private static class JsonFormat {
        public JsonObject input;
        public double hue;
        public double magnitude;
        public String output;
    }

    private static final Gson GSON = (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();

    @Override
    public MixingRecipe read(Identifier id, JsonObject json) {
        JsonFormat deserialized = GSON.fromJson(json, JsonFormat.class);
        return new MixingRecipe(id,
                Ingredient.fromJson(deserialized.input),
                deserialized.hue,
                deserialized.magnitude,
                new ItemStack(Registry.ITEM.get(Identifier.tryParse(deserialized.output))));
    }

    @Override
    public MixingRecipe read(Identifier id, PacketByteBuf buf) {
        Ingredient input = Ingredient.fromPacket(buf);
        double hue = buf.readDouble();
        double magnitude = buf.readDouble();
        ItemStack output = buf.readItemStack();
        return new MixingRecipe(id, input, hue, magnitude, output);
    }

    @Override
    public void write(PacketByteBuf buf, MixingRecipe recipe) {
        recipe.getInput().write(buf);
        buf.writeDouble(recipe.getHue());
        buf.writeDouble(recipe.getMagnitude());
        buf.writeItemStack(recipe.getOutput());
    }
}
