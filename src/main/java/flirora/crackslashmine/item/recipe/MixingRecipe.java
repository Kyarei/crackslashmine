package flirora.crackslashmine.item.recipe;

import flirora.crackslashmine.block.MixingBowlBlockEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.Recipe;
import net.minecraft.recipe.RecipeSerializer;
import net.minecraft.recipe.RecipeType;
import net.minecraft.util.Identifier;
import net.minecraft.world.World;

public class MixingRecipe implements Recipe<MixingBowlBlockEntity> {
    private final Identifier id;
    private final Ingredient input;
    private final double hue; // in degrees
    private final double magnitude;
    private final ItemStack output;

    public MixingRecipe(Identifier id, Ingredient input, double hue, double magnitude, ItemStack output) {
        this.id = id;
        this.input = input;
        this.hue = hue;
        this.magnitude = magnitude;
        this.output = output;
    }

    public Ingredient getInput() {
        return input;
    }

    public double getHue() {
        return hue;
    }

    public double getMagnitude() {
        return magnitude;
    }

    @Override
    public boolean matches(MixingBowlBlockEntity inv, World world) {
        return input.test(inv.getStack(0));
    }

    public double getEffectiveness(MixingBowlBlockEntity inv) {
        double radians = Math.toRadians(hue);
        double x = Math.cos(radians);
        double y = Math.sin(radians);
        return x * inv.getColX() + y * inv.getColY();
    }

    @Override
    public ItemStack craft(MixingBowlBlockEntity inv) {
        return output.copy();
    }

    @Override
    public boolean fits(int width, int height) {
        return false;
    }

    @Override
    public ItemStack getOutput() {
        return output;
    }

    @Override
    public Identifier getId() {
        return id;
    }

    @Override
    public RecipeSerializer<?> getSerializer() {
        return MixingRecipeSerializer.INSTANCE;
    }

    public static class Type implements RecipeType<MixingRecipe> {
        private Type() {
        }

        public static final Type INSTANCE = new Type();

        public static final String ID = "mixing_recipe";
    }


    @Override
    public RecipeType<?> getType() {
        return Type.INSTANCE;
    }
}
