package flirora.crackslashmine.item.essence.tonat;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.apache.commons.math3.analysis.polynomials.PolynomialFunction;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonSyntaxException;

import flirora.crackslashmine.core.Beautify;
import flirora.crackslashmine.core.CsmRegistries;
import flirora.crackslashmine.core.stat.Stat;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.resource.language.I18n;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;

public class VödDescription {
  public static class Entry {
    private final Stat stat;
    private final PolynomialFunction curveByLevel;
    private final double bonusPerPower;

    public Entry(Stat stat, PolynomialFunction curveByLevel,
        double bonusPerPower) {
      this.stat = stat;
      this.curveByLevel = curveByLevel;
      this.bonusPerPower = bonusPerPower;
    }

    public Stat getStat() {
      return stat;
    }

    public PolynomialFunction getCurveByLevel() {
      return curveByLevel;
    }

    public double getBonusPerPower() {
      return bonusPerPower;
    }

    private static class JsonFormat {
      public String stat;
      public double[] curveByLevel;
      public double bonusPerPower;
    }

    public static Entry fromJson(JsonElement json) {
      JsonFormat deserialized = GSON.fromJson(json, JsonFormat.class);
      Stat stat =
          CsmRegistries.STAT.get(Identifier.tryParse(deserialized.stat));
      if (stat == null)
        throw new JsonSyntaxException("Unknown stat " + stat);
      return new Entry(stat, new PolynomialFunction(deserialized.curveByLevel),
          deserialized.bonusPerPower);
    }

    public void writeToPacket(PacketByteBuf buf) {
      buf.writeIdentifier(stat.getId());
      double[] coefficients = curveByLevel.getCoefficients();
      buf.writeVarInt(coefficients.length);
      for (double c : coefficients)
        buf.writeDouble(c);
      buf.writeDouble(bonusPerPower);
    }

    public static Entry readFromPacket(PacketByteBuf buf) {
      Identifier statId = buf.readIdentifier();
      int nCoeffs = buf.readVarInt();
      double[] coefficients = new double[nCoeffs];
      for (int i = 0; i < nCoeffs; ++i)
        coefficients[i] = buf.readDouble();
      double bonusPerPower = buf.readDouble();
      return new Entry(CsmRegistries.STAT.get(statId),
          new PolynomialFunction(coefficients), bonusPerPower);
    }
  }

  private final Identifier id;
  private final String word;
  private final List<Entry> entries;

  public VödDescription(Identifier id, String word, List<Entry> entries) {
    this.id = id;
    this.word = word;
    this.entries = entries;
  }

  public Identifier getId() {
    return id;
  }

  public String getWord() {
    return word;
  }

  public List<Entry> getEntries() {
    return entries;
  }

  public Text getFlavorText() {
    return new TranslatableText(
        "csm.voed." + id.getNamespace() + "." + id.getPath())
            .formatted(Formatting.BLUE, Formatting.ITALIC);
  }

  @Environment(EnvType.CLIENT)
  public void addFlavorText(List<Text> out) {
    Beautify.addLines(out,
        I18n.translate("csm.voed." + id.getNamespace() + "." + id.getPath()),
        t -> t.formatted(Formatting.BLUE, Formatting.ITALIC));
  }

  private static final Gson GSON =
      (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();

  private static class JsonFormat {
    public String word;
    public JsonElement[] entries;
  }

  public static VödDescription fromJson(Identifier id, JsonElement json) {
    JsonFormat deserialized = GSON.fromJson(json, JsonFormat.class);
    return new VödDescription(id, deserialized.word,
        Stream.of(deserialized.entries).map(Entry::fromJson)
            .collect(Collectors.toList()));
  }

  public void writeToPacket(PacketByteBuf buf) {
    buf.writeString(word);
    buf.writeVarInt(entries.size());
    for (Entry e : entries)
      e.writeToPacket(buf);
  }

  public static VödDescription readFromPacket(Identifier id,
      PacketByteBuf buf) {
    String word = buf.readString();
    int nEntries = buf.readVarInt();
    List<Entry> entries = IntStream.range(0, nEntries)
        .mapToObj(i -> Entry.readFromPacket(buf)).collect(Collectors.toList());
    return new VödDescription(id, word, entries);
  }
}
