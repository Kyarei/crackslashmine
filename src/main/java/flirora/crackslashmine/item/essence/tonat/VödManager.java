package flirora.crackslashmine.item.essence.tonat;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import flirora.crackslashmine.CrackSlashMineMod;
import io.netty.buffer.Unpooled;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.network.PacketContext;
import net.fabricmc.fabric.api.network.ServerSidePacketRegistry;
import net.fabricmc.fabric.api.resource.IdentifiableResourceReloadListener;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.resource.JsonDataLoader;
import net.minecraft.resource.ResourceManager;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.profiler.Profiler;
import org.apache.logging.log4j.Level;
import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.stream.Stream;

public class VödManager extends JsonDataLoader implements IdentifiableResourceReloadListener {
    private static final Gson GSON = (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();
    private static final Identifier ID = new Identifier(CrackSlashMineMod.MOD_ID, "voeds");
    public static final Identifier PACKET_ID = new Identifier(CrackSlashMineMod.MOD_ID, "voed_sync");

    @Environment(EnvType.CLIENT)
    public static final VödManager CLIENT = new VödManager(true);
    public static final VödManager SERVER = new VödManager(false);

    private Map<Identifier, VödDescription> entries = ImmutableMap.of();
    private Map<String, VödDescription> descriptionsByWord = ImmutableMap.of();
    private PacketByteBuf data = null;
    private final boolean client;

    public VödManager(boolean client) {
        super(GSON, "csm/voeds");
        this.client = client;
    }

    @Override
    public Identifier getFabricId() {
        return ID;
    }

    private void initDescriptionsByWord() {
        ImmutableMap.Builder<String, VödDescription> builder = ImmutableMap.builder();
        for (VödDescription desc : entries.values()) {
            builder.put(desc.getWord(), desc);
        }
        descriptionsByWord = builder.build();
    }

    @Override
    protected void apply(Map<Identifier, JsonElement> loader, ResourceManager manager, Profiler profiler) {
        if (client) {
            throw new IllegalStateException("Tried to load JSON data on the client!");
        }
        int count = 0;
        ImmutableMap.Builder<Identifier, VödDescription> builder =
                new ImmutableMap.Builder<>();
        for (Map.Entry<Identifier, JsonElement> entry : loader.entrySet()) {
            Identifier id = entry.getKey();
            JsonElement json = entry.getValue();
            try {
                VödDescription description = VödDescription.fromJson(id, json);
                builder.put(id, description);
            } catch (Exception e) {
                CrackSlashMineMod.logException("Failed to load vöd description " + id, e);
            }
            ++count;
        }
        entries = builder.build();
        buildPacketData();
        initDescriptionsByWord();
        CrackSlashMineMod.log(Level.INFO, "Loaded " + count + " vöd descriptions");
    }

    private void buildPacketData() {
        data = new PacketByteBuf(Unpooled.buffer());
        data.writeInt(entries.size());
        for (Map.Entry<Identifier, VödDescription> e : entries.entrySet()) {
            data.writeIdentifier(e.getKey());
            e.getValue().writeToPacket(data);
        }
    }

    public void syncToClients(Stream<ServerPlayerEntity> players) {
        if (data == null) {
            CrackSlashMineMod.log(Level.WARN, "syncToClients called w/o data loaded");
            return;
        }
        players.forEach(player -> {
            ServerSidePacketRegistry.INSTANCE.sendToPlayer(player, PACKET_ID, data);
        });
    }

    public void fillDataFrom(PacketContext context, PacketByteBuf data) {
        if (!client) {
            throw new IllegalStateException("Tried to load network data on the server!");
        }
        this.data = data;
        int count = data.readInt();
        ImmutableMap.Builder<Identifier, VödDescription> builder =
                new ImmutableMap.Builder<>();
        for (int i = 0; i < count; ++i) {
            Identifier id = new Identifier("fluffy", "carrots");
            try {
                id = data.readIdentifier();
                VödDescription description = VödDescription.readFromPacket(id, data);
                builder.put(id, description);
            } catch (Exception e) {
                CrackSlashMineMod.logException("Failed to load vöd description " + id, e);
            }
        }
        final int finalCount = count;
        context.getTaskQueue().execute(() -> {
            entries = builder.build();
            initDescriptionsByWord();
            CrackSlashMineMod.log(Level.INFO, "Loaded " + finalCount + " vöd descriptions from network");
            SERVER.entries = entries;
            SERVER.initDescriptionsByWord();
        });
    }

    public @Nullable VödDescription get(Identifier id) {
        return entries.get(id);
    }

    public Map<Identifier, VödDescription> getEntries() {
        return entries;
    }

    public @Nullable VödDescription getByWord(String string) {
        return descriptionsByWord.get(string);
    }
}
