package flirora.crackslashmine.item.essence.tonat;

import flirora.crackslashmine.core.CrackSlashMineComponents;
import nerdhub.cardinal.components.api.component.Component;
import nerdhub.cardinal.components.api.util.ItemComponent;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;

import java.util.Objects;

public class TonatComponent implements ItemComponent<TonatComponent> {
    private int level;
    private int power;

    @Override
    public void fromTag(CompoundTag tag) {
        level = tag.getInt("level");
        power = tag.getInt("power");
    }

    @Override
    public CompoundTag toTag(CompoundTag tag) {
        tag.putInt("level", level);
        tag.putInt("power", power);
        return tag;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    @Override
    public boolean isComponentEqual(Component other) {
        return this.equals(other);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TonatComponent that = (TonatComponent) o;
        return level == that.level &&
                power == that.power;
    }

    @Override
    public int hashCode() {
        return Objects.hash(level, power);
    }

    public static TonatComponent getFor(ItemStack stack) {
        return CrackSlashMineComponents.TONAT_COMPONENT.get(stack);
    }
}
