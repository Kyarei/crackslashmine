package flirora.crackslashmine.item.essence.tonat;

import java.util.List;
import java.util.Random;

import org.jetbrains.annotations.Nullable;

import flirora.crackslashmine.core.Beautify;
import flirora.crackslashmine.core.MathUtils;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Rarity;
import net.minecraft.world.World;

// 'tonat' means 'letter' in Volapük
public class TonatItem extends Item {
  private final char letter;
  private final double hue;

  public TonatItem(char letter, double hue) {
    super(new Item.Settings().maxCount(1).rarity(Rarity.UNCOMMON)
        .group(ItemGroup.COMBAT));
    this.letter = letter;
    this.hue = hue;
  }

  public char getLetter() {
    return letter;
  }

  public double getHue() {
    return hue;
  }

  @Override
  public Text getName() {
    return new TranslatableText("item.crackslashmine.tonat", letter);
  }

  @Override
  public Text getName(ItemStack stack) {
    return getName();
  }

  public ItemStack createWithRandomStats(Random r, int level) {
    ItemStack result = new ItemStack(this);
    TonatComponent component = TonatComponent.getFor(result);
    component.setLevel(level);
    int power = MathUtils.nextPoisson(r, Math.sqrt(9 + level));
    component.setPower(power);
    return result;
  }

  @Override
  public void appendTooltip(ItemStack stack, @Nullable World world,
      List<Text> tooltip, TooltipContext context) {
    super.appendTooltip(stack, world, tooltip, context);
    TonatComponent component = TonatComponent.getFor(stack);
    tooltip.add(new TranslatableText("tooltip.csm.levelRequired",
        Beautify.beautify(component.getLevel())).formatted(Formatting.BLUE));
    tooltip.add(new TranslatableText("tooltip.csm.tonat.power",
        Beautify.beautify(component.getPower())).formatted(Formatting.AQUA));
  }
}
