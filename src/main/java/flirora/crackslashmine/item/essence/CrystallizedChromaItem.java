package flirora.crackslashmine.item.essence;

import flirora.crackslashmine.item.ChromaticUtils;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.text.TextColor;
import net.minecraft.text.TranslatableText;

public class CrystallizedChromaItem extends Item {
    private final int color;

    public CrystallizedChromaItem(int color) {
        super(new Item.Settings().group(ItemGroup.MISC));
        this.color = color;
    }

    public int getColor() {
        return color;
    }

    @Environment(EnvType.CLIENT)
    @Override
    public Text getName() {
        return new TranslatableText(
                "item.crackslashmine.crystallized_chroma",
                new TranslatableText(color == -1 ? "csm.color.none" : "csm.color." + color))
                .styled(s -> s.withColor(TextColor.fromRgb(
                        ChromaticUtils.fromHue(15.0 * (color != -1 ? color : Double.NaN)))));
    }

    @Environment(EnvType.CLIENT)
    @Override
    public Text getName(ItemStack stack) {
        return getName();
    }
}
