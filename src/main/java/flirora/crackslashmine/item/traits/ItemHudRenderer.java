package flirora.crackslashmine.item.traits;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.ItemStack;

@Environment(EnvType.CLIENT)
public interface ItemHudRenderer {
    void onHudRender(ItemStack stack,
                     ClientPlayerEntity player,
                     MatrixStack matrixStack,
                     float delta);
}
