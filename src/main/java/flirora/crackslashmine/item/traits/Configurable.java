package flirora.crackslashmine.item.traits;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;

public interface Configurable {
    void configure(ItemStack stack, PlayerEntity player, ServerWorld world, int slot);
}
