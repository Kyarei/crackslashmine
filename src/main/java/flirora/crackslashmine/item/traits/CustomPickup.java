package flirora.crackslashmine.item.traits;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;

public interface CustomPickup {
    boolean canPickUp(PlayerEntity player, ItemStack item);

    ItemStack doPickUp(PlayerEntity player, ItemStack item);
}
