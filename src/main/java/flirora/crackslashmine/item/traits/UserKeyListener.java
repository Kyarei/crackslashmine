package flirora.crackslashmine.item.traits;

import net.minecraft.item.ItemStack;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;

public interface UserKeyListener {
    void onKey(ItemStack stack, ServerPlayerEntity player, ServerWorld world, int num);
}
