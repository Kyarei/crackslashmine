package flirora.crackslashmine.item.traits;

import net.minecraft.item.ItemStack;

import java.util.Random;
import java.util.function.Consumer;

public interface Salvageable {
    boolean canSalvage(ItemStack stack);

    boolean salvage(ItemStack stack, double avgExpectedProducts, Consumer<ItemStack> callback, Random r);
}
