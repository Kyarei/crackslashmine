package flirora.crackslashmine.item.food;

import flirora.crackslashmine.core.Beautify;
import flirora.crackslashmine.core.xse.RangedExtendedStatusEffectInstance;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;

import java.util.List;

public class FoodUtils {
    @Environment(EnvType.CLIENT)
    public static void appendFoodInfo(ItemStack stack, List<Text> tooltip) {
        CompatibleFoodDescription desc =
                CompatibleFoodManager.CLIENT.get(stack);
        if (desc == null) return;
        if (desc.getHealthRestored() > 0) {
            tooltip.add(
                    new TranslatableText("tooltip.csm.food.health",
                            Beautify.beautify(desc.getHealthRestored()))
                            .formatted(Formatting.RED));
        }
        if (desc.getManaRestored() > 0) {
            tooltip.add(
                    new TranslatableText("tooltip.csm.food.mana",
                            Beautify.beautify(desc.getManaRestored()))
                            .formatted(Formatting.AQUA));
        }
        if (desc.getStaminaRestored() > 0) {
            tooltip.add(
                    new TranslatableText("tooltip.csm.food.stamina",
                            Beautify.beautify(desc.getStaminaRestored()))
                            .formatted(Formatting.GREEN));
        }
        for (RangedExtendedStatusEffectInstance effect : desc.getEffects()) {
            tooltip.add(effect.formatInTooltip());
        }
    }
}
