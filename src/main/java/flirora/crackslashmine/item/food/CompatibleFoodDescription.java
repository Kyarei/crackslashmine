package flirora.crackslashmine.item.food;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import flirora.crackslashmine.core.LivingEntityStats;
import flirora.crackslashmine.core.xse.RangedExtendedStatusEffectInstance;
import net.minecraft.entity.LivingEntity;
import net.minecraft.network.PacketByteBuf;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CompatibleFoodDescription {
    // TODO: use flat rates or percentages?
    private final long healthRestored;
    private final long manaRestored;
    private final long staminaRestored;
    private final List<RangedExtendedStatusEffectInstance> effects;
    private final boolean disableVanillaEffects;


    public CompatibleFoodDescription(long healthRestored, long manaRestored, long staminaRestored, List<RangedExtendedStatusEffectInstance> effects, boolean disableVanillaEffects) {
        this.healthRestored = healthRestored;
        this.manaRestored = manaRestored;
        this.staminaRestored = staminaRestored;
        this.effects = effects;
        this.disableVanillaEffects = disableVanillaEffects;
    }

    @Override
    public String toString() {
        return "CompatibleFoodDescription{" +
                "healthRestored=" + healthRestored +
                ", manaRestored=" + manaRestored +
                ", staminaRestored=" + staminaRestored +
                ", effects=" + effects +
                '}';
    }

    public long getHealthRestored() {
        return healthRestored;
    }

    public long getManaRestored() {
        return manaRestored;
    }

    public long getStaminaRestored() {
        return staminaRestored;
    }

    public List<RangedExtendedStatusEffectInstance> getEffects() {
        return effects;
    }

    public boolean isDisableVanillaEffects() {
        return disableVanillaEffects;
    }

    public void apply(LivingEntity eater) {
        LivingEntityStats stats = LivingEntityStats.getFor(eater);
        stats.heal(healthRestored, false);
        stats.setMana(stats.getMana() + manaRestored);
        stats.setStamina(stats.getStamina() + staminaRestored);
        for (RangedExtendedStatusEffectInstance effect : effects) {
            stats.applyEffect(effect.roll(eater.getRandom(), null));
        }
    }

    private static class JsonFormat {
        public long healthRestored;
        public long manaRestored;
        public long staminaRestored;
        public List<JsonElement> effects;
        public boolean disableVanillaEffects;
    }

    private static final Gson GSON = (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();

    public static CompatibleFoodDescription fromJson(JsonElement element) {
        JsonFormat deserialized = GSON.fromJson(element, JsonFormat.class);
        return new CompatibleFoodDescription(
                deserialized.healthRestored,
                deserialized.manaRestored,
                deserialized.staminaRestored,
                deserialized.effects.stream()
                        .map(RangedExtendedStatusEffectInstance::fromJson)
                        .collect(Collectors.toList()),
                deserialized.disableVanillaEffects);
    }

    public void writeToPacket(PacketByteBuf buf) {
        buf.writeLong(healthRestored);
        buf.writeLong(manaRestored);
        buf.writeLong(staminaRestored);
        buf.writeInt(effects.size());
        for (RangedExtendedStatusEffectInstance effect : effects) {
            effect.writeToPacket(buf);
        }
        buf.writeBoolean(disableVanillaEffects);
    }

    public static CompatibleFoodDescription readFromPacket(PacketByteBuf buf) {
        long healthRestored = buf.readLong();
        long manaRestored = buf.readLong();
        long staminaRestored = buf.readLong();
        List<RangedExtendedStatusEffectInstance> effects = IntStream.range(0, buf.readInt())
                .mapToObj(i -> RangedExtendedStatusEffectInstance.readFromPacket(buf))
                .collect(Collectors.toList());
        boolean disableVanillaEffects = buf.readBoolean();
        return new CompatibleFoodDescription(healthRestored, manaRestored, staminaRestored, effects, disableVanillaEffects);
    }
}
