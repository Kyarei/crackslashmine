package flirora.crackslashmine.item.food;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import flirora.crackslashmine.CrackSlashMineMod;
import io.netty.buffer.Unpooled;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.network.PacketContext;
import net.fabricmc.fabric.api.network.ServerSidePacketRegistry;
import net.fabricmc.fabric.api.resource.IdentifiableResourceReloadListener;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.resource.JsonDataLoader;
import net.minecraft.resource.ResourceManager;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.profiler.Profiler;
import net.minecraft.util.registry.Registry;
import org.apache.logging.log4j.Level;

import java.util.Map;
import java.util.stream.Stream;

public class CompatibleFoodManager extends JsonDataLoader implements IdentifiableResourceReloadListener {
    private static final Gson GSON = (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();
    private static final Identifier ID = new Identifier(CrackSlashMineMod.MOD_ID, "compatible_foods");
    public static final Identifier PACKET_ID = new Identifier(CrackSlashMineMod.MOD_ID, "compatible_food_sync");

    @Environment(EnvType.CLIENT)
    public static final CompatibleFoodManager CLIENT = new CompatibleFoodManager(true);
    public static final CompatibleFoodManager SERVER = new CompatibleFoodManager(false);

    private Map<Identifier, CompatibleFoodDescription> compatibleFoods =
            ImmutableMap.of();
    private PacketByteBuf data = null;
    private final boolean client;

    public CompatibleFoodManager(boolean client) {
        super(GSON, "csm/compatible_foods");
        this.client = client;
    }

    @Override
    public Identifier getFabricId() {
        return ID;
    }

    @Override
    protected void apply(Map<Identifier, JsonElement> loader, ResourceManager manager, Profiler profiler) {
        if (client) {
            throw new IllegalStateException("Tried to load JSON data on the client!");
        }
        int count = 0;
        ImmutableMap.Builder<Identifier, CompatibleFoodDescription> builder =
                new ImmutableMap.Builder<>();
        for (Map.Entry<Identifier, JsonElement> entry : loader.entrySet()) {
            Identifier id = entry.getKey();
            JsonElement json = entry.getValue();
            try {
                CompatibleFoodDescription description =
                        CompatibleFoodDescription.fromJson(json);
                builder.put(id, description);
            } catch (Exception e) {
                CrackSlashMineMod.logException("Failed to load compatible food description " + id, e);
            }
            ++count;
        }
        compatibleFoods = builder.build();
        buildPacketData();
        CrackSlashMineMod.log(Level.INFO, "Loaded " + count + " compatible food descriptions");
    }

    private void buildPacketData() {
        data = new PacketByteBuf(Unpooled.buffer());
        data.writeInt(compatibleFoods.size());
        for (Map.Entry<Identifier, CompatibleFoodDescription> e : compatibleFoods.entrySet()) {
            data.writeIdentifier(e.getKey());
            e.getValue().writeToPacket(data);
        }
    }

    public void syncToClients(Stream<ServerPlayerEntity> players) {
        if (data == null) {
            CrackSlashMineMod.log(Level.WARN, "syncToClients called w/o data loaded");
            return;
        }
        players.forEach(player -> {
            ServerSidePacketRegistry.INSTANCE.sendToPlayer(player, PACKET_ID, data);
        });
    }

    public void fillDataFrom(PacketContext context, PacketByteBuf data) {
        if (!client) {
            throw new IllegalStateException("Tried to load network data on the server!");
        }
        this.data = data;
        int count = data.readInt();
        ImmutableMap.Builder<Identifier, CompatibleFoodDescription> builder =
                new ImmutableMap.Builder<>();
        for (int i = 0; i < count; ++i) {
            Identifier id = new Identifier("fluffy", "carrots");
            try {
                id = data.readIdentifier();
                CompatibleFoodDescription description =
                        CompatibleFoodDescription.readFromPacket(data);
                builder.put(id, description);
            } catch (Exception e) {
                CrackSlashMineMod.logException("Failed to load compatible food description " + id, e);
            }
        }
        final int finalCount = count;
        context.getTaskQueue().execute(() -> {
            compatibleFoods = builder.build();
            CrackSlashMineMod.log(Level.INFO, "Loaded " + finalCount + " compatible food descriptions from network");
        });
    }

    public CompatibleFoodDescription get(Identifier id) {
        return compatibleFoods.get(id);
    }

    public CompatibleFoodDescription get(Item item) {
        return get(Registry.ITEM.getId(item));
    }

    public CompatibleFoodDescription get(ItemStack stack) {
        return get(stack.getItem());
    }
}
