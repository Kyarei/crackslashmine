package flirora.crackslashmine;

import flirora.crackslashmine.config.CsmConfig;
import me.sargunvohra.mcmods.autoconfig1u.AutoConfig;
import me.sargunvohra.mcmods.autoconfig1u.serializer.JanksonConfigSerializer;
import me.sargunvohra.mcmods.autoconfig1u.shadowed.blue.endless.jankson.Jankson;
import net.fabricmc.loader.api.entrypoint.PreLaunchEntrypoint;
import org.apache.commons.math3.analysis.polynomials.PolynomialFunction;

public class CrackSlashMineModPreload implements PreLaunchEntrypoint {
    @Override
    public void onPreLaunch() {
        /*
            We have to perform the following actions in the pre-launch hook,
            because server initialization will instantiate a CsmDatabase, whose
            parameters depend on some of the config options.
         */
        // Load config
        AutoConfig.register(CsmConfig.class, (def, clazz) -> {
            // Horrible hacks to support certain types used in the C/M config
            Jankson jankson = new Jankson.Builder()
                    .registerTypeAdapter(PolynomialFunction.class, json ->
                            new PolynomialFunction(
                                    json.get(double[].class, "coefficients")))
                    .build();
            return new JanksonConfigSerializer<>(def, clazz, jankson);
        });
        CrackSlashMineMod.config =
                AutoConfig.getConfigHolder(CsmConfig.class).getConfig();
    }
}
