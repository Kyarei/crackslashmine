## Crack/Mine

**Crack/Mine** is a spiritual successor to [Hack/Mine] that turns Minecraft into a hack-n'-slash game.

(No affiliation with the creator of Hack/Mine, of course.)

### Current Status

This mod is currently in an **early alpha phase!** We are at the stage of implementing the core functionality of the mod. In other words, you might find some features you expect to be missing; there is a to-do list at `docs/TODO.md`. Not much balancing has been done yet.

Check our [Discord server] for updates and feedback.

### FAQ

**Will you port to Forge?** No, I'm not familiar at all with Forge modding, and I don't want to be maintaining multiple versions of the mod at once.

**... to earlier versions?** No, unless I need to apply a critical bug fix.

**... to snapshots?** No, dependency updates for snapshots are extremely hit-or-miss.

**... to pre-releases?** Perhaps, once dependencies are updated for that version.

**How do I make data packs for this mod?** A guide is coming soon.

[Hack/Mine]: https://www.minecraftforum.net/forums/mapping-and-modding-java-edition/minecraft-mods/wip-mods/1438939-1-2-3-hack-slash-mine-java-7-10-now-supported
[Discord server]: https://discord.gg/w3MbPHd
