(define (ends-with? str suffix)
  (let* ((suffix-len (string-length suffix))
         (str-len (string-length str)))
    (string=? suffix (substring str (- str-len suffix-len) str-len))))

(define (get-files-recursively directory)
  (let* ((patterns-here (string-append directory "/*"))
         (files (cadr (file-glob patterns-here 1))))
    (append files (search-dirs files))))
(define (search-dirs dirs)
  (if (null? dirs)
    '()
    (append (get-files-recursively (head dirs))
            (search-dirs (tail dirs)))))

(define (get-files-recursively/extension directory extension)
  (filter (lambda (path) (ends-with? path extension))
    (get-files-recursively directory)))

(let* ( (files (get-files-recursively/extension "texture-src" ".xcf")) (filename "") (image 0) (layer 0) )
  (while (pair? files) 
    (set! filename (car files))
    (display (string-append "Reading file " filename "...\n") (current-output-port))
    (set! image (car (gimp-file-load RUN-NONINTERACTIVE filename filename)))
    (set! layer (car (gimp-image-merge-visible-layers image CLIP-TO-IMAGE)))
    (set! filename (string-append "src/main/resources/assets/crackslashmine/textures/" (substring filename 12)))
    (set! filename (string-append (substring filename 0 (- (string-length filename) 4)) ".png"))
    (display (string-append "Writing file " filename "...\n") (current-output-port))
    (gimp-file-save RUN-NONINTERACTIVE image layer filename filename)
    (gimp-image-delete image)
    (set! files (cdr files))
    )
    (display "Done!" (current-output-port))
  (gimp-quit 0)
  )
