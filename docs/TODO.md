## TODO

Note: entries are in no particular order

* implement more stats
* implement more trinket types
* implement more extended status effects
* feature (ore, plant) generation (also needs to be area level-gated)
* advancements!
* restore hp/mp/st on sleep
* player profile screen
* guidebook: players will start with one, but with only a few pages; more pages can be acquired through finding loot or trading.
* add more spells
* how should netherite gear be handled?
* more random mob equipment?
* more crucible and mixing bowl tiers
* Gate of Babylon integration

### Dimension ideas

* Glade of Legacy: pre-b1.8-like terrain generation, but with new mobs
* Land of Despair
	* as it says on the tin, a "dark and gloomy" land
	* plants don't grow, and light blocks don't work
	* most mobs in this dimension don't die *per se*; they respawn shortly after they are incapacitated (however, they will also drop anything in their hands)
	* dying in this dimension causes you to respawn there
	* only way to exit is to eat a **candy of hope** (found in loot chests or occasionally dropped from certain mobs) inside a **shrine**
		* all mobs native to this dimension covet candies of hope and will try to steal them from the player

### Fundamental stat rework (done, kept for future reference)

Current situation:
* Strength increases physical damage, max health, stamina regen
* Dexterity increases magical resistance, dodge chance
* Intellect increases health regen, max mana, magical damage
* Endurance: increases max stamina, physical resistance
* Charisma: increases mana regen (with plans to tie "luck" stats to this one)

Problems with current situation:
* STR is way too powerful right now, as it has three bonuses for attributes valuable for physical combat
* CHA isn't that useful, and if we were to go with tying "luck" stats to CHA, then teams might exploit this by having someone with high CHA with the purpose of dealing killing blows
* hard to balance
* in general, I think tying the maximum and the regen rate of a resource to different stats was a failed experiment

Thoughts:
* in general, fundamentals should cleanly stand on either the offensive or the defensive side
* get rid of CHA for reasons above
* max resource and regen rate of same resource should be tied to the same fundamental
* we're probably bound for having damage-related stats tied to STR and stamina-related stats to END, so maybe we should have different stats for mana and magic damage
* *maybe* additional bonuses on the fundamental with the most points (by the margin to the next-highest fundamental) to promote specialization? Or should it be a penalty to disincentivize builds that spend all of their points on one fundamental?

Tentative new list of fundamentals:
* Strength: physical damage, critical chance and damage
* Dexterity: dodge chance, speed boost
* Intellect: max mana, mana regen
* Wisdom: magical damage, "magic critical" chance and damage, healing strength
* Endurance: max stamina, stamina regen, oxygen capacity
* Fortitude: max health, health regen, all resistances
